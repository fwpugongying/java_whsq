package com.jeeplus.common.sms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;

/**
 * 飞鸽传书短信工具类
 * 
 * @author Administrator
 *
 */
public class FeigeSMSUtil {
	private static final String ACCOUNT = "17194370528";// 账号
	private static final String PWD = "9966ac58a1c1a651c85836596";// 密钥
	private static final String SIGNID = "108009";// 签名
	
	/**
	 * 发送文字普通短信
	 * @param mobile 手机号 多个号码以逗号分隔
	 * @param content 用户发送的自定义短信内容，建议长度不要超过500字
	 * @return
	 */
	public static JSONObject sendSms(String mobile, String content) {
		JSONObject json = new JSONObject();
		String result = "";
		CloseableHttpClient client = null;
		CloseableHttpResponse response = null;
		try {
			List<BasicNameValuePair> formparams = new ArrayList<>();
			formparams.add(new BasicNameValuePair("Account", ACCOUNT));
			formparams.add(new BasicNameValuePair("Pwd", PWD));// 登录后台 首页显示
			formparams.add(new BasicNameValuePair("Content", content));
			formparams.add(new BasicNameValuePair("Mobile", mobile));
			formparams.add(new BasicNameValuePair("SignId", SIGNID));// 登录后台 添加签名获取id

			HttpPost httpPost = new HttpPost("http://api.feige.ee/SmsService/Send");
			httpPost.setEntity(new UrlEncodedFormEntity(formparams, "UTF-8"));
			client = HttpClients.createDefault();
			response = client.execute(httpPost);
			HttpEntity entity = response.getEntity();
			result = EntityUtils.toString(entity);
			System.out.println(result);
			json = JSONObject.parseObject(result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (response != null) {
					response.close();
				}
				if (client != null) {
					client.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return json;
	}
	
	public static void main(String[] args) {
		sendSms("15981800206", "您的验证码是7527，有效期10分钟。");
		// {"SendId":"2019110114433951864817291","InvalidCount":0,"SuccessCount":1,"BlackCount":0,"Code":0,"Message":"OK"}
	}
}
