package com.jeeplus.common.sms;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;

/**
 * @author 陈宗杰
 * @description 聚合短信接口：基于JAVA的短信API服务接口调用 － 聚合数据
 *              在线接口文档：http://www.juhe.cn/docs/54
 */
public class JuheSMSUtils {

	/**
	 * 日志对象
	 */
	private static Logger logger = LoggerFactory.getLogger(JuheSMSUtils.class);
	
	public static final String DEF_CHATSET = "UTF-8";
	public static final int DEF_CONN_TIMEOUT = 30000;
	public static final int DEF_READ_TIMEOUT = 30000;
	public static String userAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.66 Safari/537.36";

	// 配置您申请的KEY
	private static final String APPKEY = "63332539b6ec404bdce13baa0b579d00";

	/**
	 * 屏蔽词检查测
	 * @param content 短信内容，无需模版
	 */
	public static void checkSms(String content) {
		String result = null;
		String url = "http://v.juhe.cn/sms/black";	// 请求接口地址
		Map<String, Object> params = new HashMap<String, Object>();	// 请求参数
		params.put("word", content);	// 需要检测的短信内容，需要UTF8 URLENCODE
		params.put("key", APPKEY);		// 应用APPKEY(应用详细页查询)

		try {
			result = net(url, params, "GET");
			JSONObject object = JSONObject.parseObject(result);
			if (object.getInteger("error_code") == 0) {
				logger.info("checkSms success result:{}",object.get("result"));
			} else {
				logger.info("checkSms failed error_code:{};reason:{}",object.get("error_code"),object.get("reason"));
			}
		} catch (Exception e) {
			logger.error("checkSms Exception:{}",e);
		}
	}

	/**
	 * 发送短信
	 * @param mobile  	发送内容的手机号
	 * @param tpl_id	发送内容对应的模版ID
	 * @param tpl_value	发送内容，需要是指定模版的键值对规则
	 * @return   0 代表短信服务处理成功，其他请参照官网
	 */
	public static String sendSms(String mobile, String tpl_id, String tpl_value) {
		String resultCode = null;
		String result = null;
		String url = "http://v.juhe.cn/sms/send";	// 请求接口地址
		Map<String, Object> params = new HashMap<String, Object>();	// 请求参数
		params.put("mobile", mobile);		// 接收短信的手机号码
		params.put("tpl_id", tpl_id);		// 短信模板ID，请参考个人中心短信模板设置
		params.put("tpl_value", tpl_value);	// 变量名和变量值对。如果你的变量名或者变量值中带有#&=中的任意一个特殊符号，请先分别进行urlencode编码后再传递，<a
											// href="http://www.juhe.cn/news/index/id/50"
											// target="_blank">详细说明></a>
		params.put("key", APPKEY);	// 应用APPKEY(应用详细页查询)
		params.put("dtype", "");	// 返回数据的格式,xml或json，默认json

		try {
			
			result = net(url, params, "GET");
			logger.info("{} sendSms result:{}", mobile, result);
			
			JSONObject object = JSONObject.parseObject(result);
			// 0 代表短信服务处理成功，其他请参照官网
			resultCode = object.getString("error_code");
			
		} catch (Exception e) {
			logger.error("{} sendSms Exception:{}",mobile, e);
		}
		return resultCode;
	}

	/**
	 * 短信发送验证码
	 * @param mobile	接受验证码的手机号
	 * @param code		验证码信息
	 * @param minute	验证码内容中，涉及验证码到效期提示。时间单位（分钟），有效期逻辑需要自行规定。
	 * @return  0 代表短信服务处理成功，其他请参照官网
	 * @throws UnsupportedEncodingException 
	 */
	public static String sendSmsCode(String mobile, String code,int minute) throws UnsupportedEncodingException {
		// 短信内容可以设置签名（公司名或应用名），例如池续云电站
		// 短信发送验证码模版(需要审核)
		// tpl_id:79453  【它牛APP】您的验证码是#code#，有效期#m#分钟
		//（说明：#code#变量仅允许长度为4-8位的字母数字组合，#m#允许1-3位数字）
		String tpl_id = "79453";
		// 变量名和变量值对，如：#code#=431515，整串值需要urlencode，比如正确结果为：%23code%23%3d431515。
		// 如果你的变量名或者变量值中带有#&=中的任意一个特殊符号，请先分别进行utf-8 urlencode编码后再传递
		String tpl_value = null;
		tpl_value = "#code#=" + URLEncoder.encode(code,"utf8") + "&#m#=" + minute;
		// 执行验证码发送
		String resultCode = null;
		resultCode = sendSms(mobile, tpl_id, tpl_value);
		// 测试其他流程，可以直接忽略短信发送。
//		boolean testOther = true;
//		if(testOther){
//			resultCode = "0";
//			logger.info("手机用户{}发送的短信验证码{}", mobile, code);
//		}else{
//			resultCode = sendSms(mobile, tpl_id, tpl_value);
//		}
		return resultCode;
	}
	
	public static void main(String[] args) throws UnsupportedEncodingException {
//		PropertyConfigurator.configure("E:/..../resources/properties/log4j.properties");
		// 发送短信，APPKEY，tpl_id参数值需要申请，
		// mobile：发送的手机号， 
		// tpl_value:发送的内容(发送内容，需要是指定模版（tpl_id）的键值对规则)
		//isblack-是否含有屏蔽词 0:否 1:是;word-屏蔽词
//		String smsContent = "你好，检查短信内容是否有禁用词，比如中奖。";
//		checkSms(smsContent);
//		smsContent = "你好，赌博，游戏，吸毒用词，qa。";
//		checkSms(smsContent);
		
		String code = ((int) ((Math.random() * 9 + 1) * 100000)) + "";
		Map<String, Object> tplMap = new HashMap<String, Object>();
		tplMap.put("#code#", code);
		String tplValue = JuheSMSUtils.urlencode(tplMap);
		String retInfo = JuheSMSUtils.sendSms("18790751103", "127032", tplValue);
		
		//信息: 15225077328 sendSms result:{"reason":"操作成功","result":{"sid":"201804090842495137877","fee":1,"count":1},"error_code":0}
		//输出0
		System.out.println("============" + retInfo);
		
		JuheSMSUtils.sendSmsCode("18736681019", "859641", 1);
		
		
		
	}

	/**
	 *
	 * @param strUrl
	 *            请求地址
	 * @param params
	 *            请求参数
	 * @param method
	 *            请求方法
	 * @return 网络请求字符串
	 * @throws Exception
	 */
	public static String net(String strUrl, Map<String, Object> params, String method) throws Exception {
		HttpURLConnection conn = null;
		BufferedReader reader = null;
		String rs = null;
		try {
			StringBuffer sb = new StringBuffer();
			if (method == null || method.equals("GET")) {
				strUrl = strUrl + "?" + urlencode(params);
			}
			URL url = new URL(strUrl);
			conn = (HttpURLConnection) url.openConnection();
			if (method == null || method.equals("GET")) {
				conn.setRequestMethod("GET");
			} else {
				conn.setRequestMethod("POST");
				conn.setDoOutput(true);
			}
			conn.setRequestProperty("User-agent", userAgent);
			conn.setUseCaches(false);
			conn.setConnectTimeout(DEF_CONN_TIMEOUT);
			conn.setReadTimeout(DEF_READ_TIMEOUT);
			conn.setInstanceFollowRedirects(false);
			conn.connect();
			if (params != null && method.equals("POST")) {
				try {
					DataOutputStream out = new DataOutputStream(conn.getOutputStream());
					out.writeBytes(urlencode(params));
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
			InputStream is = conn.getInputStream();
			reader = new BufferedReader(new InputStreamReader(is, DEF_CHATSET));
			String strRead = null;
			while ((strRead = reader.readLine()) != null) {
				sb.append(strRead);
			}
			rs = sb.toString();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				reader.close();
			}
			if (conn != null) {
				conn.disconnect();
			}
		}
		return rs;
	}

	// 将map型转为请求参数型
	public static String urlencode(Map<String, Object> data) {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Object> i : data.entrySet()) {
			try {
				sb.append(i.getKey()).append("=").append(URLEncoder.encode(i.getValue() + "", "UTF-8")).append("&");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}