package com.jeeplus.common.sms;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Maps;
import com.jeeplus.common.utils.XMLUtil;
import com.jeeplus.common.utils.net.HttpClientUtil;

public class MobileSMSUtil {
	protected static Logger logger = LoggerFactory.getLogger(MobileSMSUtil.class);
	
	private static final String username = "scxx";
	private static final String userpwd = "761087";
	private static final String url = "http://sms.ue35.net/sms/interface/sendmess.htm";
	
	/**
	 * 发送短信,移动10086
	 * @param phone 手机号
	 * @param content 内容
	 * @return
	 */
	public static boolean sendBy10086(String phone, String content) {
		String result = "";
		Map<String, String> param = Maps.newHashMap();
		param.put("username", username); // 用户名称
		param.put("userpwd", userpwd);// 用户密码
		param.put("mobiles", phone);// 手机号码列表，最大1000个，号码间以英文分号 ; 分隔
		param.put("content", content);// 要提交的短信内容，中文内容要使用UTF-8字符集进行URL编码，避免有特殊符号造成提交失败，例如java用URLEncoder.encode("发送内容", " UTF-8")
		param.put("mobilecount", phone.split(";").length + "");
		try {
			result = HttpClientUtil.doPost(url, param);
			Map<String, String> parse = XMLUtil.doXMLParse(result);
			String errorcode = parse.get("errorcode");
			
			logger.debug("短信发送:" + parse.toString());
			if ("1".equals(errorcode)) {
				return true;
			}
			
			logger.error("短信发送失败！" + parse.toString());
		} catch (Exception e) {
			logger.error("短信发送失败！" + e.getMessage(), e);
		}
		return false;
	}
}
