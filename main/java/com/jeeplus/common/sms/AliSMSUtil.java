package com.jeeplus.common.sms;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

/**
 * 阿里云短信工具类
 * aliyun-java-sdk-core.jar
 */
public class AliSMSUtil {

	// 验证码模版
	private static final String tpl = "";
	// 短信签名
	private static final String sign = "健康管理";

	// 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
	private static final String accessKeyId = "LTAIrOtE0R5C46xp";
	private static final String accessKeySecret = "BOoLdXlqSZZEm0IEbNk9tBwVSiNcXv";

	/**
	 * 发送验证码
	 * 
	 * @param phone
	 *            手机号
	 * @param code
	 *            验证码
	 */
	public static CommonResponse sendCode(String phone, String code) {
		DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
		IAcsClient client = new DefaultAcsClient(profile);

		CommonRequest request = new CommonRequest();
		// request.setProtocol(ProtocolType.HTTPS);
		request.setMethod(MethodType.POST);
		request.setDomain("dysmsapi.aliyuncs.com");
		request.setVersion("2017-05-25");
		request.setAction("SendSms");
		request.putQueryParameter("RegionId", "cn-hangzhou");
		request.putQueryParameter("SignName", sign);
		request.putQueryParameter("PhoneNumbers", phone);
		request.putQueryParameter("TemplateCode", tpl);
		request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");
		try {
			CommonResponse response = client.getCommonResponse(request);
			System.out.println(response.getData());
			return response;
		} catch (ServerException e) {
			e.printStackTrace();
		} catch (ClientException e) {
			e.printStackTrace();
		}
		return null;
	}

}
