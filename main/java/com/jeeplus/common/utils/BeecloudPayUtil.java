package com.jeeplus.common.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.beecloud.BCPay;
import cn.beecloud.BeeCloud;
import cn.beecloud.BCEumeration.PAY_CHANNEL;
import cn.beecloud.bean.BCException;
import cn.beecloud.bean.BCOrder;
import cn.beecloud.bean.BCRefund;

public class BeecloudPayUtil {
	private static final Logger Logs = LoggerFactory.getLogger(BeecloudPayUtil.class);
	public static final String appID = "f01cfa41-cdde-426b-beca-9f6e5e43c056";
	public static final String testSecret = "9ba68db0-722a-444a-ac7a-ef7979a176dc";
	public static final String appSecret = "8ca92e5f-1dbe-432c-aa4d-b916e4a51a4d";
	public static final String masterSecret = "2be755b3-be88-40ea-bae9-9b6f3136afc0";
	
	private BeecloudPayUtil(){}
	
	/**
	 * beecloud网页支付
	 * @param channel 渠道code
	 * @param totalFee 支付金额，单位为分
	 * @param billNo 订单号
	 * @param title	标题
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static String pay(String channel, int totalFee, String billNo, String title, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String content = "";
		BeeCloud.registerApp(appID, testSecret, appSecret, masterSecret);
		response.setContentType("text/html;charset=UTF-8");
		PrintWriter out = response.getWriter();
		BCOrder bcOrder = new BCOrder(PAY_CHANNEL.valueOf(channel), totalFee, billNo, title);//设定订单信息
		
	    bcOrder.setBillTimeout(360);//设置订单超时时间
	    bcOrder.setReturnUrl("http://payservice.beecloud.cn/spay/result.php");//设置return url
	    //bcOrder.setReturnUrl("http://60.205.215.28/api/beecloud/payReturn");//设置return url
	    
	    try {
	        bcOrder = BCPay.startBCPay(bcOrder);
	        //out.println(bcOrder.getObjectId());
	        if("WX_NATIVE".equals(channel)) {// 微信支付，放入二维码
	        	out.println(bcOrder.getUrl());
	        	content = bcOrder.getUrl();
	        } else {
	        	out.println(bcOrder.getHtml()); // 输出支付宝收银台二维码到页面
	        	content = bcOrder.getHtml();
	        }
	        
	    } catch (BCException e) {
	       Logs.error(e.getMessage(), e);
	        out.println(e.getMessage());
	    }
	    return content;
	}
	/**
	 * beecloud微信支付
	 * @param channel 渠道code
	 * @param totalFee 支付金额，单位为分
	 * @param billNo 订单号
	 * @param title	标题
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public static String wxPay(String channel, int totalFee, String billNo, String title, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String content = "";
		BeeCloud.registerApp(appID, testSecret, appSecret, masterSecret);
		//response.setContentType("text/html;charset=UTF-8");
		//PrintWriter out = response.getWriter();
		BCOrder bcOrder = new BCOrder(PAY_CHANNEL.valueOf(channel), totalFee, billNo, title);//设定订单信息
		
		bcOrder.setBillTimeout(360);//设置订单超时时间
		bcOrder.setReturnUrl("http://payservice.beecloud.cn/spay/result.php");//设置return url
		
		try {
			bcOrder = BCPay.startBCPay(bcOrder);
			//out.println(bcOrder.getObjectId());
			if("WX_NATIVE".equals(channel)) {// 微信支付，放入二维码
				//out.println(bcOrder.getUrl());
				content = bcOrder.getUrl();
			} else {
				//out.println(bcOrder.getHtml()); // 输出支付宝收银台二维码到页面
				content = bcOrder.getHtml();
			}
			
		} catch (BCException e) {
			Logs.error(e.getMessage(), e);
			//out.println(e.getMessage());
		}
		return content;
	}
	
	/**
	 * beecloud退款
	 * @param orderId 订单号
	 * @param refundId 退款单号
	 * @param amount 退款金额 单位：分
	 * @throws BCException 
	 */
	public static void refund(String orderId, String refundId, int amount) throws BCException {
		BeeCloud.registerApp(appID, testSecret, appSecret, masterSecret);
		BCRefund param = new BCRefund(orderId, refundId, amount);
		BCRefund refund = BCPay.startBCRefund(param);
		//refund.getAliRefundUrl();
	}
}
