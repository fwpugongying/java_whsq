package com.jeeplus.common.utils;

import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;

public class JPushUtil {
	private static JPushClient jpushClient = new JPushClient("bcac4f50d68692d376ed8607", "faf88abb5ccbdf3bdc5f2214");

	/**
	 * 所有平台，所有设备
	 * @param text 内容
	 * @return
	 */
	public static PushPayload buildPushObject_all_all_alert(String text) {
		return PushPayload.alertAll(text);
	}

	/**
	 * 客户端用户推送
	 * @param title 标题
	 * @param text 内容
	 * @param token 推送标识
	 * @return
	 */
	public static PushResult registerTitle(String title, String text, String token) {
		PushPayload payload = PushPayload.newBuilder().setPlatform(Platform.android_ios())
				.setAudience(Audience.registrationId(token)).setNotification(Notification.alert(text))
				.setOptions(Options.newBuilder().setApnsProduction(false).build()).build();
		try {
			return jpushClient.sendPush(payload);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 客户端所有推送
	 * @param title
	 * @param text
	 * @return
	 */
	public static PushResult registerAll(String title, String text) {
		PushPayload payload = PushPayload.newBuilder().setPlatform(Platform.android_ios()).setAudience(Audience.all())
				.setNotification(Notification.alert(text))
				.setOptions(Options.newBuilder().setApnsProduction(false).build()).build();
		try {
			return jpushClient.sendPush(payload);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
