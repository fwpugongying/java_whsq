package com.jeeplus.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Year {
    private final static int[] dayArr = new int[] { 20, 19, 21, 20, 21, 22, 23,
            23, 23, 24, 23, 22 };
    private final static String[] constellationArr = new String[] { "摩羯座",
            "水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座",
            "天蝎座", "射手座", "摩羯座" };

    /**
     * 根据出生日期计算属相和星座
     * 
     * @param args
     */
    public static void main(String[] args) {
        int month = 9;
        int day = 23;
        System.out.println("星座为：" + getConstellation(month, day));
        System.out.println("属相为:" + getYear(1993));

    }

    /**
     * Java通过生日计算星座
     * 
     * @param month
     * @param day
     * @return
     */
    public static String getConstellation(int month, int day) {
        return day < dayArr[month - 1] ? constellationArr[month - 1]
                : constellationArr[month];
    }

    /**
     * 通过生日计算属相
     * 
     * @param year
     * @return
     */
    public static String getYear(int year) {
        if (year < 1900) {
            return "未知";
        }
        int start = 1900;
        String[] years = new String[] { "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊",
                "猴", "鸡", "狗", "猪" };
        return years[(year - start) % years.length];
    }
    public static  int getshu(Date birthDay) throws Exception {
        Calendar cal = Calendar.getInstance();
 
        if (cal.before(birthDay)) {
            throw new IllegalArgumentException(
                    "The birthDay is before Now.It's unbelievable!");
        }
        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);
 
        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);
 
        int age = yearNow - yearBirth;
 
        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) age--;
            }else{
                age--;
            }
        }
        return age;
    }
    
    public static int getAge(Date brithday){
		  try {
		   Calendar calendar = Calendar.getInstance();
		   SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		   String currentTime = formatDate.format(calendar.getTime());
		   Date today = formatDate.parse(currentTime);
		   if(today.getMonth()>brithday.getMonth()){
			   return today.getYear() - brithday.getYear();
		   }else if(today.getMonth()==brithday.getMonth() && today.getDate()>=brithday.getDate()){
			   return today.getYear() - brithday.getYear();
		   }else{
			   return today.getYear() - brithday.getYear()-1;
		   }
		  } catch (Exception e) {
		   return 0;
		  }
	 }
}