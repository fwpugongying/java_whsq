/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.xhq.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.xhq.entity.Xhq;
import com.jeeplus.modules.xhq.mapper.XhqMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class XhqService extends CrudService<XhqMapper, Xhq> {


	@Autowired

	public Xhq get(String id) {
		return super.get(id);
	}

	public List<Xhq> findList(Xhq xhq) {
		return super.findList(xhq);
	}
	public List<Xhq> findUniqueByProperty(Xhq xhq) {
		return super.findList(xhq);
	}
	public Xhq getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Xhq> findPage(Page<Xhq> page, Xhq xhq) {
		return super.findPage(page, xhq);
	}
	@Transactional(readOnly = false)
	public void save(Xhq xhq) {
		super.save(xhq);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Xhq xhq) {
		super.save(xhq);

		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("OrderNO", xhq.getId()));
		JSONObject object = WohuiUtils.send(WohuiUtils.XHQPay, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("开通选货权订单号"+xhq.getId()+"失败：" + object.getString("respMsg"));
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Xhq xhq) {
		super.delete(xhq);
	}

}