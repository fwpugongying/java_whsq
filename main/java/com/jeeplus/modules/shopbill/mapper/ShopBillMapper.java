/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopbill.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.shopbill.entity.ShopBill;

/**
 * 实体店账单MAPPER接口
 * @author lixinapp
 * @version 2019-10-17
 */
@MyBatisMapper
public interface ShopBillMapper extends BaseMapper<ShopBill> {
	
}