/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopbill.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.shopbill.entity.ShopBill;
import com.jeeplus.modules.shopbill.mapper.ShopBillMapper;

/**
 * 实体店账单Service
 * @author lixinapp
 * @version 2019-10-17
 */
@Service
@Transactional(readOnly = true)
public class ShopBillService extends CrudService<ShopBillMapper, ShopBill> {

	public ShopBill get(String id) {
		return super.get(id);
	}
	
	public List<ShopBill> findList(ShopBill shopBill) {
		return super.findList(shopBill);
	}
	
	public Page<ShopBill> findPage(Page<ShopBill> page, ShopBill shopBill) {
		return super.findPage(page, shopBill);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopBill shopBill) {
		super.save(shopBill);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopBill shopBill) {
		super.delete(shopBill);
	}
	
}