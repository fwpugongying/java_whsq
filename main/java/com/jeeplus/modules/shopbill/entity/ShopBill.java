/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopbill.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeeplus.modules.shop.entity.Shop;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 实体店账单Entity
 * @author lixinapp
 * @version 2019-10-17
 */
public class ShopBill extends DataEntity<ShopBill> {
	
	private static final long serialVersionUID = 1L;
	private Shop shop;		// 商家
	private String title;		// 标题
	private String amount;		// 金额
	private String type;		// 类型 0支出 1收入
	private String orderId;		// 订单号
	private String content;		// 描述
	private Integer qty;		// 数量
	private String username;	// 收货人
	private String phone;		// 收货电话
	private Date finishDate;//订单完成时间
	public ShopBill() {
		super();
	}

	public ShopBill(String id){
		super(id);
	}

	@NotNull(message="商家不能为空")
	@ExcelField(title="商家", fieldType=Shop.class, value="shop.title", align=2, sort=1)
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}

	@ExcelField(title="标题", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="金额", align=2, sort=3)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="类型 0支出 1收入", dictType="bill_type", align=2, sort=4)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
}