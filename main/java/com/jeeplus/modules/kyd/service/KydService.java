/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.kyd.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.kyd.entity.Kyd;
import com.jeeplus.modules.kyd.mapper.KydMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class KydService extends CrudService<KydMapper, Kyd> {


	@Autowired

	public Kyd get(String id) {
		return super.get(id);
	}

	public List<Kyd> findList(Kyd kyd) {
		return super.findList(kyd);
	}
	public List<Kyd> findUniqueByProperty(Kyd kyd) {
		return super.findList(kyd);
	}

	public Kyd getOrderno(String orderno) {
		return findUniqueByProperty("orderno", orderno);
	}

	public Kyd getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Kyd> findPage(Page<Kyd> page, Kyd kyd) {
		return super.findPage(page, kyd);
	}
	@Transactional(readOnly = false)
	public void save(Kyd kyd) {
		super.save(kyd);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Kyd kyd) {
		super.save(kyd);
}
	
	@Transactional(readOnly = false)
	public void delete(Kyd kyd) {
		super.delete(kyd);
	}

}