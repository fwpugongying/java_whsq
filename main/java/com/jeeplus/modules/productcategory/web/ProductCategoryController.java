/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productcategory.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.common.config.Global;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.modules.productcategory.entity.ProductCategory;
import com.jeeplus.modules.productcategory.service.ProductCategoryService;

/**
 * 商品分类Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/productcategory/productCategory")
public class ProductCategoryController extends BaseController {

	@Autowired
	private ProductCategoryService productCategoryService;
	
	@ModelAttribute
	public ProductCategory get(@RequestParam(required=false) String id) {
		ProductCategory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = productCategoryService.get(id);
		}
		if (entity == null){
			entity = new ProductCategory();
		}
		return entity;
	}
	
	/**
	 * 商品分类列表页面
	 */
	@RequiresPermissions("productcategory:productCategory:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProductCategory productCategory,  HttpServletRequest request, HttpServletResponse response, Model model) {
	
		model.addAttribute("productCategory", productCategory);
		return "modules/productcategory/productCategoryList";
	}

	/**
	 * 查看，增加，编辑商品分类表单页面
	 */
	@RequiresPermissions(value={"productcategory:productCategory:view","productcategory:productCategory:add","productcategory:productCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProductCategory productCategory, Model model) {
		if (productCategory.getParent()!=null && StringUtils.isNotBlank(productCategory.getParent().getId())){
			productCategory.setParent(productCategoryService.get(productCategory.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(productCategory.getId())){
				ProductCategory productCategoryChild = new ProductCategory();
				productCategoryChild.setParent(new ProductCategory(productCategory.getParent().getId()));
				List<ProductCategory> list = productCategoryService.findList(productCategory); 
				if (list.size() > 0){
					productCategory.setSort(list.get(list.size()-1).getSort());
					if (productCategory.getSort() != null){
						productCategory.setSort(productCategory.getSort() + 30);
					}
				}
			}
		}
		if (productCategory.getSort() == null){
			productCategory.setSort(30);
		}
		model.addAttribute("productCategory", productCategory);
		return "modules/productcategory/productCategoryForm";
	}

	/**
	 * 保存商品分类
	 */
	@ResponseBody
	@RequiresPermissions(value={"productcategory:productCategory:add","productcategory:productCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProductCategory productCategory, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		if (productCategory.getParent() == null || StringUtils.isBlank(productCategory.getParentId()) || "0".equals(productCategory.getParentId())){
			productCategory.setType("1");
		} else {
			productCategory.setType("2");
		}
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(productCategory);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}

		//新增或编辑表单保存
		productCategoryService.save(productCategory);//保存
		j.setSuccess(true);
		j.put("productCategory", productCategory);
		j.setMsg("保存商品分类成功");
		return j;
	}
	
	@ResponseBody
	@RequestMapping(value = "getChildren")
	public List<ProductCategory> getChildren(String parentId){
		if("-1".equals(parentId)){//如果是-1，没指定任何父节点，就从根节点开始查找
			parentId = "0";
		}
		return productCategoryService.getChildren(parentId);
	}
	
	/**
	 * 删除商品分类
	 */
	@ResponseBody
	@RequiresPermissions("productcategory:productCategory:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProductCategory productCategory) {
		AjaxJson j = new AjaxJson();
		productCategoryService.delete(productCategory);
		j.setSuccess(true);
		j.setMsg("删除商品分类成功");
		return j;
	}

	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		List<ProductCategory> list = productCategoryService.findList(new ProductCategory());
		for (int i=0; i<list.size(); i++){
			ProductCategory e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("text", e.getName());
				if(StringUtils.isBlank(e.getParentId()) || "0".equals(e.getParentId())){
					map.put("parent", "#");
					Map<String, Object> state = Maps.newHashMap();
					state.put("opened", true);
					map.put("state", state);
				}else{
					map.put("parent", e.getParentId());
				}
				mapList.add(map);
			}
		}
		return mapList;
	}
	
	/**
	 * 一级分类列表
	 * @param extId
	 * @param response
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData1")
	public List<Map<String, Object>> treeData1(@RequestParam(required=false) String extId, HttpServletResponse response) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		ProductCategory productCategory = new ProductCategory();
		productCategory.setType("1");
		List<ProductCategory> list = productCategoryService.findList(productCategory);
		for (int i=0; i<list.size(); i++){
			ProductCategory e = list.get(i);
			if (StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("text", e.getName());
				if(StringUtils.isBlank(e.getParentId()) || "0".equals(e.getParentId())){
					map.put("parent", "#");
					Map<String, Object> state = Maps.newHashMap();
					state.put("opened", true);
					map.put("state", state);
				}else{
					map.put("parent", e.getParentId());
				}
				mapList.add(map);
			}
		}
		return mapList;
	}
}