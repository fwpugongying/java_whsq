/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productcategory.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.TreeService;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.modules.productcategory.entity.ProductCategory;
import com.jeeplus.modules.productcategory.mapper.ProductCategoryMapper;

/**
 * 商品分类Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class ProductCategoryService extends TreeService<ProductCategoryMapper, ProductCategory> {

	public ProductCategory get(String id) {
		return super.get(id);
	}
	
	public List<ProductCategory> findList(ProductCategory productCategory) {
		if (StringUtils.isNotBlank(productCategory.getParentIds())){
			productCategory.setParentIds(","+productCategory.getParentIds()+",");
		}
		return super.findList(productCategory);
	}
	
	@Transactional(readOnly = false)
	public void save(ProductCategory productCategory) {
		super.save(productCategory);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProductCategory productCategory) {
		super.delete(productCategory);
	}
	
}