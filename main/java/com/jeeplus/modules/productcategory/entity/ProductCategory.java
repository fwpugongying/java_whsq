/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productcategory.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import com.jeeplus.core.persistence.TreeEntity;

/**
 * 商品分类Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class ProductCategory extends TreeEntity<ProductCategory> {
	
	private static final long serialVersionUID = 1L;
	private String type;		// 1:1级分类 2:2级分类
	private String state;		// 状态 0正常 1禁用
	private String icon;		// 图标
	private String hot;		// 推荐 0否 1是
	
	
	public ProductCategory() {
		super();
	}

	public ProductCategory(String id){
		super(id);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	public String getHot() {
		return hot;
	}

	public void setHot(String hot) {
		this.hot = hot;
	}
	
	public  ProductCategory getParent() {
			return parent;
	}
	
	@Override
	public void setParent(ProductCategory parent) {
		this.parent = parent;
		
	}
	
	public String getParentId() {
		return parent != null && parent.getId() != null ? parent.getId() : "0";
	}
}