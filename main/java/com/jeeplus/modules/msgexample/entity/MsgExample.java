/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.msgexample.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 消息文案Entity
 * @author lixinapp
 * @version 2019-11-15
 */
public class MsgExample extends DataEntity<MsgExample> {
	
	private static final long serialVersionUID = 1L;
	private String type;		// 类型
	private String content;		// 内容
	
	public MsgExample() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public MsgExample(String id){
		super(id);
	}

	@ExcelField(title="类型", dictType="msg_example_type", align=2, sort=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="内容", align=2, sort=2)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}