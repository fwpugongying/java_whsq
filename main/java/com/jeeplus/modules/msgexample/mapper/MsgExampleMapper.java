/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.msgexample.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.msgexample.entity.MsgExample;

/**
 * 消息文案MAPPER接口
 * @author lixinapp
 * @version 2019-11-15
 */
@MyBatisMapper
public interface MsgExampleMapper extends BaseMapper<MsgExample> {
	
}