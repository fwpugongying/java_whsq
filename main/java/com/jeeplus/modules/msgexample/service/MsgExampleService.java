/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.msgexample.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.msgexample.entity.MsgExample;
import com.jeeplus.modules.msgexample.mapper.MsgExampleMapper;

/**
 * 消息文案Service
 * @author lixinapp
 * @version 2019-11-15
 */
@Service
@Transactional(readOnly = true)
public class MsgExampleService extends CrudService<MsgExampleMapper, MsgExample> {

	public MsgExample get(String id) {
		return super.get(id);
	}
	
	public List<MsgExample> findList(MsgExample msgExample) {
		return super.findList(msgExample);
	}
	
	public Page<MsgExample> findPage(Page<MsgExample> page, MsgExample msgExample) {
		return super.findPage(page, msgExample);
	}
	
	@Transactional(readOnly = false)
	public void save(MsgExample msgExample) {
		super.save(msgExample);
	}
	
	@Transactional(readOnly = false)
	public void delete(MsgExample msgExample) {
		super.delete(msgExample);
	}
	
	public MsgExample getByType(String type) {
		return findUniqueByProperty("type", type);
	}
}