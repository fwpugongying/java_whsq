/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.msgexample.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msgexample.entity.MsgExample;
import com.jeeplus.modules.msgexample.service.MsgExampleService;

/**
 * 消息文案Controller
 * @author lixinapp
 * @version 2019-11-15
 */
@Controller
@RequestMapping(value = "${adminPath}/msgexample/msgExample")
public class MsgExampleController extends BaseController {

	@Autowired
	private MsgExampleService msgExampleService;
	
	@ModelAttribute
	public MsgExample get(@RequestParam(required=false) String id) {
		MsgExample entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = msgExampleService.get(id);
		}
		if (entity == null){
			entity = new MsgExample();
		}
		return entity;
	}
	
	/**
	 * 消息文案列表页面
	 */
	@RequiresPermissions("msgexample:msgExample:list")
	@RequestMapping(value = {"list", ""})
	public String list(MsgExample msgExample, Model model) {
		model.addAttribute("msgExample", msgExample);
		return "modules/msgexample/msgExampleList";
	}
	
		/**
	 * 消息文案列表数据
	 */
	@ResponseBody
	@RequiresPermissions("msgexample:msgExample:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(MsgExample msgExample, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<MsgExample> page = msgExampleService.findPage(new Page<MsgExample>(request, response), msgExample); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑消息文案表单页面
	 */
	@RequiresPermissions(value={"msgexample:msgExample:view","msgexample:msgExample:add","msgexample:msgExample:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(MsgExample msgExample, Model model) {
		model.addAttribute("msgExample", msgExample);
		return "modules/msgexample/msgExampleForm";
	}

	/**
	 * 保存消息文案
	 */
	@ResponseBody
	@RequiresPermissions(value={"msgexample:msgExample:add","msgexample:msgExample:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(MsgExample msgExample, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(msgExample);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		msgExampleService.save(msgExample);//保存
		j.setSuccess(true);
		j.setMsg("保存消息文案成功");
		return j;
	}
	
	/**
	 * 删除消息文案
	 */
	@ResponseBody
	@RequiresPermissions("msgexample:msgExample:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(MsgExample msgExample) {
		AjaxJson j = new AjaxJson();
		msgExampleService.delete(msgExample);
		j.setMsg("删除消息文案成功");
		return j;
	}
	
	/**
	 * 批量删除消息文案
	 */
	@ResponseBody
	@RequiresPermissions("msgexample:msgExample:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			msgExampleService.delete(msgExampleService.get(id));
		}
		j.setMsg("删除消息文案成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("msgexample:msgExample:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(MsgExample msgExample, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "消息文案"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<MsgExample> page = msgExampleService.findPage(new Page<MsgExample>(request, response, -1), msgExample);
    		new ExportExcel("消息文案", MsgExample.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出消息文案记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("msgexample:msgExample:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<MsgExample> list = ei.getDataList(MsgExample.class);
			for (MsgExample msgExample : list){
				try{
					msgExampleService.save(msgExample);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条消息文案记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条消息文案记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入消息文案失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入消息文案数据模板
	 */
	@ResponseBody
	@RequiresPermissions("msgexample:msgExample:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "消息文案数据导入模板.xlsx";
    		List<MsgExample> list = Lists.newArrayList(); 
    		new ExportExcel("消息文案数据", MsgExample.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}