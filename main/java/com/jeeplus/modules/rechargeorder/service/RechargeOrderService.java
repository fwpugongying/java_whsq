/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.rechargeorder.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.rechargeorder.entity.RechargeOrder;
import com.jeeplus.modules.rechargeorder.mapper.RechargeOrderMapper;

/**
 * 话费充值订单Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class RechargeOrderService extends CrudService<RechargeOrderMapper, RechargeOrder> {

	public RechargeOrder get(String id) {
		return super.get(id);
	}
	
	public List<RechargeOrder> findList(RechargeOrder rechargeOrder) {
		return super.findList(rechargeOrder);
	}
	
	public Page<RechargeOrder> findPage(Page<RechargeOrder> page, RechargeOrder rechargeOrder) {
		return super.findPage(page, rechargeOrder);
	}
	
	@Transactional(readOnly = false)
	public void save(RechargeOrder rechargeOrder) {
		super.save(rechargeOrder);
	}
	
	@Transactional(readOnly = false)
	public void delete(RechargeOrder rechargeOrder) {
		super.delete(rechargeOrder);
	}
	
}