/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.rechargeorder.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.rechargeorder.entity.RechargeOrder;

/**
 * 话费充值订单MAPPER接口
 * @author lixinapp
 * @version 2019-10-14
 */
@MyBatisMapper
public interface RechargeOrderMapper extends BaseMapper<RechargeOrder> {
	
}