/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.address.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 收货地址Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Address extends DataEntity<Address> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String username;		// 联系人
	private String phone;		// 联系电话
	private String province;		// 省份
	private String city;		// 城市
	private String area;		// 区县
	private String details;		// 详细地址
	private String state;		// 默认 0否 1是
	
	public Address() {
		super();
	}

	public Address(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="联系人", align=2, sort=2)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="联系电话", align=2, sort=3)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@ExcelField(title="省份", align=2, sort=4)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@ExcelField(title="城市", align=2, sort=5)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@ExcelField(title="区县", align=2, sort=6)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	@ExcelField(title="详细地址", align=2, sort=7)
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	@ExcelField(title="默认 0否 1是", align=2, sort=8)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}