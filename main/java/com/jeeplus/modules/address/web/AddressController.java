/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.address.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.address.entity.Address;
import com.jeeplus.modules.address.service.AddressService;

/**
 * 收货地址Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/address/address")
public class AddressController extends BaseController {

	@Autowired
	private AddressService addressService;
	
	@ModelAttribute
	public Address get(@RequestParam(required=false) String id) {
		Address entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = addressService.get(id);
		}
		if (entity == null){
			entity = new Address();
		}
		return entity;
	}
	
	/**
	 * 收货地址列表页面
	 */
	@RequiresPermissions("address:address:list")
	@RequestMapping(value = {"list", ""})
	public String list(Address address, Model model) {
		model.addAttribute("address", address);
		return "modules/address/addressList";
	}
	
		/**
	 * 收货地址列表数据
	 */
	@ResponseBody
	@RequiresPermissions("address:address:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Address address, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Address> page = addressService.findPage(new Page<Address>(request, response), address); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑收货地址表单页面
	 */
	@RequiresPermissions(value={"address:address:view","address:address:add","address:address:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Address address, Model model) {
		model.addAttribute("address", address);
		return "modules/address/addressForm";
	}

	/**
	 * 保存收货地址
	 */
	@ResponseBody
	@RequiresPermissions(value={"address:address:add","address:address:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Address address, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(address);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		addressService.save(address);//保存
		j.setSuccess(true);
		j.setMsg("保存收货地址成功");
		return j;
	}
	
	/**
	 * 删除收货地址
	 */
	@ResponseBody
	@RequiresPermissions("address:address:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Address address) {
		AjaxJson j = new AjaxJson();
		addressService.delete(address);
		j.setMsg("删除收货地址成功");
		return j;
	}
	
	/**
	 * 批量删除收货地址
	 */
	@ResponseBody
	@RequiresPermissions("address:address:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			addressService.delete(addressService.get(id));
		}
		j.setMsg("删除收货地址成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("address:address:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Address address, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "收货地址"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Address> page = addressService.findPage(new Page<Address>(request, response, -1), address);
    		new ExportExcel("收货地址", Address.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出收货地址记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("address:address:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Address> list = ei.getDataList(Address.class);
			for (Address address : list){
				try{
					addressService.save(address);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条收货地址记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条收货地址记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入收货地址失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入收货地址数据模板
	 */
	@ResponseBody
	@RequiresPermissions("address:address:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "收货地址数据导入模板.xlsx";
    		List<Address> list = Lists.newArrayList(); 
    		new ExportExcel("收货地址数据", Address.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}