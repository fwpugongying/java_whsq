/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.upgrade.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.upgrade.entity.Upgrade;
import com.jeeplus.modules.upgrade.service.UpgradeService;

/**
 * 版本更新Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/upgrade/upgrade")
public class UpgradeController extends BaseController {

	@Autowired
	private UpgradeService upgradeService;
	
	@ModelAttribute
	public Upgrade get(@RequestParam(required=false) String id) {
		Upgrade entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = upgradeService.get(id);
		}
		if (entity == null){
			entity = new Upgrade();
		}
		return entity;
	}
	
	/**
	 * 版本更新列表页面
	 */
	@RequiresPermissions("upgrade:upgrade:list")
	@RequestMapping(value = {"list", ""})
	public String list(Upgrade upgrade, Model model) {
		model.addAttribute("upgrade", upgrade);
		return "modules/upgrade/upgradeList";
	}
	
		/**
	 * 版本更新列表数据
	 */
	@ResponseBody
	@RequiresPermissions("upgrade:upgrade:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Upgrade upgrade, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Upgrade> page = upgradeService.findPage(new Page<Upgrade>(request, response), upgrade); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑版本更新表单页面
	 */
	@RequiresPermissions(value={"upgrade:upgrade:view","upgrade:upgrade:add","upgrade:upgrade:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Upgrade upgrade, Model model) {
		model.addAttribute("upgrade", upgrade);
		return "modules/upgrade/upgradeForm";
	}

	/**
	 * 保存版本更新
	 */
	@ResponseBody
	@RequiresPermissions(value={"upgrade:upgrade:add","upgrade:upgrade:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Upgrade upgrade, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(upgrade);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		upgradeService.save(upgrade);//保存
		j.setSuccess(true);
		j.setMsg("保存版本更新成功");
		return j;
	}
	
	/**
	 * 删除版本更新
	 */
	@ResponseBody
	@RequiresPermissions("upgrade:upgrade:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Upgrade upgrade) {
		AjaxJson j = new AjaxJson();
		upgradeService.delete(upgrade);
		j.setMsg("删除版本更新成功");
		return j;
	}
	
	/**
	 * 批量删除版本更新
	 */
	@ResponseBody
	@RequiresPermissions("upgrade:upgrade:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			upgradeService.delete(upgradeService.get(id));
		}
		j.setMsg("删除版本更新成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("upgrade:upgrade:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Upgrade upgrade, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "版本更新"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Upgrade> page = upgradeService.findPage(new Page<Upgrade>(request, response, -1), upgrade);
    		new ExportExcel("版本更新", Upgrade.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出版本更新记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("upgrade:upgrade:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Upgrade> list = ei.getDataList(Upgrade.class);
			for (Upgrade upgrade : list){
				try{
					upgradeService.save(upgrade);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条版本更新记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条版本更新记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入版本更新失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入版本更新数据模板
	 */
	@ResponseBody
	@RequiresPermissions("upgrade:upgrade:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "版本更新数据导入模板.xlsx";
    		List<Upgrade> list = Lists.newArrayList(); 
    		new ExportExcel("版本更新数据", Upgrade.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}