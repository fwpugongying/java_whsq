/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.upgrade.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 版本更新Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Upgrade extends DataEntity<Upgrade> {
	
	private static final long serialVersionUID = 1L;
	private Integer number;		// 升级编号
	private Integer minnumber;		// 最低升级编号
	private String version;		// 版本号
	private String url;		// 地址
	private String content;		// 更新内容
	private String type;		// 强制更新 0否 1是
	
	public Upgrade() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Upgrade(String id){
		super(id);
	}

	@NotNull(message="升级编号不能为空")
	@ExcelField(title="升级编号", align=2, sort=1)
	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}
	@NotNull(message="最低升级编号不能为空")
	@ExcelField(title="最低升级编号", align=2, sort=1)
	public Integer getMinnumber() {
		return minnumber;
	}

	public void setMinnumber(Integer minnumber) {
		this.minnumber = minnumber;
	}
	
	@ExcelField(title="版本号", align=2, sort=2)
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	@ExcelField(title="地址", align=2, sort=3)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@ExcelField(title="更新内容", align=2, sort=4)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="强制更新 0否 1是", dictType="yes_no", align=2, sort=5)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}