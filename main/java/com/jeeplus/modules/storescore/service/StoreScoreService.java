/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storescore.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.storescore.entity.StoreScore;
import com.jeeplus.modules.storescore.mapper.StoreScoreMapper;

/**
 * 店铺评分Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class StoreScoreService extends CrudService<StoreScoreMapper, StoreScore> {

	public StoreScore get(String id) {
		return super.get(id);
	}
	
	public List<StoreScore> findList(StoreScore storeScore) {
		return super.findList(storeScore);
	}
	
	public Page<StoreScore> findPage(Page<StoreScore> page, StoreScore storeScore) {
		return super.findPage(page, storeScore);
	}
	
	@Transactional(readOnly = false)
	public void save(StoreScore storeScore) {
		super.save(storeScore);
	}
	
	@Transactional(readOnly = false)
	public void delete(StoreScore storeScore) {
		super.delete(storeScore);
	}
	
}