/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storescore.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.storescore.entity.StoreScore;

/**
 * 店铺评分MAPPER接口
 * @author lixinapp
 * @version 2019-10-14
 */
@MyBatisMapper
public interface StoreScoreMapper extends BaseMapper<StoreScore> {
	
}