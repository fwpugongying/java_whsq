/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storescore.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.storescore.entity.StoreScore;
import com.jeeplus.modules.storescore.service.StoreScoreService;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;

/**
 * 店铺评分Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/storescore/storeScore")
public class StoreScoreController extends BaseController {

	@Autowired
	private StoreScoreService storeScoreService;
	
	@ModelAttribute
	public StoreScore get(@RequestParam(required=false) String id) {
		StoreScore entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = storeScoreService.get(id);
		}
		if (entity == null){
			entity = new StoreScore();
		}
		return entity;
	}
	
	/**
	 * 店铺评分列表页面
	 */
	@RequiresPermissions("storescore:storeScore:list")
	@RequestMapping(value = {"list", ""})
	public String list(StoreScore storeScore, Model model) {
		model.addAttribute("storeScore", storeScore);
		return "modules/storescore/storeScoreList";
	}
	
		/**
	 * 店铺评分列表数据
	 */
	@ResponseBody
	@RequiresPermissions("storescore:storeScore:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(StoreScore storeScore, HttpServletRequest request, HttpServletResponse response, Model model) {
		// 判断当前登录用户
		User user = UserUtils.getUser();
		if (user.isShop()) {
			storeScore.setDataScope(" AND store.user_id = '"+user.getId()+"' ");
		}
		Page<StoreScore> page = storeScoreService.findPage(new Page<StoreScore>(request, response), storeScore); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑店铺评分表单页面
	 */
	@RequiresPermissions(value={"storescore:storeScore:view","storescore:storeScore:add","storescore:storeScore:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(StoreScore storeScore, Model model) {
		model.addAttribute("storeScore", storeScore);
		return "modules/storescore/storeScoreForm";
	}

	/**
	 * 保存店铺评分
	 */
	@ResponseBody
	@RequiresPermissions(value={"storescore:storeScore:add","storescore:storeScore:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(StoreScore storeScore, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(storeScore);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		storeScoreService.save(storeScore);//保存
		j.setSuccess(true);
		j.setMsg("保存店铺评分成功");
		return j;
	}
	
	/**
	 * 删除店铺评分
	 */
	@ResponseBody
	@RequiresPermissions("storescore:storeScore:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(StoreScore storeScore) {
		AjaxJson j = new AjaxJson();
		storeScoreService.delete(storeScore);
		j.setMsg("删除店铺评分成功");
		return j;
	}
	
	/**
	 * 批量删除店铺评分
	 */
	@ResponseBody
	@RequiresPermissions("storescore:storeScore:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			storeScoreService.delete(storeScoreService.get(id));
		}
		j.setMsg("删除店铺评分成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("storescore:storeScore:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(StoreScore storeScore, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "店铺评分"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<StoreScore> page = storeScoreService.findPage(new Page<StoreScore>(request, response, -1), storeScore);
    		new ExportExcel("店铺评分", StoreScore.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出店铺评分记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("storescore:storeScore:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<StoreScore> list = ei.getDataList(StoreScore.class);
			for (StoreScore storeScore : list){
				try{
					storeScoreService.save(storeScore);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条店铺评分记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条店铺评分记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入店铺评分失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入店铺评分数据模板
	 */
	@ResponseBody
	@RequiresPermissions("storescore:storeScore:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "店铺评分数据导入模板.xlsx";
    		List<StoreScore> list = Lists.newArrayList(); 
    		new ExportExcel("店铺评分数据", StoreScore.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}