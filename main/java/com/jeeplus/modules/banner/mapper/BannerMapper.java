/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.banner.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.banner.entity.Banner;

/**
 * 轮播图MAPPER接口
 * @author lixinapp
 * @version 2019-10-14
 */
@MyBatisMapper
public interface BannerMapper extends BaseMapper<Banner> {
	
}