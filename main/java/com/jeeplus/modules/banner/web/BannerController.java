/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.banner.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.banner.entity.Banner;
import com.jeeplus.modules.banner.service.BannerService;

/**
 * 轮播图Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/banner/banner")
public class BannerController extends BaseController {

	@Autowired
	private BannerService bannerService;
	
	@ModelAttribute
	public Banner get(@RequestParam(required=false) String id) {
		Banner entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bannerService.get(id);
		}
		if (entity == null){
			entity = new Banner();
		}
		return entity;
	}
	
	/**
	 * 轮播图列表页面
	 */
	@RequiresPermissions("banner:banner:list")
	@RequestMapping(value = {"list", ""})
	public String list(Banner banner, Model model) {
		model.addAttribute("banner", banner);
		return "modules/banner/bannerList";
	}
	
		/**
	 * 轮播图列表数据
	 */
	@ResponseBody
	@RequiresPermissions("banner:banner:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Banner banner, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Banner> page = bannerService.findPage(new Page<Banner>(request, response), banner); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑轮播图表单页面
	 */
	@RequiresPermissions(value={"banner:banner:view","banner:banner:add","banner:banner:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Banner banner, Model model) {
		model.addAttribute("banner", banner);
		return "modules/banner/bannerForm";
	}

	/**
	 * 保存轮播图
	 */
	@ResponseBody
	@RequiresPermissions(value={"banner:banner:add","banner:banner:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Banner banner, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(banner);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		bannerService.save(banner);//保存
		j.setSuccess(true);
		j.setMsg("保存轮播图成功");
		return j;
	}
	
	/**
	 * 删除轮播图
	 */
	@ResponseBody
	@RequiresPermissions("banner:banner:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Banner banner) {
		AjaxJson j = new AjaxJson();
		bannerService.delete(banner);
		j.setMsg("删除轮播图成功");
		return j;
	}
	
	/**
	 * 批量删除轮播图
	 */
	@ResponseBody
	@RequiresPermissions("banner:banner:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			bannerService.delete(bannerService.get(id));
		}
		j.setMsg("删除轮播图成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("banner:banner:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Banner banner, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "轮播图"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Banner> page = bannerService.findPage(new Page<Banner>(request, response, -1), banner);
    		new ExportExcel("轮播图", Banner.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出轮播图记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("banner:banner:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Banner> list = ei.getDataList(Banner.class);
			for (Banner banner : list){
				try{
					bannerService.save(banner);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条轮播图记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条轮播图记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入轮播图失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入轮播图数据模板
	 */
	@ResponseBody
	@RequiresPermissions("banner:banner:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "轮播图数据导入模板.xlsx";
    		List<Banner> list = Lists.newArrayList(); 
    		new ExportExcel("轮播图数据", Banner.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}