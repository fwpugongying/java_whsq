/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.banner.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.productcategory.entity.ProductCategory;
import com.jeeplus.modules.store.entity.Store;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 轮播图Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Banner extends DataEntity<Banner> {
	
	private static final long serialVersionUID = 1L;
	private String image;		// 图片
	private Integer sort;		// 排序
	private String type;		// 类型
	private String category;		// 分类 1富文本 2商品
	private String content;		// 富文本内容
	private Product product;// 商品
	private Store store;// 厂家
	private String thirdLink;// 外部链接
	private ProductCategory productCategory;// 商品分类
	public Banner() {
		super();
	}

	public Banner(String id){
		super(id);
	}

	@ExcelField(title="图片", align=2, sort=1)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=2)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	@ExcelField(title="类型", dictType="banner_type", align=2, sort=3)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public String getThirdLink() {
		if(StringUtils.isNotBlank(thirdLink)){
			return thirdLink.replace("&amp;", "&");
		}else{
			return thirdLink;
		}
	}

	public void setThirdLink(String thirdLink) {
		this.thirdLink = thirdLink;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	/**
	 * 富文本路径
	 * @return
	 */
	public String getUrl() {
		return Global.getConfig("filePath") + "/display/banner?id=" + id;
	}
}