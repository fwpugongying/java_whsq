/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pickday.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.pickday.entity.PickDay;

/**
 * 选货日MAPPER接口
 * @author lixinapp
 * @version 2019-11-22
 */
@MyBatisMapper
public interface PickDayMapper extends BaseMapper<PickDay> {
	
}