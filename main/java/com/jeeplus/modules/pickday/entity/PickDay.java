/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pickday.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 选货日Entity
 * @author lixinapp
 * @version 2019-11-22
 */
public class PickDay extends DataEntity<PickDay> {
	
	private static final long serialVersionUID = 1L;
	private String dates;		// 日期
	
	public PickDay() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public PickDay(String id){
		super(id);
	}

	@NotNull(message="日期不能为空")
	@ExcelField(title="日期", align=2, sort=1)
	public String getDates() {
		return dates;
	}

	public void setDates(String dates) {
		this.dates = dates;
	}
	
}