/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pickday.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.pickday.entity.PickDay;
import com.jeeplus.modules.pickday.service.PickDayService;

/**
 * 选货日Controller
 * @author lixinapp
 * @version 2019-11-22
 */
@Controller
@RequestMapping(value = "${adminPath}/pickday/pickDay")
public class PickDayController extends BaseController {

	@Autowired
	private PickDayService pickDayService;
	
	@ModelAttribute
	public PickDay get(@RequestParam(required=false) String id) {
		PickDay entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = pickDayService.get(id);
		}
		if (entity == null){
			entity = new PickDay();
		}
		return entity;
	}
	
	/**
	 * 选货日列表页面
	 */
	@RequiresPermissions("pickday:pickDay:list")
	@RequestMapping(value = {"list", ""})
	public String list(PickDay pickDay, Model model) {
		model.addAttribute("pickDay", pickDay);
		return "modules/pickday/pickDayList";
	}
	
		/**
	 * 选货日列表数据
	 */
	@ResponseBody
	@RequiresPermissions("pickday:pickDay:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(PickDay pickDay, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<PickDay> page = pickDayService.findPage(new Page<PickDay>(request, response), pickDay); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑选货日表单页面
	 */
	@RequiresPermissions(value={"pickday:pickDay:view","pickday:pickDay:add","pickday:pickDay:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(PickDay pickDay, Model model) {
		model.addAttribute("pickDay", pickDay);
		return "modules/pickday/pickDayForm";
	}

	/**
	 * 保存选货日
	 */
	@ResponseBody
	@RequiresPermissions(value={"pickday:pickDay:add","pickday:pickDay:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(PickDay pickDay, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(pickDay);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		pickDayService.save(pickDay);//保存
		j.setSuccess(true);
		j.setMsg("保存选货日成功");
		return j;
	}
	
	/**
	 * 删除选货日
	 */
	@ResponseBody
	@RequiresPermissions("pickday:pickDay:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(PickDay pickDay) {
		AjaxJson j = new AjaxJson();
		pickDayService.delete(pickDay);
		j.setMsg("删除选货日成功");
		return j;
	}
	
	/**
	 * 批量删除选货日
	 */
	@ResponseBody
	@RequiresPermissions("pickday:pickDay:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			pickDayService.delete(pickDayService.get(id));
		}
		j.setMsg("删除选货日成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("pickday:pickDay:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(PickDay pickDay, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "选货日"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<PickDay> page = pickDayService.findPage(new Page<PickDay>(request, response, -1), pickDay);
    		new ExportExcel("选货日", PickDay.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出选货日记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("pickday:pickDay:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<PickDay> list = ei.getDataList(PickDay.class);
			for (PickDay pickDay : list){
				try{
					pickDayService.save(pickDay);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条选货日记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条选货日记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入选货日失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入选货日数据模板
	 */
	@ResponseBody
	@RequiresPermissions("pickday:pickDay:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "选货日数据导入模板.xlsx";
    		List<PickDay> list = Lists.newArrayList(); 
    		new ExportExcel("选货日数据", PickDay.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}