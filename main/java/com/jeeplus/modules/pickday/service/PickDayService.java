/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pickday.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.pickday.entity.PickDay;
import com.jeeplus.modules.pickday.mapper.PickDayMapper;

/**
 * 选货日Service
 * @author lixinapp
 * @version 2019-11-22
 */
@Service
@Transactional(readOnly = true)
public class PickDayService extends CrudService<PickDayMapper, PickDay> {

	public PickDay get(String id) {
		return super.get(id);
	}
	
	public List<PickDay> findList(PickDay pickDay) {
		return super.findList(pickDay);
	}
	
	public Page<PickDay> findPage(Page<PickDay> page, PickDay pickDay) {
		return super.findPage(page, pickDay);
	}
	
	@Transactional(readOnly = false)
	public void save(PickDay pickDay) {
		super.save(pickDay);
	}
	
	@Transactional(readOnly = false)
	public void delete(PickDay pickDay) {
		super.delete(pickDay);
	}
	
}