/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcard.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.creditcard.entity.CreditCard;
import com.jeeplus.modules.creditcard.mapper.CreditCardMapper;

/**
 * 信用卡Service
 * @author lixinapp
 * @version 2019-11-01
 */
@Service
@Transactional(readOnly = true)
public class CreditCardService extends CrudService<CreditCardMapper, CreditCard> {

	public CreditCard get(String id) {
		return super.get(id);
	}
	
	public List<CreditCard> findList(CreditCard creditCard) {
		return super.findList(creditCard);
	}
	
	public Page<CreditCard> findPage(Page<CreditCard> page, CreditCard creditCard) {
		return super.findPage(page, creditCard);
	}
	
	@Transactional(readOnly = false)
	public void save(CreditCard creditCard) {
		super.save(creditCard);
	}
	
	@Transactional(readOnly = false)
	public void delete(CreditCard creditCard) {
		super.delete(creditCard);
	}

}