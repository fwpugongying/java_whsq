/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcard.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 信用卡Entity
 * @author lixinapp
 * @version 2019-11-01
 */
public class CreditCard extends DataEntity<CreditCard> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 信用卡编码
	private String title;		// 名称
	private String icon;		// 图片
	private String point;		// 铜板
	private String state;		// 状态 0正常 1停用
	private Integer sort;		// 排序
	private String content;		// 描述
	
	public CreditCard() {
		super();
	}

	public CreditCard(String id){
		super(id);
	}

	@ExcelField(title="信用卡编码", align=2, sort=1)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="名称", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="图片", align=2, sort=3)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@ExcelField(title="铜板", align=2, sort=4)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	@ExcelField(title="状态 0正常 1停用", dictType="credit_card_state", align=2, sort=5)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=6)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	@ExcelField(title="描述", align=2, sort=7)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}