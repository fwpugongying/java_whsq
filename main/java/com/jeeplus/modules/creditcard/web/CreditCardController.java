/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcard.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.creditcard.entity.CreditCard;
import com.jeeplus.modules.creditcard.service.CreditCardService;

/**
 * 信用卡Controller
 * @author lixinapp
 * @version 2019-11-01
 */
@Controller
@RequestMapping(value = "${adminPath}/creditcard/creditCard")
public class CreditCardController extends BaseController {

	@Autowired
	private CreditCardService creditCardService;
	
	@ModelAttribute
	public CreditCard get(@RequestParam(required=false) String id) {
		CreditCard entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = creditCardService.get(id);
		}
		if (entity == null){
			entity = new CreditCard();
		}
		return entity;
	}
	
	/**
	 * 信用卡列表页面
	 */
	@RequiresPermissions("creditcard:creditCard:list")
	@RequestMapping(value = {"list", ""})
	public String list(CreditCard creditCard, Model model) {
		model.addAttribute("creditCard", creditCard);
		return "modules/creditcard/creditCardList";
	}
	
		/**
	 * 信用卡列表数据
	 */
	@ResponseBody
	@RequiresPermissions("creditcard:creditCard:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(CreditCard creditCard, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CreditCard> page = creditCardService.findPage(new Page<CreditCard>(request, response), creditCard); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑信用卡表单页面
	 */
	@RequiresPermissions(value={"creditcard:creditCard:view","creditcard:creditCard:add","creditcard:creditCard:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(CreditCard creditCard, Model model) {
		model.addAttribute("creditCard", creditCard);
		return "modules/creditcard/creditCardForm";
	}

	/**
	 * 保存信用卡
	 */
	@ResponseBody
	@RequiresPermissions(value={"creditcard:creditCard:add","creditcard:creditCard:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(CreditCard creditCard, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(creditCard);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		creditCardService.save(creditCard);//保存
		j.setSuccess(true);
		j.setMsg("保存信用卡成功");
		return j;
	}
	
	/**
	 * 删除信用卡
	 */
	@ResponseBody
	@RequiresPermissions("creditcard:creditCard:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(CreditCard creditCard) {
		AjaxJson j = new AjaxJson();
		creditCardService.delete(creditCard);
		j.setMsg("删除信用卡成功");
		return j;
	}
	
	/**
	 * 批量删除信用卡
	 */
	@ResponseBody
	@RequiresPermissions("creditcard:creditCard:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			creditCardService.delete(creditCardService.get(id));
		}
		j.setMsg("删除信用卡成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("creditcard:creditCard:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(CreditCard creditCard, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "信用卡"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<CreditCard> page = creditCardService.findPage(new Page<CreditCard>(request, response, -1), creditCard);
    		new ExportExcel("信用卡", CreditCard.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出信用卡记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("creditcard:creditCard:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<CreditCard> list = ei.getDataList(CreditCard.class);
			for (CreditCard creditCard : list){
				try{
					creditCardService.save(creditCard);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条信用卡记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条信用卡记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入信用卡失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入信用卡数据模板
	 */
	@ResponseBody
	@RequiresPermissions("creditcard:creditCard:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "信用卡数据导入模板.xlsx";
    		List<CreditCard> list = Lists.newArrayList(); 
    		new ExportExcel("信用卡数据", CreditCard.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}