/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcard.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.creditcard.entity.CreditCard;

/**
 * 信用卡MAPPER接口
 * @author lixinapp
 * @version 2019-11-01
 */
@MyBatisMapper
public interface CreditCardMapper extends BaseMapper<CreditCard> {
	
}