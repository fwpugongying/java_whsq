/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storebill.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.storebill.entity.StoreBill;
import com.jeeplus.modules.storebill.service.StoreBillService;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;

/**
 * 厂家店铺账单Controller
 * @author lixinapp
 * @version 2019-10-17
 */
@Controller
@RequestMapping(value = "${adminPath}/storebill/storeBill")
public class StoreBillController extends BaseController {

	@Autowired
	private StoreBillService storeBillService;
	
	@ModelAttribute
	public StoreBill get(@RequestParam(required=false) String id) {
		StoreBill entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = storeBillService.get(id);
		}
		if (entity == null){
			entity = new StoreBill();
		}
		return entity;
	}
	
	/**
	 * 厂家店铺账单列表页面
	 */
	@RequiresPermissions("storebill:storeBill:list")
	@RequestMapping(value = {"list", ""})
	public String list(StoreBill storeBill, Model model) {
		model.addAttribute("storeBill", storeBill);
		return "modules/storebill/storeBillList";
	}
	
		/**
	 * 厂家店铺账单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("storebill:storeBill:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(StoreBill storeBill, HttpServletRequest request, HttpServletResponse response, Model model) {
		// 判断当前登录用户
		User user = UserUtils.getUser();
		if (user.isShop()) {
			storeBill.setDataScope(" AND store.user_id = '"+user.getId()+"' ");
		}
		Page<StoreBill> page = storeBillService.findPage(new Page<StoreBill>(request, response), storeBill); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑厂家店铺账单表单页面
	 */
	@RequiresPermissions(value={"storebill:storeBill:view","storebill:storeBill:add","storebill:storeBill:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(StoreBill storeBill, Model model) {
		model.addAttribute("storeBill", storeBill);
		return "modules/storebill/storeBillForm";
	}

	/**
	 * 保存厂家店铺账单
	 */
	@ResponseBody
	@RequiresPermissions(value={"storebill:storeBill:add","storebill:storeBill:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(StoreBill storeBill, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(storeBill);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		storeBillService.save(storeBill);//保存
		j.setSuccess(true);
		j.setMsg("保存厂家店铺账单成功");
		return j;
	}
	
	/**
	 * 删除厂家店铺账单
	 */
	@ResponseBody
	@RequiresPermissions("storebill:storeBill:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(StoreBill storeBill) {
		AjaxJson j = new AjaxJson();
		storeBillService.delete(storeBill);
		j.setMsg("删除厂家店铺账单成功");
		return j;
	}
	
	/**
	 * 批量删除厂家店铺账单
	 */
	@ResponseBody
	@RequiresPermissions("storebill:storeBill:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			storeBillService.delete(storeBillService.get(id));
		}
		j.setMsg("删除厂家店铺账单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("storebill:storeBill:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(StoreBill storeBill, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "厂家店铺账单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<StoreBill> page = storeBillService.findPage(new Page<StoreBill>(request, response, -1), storeBill);
    		new ExportExcel("厂家店铺账单", StoreBill.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出厂家店铺账单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("storebill:storeBill:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<StoreBill> list = ei.getDataList(StoreBill.class);
			for (StoreBill storeBill : list){
				try{
					storeBillService.save(storeBill);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条厂家店铺账单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条厂家店铺账单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入厂家店铺账单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入厂家店铺账单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("storebill:storeBill:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "厂家店铺账单数据导入模板.xlsx";
    		List<StoreBill> list = Lists.newArrayList(); 
    		new ExportExcel("厂家店铺账单数据", StoreBill.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}