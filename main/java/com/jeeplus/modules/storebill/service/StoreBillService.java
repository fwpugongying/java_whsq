/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storebill.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.storebill.entity.StoreBill;
import com.jeeplus.modules.storebill.mapper.StoreBillMapper;

/**
 * 厂家店铺账单Service
 * @author lixinapp
 * @version 2019-10-17
 */
@Service
@Transactional(readOnly = true)
public class StoreBillService extends CrudService<StoreBillMapper, StoreBill> {

	public StoreBill get(String id) {
		return super.get(id);
	}
	
	public List<StoreBill> findList(StoreBill storeBill) {
		return super.findList(storeBill);
	}
	
	public Page<StoreBill> findPage(Page<StoreBill> page, StoreBill storeBill) {
		return super.findPage(page, storeBill);
	}
	
	@Transactional(readOnly = false)
	public void save(StoreBill storeBill) {
		super.save(storeBill);
	}
	
	@Transactional(readOnly = false)
	public void delete(StoreBill storeBill) {
		super.delete(storeBill);
	}
	
}