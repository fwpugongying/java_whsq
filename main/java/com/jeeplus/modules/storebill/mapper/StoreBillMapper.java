/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storebill.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.storebill.entity.StoreBill;

/**
 * 厂家店铺账单MAPPER接口
 * @author lixinapp
 * @version 2019-10-17
 */
@MyBatisMapper
public interface StoreBillMapper extends BaseMapper<StoreBill> {
	
}