/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyprice.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 单品代理价格Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class ProxyPrice extends DataEntity<ProxyPrice> {
	
	private static final long serialVersionUID = 1L;
	private String price;		// 价格
	private String monthprice;		// 月价格
	private String cfmMargin;		// 保证金
	private Integer years;		// 年限
	private String amount;// 总成长值
	private String pickPrice;// 增选价格
	private String dpdlprice;// 新增单品代理价格
	private String ydAmount;// 云店成长值
	private String tbAmount;// 铜板成长值
	
	public ProxyPrice() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public ProxyPrice(String id){
		super(id);
	}

	@ExcelField(title="价格", align=2, sort=1)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@NotNull(message="年限不能为空")
	@ExcelField(title="年限", align=2, sort=2)
	public Integer getYears() {
		return years;
	}

	public void setYears(Integer years) {
		this.years = years;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getPickPrice() {
		return pickPrice;
	}

	public void setPickPrice(String pickPrice) {
		this.pickPrice = pickPrice;
	}

	public String getMonthprice() {
		return monthprice;
	}

	public void setMonthprice(String monthprice) {
		this.pickPrice = monthprice;
	}
	public String getCfmMargin() {
		return cfmMargin;
	}

	public void setCfmMargin(String cfmMargin) {
		this.cfmMargin = cfmMargin;
	}

	public String getDpdlPrice() {
		return dpdlprice;
	}
	public void setDpdlPrice(String dpdlprice) {
		this.dpdlprice = dpdlprice;
	}
	public String getYdAmount() {
		return ydAmount;
	}

	public void setYdAmount(String ydAmount) {
		this.ydAmount = ydAmount;
	}

	public String getTbAmount() {
		return tbAmount;
	}

	public void setTbAmount(String tbAmount) {
		this.tbAmount = tbAmount;
	}
	
}