/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyprice.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.proxyprice.entity.ProxyPrice;
import com.jeeplus.modules.proxyprice.service.ProxyPriceService;

/**
 * 单品代理价格Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/proxyprice/proxyPrice")
public class ProxyPriceController extends BaseController {

	@Autowired
	private ProxyPriceService proxyPriceService;
	
	@ModelAttribute
	public ProxyPrice get(@RequestParam(required=false) String id) {
		ProxyPrice entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = proxyPriceService.get(id);
		}
		if (entity == null){
			entity = new ProxyPrice();
		}
		return entity;
	}
	
	/**
	 * 单品代理价格列表页面
	 */
	@RequiresPermissions("proxyprice:proxyPrice:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProxyPrice proxyPrice, Model model) {
		model.addAttribute("proxyPrice", proxyPrice);
		return "modules/proxyprice/proxyPriceList";
	}
	
		/**
	 * 单品代理价格列表数据
	 */
	@ResponseBody
	@RequiresPermissions("proxyprice:proxyPrice:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProxyPrice proxyPrice, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProxyPrice> page = proxyPriceService.findPage(new Page<ProxyPrice>(request, response), proxyPrice); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑单品代理价格表单页面
	 */
	@RequiresPermissions(value={"proxyprice:proxyPrice:view","proxyprice:proxyPrice:add","proxyprice:proxyPrice:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProxyPrice proxyPrice, Model model) {
		model.addAttribute("proxyPrice", proxyPrice);
		return "modules/proxyprice/proxyPriceForm";
	}

	/**
	 * 保存单品代理价格
	 */
	@ResponseBody
	@RequiresPermissions(value={"proxyprice:proxyPrice:add","proxyprice:proxyPrice:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProxyPrice proxyPrice, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(proxyPrice);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		proxyPriceService.save(proxyPrice);//保存
		j.setSuccess(true);
		j.setMsg("保存单品代理价格成功");
		return j;
	}
	
	/**
	 * 删除单品代理价格
	 */
	@ResponseBody
	@RequiresPermissions("proxyprice:proxyPrice:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProxyPrice proxyPrice) {
		AjaxJson j = new AjaxJson();
		proxyPriceService.delete(proxyPrice);
		j.setMsg("删除单品代理价格成功");
		return j;
	}
	
	/**
	 * 批量删除单品代理价格
	 */
	@ResponseBody
	@RequiresPermissions("proxyprice:proxyPrice:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			proxyPriceService.delete(proxyPriceService.get(id));
		}
		j.setMsg("删除单品代理价格成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("proxyprice:proxyPrice:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProxyPrice proxyPrice, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "单品代理价格"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProxyPrice> page = proxyPriceService.findPage(new Page<ProxyPrice>(request, response, -1), proxyPrice);
    		new ExportExcel("单品代理价格", ProxyPrice.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出单品代理价格记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("proxyprice:proxyPrice:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProxyPrice> list = ei.getDataList(ProxyPrice.class);
			for (ProxyPrice proxyPrice : list){
				try{
					proxyPriceService.save(proxyPrice);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条单品代理价格记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条单品代理价格记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入单品代理价格失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入单品代理价格数据模板
	 */
	@ResponseBody
	@RequiresPermissions("proxyprice:proxyPrice:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "单品代理价格数据导入模板.xlsx";
    		List<ProxyPrice> list = Lists.newArrayList(); 
    		new ExportExcel("单品代理价格数据", ProxyPrice.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}