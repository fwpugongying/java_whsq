/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyprice.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.proxyprice.entity.ProxyPrice;
import com.jeeplus.modules.proxyprice.mapper.ProxyPriceMapper;

/**
 * 单品代理价格Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class ProxyPriceService extends CrudService<ProxyPriceMapper, ProxyPrice> {

	public ProxyPrice get(String id) {
		return super.get(id);
	}
	
	public List<ProxyPrice> findList(ProxyPrice proxyPrice) {
		return super.findList(proxyPrice);
	}
	
	public Page<ProxyPrice> findPage(Page<ProxyPrice> page, ProxyPrice proxyPrice) {
		return super.findPage(page, proxyPrice);
	}
	
	@Transactional(readOnly = false)
	public void save(ProxyPrice proxyPrice) {
		super.save(proxyPrice);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProxyPrice proxyPrice) {
		super.delete(proxyPrice);
	}
	
}