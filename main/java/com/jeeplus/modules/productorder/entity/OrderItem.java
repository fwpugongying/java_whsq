/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productorder.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品订单项Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class OrderItem extends DataEntity<OrderItem> {
	
	private static final long serialVersionUID = 1L;
	private ProductOrder order;		// 订单号 父类
	private String productId;		// 商品
	private String skuId;		// 规格
	private String productTitle;		// 商品名称
	private String productIcon;		// 商品图片
	private String skuName;		// 规格名称
	private Integer qty;		// 数量
	private String price;		// 价格
	private String point;		// 铜板
	private String discount;		// 优惠金额
	private String productCode;		// 商品编码
	private String isDl;// 是否是代理商品
	private String isYg;// 是否预购1是0否\
	private String goodstype;		// 商品类型1套餐专区
	private String diffprice;//差额

	public String getDiffprice() {
		return diffprice;
	}
	public void setDiffprice(String diffprice) {
		this.diffprice = diffprice;
	}

	public String getIsDl() {
		return isDl;
	}

	public void setIsDl(String isDl) {
		this.isDl = isDl;
	}

	public String getGoodstype() {
		return goodstype;
	}

	public void setGoodstype(String goodstype) {
		this.goodstype = goodstype;
	}


	public String getIsYg() {
		return isYg;
	}

	public void setIsYg(String isYg) {
		this.isYg = isYg;
	}
	
	public OrderItem() {
		super();
	}

	public OrderItem(String id){
		super(id);
	}

	public OrderItem(ProductOrder order){
		this.order = order;
	}

	public ProductOrder getOrder() {
		return order;
	}

	public void setOrder(ProductOrder order) {
		this.order = order;
	}
	
	@ExcelField(title="商品", align=2, sort=2)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	@ExcelField(title="规格", align=2, sort=3)
	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	
	@ExcelField(title="商品名称", align=2, sort=4)
	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	
	@ExcelField(title="商品图片", align=2, sort=5)
	public String getProductIcon() {
		return productIcon;
	}

	public void setProductIcon(String productIcon) {
		this.productIcon = productIcon;
	}
	
	@ExcelField(title="规格名称", align=2, sort=6)
	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}
	
	@NotNull(message="数量不能为空")
	@ExcelField(title="数量", align=2, sort=7)
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@ExcelField(title="价格", align=2, sort=8)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@ExcelField(title="铜板", align=2, sort=9)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
}