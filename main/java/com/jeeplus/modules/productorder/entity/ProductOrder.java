/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productorder.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.store.entity.Store;
import com.jeeplus.modules.sys.entity.Area;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.google.common.collect.Lists;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品订单Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class ProductOrder extends DataEntity<ProductOrder> {
	
	private static final long serialVersionUID = 1L;
	private String id;// 订单号
	private Member member;		// 用户
	private Member daimai;		// 代买 
	private Store store;		// 店铺
	private String code;		// 商品编码
	private String totalId;		// 总订单号
	private String price;		// 商品总价
	private String amount;		// 订单总额
	private String money;		// 订单结算价
	private String point;		// 铜板
	private String freight;		// 运费
	private String giftprice;		// 赠品需支付价格
	private String proxyType;		// 申请代理 0否 1是
	private String proxyPrice;		// 代理费用
	private String discount;		// 优惠金额
	private String receipt;		// 发票抬头
	private String username;		// 收货人
	private String phone;		// 收货电话
	private String address;		// 收货地址
	private Date payDate;		// 付款时间
	private String payType;		// 付款方式 1支付宝 2微信
	private String state;		// 订单状态 0待付款 
	private Date sendDate;		// 发货时间
	private String expressCode;		// 物流编码
	private String expressName;		// 物流公司
	private String expressNo;		// 物流单号
	private Date cancelDate;		// 取消时间
	private Date expireDate;		// 过期时间
	private String payNo;		// 付款单号
	private Date finishDate;		// 收货时间
	private Date refundDate;		// 退款时间
	private String cancelReason;		// 取消原因
	private String refundReason;		// 退款原因
	private Date completeDate;		// 完成时间
	private List<OrderItem> orderItemList = Lists.newArrayList();		// 子表列表
	private String fenzu;		// 辅助字段-按商品编码分组
	private Date beginCreateDate; // 开始 下单时间
	private Date endCreateDate; // 结束 下单时间
	private Date createDate;// 下单时间
	private String productName;// 商品名称
	private String trade_type;// 支付类型
	private String skuName;// 规格名称
	private String qty;// 规格名称
	private String productTitle;// 规格名称
	private String productIcon;//商品图
	private String province;		// 省份
	private String city;		// 城市
	private String marea;		// 区县
	private String isHot;		// 是否砍一刀订单:0-否，1-是
	private String gwbprice;		// 购物币优惠价
	private String cftype;		// c2f购买类型 1 补贴价 2预售价
	private String ordertype;		// 订单类型1使用消费券
	private Date expiretaskDate;		// 过期时间
	private String zsqtype;		// 订单类型1专属券
	private String zsqprice;		// 专属券价
	private String xfqprice;		// 消费券价

	@ExcelField(title="专属券", align=2, sort=4)
	public String getZsqtype() {
		return zsqtype;
	}

	public void setZsqtype(String zsqtype) {
		this.zsqtype = zsqtype;
	}
	@ExcelField(title="专属券价", align=2, sort=4)
	public String getZsqprice() {
		return zsqprice;
	}
	public void setZsqprice(String zsqprice) {
		this.zsqprice = zsqprice;
	}
	@ExcelField(title="消费券价", align=2, sort=4)
	public String getXfqprice() {
		return xfqprice;
	}
	public void setXfqprice(String xfqprice) {
		this.xfqprice = xfqprice;
	}
	@ExcelField(title="c2f购买类型", align=2, sort=4)
	public String getOrdertype() {
		return ordertype;
	}

	public void setOrdertype(String ordertype) {
		this.ordertype = ordertype;
	}

	@ExcelField(title="c2f购买类型", align=2, sort=4)
	public String getCftype() {
		return cftype;
	}

	public void setCftype(String cftype) {
		this.cftype = cftype;
	}
	@ExcelField(title="购物币优惠价", align=2, sort=4)
	public String getGwbprice() {
		return gwbprice;
	}
	public void setGwbprice(String gwbprice) {
		this.gwbprice = gwbprice;
	}

	public String getIsHot() {
		return isHot;
	}
	public void setIsHot(String isHot) {
		this.isHot = isHot;
	}

	@ExcelField(title = "商品图", align = 2, sort = 6)
	public String getProductIcon() {
		return productIcon;
	}

	public void setProductIcon(String productIcon) {
		this.productIcon = productIcon;
	}
	public ProductOrder() {
		super();
	}

	public ProductOrder(String id){
		super(id);
	}

	@ExcelField(title="订单号", align=2, sort=1)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
//	@ExcelField(title="商品名称", align=2, sort=11)
	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=2)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Store.class, value="store.title", align=2, sort=3)
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public String getTotalId() {
		return totalId;
	}

	public void setTotalId(String totalId) {
		this.totalId = totalId;
	}
	
	@ExcelField(title="商品总价", align=2, sort=4)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@ExcelField(title="订单总额", align=2, sort=5)
	public String getAmount() {
		return amount;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	@ExcelField(
			title = "购买数量",
			align = 2,
			sort = 12
	)
	public String getQty() {
		return qty;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="铜板", align=2, sort=9)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	@ExcelField(title="运费", align=2, sort=7)
	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}

	@ExcelField(title="赠品需支付价格", align=2, sort=7)
	public String getGiftprice() {
		return giftprice;
	}

	public void setGiftprice(String giftprice) {
		this.giftprice = giftprice;
	}
	
	public String getProxyType() {
		return proxyType;
	}

	public void setProxyType(String proxyType) {
		this.proxyType = proxyType;
	}
	
	public String getProxyPrice() {
		return proxyPrice;
	}

	public void setProxyPrice(String proxyPrice) {
		this.proxyPrice = proxyPrice;
	}
	
	@ExcelField(title="优惠金额", align=2, sort=6)
	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	@ExcelField(title="发票抬头", align=2, sort=12)
	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}

	@ExcelField(title="收货人", align=2, sort=13)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="收货电话", align=2, sort=14)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@ExcelField(title="收货地址", align=2, sort=15) 
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	@ExcelField(title="省份", align=2, sort=4)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@ExcelField(title="城市", align=2, sort=5)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@ExcelField(title="区县", align=2, sort=6)
	public String getMarea() {
		return marea;
	}

	public void setMarea(String marea) {
		this.marea = marea;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="付款时间", align=2, sort=17)
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	
	@ExcelField(title="付款方式", dictType="pay_type", align=2, sort=18)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	@ExcelField(title="订单状态", dictType="product_order_state", align=2, sort=19)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="发货时间", align=2, sort=22)
	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
	public String getExpressCode() {
		return expressCode;
	}

	public void setExpressCode(String expressCode) {
		this.expressCode = expressCode;
	}
	
	@ExcelField(title="物流公司", align=2, sort=20)
	public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}
	
	@ExcelField(title="物流单号", align=2, sort=21)
	public String getExpressNo() {
		return expressNo;
	}

	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getExpiretaskDate() {
		return expiretaskDate;
	}

	public void setExpiretaskDate(Date expiretaskDate) {
		this.expiretaskDate = expiretaskDate;
	}
	
	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}
	
	public List<OrderItem> getOrderItemList() {
		return orderItemList;
	}

	public void setOrderItemList(List<OrderItem> orderItemList) {
		this.orderItemList = orderItemList;
	}

	public String getFenzu() {
		return fenzu;
	}

	public void setFenzu(String fenzu) {
		this.fenzu = fenzu;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public Date getCompleteDate() {
		return completeDate;
	}

	public void setCompleteDate(Date completeDate) {
		this.completeDate = completeDate;
	}

	public Member getDaimai() {
		return daimai;
	}

	public void setDaimai(Member daimai) {
		this.daimai = daimai;
	}

	@ExcelField(title="结算金额", align=2, sort=8)
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public Date getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(Date beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}

	public Date getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="下单时间", align=2, sort=16)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ExcelField(title="商品名称", align=2, sort=10)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@ExcelField(title="规格名称", align=2, sort=11)
	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }
    public String getTrade_type() {
        return trade_type;
    }
}