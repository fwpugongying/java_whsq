/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productorder.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.jeeplus.modules.hkqy.entity.Hkqy;
import com.jeeplus.modules.hkqy.mapper.HkqyMapper;
import com.jeeplus.modules.paa.entity.Paa;
import com.jeeplus.modules.paa.mapper.PaaMapper;
import com.jeeplus.modules.ptc.entity.Ptc;
import com.jeeplus.modules.ptc.mapper.PtcMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.IdGen;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.modules.productorder.entity.ProductOrder;
import com.jeeplus.modules.productorder.mapper.ProductOrderMapper;
import com.jeeplus.modules.productsku.entity.ProductSku;
import com.jeeplus.modules.productsku.mapper.ProductSkuMapper;
import com.jeeplus.modules.proxyorder.entity.ProxyOrder;
import com.jeeplus.modules.proxyorder.service.ProxyOrderService;
import com.jeeplus.modules.store.service.StoreService;
import com.jeeplus.modules.api.service.ApiService;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.modules.member.service.MemberService;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.msgexample.entity.MsgExample;
import com.jeeplus.modules.msgexample.service.MsgExampleService;
import com.jeeplus.modules.orderrefund.entity.OrderRefund;
import com.jeeplus.modules.orderrefund.mapper.OrderRefundMapper;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.productorder.entity.OrderItem;
import com.jeeplus.modules.productorder.mapper.OrderItemMapper;

/**
 * 商品订单Service
 *
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class ProductOrderService extends CrudService<ProductOrderMapper, ProductOrder> {

	@Autowired
	private OrderItemMapper orderItemMapper;
	@Autowired
	private ProxyOrderService proxyOrderService;
	@Autowired
	private OrderRefundMapper orderRefundMapper;
	@Autowired
	private PaaMapper paaMapper;
	@Autowired
	private PtcMapper ptcMapper;
	@Autowired
	private HkqyMapper hkqyMapper;
	@Autowired
	private ApiService apiService;
	@Autowired
	private StoreService storeService;
	@Autowired
	private MemberService memberService;
	@Autowired
	private MsgExampleService msgExampleService;
	@Autowired
	private MsgService msgService;

	public ProductOrder get(String id) {
		ProductOrder productOrder = super.get(id);
		if (productOrder != null) {
			productOrder.setOrderItemList(orderItemMapper.findList(new OrderItem(productOrder)));
		}
		return productOrder;
	}

	public List<ProductOrder> findList(ProductOrder productOrder) {
		return super.findList(productOrder);
	}

	public Page<ProductOrder> findPage(Page<ProductOrder> page, ProductOrder productOrder) {
		return super.findPage(page, productOrder);
	}

	@Transactional(readOnly = false)
	public void save(ProductOrder productOrder) {
		super.save(productOrder);
		for (OrderItem orderItem : productOrder.getOrderItemList()){
			if (orderItem.getId() == null){
				continue;
			}
			if (OrderItem.DEL_FLAG_NORMAL.equals(orderItem.getDelFlag())){
				if (StringUtils.isBlank(orderItem.getId())){
					orderItem.setOrder(productOrder);
					orderItem.preInsert();
					orderItemMapper.insert(orderItem);
				}else{
					orderItem.preUpdate();
					orderItemMapper.update(orderItem);
				}
			}else{
				orderItemMapper.delete(orderItem);
			}
		}
	}

	@Transactional(readOnly = false)
	public void delete(ProductOrder productOrder) {
		super.delete(productOrder);
		orderItemMapper.delete(new OrderItem(productOrder));
	}

	/**
	 * 生成订单
	 * @param orderList 订单列表
//	 * @param isProxy 是否代理
//	 * @param member 代理人
//	 * @param product 代理商品
//	 * @param totalId 大订单号
//	 * @param fee 代理费用
//	 * @param amount 订单总额
//	 * @param province 代理省份
//	 * @param city 代理城市
//	 * @param area 代理县区
	 */
	@Transactional(readOnly = false)
	public void insert(List<ProductOrder> orderList) {
		for (ProductOrder order: orderList) {
			save(order);

			for (OrderItem item : order.getOrderItemList()) {
				// 减库存
				orderItemMapper.execUpdateSql("UPDATE t_product_sku SET stock = stock - "+item.getQty()+" WHERE id = '"+item.getSkuId()+"'");
				// 加销量
				orderItemMapper.execUpdateSql("UPDATE t_product SET sales = sales + "+item.getQty()+" WHERE id = '"+item.getProductId()+"'");
			}
		}
    }

    /**
     * 大订单支付
     *
     * @param list
     * @param type         1支付宝 2微信
     * @param out_trade_no 支付单号
     */
    @Transactional(readOnly = false)
    public void pay(List<ProductOrder> list, String out_trade_no, String type,String trade_type) {
//        String isKyd = "0";//是否有砍一刀战区的订单
//		String POrderNO = "";
//		String uid = "";

		BigDecimal point = BigDecimal.ZERO;// 商品铜板
        for (ProductOrder order : list) {
			String Area ="";
			String Diff ="0";//差额
            List<OrderItem> itemList = orderItemMapper.findList(new OrderItem(order));
            if ("0".equals(order.getState())) {
                order.setState("1");
                order.setPayNo(out_trade_no);
                order.setTrade_type(trade_type);
                order.setPayType(type);
                order.setPayDate(new Date());
                super.save(order);
            }


            // 调用三方
            Member member = memberService.get(order.getMember());
            if (member != null) {
				if("1".equals(order.getIsHot())){
					point = point.add(new BigDecimal(order.getPoint()));//
					Area ="BPZQ";
//					isKyd = "1";
//					POrderNO =order.getTotalId();
//					uid =member.getId();
				}
				if("2".equals(itemList.get(0).getGoodstype())){
					Area ="HTHHZQ";//惠淘好货
				}else if("4".equals(itemList.get(0).getGoodstype())){
					Area ="HKYPZQ";//惠康优品专区
					Diff=itemList.get(0).getDiffprice();
				}else if("7".equals(itemList.get(0).getGoodstype())){
					Area ="CFMZQ";//云店铺专区
				}else if("6".equals(itemList.get(0).getGoodstype())){
					Area="C2FYXZQ";//C2F专区
				}
				String Price ="";
				Price = String.valueOf(new BigDecimal(order.getPrice()).add(new BigDecimal(order.getFreight())));
					JSONObject object = apiService.orderPay(String.valueOf(itemList.get(0).getQty()), order.getId(), member.getId(), order.getProvince(), order.getCity(), order.getMarea(), order.getStore().getStoreCode(), order.getAmount(), order.getPoint(), "0", DateUtils.getDateTime(), DateUtils.getDateTime(), "YD",  itemList.get(0).getProductId(), Area, "0", "0",itemList.get(0).getProductTitle(),Diff, Price, order.getXfqprice(), order.getZsqprice());
					if (!"0000".equals(object.getString("respCode"))) {
						logger.error("我惠会员系统订单(大订单)支付失败：支付类型" + type + out_trade_no+"单号" + order.getId() + order.getProvince() + object.getString("respMsg"));
					}
				if("4".equals(itemList.get(0).getGoodstype()) && (new BigDecimal(order.getAmount()).divide(new BigDecimal(itemList.get(0).getQty()))).compareTo(new BigDecimal("1999"))  >= 0){//惠康优品专区且订单金额大于1999
					// 调用三方
					List<WohuiEntity> listhk = Lists.newArrayList();
					listhk.add(new WohuiEntity("GoodsID", itemList.get(0).getProductId()));
					JSONObject objecthk = WohuiUtils.send(WohuiUtils.GetHKYPConfigByGoodsID, listhk);
					if ("0000".equals(objecthk.getString("respCode"))) {
						JSONArray datahk = objecthk.getJSONArray("data");
						JSONObject objhk = datahk.getJSONObject(0);
						String orderId ="PTC" + IdGen.getOrderNo();//产品周转中心
						Ptc o = new Ptc();
						o.setId(orderId);
						o.setOrderNo(order.getId());
						o.setGoodsnum(String.valueOf(itemList.get(0).getQty()));
						o.setCreateDate(new Date());
						BigDecimal Money = new BigDecimal(0.5).multiply(new BigDecimal(itemList.get(0).getQty()));
						o.setMoney(String.valueOf(Money));
						o.setMember(member);
						o.setState("0");
						o.setType(objhk.getString("HC_PTCType"));
						o.setTitle(objhk.getString("HC_PTCName"));
						ptcMapper.insert(o);
					}else{
						logger.error("产品周转中心信息接口获取失败：" +itemList.get(0).getProductId()+ objecthk.getString("respMsg"));
					}
				}
				//49d05dc86a4e47ecb5e8502743939c2e 唐宁泰苦瓜玉竹压片糖果56盒（28盒购买+28盒签到)10片/盒
				// 0b260597c8284451aabe73fe7b91d473  龙鼎鹿鞭海参牡蛎片32盒（16盒购买+16盒签到）10片/盒
				//06eef3d1edb649dab90c7e0b36b4dd02 	葛冬片48盒（24盒购买+24盒签到）10片/盒

				if("4".equals(itemList.get(0).getGoodstype()) && ("49d05dc86a4e47ecb5e8502743939c2e".equals(itemList.get(0).getProductId())
						|| "0b260597c8284451aabe73fe7b91d473".equals(itemList.get(0).getProductId())
						|| "06eef3d1edb649dab90c7e0b36b4dd02".equals(itemList.get(0).getProductId())
				)){//惠康优品专区
						String orderId ="HKQY" + IdGen.getOrderNo();//
						Hkqy o = new Hkqy();
						o.setId(orderId);
						o.setOrderNo(order.getId());
						o.setGoodsnum(String.valueOf(itemList.get(0).getQty()));
						o.setCreateDate(new Date());
						o.setMoney("0");
						o.setMember(member);
						o.setState("0");
						if("0b260597c8284451aabe73fe7b91d473".equals(itemList.get(0).getProductId())){//21
							o.setType("21");//
							o.setTitle("龙鼎");
						}else if("49d05dc86a4e47ecb5e8502743939c2e".equals(itemList.get(0).getProductId())){
							o.setType("10");//
							o.setTitle("鬼谷回春堂");
						}else if("06eef3d1edb649dab90c7e0b36b4dd02".equals(itemList.get(0).getProductId())){
							o.setType("20");//
							o.setTitle("葛冬片");
						}
							o.setTzType("11");//体验订单
						hkqyMapper.insert(o);
				}
				if("2d843735f8d54ad7a3a2f66f38036a65".equals(itemList.get(0).getProductId())){//平台区域
					String orderId ="PAA" + IdGen.getOrderNo();//平台区域
					Paa o = new Paa();
					o.setId(orderId);
					o.setOrderNo(order.getId());
					o.setGoodsnum(String.valueOf(itemList.get(0).getQty()));
					o.setCreateDate(new Date());
					o.setMoney(String.valueOf(order.getAmount()));
					o.setMember(member);
					o.setState("0");
					o.setType("1");
					paaMapper.insert(o);
				}
                // 代金券支付
                if (StringUtils.isNotBlank(order.getDiscount())) {
                    List<WohuiEntity> entityList = Lists.newArrayList();
                    entityList.add(new WohuiEntity("CardGUID", member.getId()));
                    entityList.add(new WohuiEntity("DJQ", order.getDiscount()));
                    entityList.add(new WohuiEntity("Source", "YD"));
                    entityList.add(new WohuiEntity("Remark", ""));
                    JSONObject result = WohuiUtils.send(WohuiUtils.DJQPay, entityList);
                    if (!"0000".equals(result.getString("respCode"))) {
                        logger.error("我惠会员系统代金券支付失败（代金券）：" +order.getId()+ result.getString("respMsg"));
                    }
                }
            }
        }
        // 发消息
        MsgExample example = msgExampleService.getByType("9");
        if (example != null) {
            msgService.insert(list.get(0).getMember(), "系统消息", example.getContent());
        }
	}

	/**
	 * 普通订单支付

	 */
	@Transactional(readOnly = false)
	public void pay(ProductOrder productOrder) {
		super.save(productOrder);
		String Area ="";
		String Diff ="0";//差额
		if("1".equals(productOrder.getIsHot())){
			Area ="BPZQ";//爆品专区
		}
		List<OrderItem> itemList = orderItemMapper.findList(new OrderItem(productOrder));
		if("2".equals(itemList.get(0).getGoodstype())){
			Area ="HTHHZQ";//惠淘好货
		}else if("4".equals(itemList.get(0).getGoodstype())){
			Area ="HKYPZQ";//惠康优品专区
			Diff=itemList.get(0).getDiffprice();
		}else if("7".equals(itemList.get(0).getGoodstype())){
			Area ="CFMZQ";//云店铺专区
		}else if("6".equals(itemList.get(0).getGoodstype())){
			Area ="C2FYXZQ";//C2F专区
		}
		String Price ="";
		Price = String.valueOf(new BigDecimal(productOrder.getPrice()).add(new BigDecimal(productOrder.getFreight())));
		// 调用三方
		Member member = memberService.get(productOrder.getMember());
		if (member != null) {
			JSONObject object = apiService.orderPay(String.valueOf(itemList.get(0).getQty()), productOrder.getId(), member.getId(), productOrder.getProvince(), productOrder.getCity(), productOrder.getMarea(), productOrder.getStore().getStoreCode(), productOrder.getAmount(), productOrder.getPoint(),"0", DateUtils.getDateTime(), DateUtils.getDateTime(),  "YD",  itemList.get(0).getProductId(),Area,"0","0",itemList.get(0).getProductTitle(),Diff,Price, productOrder.getXfqprice(), productOrder.getZsqprice());
			if (!"0000".equals(object.getString("respCode"))) {
				logger.error("我惠会员系统订单(普通订单)支付失败：" +productOrder.getId()+object.getString("respMsg"));
			}
			if("4".equals(itemList.get(0).getGoodstype()) && (new BigDecimal(productOrder.getAmount()).divide(new BigDecimal(itemList.get(0).getQty()))).compareTo(new BigDecimal("1999"))  >= 0){//惠康优品专区且订单金额大于1999
				// 调用三方
				List<WohuiEntity> list = Lists.newArrayList();
				list.add(new WohuiEntity("GoodsID", itemList.get(0).getProductId()));
				JSONObject objecthk = WohuiUtils.send(WohuiUtils.GetHKYPConfigByGoodsID, list);
				if ("0000".equals(objecthk.getString("respCode"))) {
					JSONArray datahk = objecthk.getJSONArray("data");
					JSONObject objhk = datahk.getJSONObject(0);
					String orderId ="PTC" + IdGen.getOrderNo();//产品周转中心
					Ptc o = new Ptc();
					o.setId(orderId);
					o.setOrderNo(productOrder.getId());
					o.setGoodsnum(String.valueOf(itemList.get(0).getQty()));
					o.setCreateDate(new Date());
					BigDecimal Money = new BigDecimal(0.5).multiply(new BigDecimal(itemList.get(0).getQty()));
					o.setMoney(String.valueOf(Money));
					o.setMember(member);
					o.setState("0");
					o.setType(objhk.getString("HC_PTCType"));
					o.setTitle(objhk.getString("HC_PTCName"));
					ptcMapper.insert(o);
				}else{
					logger.error("产品周转中心信息接口获取失败：" +itemList.get(0).getProductId()+ objecthk.getString("respMsg"));
				}

			}
			//5ae6ae2ca4d141e9819493d52da24052 买五送十龙鼎鹿鞭海参牡蛎片10片/盒 fb5b4a80f8654f8890951ccb5bf539c8  买五送十盒葛冬片20片/盒团购价
			if("4".equals(itemList.get(0).getGoodstype()) &&("49d05dc86a4e47ecb5e8502743939c2e".equals(itemList.get(0).getProductId())
					|| "0b260597c8284451aabe73fe7b91d473".equals(itemList.get(0).getProductId())
					|| "06eef3d1edb649dab90c7e0b36b4dd02".equals(itemList.get(0).getProductId())
			)){//
					String orderId ="HKQY" + IdGen.getOrderNo();//惠康区域
					Hkqy o = new Hkqy();
					o.setId(orderId);
					o.setOrderNo(productOrder.getId());
					o.setGoodsnum(String.valueOf(itemList.get(0).getQty()));
					o.setCreateDate(new Date());
					o.setMoney("0");
					o.setMember(member);
					o.setState("0");
					if("0b260597c8284451aabe73fe7b91d473".equals(itemList.get(0).getProductId())){//21
						o.setType("21");//
						o.setTitle("龙鼎");
					}else if("49d05dc86a4e47ecb5e8502743939c2e".equals(itemList.get(0).getProductId())){
						o.setType("10");//
						o.setTitle("鬼谷回春堂");
					}else if("06eef3d1edb649dab90c7e0b36b4dd02".equals(itemList.get(0).getProductId())){
						o.setType("20");//
						o.setTitle("葛冬片");
					}
					o.setTzType("11");//体验订单
					hkqyMapper.insert(o);
			}
			if("2d843735f8d54ad7a3a2f66f38036a65".equals(itemList.get(0).getProductId())){//平台区域
				String orderId ="PAA" + IdGen.getOrderNo();//平台区域
				Paa o = new Paa();
				o.setId(orderId);
				o.setOrderNo(productOrder.getId());
				o.setGoodsnum(String.valueOf(itemList.get(0).getQty()));
				o.setCreateDate(new Date());
				o.setMoney(String.valueOf(productOrder.getAmount()));
				o.setMember(member);
				o.setState("0");
				o.setType("1");
				paaMapper.insert(o);
			}

			// 代金券支付
			if (StringUtils.isNotBlank(productOrder.getDiscount())) {
				List<WohuiEntity> entityList = Lists.newArrayList();
				entityList.add(new WohuiEntity("CardGUID", member.getId()));
				entityList.add(new WohuiEntity("DJQ", productOrder.getDiscount()));
				entityList.add(new WohuiEntity("Source", "YD"));
				entityList.add(new WohuiEntity("Remark", ""));
				JSONObject result = WohuiUtils.send(WohuiUtils.DJQPay, entityList);
				if (!"0000".equals(result.getString("respCode"))) {
					logger.error("我惠会员系统代金券支付失败：" + result.getString("respMsg"));
				}
			}
		}

		// 发消息
		MsgExample example = msgExampleService.getByType("9");
		if (example != null) {
			msgService.insert(productOrder.getMember(), "系统消息", example.getContent());
		}
    }

    /**
     * 退款处理
     *
     * @param order
     * @param type  1同意 2拒绝
     */
    @Transactional(readOnly = false)
    public void refund(ProductOrder order, String type) {
        // 查询退款申请列表
        OrderRefund orderRefund = new OrderRefund();
        orderRefund.setOrderId(order.getId());
        orderRefund.setState("0");
        List<OrderRefund> list = orderRefundMapper.findList(orderRefund);
        for (OrderRefund refund : list) {
            if ("1".equals(type)) {// 同意
                refund.setState("1");
            } else {// 拒绝
                refund.setState("2");
            }
            refund.setAuditDate(new Date());
            orderRefundMapper.update(refund);
        }
        String amount = "0";// 退款金额
        // 修改订单状态
        if ("1".equals(type)) {// 同意
            order.setRefundDate(new Date());
            order.setState("7");
            amount = list.get(0).getAmount();
        } else {// 拒绝
            order.setState(list.get(0).getOrderState());
        }
        mapper.update(order);

        // 退款
        if ("1".equals(type)) {// 同意
            // 给用户退款
            String totalFee = mapper.executeGetSql("SELECT IFNULL(SUM(amount),0) FROM t_product_order WHERE pay_no = '" + order.getPayNo() + "'").toString();
            apiService.refund(order.getPayNo(), amount, totalFee, order.getPayType(),order.getTrade_type());
            // 给商家结算
            BigDecimal money = new BigDecimal(order.getMoney()).subtract(new BigDecimal(amount));

			if("1".equals(order.getOrdertype())//使用消费券
			){
				money = money.subtract(new BigDecimal(order.getXfqprice()));
			}
			if("1".equals(order.getZsqtype())//使用专属券
			){
				money = money.subtract(new BigDecimal(order.getZsqprice()));
			}
            if (money.compareTo(BigDecimal.ZERO) < 0) {
                money = BigDecimal.ZERO;
            }
			
            storeService.updateBalance(order.getStore(), "订单" + order.getId(), money.toString(), "1", order.getId(), order.getFreight(), order.getUsername(), order.getPhone(), new Date());

            // 调用三方失效订单
            JSONObject object = apiService.orderInvalid(order.getId());
            if (!"0000".equals(object.getString("respCode"))) {
                logger.error("我惠会员系统请求失效订单失败：" + object.getString("respMsg"));
            }
		}

	}

	/**
	 * 收货
	 * @param productOrder
	 */
	@Transactional(readOnly = false)
	public void finish(ProductOrder productOrder) {
		super.save(productOrder);
		storeService.updateBalance(productOrder.getStore(), "订单" + productOrder.getId(), productOrder.getMoney(), "1", productOrder.getId(), productOrder.getFreight(), productOrder.getUsername(), productOrder.getPhone(), new Date());


		// 调用三方会员系统
		JSONObject object = apiService.orderSettle(productOrder.getId(), productOrder.getAmount(), productOrder.getPoint());
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
		}
		// 发消息
		MsgExample example = msgExampleService.getByType("11");
		if (example != null) {
			msgService.insert(productOrder.getMember(), "系统消息", example.getContent());
		}
	}

	/**
	 * 取消订单
	 * @param productOrder
	 */
	@Transactional(readOnly = false)
	public void cancel(ProductOrder productOrder) {
		super.save(productOrder);

		List<OrderItem> itemList = orderItemMapper.findList(new OrderItem(productOrder));
		for (OrderItem item : itemList) {
			// 加库存
			orderItemMapper.execUpdateSql("UPDATE t_product_sku SET stock = stock + "+item.getQty()+" WHERE id = '"+item.getSkuId()+"'");
			// 减销量
			orderItemMapper.execUpdateSql("UPDATE t_product SET sales = sales - "+item.getQty()+" WHERE id = '"+item.getProductId()+"'");
		}
		if (productOrder != null) {
			if("1".equals(productOrder.getOrdertype())) {
				List<WohuiEntity> entityList = Lists.newArrayList();
				entityList.add(new WohuiEntity("OrderNO", productOrder.getTotalId()));
				JSONObject result = WohuiUtils.send(WohuiUtils.XFQRefund, entityList);
				if (!"0000".equals(result.getString("respCode"))) {
					logger.error("我惠会员系统消费券取消订单失败总单号：" + productOrder.getTotalId()+ result.getString("respMsg"));
				}
			}
			if("1".equals(productOrder.getZsqtype())) {
				List<WohuiEntity> entityList = Lists.newArrayList();
				entityList.add(new WohuiEntity("OrderNO", productOrder.getTotalId()));
				JSONObject result = WohuiUtils.send(WohuiUtils.ZSQRefund, entityList);
				if (!"0000".equals(result.getString("respCode"))) {
					logger.error("我惠会员系统专属券取消订单失败总单号：" + productOrder.getTotalId()+ result.getString("respMsg"));
				}
			}
		}
	}
	/**
	 * 修改物流
	 * @param productOrder
	 */
	@Transactional(readOnly = false)
	public void saveSend(ProductOrder productOrder) {
		super.save(productOrder);
	}
	/**
	 * 发货
	 * @param productOrder
	 */
	@Transactional(readOnly = false)
	public void send(ProductOrder productOrder) {
		super.save(productOrder);

		// 发消息
		MsgExample example = msgExampleService.getByType("10");
		if (example != null) {
			msgService.insert(productOrder.getMember(), "系统消息", example.getContent());
		}
	}
	/**
	 * 预售
	 */
	@Transactional(readOnly = false)
	public void saveYStask(ProductOrder productOrder) {
		super.save(productOrder);

	}

	/**
	 * 导出列表
	 * @param productOrder
	 * @return
	 */
	public List<ProductOrder> findListForExport(ProductOrder productOrder) {
		return mapper.findListForExport(productOrder);
	}

}