/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productorder.mapper;

import java.util.List;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.productorder.entity.ProductOrder;

/**
 * 商品订单MAPPER接口
 * @author lixinapp
 * @version 2019-10-16
 */
@MyBatisMapper
public interface ProductOrderMapper extends BaseMapper<ProductOrder> {

	List<ProductOrder> findListForExport(ProductOrder productOrder);
	
}