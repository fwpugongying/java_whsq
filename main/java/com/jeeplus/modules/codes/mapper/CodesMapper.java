/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.codes.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.codes.entity.Codes;

/**
 * 编码库MAPPER接口
 * @author lixinapp
 * @version 2019-10-16
 */
@MyBatisMapper
public interface CodesMapper extends BaseMapper<Codes> {
	
}