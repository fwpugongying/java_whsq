/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.codes.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.codes.entity.Codes;
import com.jeeplus.modules.codes.mapper.CodesMapper;

/**
 * 编码库Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class CodesService extends CrudService<CodesMapper, Codes> {

	public Codes get(String id) {
		return super.get(id);
	}
	
	public List<Codes> findList(Codes codes) {
		return super.findList(codes);
	}
	
	public Page<Codes> findPage(Page<Codes> page, Codes codes) {
		return super.findPage(page, codes);
	}
	
	@Transactional(readOnly = false)
	public void save(Codes codes) {
		super.save(codes);
	}
	
	@Transactional(readOnly = false)
	public void delete(Codes codes) {
		super.delete(codes);
	}
	
}