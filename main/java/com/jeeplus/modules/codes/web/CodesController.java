/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.codes.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.codes.entity.Codes;
import com.jeeplus.modules.codes.service.CodesService;

/**
 * 编码库Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/codes/codes")
public class CodesController extends BaseController {

	@Autowired
	private CodesService codesService;
	
	@ModelAttribute
	public Codes get(@RequestParam(required=false) String id) {
		Codes entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = codesService.get(id);
		}
		if (entity == null){
			entity = new Codes();
		}
		return entity;
	}
	
	/**
	 * 编码列表页面
	 */
	@RequiresPermissions("codes:codes:list")
	@RequestMapping(value = {"list", ""})
	public String list(Codes codes, Model model) {
		model.addAttribute("codes", codes);
		return "modules/codes/codesList";
	}
	
		/**
	 * 编码列表数据
	 */
	@ResponseBody
	@RequiresPermissions("codes:codes:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Codes codes, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Codes> page = codesService.findPage(new Page<Codes>(request, response), codes); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑编码表单页面
	 */
	@RequiresPermissions(value={"codes:codes:view","codes:codes:add","codes:codes:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Codes codes, Model model) {
		model.addAttribute("codes", codes);
		return "modules/codes/codesForm";
	}

	/**
	 * 保存编码
	 */
	@ResponseBody
	@RequiresPermissions(value={"codes:codes:add","codes:codes:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Codes codes, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(codes);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		codesService.save(codes);//保存
		j.setSuccess(true);
		j.setMsg("保存编码成功");
		return j;
	}
	
	/**
	 * 删除编码
	 */
	@ResponseBody
	@RequiresPermissions("codes:codes:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Codes codes) {
		AjaxJson j = new AjaxJson();
		codesService.delete(codes);
		j.setMsg("删除编码成功");
		return j;
	}
	
	/**
	 * 批量删除编码
	 */
	@ResponseBody
	@RequiresPermissions("codes:codes:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			codesService.delete(codesService.get(id));
		}
		j.setMsg("删除编码成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("codes:codes:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Codes codes, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "编码"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Codes> page = codesService.findPage(new Page<Codes>(request, response, -1), codes);
    		new ExportExcel("编码", Codes.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出编码记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("codes:codes:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Codes> list = ei.getDataList(Codes.class);
			for (Codes codes : list){
				try{
					codesService.save(codes);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条编码记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条编码记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入编码失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入编码数据模板
	 */
	@ResponseBody
	@RequiresPermissions("codes:codes:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "编码数据导入模板.xlsx";
    		List<Codes> list = Lists.newArrayList(); 
    		new ExportExcel("编码数据", Codes.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}