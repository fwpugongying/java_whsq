/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.codes.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 编码库Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Codes extends DataEntity<Codes> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 编码
	private String title;		// 名称
	private String type;		// 类型
	private String state;		// 状态 0正常 1停用
	
	public Codes() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Codes(String id){
		super(id);
	}

	@ExcelField(title="编码", align=2, sort=1)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="名称", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="类型", dictType="codes_type", align=2, sort=3)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="状态 0正常 1停用", dictType="codes_state", align=2, sort=4)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}