/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouppingou.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.product.entity.Product;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品拼团Entity
 * @author lixinapp
 * @version 2019-10-26
 */
public class GroupPingou extends DataEntity<GroupPingou> {
	
	private static final long serialVersionUID = 1L;
	private Product product;		// 商品
	private String state;		// 状态 0进行中 1拼购成功
	private Date CreateDate;		// 开始时间
	private Date endDate;		// 成功时间
	private String pgNum;// 参总拼购人数
	private String ypgNum; // 已经拼购人数
	private String zjorderid;// 中奖订单id
	private String pgorders;// 所有拼购字订单id

	@Override
	public Date getCreateDate() {
		return CreateDate;
	}

	@Override
	public void setCreateDate(Date createDate) {
		CreateDate = createDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPgNum() {
		return pgNum;
	}

	public void setPgNum(String pgNum) {
		this.pgNum = pgNum;
	}

	public String getYpgNum() {
		return ypgNum;
	}

	public void setYpgNum(String ypgNum) {
		this.ypgNum = ypgNum;
	}

	public String getZjorderid() {
		return zjorderid;
	}

	public void setZjorderid(String zjorderid) {
		this.zjorderid = zjorderid;
	}

	public String getPgorders() {
		return pgorders;
	}

	public void setPgorders(String pgorders) {
		this.pgorders = pgorders;
	}



	public GroupPingou(){
		super();
	}
	public GroupPingou(String id){
		super(id);
	}

	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=2)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ExcelField(title="状态 0进行中 1拼购成功 ", align=2, sort=4)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}