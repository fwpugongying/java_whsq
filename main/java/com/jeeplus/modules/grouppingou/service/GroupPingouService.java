/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouppingou.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.grouppingou.entity.GroupPingou;
import com.jeeplus.modules.grouppingou.mapper.GroupPingouMapper;

/**
 * 商品拼团Service
 * @author lixinapp
 * @version 2019-10-26
 */
@Service
@Transactional(readOnly = true)
public class GroupPingouService extends CrudService<GroupPingouMapper, GroupPingou> {

	public GroupPingou get(String id) {
		return super.get(id);
	}
	
	public List<GroupPingou> findList(GroupPingou groupPingou) {
		return super.findList(groupPingou);
	}
	
	public Page<GroupPingou> findPage(Page<GroupPingou> page, GroupPingou groupPingou) {
		return super.findPage(page, groupPingou);
	}
	
	@Transactional(readOnly = false)
	public void save(GroupPingou groupPingou) {
		super.save(groupPingou);
	}
	
	@Transactional(readOnly = false)
	public void delete(GroupPingou groupPingou) {
		super.delete(groupPingou);
	}
	
}