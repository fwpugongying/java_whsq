/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouppingou.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.grouppingou.entity.GroupPingou;

/**
 * 商品拼团MAPPER接口
 * @author lixinapp
 * @version 2019-10-26
 */
@MyBatisMapper
public interface GroupPingouMapper extends BaseMapper<GroupPingou> {
	
}