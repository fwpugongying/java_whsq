/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxylog.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.proxylog.entity.ProxyLog;
import com.jeeplus.modules.proxylog.mapper.ProxyLogMapper;

/**
 * 代理信息Service
 * @author lixinapp
 * @version 2019-10-28
 */
@Service
@Transactional(readOnly = true)
public class ProxyLogService extends CrudService<ProxyLogMapper, ProxyLog> {

	public ProxyLog get(String id) {
		return super.get(id);
	}
	
	public List<ProxyLog> findList(ProxyLog proxyLog) {
		return super.findList(proxyLog);
	}
	
	public Page<ProxyLog> findPage(Page<ProxyLog> page, ProxyLog proxyLog) {
		return super.findPage(page, proxyLog);
	}
	
	@Transactional(readOnly = false)
	public void save(ProxyLog proxyLog) {
		super.save(proxyLog);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProxyLog proxyLog) {
		super.delete(proxyLog);
	}
	
}