/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxylog.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.proxylog.entity.ProxyLog;
import com.jeeplus.modules.proxylog.service.ProxyLogService;

/**
 * 代理信息Controller
 * @author lixinapp
 * @version 2019-10-28
 */
@Controller
@RequestMapping(value = "${adminPath}/proxylog/proxyLog")
public class ProxyLogController extends BaseController {

	@Autowired
	private ProxyLogService proxyLogService;
	
	@ModelAttribute
	public ProxyLog get(@RequestParam(required=false) String id) {
		ProxyLog entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = proxyLogService.get(id);
		}
		if (entity == null){
			entity = new ProxyLog();
		}
		return entity;
	}
	
	/**
	 * 代理信息列表页面
	 */
	@RequiresPermissions("proxylog:proxyLog:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProxyLog proxyLog, Model model) {
		model.addAttribute("proxyLog", proxyLog);
		return "modules/proxylog/proxyLogList";
	}
	
		/**
	 * 代理信息列表数据
	 */
	@ResponseBody
	@RequiresPermissions("proxylog:proxyLog:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProxyLog proxyLog, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProxyLog> page = proxyLogService.findPage(new Page<ProxyLog>(request, response), proxyLog); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑代理信息表单页面
	 */
	@RequiresPermissions(value={"proxylog:proxyLog:view","proxylog:proxyLog:add","proxylog:proxyLog:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProxyLog proxyLog, Model model) {
		model.addAttribute("proxyLog", proxyLog);
		return "modules/proxylog/proxyLogForm";
	}

	/**
	 * 保存代理信息
	 */
	@ResponseBody
	@RequiresPermissions(value={"proxylog:proxyLog:add","proxylog:proxyLog:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProxyLog proxyLog, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(proxyLog);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		proxyLogService.save(proxyLog);//保存
		j.setSuccess(true);
		j.setMsg("保存代理信息成功");
		return j;
	}
	
	/**
	 * 删除代理信息
	 */
	@ResponseBody
	@RequiresPermissions("proxylog:proxyLog:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProxyLog proxyLog) {
		AjaxJson j = new AjaxJson();
		proxyLogService.delete(proxyLog);
		j.setMsg("删除代理信息成功");
		return j;
	}
	
	/**
	 * 批量删除代理信息
	 */
	@ResponseBody
	@RequiresPermissions("proxylog:proxyLog:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			proxyLogService.delete(proxyLogService.get(id));
		}
		j.setMsg("删除代理信息成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("proxylog:proxyLog:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProxyLog proxyLog, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "代理信息"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProxyLog> page = proxyLogService.findPage(new Page<ProxyLog>(request, response, -1), proxyLog);
    		new ExportExcel("代理信息", ProxyLog.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出代理信息记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("proxylog:proxyLog:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProxyLog> list = ei.getDataList(ProxyLog.class);
			for (ProxyLog proxyLog : list){
				try{
					proxyLogService.save(proxyLog);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条代理信息记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条代理信息记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入代理信息失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入代理信息数据模板
	 */
	@ResponseBody
	@RequiresPermissions("proxylog:proxyLog:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "代理信息数据导入模板.xlsx";
    		List<ProxyLog> list = Lists.newArrayList(); 
    		new ExportExcel("代理信息数据", ProxyLog.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}