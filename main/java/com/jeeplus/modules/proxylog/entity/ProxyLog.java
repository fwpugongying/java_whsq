/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxylog.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 代理信息Entity
 * @author lixinapp
 * @version 2019-10-28
 */
public class ProxyLog extends DataEntity<ProxyLog> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String code;		// 代理编码
	private String productIds;		// 淘客商品
	private String type;		// 类型 0云店 1淘客 2京东 3拼多多
	private Date endDate;		// 代理截止时间
	private String state;		// 状态 0待完成 1代理中 2取消 3过期
	private String orderId;		// 订单号
	private String province;		// 省份
	private String city;		// 城市
	private String area;		// 区县
	private String category;		// 分类 0默认型 1成长型 2赠送型
	
	public ProxyLog() {
		super();
	}

	public ProxyLog(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="代理编码", align=2, sort=2)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="淘客商品", align=2, sort=3)
	public String getProductIds() {
		return productIds;
	}

	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}
	
	@ExcelField(title="类型 0云店 1淘客 2京东 3拼多多", dictType="proxy_type", align=2, sort=4)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="代理截止时间", align=2, sort=6)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@ExcelField(title="状态 0待完成 1代理中 2取消 3过期", dictType="proxy_state", align=2, sort=7)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="订单号", align=2, sort=8)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	@ExcelField(title="省份", align=2, sort=9)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@ExcelField(title="城市", align=2, sort=10)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@ExcelField(title="区县", align=2, sort=11)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	@ExcelField(title="分类 0默认型 1成长型 2赠送型", dictType="proxy_category", align=2, sort=12)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
}