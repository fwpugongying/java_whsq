/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.ordertask.service;

import java.util.List;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.proxyorder.entity.ProxyOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.ordertask.entity.Ordertask;
import com.jeeplus.modules.ordertask.mapper.OrdertaskMapper;
/**
 * 任务状态Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class OrdertaskService extends CrudService<OrdertaskMapper, Ordertask> {


	public Ordertask get(String id) {
		return super.get(id);
	}

	public List<Ordertask> findList(Ordertask ordertask) {
		return super.findList(ordertask);
	}
	public List<Ordertask> findUniqueByProperty(Ordertask ordertask) {
		return super.findList(ordertask);
	}
	public Ordertask getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Ordertask> findPage(Page<Ordertask> page, Ordertask ordertask) {
		return super.findPage(page, ordertask);
	}
	@Transactional(readOnly = false)
	public void save(Ordertask ordertask) {
		super.save(ordertask);
	}

	@Transactional(readOnly = false)
	public void delete(Ordertask ordertask) {
		super.delete(ordertask);
	}


	@Transactional(readOnly = false)
	public void insert(Ordertask ordertask) {
		super.save(ordertask);
	}
	/**
	 * 订单取消，任务取消
	 */
	@Transactional(readOnly = false)
	public synchronized void updatetask(String id, String state) {
		mapper.execUpdateSql("UPDATE t_order_task SET state = '"+state+"' WHERE id = '"+id+"'");
	}
}