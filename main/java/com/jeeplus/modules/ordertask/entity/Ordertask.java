/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.ordertask.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import java.util.Date;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 任务状态Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Ordertask extends DataEntity<Ordertask> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String orderno;		// 订单号
	private String productid;		// 商品id
	private String num;		// 任务次数
	private Date createdate;		// 申请时间
	private Date finishdate;		// 完成时间
	private Date expiredate;		// 超时时间
	private Date canceldate;		// 取消时间
	private String state;		// 状态任务状态0待完成 1已完成 2已使用 3已失效
	private String clickguid;		// 点击人
	private String clickicon;		// 点击人头像
	private String clickname;		// 点击人昵称

	public Ordertask() {
		super();
	}

	public Ordertask(String id){
		super(id);
	}

	@ExcelField(title="点击人", align=2, sort=3)
	public String getClickguid() {
		return clickguid;
	}

	public void setClickguid(String clickguid) {
		this.clickguid = clickguid;
	}

	@ExcelField(title="点击人", align=2, sort=3)
	public String getClickicon() {
		return clickicon;
	}

	public void setClickicon(String clickicon) {
		this.clickicon = clickicon;
	}

	@ExcelField(title="点击人", align=2, sort=3)
	public String getClickname() {
		return clickname;
	}

	public void setClickname(String clickname) {
		this.clickname = clickname;
	}

	@ExcelField(title="商品id", align=2, sort=1)
	public String getProductid() {
		return productid;
	}
	public void setProductid(String productid) {
		this.productid = productid;
	}

	@ExcelField(title="订单号", align=2, sort=1)
	public String getOrderno() {
		return orderno;
	}

	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}

	@ExcelField(title="ID号", align=2, sort=1)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}


	@ExcelField(title="", align=2, sort=2)
	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	@ExcelField(title="申请时间", align=2, sort=4)
	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	@ExcelField(title="完成时间", align=2, sort=7)
	public Date getFinishdate() {
		return finishdate;
	}
	public void setFinishdate(Date finishdate) {
		this.finishdate = finishdate;
	}
	@ExcelField(title="超时时间", align=2, sort=7)
	public Date getExpiredate() {
		return expiredate;
	}
	public void setExpiredate(Date expiredate) {
		this.expiredate = expiredate;
	}
	@ExcelField(title="取消时间", align=2, sort=8)
	public Date getCanceldate() {
		return canceldate;
	}
	public void setCanceldate(Date canceldate) {
		this.canceldate = canceldate;
	}
	@ExcelField(title="任务状态0待完成 1已完成 2已使用 3已失效", dictType="member_state", align=2, sort=18)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}