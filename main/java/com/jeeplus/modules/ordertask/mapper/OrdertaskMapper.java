/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.ordertask.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.ordertask.entity.Ordertask;

/**
 * 任务状态接口
 * @author lixinapp
 * @version 2019-10-16
 */
@MyBatisMapper
public interface OrdertaskMapper extends BaseMapper<Ordertask> {


}