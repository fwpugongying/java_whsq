/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopbusorder.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jeeplus.modules.api.utils.WxPayUtils;
import com.jeeplus.modules.shop.entity.Shop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.api.service.ApiService;
import com.jeeplus.modules.shopbusorder.entity.ShopbusOrder;
import com.jeeplus.modules.shopbusorder.mapper.ShopbusOrderMapper;
import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.modules.member.service.MemberService;
import com.jeeplus.modules.shop.service.ShopService;

/**
 * 实体店优惠券订单Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class ShopbusOrderService extends CrudService<ShopbusOrderMapper, ShopbusOrder> {
	@Autowired
	private ApiService apiService;
	@Autowired
	private ShopService shopService;
	@Autowired
	private MemberService memberService;
	
	public ShopbusOrder get(String id) {
		return super.get(id);
	}
	
	public List<ShopbusOrder> findList(ShopbusOrder shopbusorder) {
		return super.findList(shopbusorder);
	}
	
	public Page<ShopbusOrder> findPage(Page<ShopbusOrder> page, ShopbusOrder shopbusorder) {
		return super.findPage(page, shopbusorder);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopbusOrder shopbusorder) {
		super.save(shopbusorder);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopbusOrder shopbusorder) {
		super.delete(shopbusorder);
	}

	/**
	 * 支付处理
	 * @param shopbusorder
	 */
	@Transactional(readOnly = false)
	public void pay(ShopbusOrder shopbusorder) throws Exception {
		super.save(shopbusorder);

		Shop shop= shopService.get(shopbusorder.getShop().getId());
		// 调用三方
		Member member = memberService.get(shop.getMember().getId());
		Member member2 = memberService.get(shopbusorder.getMember());
		if (member != null) {
			if("2".equals(shopbusorder.getState())){
				String PTFL = new BigDecimal(0.4).multiply(new BigDecimal(shopbusorder.getPtrefee())).setScale(4, BigDecimal.ROUND_DOWN).toString();
				JSONObject object = apiService.orderPay("1",shopbusorder.getId(), member2.getId(), member2.getProvince(), member2.getCity(), member2.getArea(), shopbusorder.getShop().getShopCode(), shopbusorder.getPrice(), PTFL, shopbusorder.getHyrefee(), DateUtils.getDateTime(), DateUtils.getDateTime(),  "YD_YHQ",  "", "", "0", "0","","0", shopbusorder.getPrice(),"0","0");
				if (!"0000".equals(object.getString("respCode"))) {
					logger.error("我惠会员商圈订单支付失败：" + object.getString("respMsg") + "状态：" + object.getString("respCode") + "单号：" + shopbusorder.getId());
				}
				if(member.getTxopenid() !=null){
					String openid =member.getTxopenid();
					BigDecimal Amount = new BigDecimal(100).multiply( new BigDecimal(shopbusorder.getAmount()));
					String payMoney = String.valueOf(Amount.setScale( 0, BigDecimal.ROUND_DOWN ).longValue());
					String orderId = shopbusorder.getId();
					String desc = "附近商圈商家提现";
					Map<String, String> body = WxPayUtils.wxqyPay(openid, payMoney, orderId, desc);
					String status = body.get("status");
					String msg = body.get("msg");
					if ("1".equals(status)) {
						logger.error("附近商圈商家提现失败：" + msg);
					}
					//状态改为已分账3
					mapper.execUpdateSql("UPDATE t_shop_order SET state = 3 WHERE id = '"+shopbusorder.getId()+"'");
					if(shopbusorder.getGiveupfee()!=null){
						shopService.updateBalance(shop, "订单" + orderId, shopbusorder.getGiveupfee(), "1", orderId, "附近商圈未结金额", member.getNickname(), member.getPhone(), new Date());
					}
				}
				JSONObject object2 = apiService.orderSettle(shopbusorder.getId(), shopbusorder.getPrice(), PTFL);
				if (!"0000".equals(object2.getString("respCode"))) {
					logger.error("我惠会员系统商圈请求结算订单失败：" + object2.getString("respMsg"));
				}
			}
		}
	}

	/**
	 * 退款
	 * @param
	 */
	@Transactional(readOnly = false)
	public void refund(ShopbusOrder shopbusorder) {
		super.save(shopbusorder);
		
		// 调用退款
		apiService.refund(shopbusorder.getPayNo(), shopbusorder.getPrice(), shopbusorder.getPrice(), shopbusorder.getPayType(),shopbusorder.getTrade_type());
		
		// 调用三方失效订单
		JSONObject object = apiService.orderInvalid(shopbusorder.getId());
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("我惠会员系统请求失效订单失败：" + object.getString("respMsg"));
		}
	}
	
}