/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopbusorder.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.shopbusorder.entity.ShopbusOrder;
import com.jeeplus.modules.shopbusorder.service.ShopbusOrderService;

/**
 * 实体店优惠券订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/shopbusorder/shopbusorder")
public class ShopbusOrderController extends BaseController {

	@Autowired
	private ShopbusOrderService shopbusorderService;
	
	@ModelAttribute
	public ShopbusOrder get(@RequestParam(required=false) String id) {
		ShopbusOrder entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopbusorderService.get(id);
		}
		if (entity == null){
			entity = new ShopbusOrder();
		}
		return entity;
	}
	
	/**
	 * 实体店优惠券订单列表页面
	 */
	@RequiresPermissions("shopbusorder:shopbusorder:list")
	@RequestMapping(value = {"list", ""})
	public String list(ShopbusOrder shopbusorder, Model model) {
		model.addAttribute("shopbusorder", shopbusorder);
		return "modules/shopbusorder/shopbusorderList";
	}
	
		/**
	 * 实体店优惠券订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("shopbusorder:shopbusorder:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ShopbusOrder shopbusorder, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopbusOrder> page = shopbusorderService.findPage(new Page<ShopbusOrder>(request, response), shopbusorder); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑实体店优惠券订单表单页面
	 */
	@RequiresPermissions(value={"shopbusorder:shopbusorder:view","shopbusorder:shopbusorder:add","shopbusorder:shopbusorder:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ShopbusOrder shopbusorder, Model model) {
		model.addAttribute("shopbusorder", shopbusorder);
		return "modules/shopbusorder/shopbusorderForm";
	}

	/**
	 * 保存实体店优惠券订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"shopbusorder:shopbusorder:add","shopbusorder:shopbusorder:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ShopbusOrder shopbusorder, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(shopbusorder);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		shopbusorderService.save(shopbusorder);//保存
		j.setSuccess(true);
		j.setMsg("保存实体店优惠券订单成功");
		return j;
	}
	
	/**
	 * 删除实体店优惠券订单
	 */
	@ResponseBody
	@RequiresPermissions("shopbusorder:shopbusorder:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ShopbusOrder shopbusorder) {
		AjaxJson j = new AjaxJson();
		shopbusorderService.delete(shopbusorder);
		j.setMsg("删除实体店优惠券订单成功");
		return j;
	}
	
	/**
	 * 批量删除实体店优惠券订单
	 */
	@ResponseBody
	@RequiresPermissions("shopbusorder:shopbusorder:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			shopbusorderService.delete(shopbusorderService.get(id));
		}
		j.setMsg("删除实体店优惠券订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("shopbusorder:shopbusorder:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ShopbusOrder shopbusorder, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店优惠券订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ShopbusOrder> page = shopbusorderService.findPage(new Page<ShopbusOrder>(request, response, -1), shopbusorder);
    		new ExportExcel("实体店优惠券订单", ShopbusOrder.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出实体店优惠券订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("shopbusorder:shopbusorder:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ShopbusOrder> list = ei.getDataList(ShopbusOrder.class);
			for (ShopbusOrder shopbusorder : list){
				try{
					shopbusorderService.save(shopbusorder);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条实体店优惠券订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条实体店优惠券订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入实体店优惠券订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入实体店优惠券订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("shopbusorder:shopbusorder:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店优惠券订单数据导入模板.xlsx";
    		List<ShopbusOrder> list = Lists.newArrayList(); 
    		new ExportExcel("实体店优惠券订单数据", ShopbusOrder.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}