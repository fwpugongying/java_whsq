/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cart.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.cart.entity.Cart;
import com.jeeplus.modules.cart.service.CartService;

/**
 * 购物车Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/cart/cart")
public class CartController extends BaseController {

	@Autowired
	private CartService cartService;
	
	@ModelAttribute
	public Cart get(@RequestParam(required=false) String id) {
		Cart entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = cartService.get(id);
		}
		if (entity == null){
			entity = new Cart();
		}
		return entity;
	}
	
	/**
	 * 购物车列表页面
	 */
	@RequiresPermissions("cart:cart:list")
	@RequestMapping(value = {"list", ""})
	public String list(Cart cart, Model model) {
		model.addAttribute("cart", cart);
		return "modules/cart/cartList";
	}
	
		/**
	 * 购物车列表数据
	 */
	@ResponseBody
	@RequiresPermissions("cart:cart:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Cart cart, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Cart> page = cartService.findPage(new Page<Cart>(request, response), cart); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑购物车表单页面
	 */
	@RequiresPermissions(value={"cart:cart:view","cart:cart:add","cart:cart:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Cart cart, Model model) {
		model.addAttribute("cart", cart);
		return "modules/cart/cartForm";
	}

	/**
	 * 保存购物车
	 */
	@ResponseBody
	@RequiresPermissions(value={"cart:cart:add","cart:cart:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Cart cart, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(cart);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		cartService.save(cart);//保存
		j.setSuccess(true);
		j.setMsg("保存购物车成功");
		return j;
	}
	
	/**
	 * 删除购物车
	 */
	@ResponseBody
	@RequiresPermissions("cart:cart:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Cart cart) {
		AjaxJson j = new AjaxJson();
		cartService.delete(cart);
		j.setMsg("删除购物车成功");
		return j;
	}
	
	/**
	 * 批量删除购物车
	 */
	@ResponseBody
	@RequiresPermissions("cart:cart:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			cartService.delete(cartService.get(id));
		}
		j.setMsg("删除购物车成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("cart:cart:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Cart cart, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "购物车"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Cart> page = cartService.findPage(new Page<Cart>(request, response, -1), cart);
    		new ExportExcel("购物车", Cart.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出购物车记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("cart:cart:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Cart> list = ei.getDataList(Cart.class);
			for (Cart cart : list){
				try{
					cartService.save(cart);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条购物车记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条购物车记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入购物车失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入购物车数据模板
	 */
	@ResponseBody
	@RequiresPermissions("cart:cart:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "购物车数据导入模板.xlsx";
    		List<Cart> list = Lists.newArrayList(); 
    		new ExportExcel("购物车数据", Cart.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}