/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cart.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.productsku.entity.ProductSku;
import com.jeeplus.modules.store.entity.Store;
import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 购物车Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Cart extends DataEntity<Cart> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private Product product;		// 商品
	private Store store;		// 商家
	private ProductSku sku;		// 规格
	private Integer qty;		// 数量
	private String fenzu;		// 辅助字段-分组
	
	public Cart() {
		super();
	}

	public Cart(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=2)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ExcelField(title="规格", align=2, sort=3)
	public ProductSku getSku() {
		return sku;
	}

	public void setSku(ProductSku sku) {
		this.sku = sku;
	}
	
	@NotNull(message="数量不能为空")
	@ExcelField(title="数量", align=2, sort=4)
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public String getFenzu() {
		return fenzu;
	}

	public void setFenzu(String fenzu) {
		this.fenzu = fenzu;
	}
	
}