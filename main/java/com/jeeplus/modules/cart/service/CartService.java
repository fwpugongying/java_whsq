/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cart.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.cart.entity.Cart;
import com.jeeplus.modules.cart.mapper.CartMapper;

/**
 * 购物车Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class CartService extends CrudService<CartMapper, Cart> {

	public Cart get(String id) {
		return super.get(id);
	}
	
	public List<Cart> findList(Cart cart) {
		return super.findList(cart);
	}
	
	public Page<Cart> findPage(Page<Cart> page, Cart cart) {
		return super.findPage(page, cart);
	}
	
	@Transactional(readOnly = false)
	public void save(Cart cart) {
		super.save(cart);
	}
	
	@Transactional(readOnly = false)
	public void delete(Cart cart) {
		super.delete(cart);
	}
	
}