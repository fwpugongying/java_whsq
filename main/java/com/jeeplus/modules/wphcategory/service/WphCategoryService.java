/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.wphcategory.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.wphcategory.entity.WphCategory;
import com.jeeplus.modules.wphcategory.mapper.WphCategoryMapper;

/**
 * 唯品会分类Service
 * @author lixinapp
 * @version 2019-11-09
 */
@Service
@Transactional(readOnly = true)
public class WphCategoryService extends CrudService<WphCategoryMapper, WphCategory> {

	public WphCategory get(String id) {
		return super.get(id);
	}
	
	public List<WphCategory> findList(WphCategory wphCategory) {
		return super.findList(wphCategory);
	}
	
	public Page<WphCategory> findPage(Page<WphCategory> page, WphCategory wphCategory) {
		return super.findPage(page, wphCategory);
	}
	
	@Transactional(readOnly = false)
	public void save(WphCategory wphCategory) {
		super.save(wphCategory);
	}
	
	@Transactional(readOnly = false)
	public void delete(WphCategory wphCategory) {
		super.delete(wphCategory);
	}
	
}