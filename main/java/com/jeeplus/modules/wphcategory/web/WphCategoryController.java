/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.wphcategory.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.wphcategory.entity.WphCategory;
import com.jeeplus.modules.wphcategory.service.WphCategoryService;

/**
 * 唯品会分类Controller
 * @author lixinapp
 * @version 2019-11-09
 */
@Controller
@RequestMapping(value = "${adminPath}/wphcategory/wphCategory")
public class WphCategoryController extends BaseController {

	@Autowired
	private WphCategoryService wphCategoryService;
	
	@ModelAttribute
	public WphCategory get(@RequestParam(required=false) String id) {
		WphCategory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = wphCategoryService.get(id);
		}
		if (entity == null){
			entity = new WphCategory();
		}
		return entity;
	}
	
	/**
	 * 唯品会分类列表页面
	 */
	@RequiresPermissions("wphcategory:wphCategory:list")
	@RequestMapping(value = {"list", ""})
	public String list(WphCategory wphCategory, Model model) {
		model.addAttribute("wphCategory", wphCategory);
		return "modules/wphcategory/wphCategoryList";
	}
	
		/**
	 * 唯品会分类列表数据
	 */
	@ResponseBody
	@RequiresPermissions("wphcategory:wphCategory:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(WphCategory wphCategory, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<WphCategory> page = wphCategoryService.findPage(new Page<WphCategory>(request, response), wphCategory); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑唯品会分类表单页面
	 */
	@RequiresPermissions(value={"wphcategory:wphCategory:view","wphcategory:wphCategory:add","wphcategory:wphCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(WphCategory wphCategory, Model model) {
		model.addAttribute("wphCategory", wphCategory);
		return "modules/wphcategory/wphCategoryForm";
	}

	/**
	 * 保存唯品会分类
	 */
	@ResponseBody
	@RequiresPermissions(value={"wphcategory:wphCategory:add","wphcategory:wphCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(WphCategory wphCategory, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(wphCategory);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		wphCategoryService.save(wphCategory);//保存
		j.setSuccess(true);
		j.setMsg("保存唯品会分类成功");
		return j;
	}
	
	/**
	 * 删除唯品会分类
	 */
	@ResponseBody
	@RequiresPermissions("wphcategory:wphCategory:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(WphCategory wphCategory) {
		AjaxJson j = new AjaxJson();
		wphCategoryService.delete(wphCategory);
		j.setMsg("删除唯品会分类成功");
		return j;
	}
	
	/**
	 * 批量删除唯品会分类
	 */
	@ResponseBody
	@RequiresPermissions("wphcategory:wphCategory:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			wphCategoryService.delete(wphCategoryService.get(id));
		}
		j.setMsg("删除唯品会分类成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("wphcategory:wphCategory:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(WphCategory wphCategory, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "唯品会分类"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<WphCategory> page = wphCategoryService.findPage(new Page<WphCategory>(request, response, -1), wphCategory);
    		new ExportExcel("唯品会分类", WphCategory.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出唯品会分类记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("wphcategory:wphCategory:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<WphCategory> list = ei.getDataList(WphCategory.class);
			for (WphCategory wphCategory : list){
				try{
					wphCategoryService.save(wphCategory);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条唯品会分类记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条唯品会分类记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入唯品会分类失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入唯品会分类数据模板
	 */
	@ResponseBody
	@RequiresPermissions("wphcategory:wphCategory:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "唯品会分类数据导入模板.xlsx";
    		List<WphCategory> list = Lists.newArrayList(); 
    		new ExportExcel("唯品会分类数据", WphCategory.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}