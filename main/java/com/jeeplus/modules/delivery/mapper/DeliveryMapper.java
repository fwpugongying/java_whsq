/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.delivery.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.delivery.entity.Delivery;

/**
 * 物流公司MAPPER接口
 * @author lixinapp
 * @version 2019-11-06
 */
@MyBatisMapper
public interface DeliveryMapper extends BaseMapper<Delivery> {
	
}