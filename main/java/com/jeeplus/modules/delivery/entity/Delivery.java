/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.delivery.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 物流公司Entity
 * @author lixinapp
 * @version 2019-11-06
 */
public class Delivery extends DataEntity<Delivery> {
	
	private static final long serialVersionUID = 1L;
	private String corp;		// 物流公司
	private String code;		// 代码
	private String state;		// 状态 0正常 1禁用
	
	public Delivery() {
		super();
	}

	public Delivery(String id){
		super(id);
	}

	@ExcelField(title="物流公司", align=2, sort=1)
	public String getCorp() {
		return corp;
	}

	public void setCorp(String corp) {
		this.corp = corp;
	}
	
	@ExcelField(title="代码", align=2, sort=2)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="状态 0正常 1禁用", dictType="delivery_state", align=2, sort=3)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}