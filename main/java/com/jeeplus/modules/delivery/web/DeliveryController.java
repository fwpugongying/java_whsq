/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.delivery.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.delivery.entity.Delivery;
import com.jeeplus.modules.delivery.service.DeliveryService;

/**
 * 物流公司Controller
 * @author lixinapp
 * @version 2019-11-06
 */
@Controller
@RequestMapping(value = "${adminPath}/delivery/delivery")
public class DeliveryController extends BaseController {

	@Autowired
	private DeliveryService deliveryService;
	
	@ModelAttribute
	public Delivery get(@RequestParam(required=false) String id) {
		Delivery entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = deliveryService.get(id);
		}
		if (entity == null){
			entity = new Delivery();
		}
		return entity;
	}
	
	/**
	 * 物流公司列表页面
	 */
	@RequiresPermissions("delivery:delivery:list")
	@RequestMapping(value = {"list", ""})
	public String list(Delivery delivery, Model model) {
		model.addAttribute("delivery", delivery);
		return "modules/delivery/deliveryList";
	}
	
		/**
	 * 物流公司列表数据
	 */
	@ResponseBody
	@RequiresPermissions("delivery:delivery:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Delivery delivery, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Delivery> page = deliveryService.findPage(new Page<Delivery>(request, response), delivery); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑物流公司表单页面
	 */
	@RequiresPermissions(value={"delivery:delivery:view","delivery:delivery:add","delivery:delivery:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Delivery delivery, Model model) {
		model.addAttribute("delivery", delivery);
		return "modules/delivery/deliveryForm";
	}

	/**
	 * 保存物流公司
	 */
	@ResponseBody
	@RequiresPermissions(value={"delivery:delivery:add","delivery:delivery:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Delivery delivery, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(delivery);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		deliveryService.save(delivery);//保存
		j.setSuccess(true);
		j.setMsg("保存物流公司成功");
		return j;
	}
	
	/**
	 * 删除物流公司
	 */
	@ResponseBody
	@RequiresPermissions("delivery:delivery:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Delivery delivery) {
		AjaxJson j = new AjaxJson();
		deliveryService.delete(delivery);
		j.setMsg("删除物流公司成功");
		return j;
	}
	
	/**
	 * 批量删除物流公司
	 */
	@ResponseBody
	@RequiresPermissions("delivery:delivery:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			deliveryService.delete(deliveryService.get(id));
		}
		j.setMsg("删除物流公司成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("delivery:delivery:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Delivery delivery, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "物流公司"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Delivery> page = deliveryService.findPage(new Page<Delivery>(request, response, -1), delivery);
    		new ExportExcel("物流公司", Delivery.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出物流公司记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("delivery:delivery:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Delivery> list = ei.getDataList(Delivery.class);
			for (Delivery delivery : list){
				try{
					deliveryService.save(delivery);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条物流公司记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条物流公司记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入物流公司失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入物流公司数据模板
	 */
	@ResponseBody
	@RequiresPermissions("delivery:delivery:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "物流公司数据导入模板.xlsx";
    		List<Delivery> list = Lists.newArrayList(); 
    		new ExportExcel("物流公司数据", Delivery.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}