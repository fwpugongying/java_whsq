/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.delivery.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.delivery.entity.Delivery;
import com.jeeplus.modules.delivery.mapper.DeliveryMapper;

/**
 * 物流公司Service
 * @author lixinapp
 * @version 2019-11-06
 */
@Service
@Transactional(readOnly = true)
public class DeliveryService extends CrudService<DeliveryMapper, Delivery> {

	public Delivery get(String id) {
		return super.get(id);
	}
	
	public List<Delivery> findList(Delivery delivery) {
		return super.findList(delivery);
	}
	
	public Page<Delivery> findPage(Page<Delivery> page, Delivery delivery) {
		return super.findPage(page, delivery);
	}
	
	@Transactional(readOnly = false)
	public void save(Delivery delivery) {
		super.save(delivery);
	}
	
	@Transactional(readOnly = false)
	public void delete(Delivery delivery) {
		super.delete(delivery);
	}
	
}