/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.message.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 留言区Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Message extends DataEntity<Message> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String category;		// 商品分类
	private String brand;		// 品牌
	private String model;		// 型号
	private String property;		// 属性
	private String cycle;		// 周期
	private String minPrice;		// 预期最低价
	private String maxPrice;		// 预期最高价
	private Date chooseDate;		// 选择日期
	private String content;		// 留言内容
	private String province;		// 省份
	private String city;		// 城市
	private String area;		// 区县
	
	public Message() {
		super();
	}

	public Message(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="商品分类", align=2, sort=2)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@ExcelField(title="品牌", align=2, sort=3)
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	@ExcelField(title="型号", align=2, sort=4)
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	@ExcelField(title="属性", align=2, sort=5)
	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}
	
	@ExcelField(title="周期", align=2, sort=6)
	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	
	@ExcelField(title="预期最低价", align=2, sort=7)
	public String getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(String minPrice) {
		this.minPrice = minPrice;
	}
	
	@ExcelField(title="预期最高价", align=2, sort=8)
	public String getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(String maxPrice) {
		this.maxPrice = maxPrice;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="选择日期", align=2, sort=9)
	public Date getChooseDate() {
		return chooseDate;
	}

	public void setChooseDate(Date chooseDate) {
		this.chooseDate = chooseDate;
	}
	
	@ExcelField(title="留言内容", align=2, sort=10)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="省份", align=2, sort=11)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@ExcelField(title="城市", align=2, sort=12)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@ExcelField(title="区县", align=2, sort=13)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
}