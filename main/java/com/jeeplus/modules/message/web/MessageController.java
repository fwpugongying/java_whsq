/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.message.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.message.entity.Message;
import com.jeeplus.modules.message.service.MessageService;

/**
 * 留言区Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/message/message")
public class MessageController extends BaseController {

	@Autowired
	private MessageService messageService;
	
	@ModelAttribute
	public Message get(@RequestParam(required=false) String id) {
		Message entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = messageService.get(id);
		}
		if (entity == null){
			entity = new Message();
		}
		return entity;
	}
	
	/**
	 * 用户留言列表页面
	 */
	@RequiresPermissions("message:message:list")
	@RequestMapping(value = {"list", ""})
	public String list(Message message, Model model) {
		model.addAttribute("message", message);
		return "modules/message/messageList";
	}
	
		/**
	 * 用户留言列表数据
	 */
	@ResponseBody
	@RequiresPermissions("message:message:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Message message, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Message> page = messageService.findPage(new Page<Message>(request, response), message); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑用户留言表单页面
	 */
	@RequiresPermissions(value={"message:message:view","message:message:add","message:message:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Message message, Model model) {
		model.addAttribute("message", message);
		return "modules/message/messageForm";
	}

	/**
	 * 保存用户留言
	 */
	@ResponseBody
	@RequiresPermissions(value={"message:message:add","message:message:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Message message, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(message);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		messageService.save(message);//保存
		j.setSuccess(true);
		j.setMsg("保存用户留言成功");
		return j;
	}
	
	/**
	 * 删除用户留言
	 */
	@ResponseBody
	@RequiresPermissions("message:message:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Message message) {
		AjaxJson j = new AjaxJson();
		messageService.delete(message);
		j.setMsg("删除用户留言成功");
		return j;
	}
	
	/**
	 * 批量删除用户留言
	 */
	@ResponseBody
	@RequiresPermissions("message:message:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			messageService.delete(messageService.get(id));
		}
		j.setMsg("删除用户留言成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("message:message:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Message message, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "用户留言"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Message> page = messageService.findPage(new Page<Message>(request, response, -1), message);
    		new ExportExcel("用户留言", Message.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出用户留言记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("message:message:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Message> list = ei.getDataList(Message.class);
			for (Message message : list){
				try{
					messageService.save(message);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条用户留言记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条用户留言记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入用户留言失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入用户留言数据模板
	 */
	@ResponseBody
	@RequiresPermissions("message:message:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "用户留言数据导入模板.xlsx";
    		List<Message> list = Lists.newArrayList(); 
    		new ExportExcel("用户留言数据", Message.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}