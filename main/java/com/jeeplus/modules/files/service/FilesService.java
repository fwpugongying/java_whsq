/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.files.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.files.entity.Files;
import com.jeeplus.modules.files.mapper.FilesMapper;

/**
 * 上传文件Service
 * @author lixinapp
 * @version 2019-11-20
 */
@Service
@Transactional(readOnly = true)
public class FilesService extends CrudService<FilesMapper, Files> {

	public Files get(String id) {
		return super.get(id);
	}
	
	public List<Files> findList(Files files) {
		return super.findList(files);
	}
	
	public Page<Files> findPage(Page<Files> page, Files files) {
		return super.findPage(page, files);
	}
	
	@Transactional(readOnly = false)
	public void save(Files files) {
		super.save(files);
	}
	
	@Transactional(readOnly = false)
	public void delete(Files files) {
		super.delete(files);
	}
	
}