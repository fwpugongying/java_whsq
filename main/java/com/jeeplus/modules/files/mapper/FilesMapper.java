/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.files.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.files.entity.Files;

/**
 * 上传文件MAPPER接口
 * @author lixinapp
 * @version 2019-11-20
 */
@MyBatisMapper
public interface FilesMapper extends BaseMapper<Files> {
	
}