/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.files.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 上传文件Entity
 * @author lixinapp
 * @version 2019-11-20
 */
public class Files extends DataEntity<Files> {
	
	private static final long serialVersionUID = 1L;
	private String url;		// 文件路径
	
	public Files() {
		super();
	}

	public Files(String id){
		super(id);
	}

	@ExcelField(title="文件路径", align=2, sort=1)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}