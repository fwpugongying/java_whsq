/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.files.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.files.entity.Files;
import com.jeeplus.modules.files.service.FilesService;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;

/**
 * 上传文件Controller
 * @author lixinapp
 * @version 2019-11-20
 */
@Controller
@RequestMapping(value = "${adminPath}/files/files")
public class FilesController extends BaseController {

	@Autowired
	private FilesService filesService;
	
	@ModelAttribute
	public Files get(@RequestParam(required=false) String id) {
		Files entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = filesService.get(id);
		}
		if (entity == null){
			entity = new Files();
		}
		return entity;
	}
	
	/**
	 * 上传文件列表页面
	 */
	@RequiresPermissions("files:files:list")
	@RequestMapping(value = {"list", ""})
	public String list(Files files, Model model) {
		model.addAttribute("files", files);
		return "modules/files/filesList";
	}
	
		/**
	 * 上传文件列表数据
	 */
	@ResponseBody
	@RequiresPermissions("files:files:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Files files, HttpServletRequest request, HttpServletResponse response, Model model) {
		User user = UserUtils.getUser();
		if (user.isShop()) {
			files.setCreateBy(user);
		}
		Page<Files> page = filesService.findPage(new Page<Files>(request, response), files); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑上传文件表单页面
	 */
	@RequiresPermissions(value={"files:files:view","files:files:add","files:files:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Files files, Model model) {
		model.addAttribute("files", files);
		return "modules/files/filesForm";
	}

	/**
	 * 保存上传文件
	 */
	@ResponseBody
	@RequiresPermissions(value={"files:files:add","files:files:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Files files, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(files);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		filesService.save(files);//保存
		j.setSuccess(true);
		j.setMsg("保存上传文件成功");
		return j;
	}
	
	/**
	 * 删除上传文件
	 */
	@ResponseBody
	@RequiresPermissions("files:files:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Files files) {
		AjaxJson j = new AjaxJson();
		filesService.delete(files);
		j.setMsg("删除上传文件成功");
		return j;
	}
	
	/**
	 * 批量删除上传文件
	 */
	@ResponseBody
	@RequiresPermissions("files:files:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			filesService.delete(filesService.get(id));
		}
		j.setMsg("删除上传文件成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("files:files:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Files files, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "上传文件"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Files> page = filesService.findPage(new Page<Files>(request, response, -1), files);
    		new ExportExcel("上传文件", Files.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出上传文件记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("files:files:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Files> list = ei.getDataList(Files.class);
			for (Files files : list){
				try{
					filesService.save(files);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条上传文件记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条上传文件记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入上传文件失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入上传文件数据模板
	 */
	@ResponseBody
	@RequiresPermissions("files:files:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "上传文件数据导入模板.xlsx";
    		List<Files> list = Lists.newArrayList(); 
    		new ExportExcel("上传文件数据", Files.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}