/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.specialproduct.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.specialproduct.entity.SpecialProduct;
import com.jeeplus.modules.specialproduct.mapper.SpecialProductMapper;

/**
 * 云店特色商品Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class SpecialProductService extends CrudService<SpecialProductMapper, SpecialProduct> {

	public SpecialProduct get(String id) {
		return super.get(id);
	}
	
	public List<SpecialProduct> findList(SpecialProduct specialProduct) {
		return super.findList(specialProduct);
	}
	
	public Page<SpecialProduct> findPage(Page<SpecialProduct> page, SpecialProduct specialProduct) {
		return super.findPage(page, specialProduct);
	}
	
	@Transactional(readOnly = false)
	public void save(SpecialProduct specialProduct) {
		super.save(specialProduct);
	}
	
	@Transactional(readOnly = false)
	public void delete(SpecialProduct specialProduct) {
		super.delete(specialProduct);
	}
	
}