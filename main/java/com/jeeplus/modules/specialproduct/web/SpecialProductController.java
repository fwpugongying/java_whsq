/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.specialproduct.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.specialproduct.entity.SpecialProduct;
import com.jeeplus.modules.specialproduct.service.SpecialProductService;

/**
 * 云店特色商品Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/specialproduct/specialProduct")
public class SpecialProductController extends BaseController {

	@Autowired
	private SpecialProductService specialProductService;
	
	@ModelAttribute
	public SpecialProduct get(@RequestParam(required=false) String id) {
		SpecialProduct entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = specialProductService.get(id);
		}
		if (entity == null){
			entity = new SpecialProduct();
		}
		return entity;
	}
	
	/**
	 * 云店特色商品列表页面
	 */
	@RequiresPermissions("specialproduct:specialProduct:list")
	@RequestMapping(value = {"list", ""})
	public String list(SpecialProduct specialProduct, Model model) {
		model.addAttribute("specialProduct", specialProduct);
		return "modules/specialproduct/specialProductList";
	}
	
		/**
	 * 云店特色商品列表数据
	 */
	@ResponseBody
	@RequiresPermissions("specialproduct:specialProduct:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(SpecialProduct specialProduct, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SpecialProduct> page = specialProductService.findPage(new Page<SpecialProduct>(request, response), specialProduct); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑云店特色商品表单页面
	 */
	@RequiresPermissions(value={"specialproduct:specialProduct:view","specialproduct:specialProduct:add","specialproduct:specialProduct:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(SpecialProduct specialProduct, Model model) {
		model.addAttribute("specialProduct", specialProduct);
		return "modules/specialproduct/specialProductForm";
	}

	/**
	 * 保存云店特色商品
	 */
	@ResponseBody
	@RequiresPermissions(value={"specialproduct:specialProduct:add","specialproduct:specialProduct:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(SpecialProduct specialProduct, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(specialProduct);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		specialProductService.save(specialProduct);//保存
		j.setSuccess(true);
		j.setMsg("保存云店特色商品成功");
		return j;
	}
	
	/**
	 * 删除云店特色商品
	 */
	@ResponseBody
	@RequiresPermissions("specialproduct:specialProduct:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(SpecialProduct specialProduct) {
		AjaxJson j = new AjaxJson();
		specialProductService.delete(specialProduct);
		j.setMsg("删除云店特色商品成功");
		return j;
	}
	
	/**
	 * 批量删除云店特色商品
	 */
	@ResponseBody
	@RequiresPermissions("specialproduct:specialProduct:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			specialProductService.delete(specialProductService.get(id));
		}
		j.setMsg("删除云店特色商品成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("specialproduct:specialProduct:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(SpecialProduct specialProduct, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "云店特色商品"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<SpecialProduct> page = specialProductService.findPage(new Page<SpecialProduct>(request, response, -1), specialProduct);
    		new ExportExcel("云店特色商品", SpecialProduct.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出云店特色商品记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("specialproduct:specialProduct:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<SpecialProduct> list = ei.getDataList(SpecialProduct.class);
			for (SpecialProduct specialProduct : list){
				try{
					specialProductService.save(specialProduct);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条云店特色商品记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条云店特色商品记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入云店特色商品失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入云店特色商品数据模板
	 */
	@ResponseBody
	@RequiresPermissions("specialproduct:specialProduct:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "云店特色商品数据导入模板.xlsx";
    		List<SpecialProduct> list = Lists.newArrayList(); 
    		new ExportExcel("云店特色商品数据", SpecialProduct.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}