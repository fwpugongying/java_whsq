/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.specialproduct.entity;

import com.jeeplus.modules.product.entity.Product;
import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 云店特色商品Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class SpecialProduct extends DataEntity<SpecialProduct> {
	
	private static final long serialVersionUID = 1L;
	private Product product;		// 商品
	private String type;		// 类型 1购特色 2购好货 3爱逛街 4爱生活
	
	public SpecialProduct() {
		super();
	}

	public SpecialProduct(String id){
		super(id);
	}

	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=1)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ExcelField(title="类型 1购特色 2购好货 3爱逛街 4爱生活", dictType="special_product_type", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}