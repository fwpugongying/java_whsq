/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.collectstore.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.collectstore.entity.CollectStore;
import com.jeeplus.modules.collectstore.service.CollectStoreService;

/**
 * 店铺收藏Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/collectstore/collectStore")
public class CollectStoreController extends BaseController {

	@Autowired
	private CollectStoreService collectStoreService;
	
	@ModelAttribute
	public CollectStore get(@RequestParam(required=false) String id) {
		CollectStore entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = collectStoreService.get(id);
		}
		if (entity == null){
			entity = new CollectStore();
		}
		return entity;
	}
	
	/**
	 * 店铺收藏列表页面
	 */
	@RequiresPermissions("collectstore:collectStore:list")
	@RequestMapping(value = {"list", ""})
	public String list(CollectStore collectStore, Model model) {
		model.addAttribute("collectStore", collectStore);
		return "modules/collectstore/collectStoreList";
	}
	
		/**
	 * 店铺收藏列表数据
	 */
	@ResponseBody
	@RequiresPermissions("collectstore:collectStore:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(CollectStore collectStore, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CollectStore> page = collectStoreService.findPage(new Page<CollectStore>(request, response), collectStore); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑店铺收藏表单页面
	 */
	@RequiresPermissions(value={"collectstore:collectStore:view","collectstore:collectStore:add","collectstore:collectStore:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(CollectStore collectStore, Model model) {
		model.addAttribute("collectStore", collectStore);
		return "modules/collectstore/collectStoreForm";
	}

	/**
	 * 保存店铺收藏
	 */
	@ResponseBody
	@RequiresPermissions(value={"collectstore:collectStore:add","collectstore:collectStore:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(CollectStore collectStore, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(collectStore);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		collectStoreService.save(collectStore);//保存
		j.setSuccess(true);
		j.setMsg("保存店铺收藏成功");
		return j;
	}
	
	/**
	 * 删除店铺收藏
	 */
	@ResponseBody
	@RequiresPermissions("collectstore:collectStore:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(CollectStore collectStore) {
		AjaxJson j = new AjaxJson();
		collectStoreService.delete(collectStore);
		j.setMsg("删除店铺收藏成功");
		return j;
	}
	
	/**
	 * 批量删除店铺收藏
	 */
	@ResponseBody
	@RequiresPermissions("collectstore:collectStore:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			collectStoreService.delete(collectStoreService.get(id));
		}
		j.setMsg("删除店铺收藏成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("collectstore:collectStore:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(CollectStore collectStore, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "店铺收藏"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<CollectStore> page = collectStoreService.findPage(new Page<CollectStore>(request, response, -1), collectStore);
    		new ExportExcel("店铺收藏", CollectStore.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出店铺收藏记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("collectstore:collectStore:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<CollectStore> list = ei.getDataList(CollectStore.class);
			for (CollectStore collectStore : list){
				try{
					collectStoreService.save(collectStore);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条店铺收藏记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条店铺收藏记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入店铺收藏失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入店铺收藏数据模板
	 */
	@ResponseBody
	@RequiresPermissions("collectstore:collectStore:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "店铺收藏数据导入模板.xlsx";
    		List<CollectStore> list = Lists.newArrayList(); 
    		new ExportExcel("店铺收藏数据", CollectStore.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}