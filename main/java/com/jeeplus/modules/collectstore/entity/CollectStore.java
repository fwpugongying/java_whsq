/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.collectstore.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.store.entity.Store;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 店铺收藏Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class CollectStore extends DataEntity<CollectStore> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private Store store;		// 店铺
	
	public CollectStore() {
		super();
	}

	public CollectStore(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Store.class, value="store.title", align=2, sort=2)
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}
	
}