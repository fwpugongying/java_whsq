/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.collectstore.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.collectstore.entity.CollectStore;
import com.jeeplus.modules.collectstore.mapper.CollectStoreMapper;

/**
 * 店铺收藏Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class CollectStoreService extends CrudService<CollectStoreMapper, CollectStore> {

	public CollectStore get(String id) {
		return super.get(id);
	}
	
	public List<CollectStore> findList(CollectStore collectStore) {
		return super.findList(collectStore);
	}
	
	public Page<CollectStore> findPage(Page<CollectStore> page, CollectStore collectStore) {
		return super.findPage(page, collectStore);
	}
	
	@Transactional(readOnly = false)
	public void save(CollectStore collectStore) {
		super.save(collectStore);
	}
	
	@Transactional(readOnly = false)
	public void delete(CollectStore collectStore) {
		super.delete(collectStore);
	}
	
}