/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.orderrefund.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.orderrefund.entity.OrderRefund;
import com.jeeplus.modules.orderrefund.mapper.OrderRefundMapper;

/**
 * 商品订单售后Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class OrderRefundService extends CrudService<OrderRefundMapper, OrderRefund> {

	public OrderRefund get(String id) {
		return super.get(id);
	}
	
	public List<OrderRefund> findList(OrderRefund orderRefund) {
		return super.findList(orderRefund);
	}
	
	public Page<OrderRefund> findPage(Page<OrderRefund> page, OrderRefund orderRefund) {
		return super.findPage(page, orderRefund);
	}
	
	@Transactional(readOnly = false)
	public void save(OrderRefund orderRefund) {
		super.save(orderRefund);
	}
	
	@Transactional(readOnly = false)
	public void delete(OrderRefund orderRefund) {
		super.delete(orderRefund);
	}
	
}