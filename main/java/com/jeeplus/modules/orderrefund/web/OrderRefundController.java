/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.orderrefund.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import com.alibaba.fastjson.JSONObject;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import com.jeeplus.modules.comment.entity.Comment;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.orderrefund.entity.OrderRefund;
import com.jeeplus.modules.orderrefund.service.OrderRefundService;
import com.jeeplus.modules.productorder.entity.ProductOrder;
import com.jeeplus.modules.productorder.service.ProductOrderService;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;

/**
 * 商品订单售后Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/orderrefund/orderRefund")
public class OrderRefundController extends BaseController {

	@Autowired
	private OrderRefundService orderRefundService;
	@Autowired
	private ProductOrderService productOrderService;
	
	@ModelAttribute
	public OrderRefund get(@RequestParam(required=false) String id) {
		OrderRefund entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = orderRefundService.get(id);
		}
		if (entity == null){
			entity = new OrderRefund();
		}
		return entity;
	}
	
	/**
	 * 商品订单售后列表页面
	 */
	@RequiresPermissions("orderrefund:orderRefund:list")
	@RequestMapping(value = {"list", ""})
	public String list(OrderRefund orderRefund, Model model) {
		model.addAttribute("orderRefund", orderRefund);
		return "modules/orderrefund/orderRefundList";
	}
	
		/**
	 * 商品订单售后列表数据
	 */
	@ResponseBody
	@RequiresPermissions("orderrefund:orderRefund:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(OrderRefund orderRefund, HttpServletRequest request, HttpServletResponse response, Model model) {
		// 判断当前登录用户
		User user = UserUtils.getUser();
		if (user.isShop()) {
			orderRefund.setDataScope(" AND store.user_id = '"+user.getId()+"' ");
		}
		Page<OrderRefund> page = orderRefundService.findPage(new Page<OrderRefund>(request, response), orderRefund); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑商品订单售后表单页面
	 */
	@RequiresPermissions(value={"orderrefund:orderRefund:view","orderrefund:orderRefund:add","orderrefund:orderRefund:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(OrderRefund orderRefund, Model model) {
		model.addAttribute("orderRefund", orderRefund);
		return "modules/orderrefund/orderRefundForm";
	}

	/**
	 * 保存商品订单售后
	 */
	@ResponseBody
	@RequiresPermissions(value={"orderrefund:orderRefund:add","orderrefund:orderRefund:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(OrderRefund orderRefund, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(orderRefund);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		orderRefundService.save(orderRefund);//保存
		j.setSuccess(true);
		j.setMsg("保存商品订单售后成功");
		return j;
	}
	
	/**
	 * 退款审核
	 */
	@ResponseBody
	@RequestMapping(value = "audit")
	public AjaxJson audit(OrderRefund orderRefund, String type,String price) throws Exception{
		AjaxJson j = new AjaxJson();
		if (!"0".equals(orderRefund.getState())) {
			j.setSuccess(false);
			j.setMsg("只能操作退款中的订单");
			return j;
		}
		if("3".equals(type)){
			ProductOrder order = productOrderService.get(orderRefund.getOrderId());
			if (order != null) {
				BigDecimal amount = new BigDecimal(order.getAmount());
				BigDecimal price2 = new BigDecimal(price);
				if(price2.compareTo(amount) >0){
					j.setMsg("退款金额，不能大于订单金额");
					return j;
				}
			}
			orderRefund.setAmount(price);
			orderRefundService.save(orderRefund);
			j.setSuccess(true);
			j.setMsg("操作成功");
			return j;
		}
		//orderRefund.setState(type);
		//orderRefund.setAuditDate(new Date());
		//orderRefundService.refund(orderRefund);//保存
		ProductOrder order = productOrderService.get(orderRefund.getOrderId());
		if (order != null) {
			if("1".equals(type)) {
				if("1".equals(order.getOrdertype())) {
					List<WohuiEntity> entityList = Lists.newArrayList();
					entityList.add(new WohuiEntity("OrderNO", order.getTotalId()));
					JSONObject result = WohuiUtils.send(WohuiUtils.XFQRefund, entityList);
					if (!"0000".equals(result.getString("respCode"))) {
						j.setSuccess(false);
						j.setMsg("我惠会员系统消费券退款失败总单号：" +order.getTotalId()+ result.getString("respMsg"));
						return j;
					}
				}
				if("1".equals(order.getZsqtype())) {
					List<WohuiEntity> entityList = Lists.newArrayList();
					entityList.add(new WohuiEntity("OrderNO", order.getTotalId()));
					JSONObject result = WohuiUtils.send(WohuiUtils.ZSQRefund, entityList);
					if (!"0000".equals(result.getString("respCode"))) {
						j.setSuccess(false);
						j.setMsg("我惠会员系统专属券退款失败总单号：" +order.getTotalId()+ result.getString("respMsg"));
						return j;
					}
				}
			}
			productOrderService.refund(order, type);
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	/**
	 * 删除商品订单售后
	 */
	@ResponseBody
	@RequiresPermissions("orderrefund:orderRefund:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(OrderRefund orderRefund) {
		AjaxJson j = new AjaxJson();
		orderRefundService.delete(orderRefund);
		j.setMsg("删除商品订单售后成功");
		return j;
	}
	
	/**
	 * 批量删除商品订单售后
	 */
	@ResponseBody
	@RequiresPermissions("orderrefund:orderRefund:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			orderRefundService.delete(orderRefundService.get(id));
		}
		j.setMsg("删除商品订单售后成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("orderrefund:orderRefund:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(OrderRefund orderRefund, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品订单售后"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<OrderRefund> page = orderRefundService.findPage(new Page<OrderRefund>(request, response, -1), orderRefund);
    		new ExportExcel("商品订单售后", OrderRefund.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出商品订单售后记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("orderrefund:orderRefund:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<OrderRefund> list = ei.getDataList(OrderRefund.class);
			for (OrderRefund orderRefund : list){
				try{
					orderRefundService.save(orderRefund);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条商品订单售后记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条商品订单售后记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入商品订单售后失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入商品订单售后数据模板
	 */
	@ResponseBody
	@RequiresPermissions("orderrefund:orderRefund:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品订单售后数据导入模板.xlsx";
    		List<OrderRefund> list = Lists.newArrayList(); 
    		new ExportExcel("商品订单售后数据", OrderRefund.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}