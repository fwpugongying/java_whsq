/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shop.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.shopcategory.entity.ShopCategory;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 实体商家Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Shop extends DataEntity<Shop> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String shopCode;		// 店铺编码
	private String couponCode;		// 优惠券编码
	private String title;		// 商户名称
	private String icon;		// 店铺LOGO
	private ShopCategory category;		// 行业
	private String city;		// 城市
	private String address;		// 地址
	private Double lon;		// 经度
	private Double lat;		// 纬度
	private String username;		// 联系人
	private String phone;		// 手机号
	private Member invite;		// 签约人
	private String licence;		// 营业执照
	private String idcard;		// 身份证
	private String others;		// 其他证书
	private String state;		// 状态 0正常 1冻结
	private String auditState;		// 审核状态 0待支付 1待审核 2审核通过 3审核拒绝 4已退款
	private String amount;		// 入驻金额
	private String orderNo;		// 支付单号
	private String payType;		// 支付方式 1支付宝 2微信
	private Date auditDate;		// 审核时间
	private String content;		// 店铺简介
	private String images;		// 店铺图片
	private String showState;		// 简介状态 0待审核 1通过 2拒绝
	private String balance;		// 余额
	private String hyrebate;		// 会员返利比例
	private String ptrebate;		// 平台返利比例（铜板）
	private String qrcode;		// 二微码
	private String invitePhone;// 签约人手机号
	private String inviteName;// 签约人姓名
	private String trade_type;// 签约人姓名
	private String lovetype;// 爱心标记
	private String xhtype;// 0选择的200款差额 1是1000元的押金
	private String paymoney;		// 付款200款的差额
	public Shop() {
		super();
	}

	public Shop(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@ExcelField(title="二微码", align=2, sort=2)
	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	@ExcelField(title="爱心标记", align=2, sort=2)
	public String getLovetype() {
		return lovetype;
	}

	public void setLovetype(String lovetype) {
		this.lovetype = lovetype;
	}

	@ExcelField(title="0选择的200款差额 1是1000元的押金", align=2, sort=2)
	public String getXhtype() {
		return xhtype;
	}

	public void setXhtype(String xhtype) {
		this.xhtype = xhtype;
	}

	@ExcelField(title="会员返利比例", align=2, sort=2)
	public String getHyrebate() {
		return hyrebate;
	}

	public void setHyrebate(String hyrebate) {
		this.hyrebate = hyrebate;
	}

	@ExcelField(title="平台返利比例", align=2, sort=2)
	public String getPtrebate() {
		return ptrebate;
	}

	public void setPtrebate(String ptrebate) {
		this.ptrebate = ptrebate;
	}
	
	@ExcelField(title="店铺编码", align=2, sort=2)
	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	
	@ExcelField(title="优惠券编码", align=2, sort=3)
	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}
	
	@ExcelField(title="商户名称", align=2, sort=4)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="店铺LOGO", align=2, sort=5)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@NotNull(message="行业不能为空")
	@ExcelField(title="行业", fieldType=ShopCategory.class, value="category.title", align=2, sort=6)
	public ShopCategory getCategory() {
		return category;
	}

	public void setCategory(ShopCategory category) {
		this.category = category;
	}
	
	@ExcelField(title="城市", align=2, sort=7)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@ExcelField(title="地址", align=2, sort=8)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@NotNull(message="经度不能为空")
	@ExcelField(title="经度", align=2, sort=9)
	public Double getLon() {
		return lon;
	}

	public void setLon(Double lon) {
		this.lon = lon;
	}
	
	@NotNull(message="纬度不能为空")
	@ExcelField(title="纬度", align=2, sort=10)
	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	@ExcelField(title="联系人", align=2, sort=11)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="手机号", align=2, sort=12)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@NotNull(message="签约人不能为空")
	@ExcelField(title="签约人", fieldType=Member.class, value="invite.phone", align=2, sort=13)
	public Member getInvite() {
		return invite;
	}

	public void setInvite(Member invite) {
		this.invite = invite;
	}
	
	@ExcelField(title="营业执照", align=2, sort=14)
	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}
	
	@ExcelField(title="身份证", align=2, sort=15)
	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	
	@ExcelField(title="其他证书", align=2, sort=16)
	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}
	
	@ExcelField(title="状态 0正常 1冻结", dictType="member_state", align=2, sort=18)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="审核状态 0待支付 1已支付 2审核通过 3审核拒绝 4已退款", dictType="shop_audit", align=2, sort=19)
	public String getAuditState() {
		return auditState;
	}

	public void setAuditState(String auditState) {
		this.auditState = auditState;
	}
	
	@ExcelField(title="入驻金额", align=2, sort=20)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@ExcelField(title="付款200款的差额", align=2, sort=20)
	public String getPaymoney() {
		return paymoney;
	}

	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}
	
	@ExcelField(title="支付单号", align=2, sort=21)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="支付方式 1支付宝 2微信", dictType="pay_type", align=2, sort=22)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=23)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	
	@ExcelField(title="店铺简介", align=2, sort=25)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="店铺图片", align=2, sort=26)
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}
	
	@ExcelField(title="简介状态 0待审核 1通过 2拒绝", dictType="shop_show", align=2, sort=27)
	public String getShowState() {
		return showState;
	}

	public void setShowState(String showState) {
		this.showState = showState;
	}
	
	@ExcelField(title="余额", align=2, sort=28)
	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getInvitePhone() {
		return invitePhone;
	}

	public void setInvitePhone(String invitePhone) {
		this.invitePhone = invitePhone;
	}

	public String getInviteName() {
		return inviteName;
	}

	public void setInviteName(String inviteName) {
		this.inviteName = inviteName;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	public String getTrade_type() {
		return trade_type;
	}
}