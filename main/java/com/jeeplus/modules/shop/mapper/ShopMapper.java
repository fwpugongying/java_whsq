/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shop.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.shop.entity.Shop;

/**
 * 实体商家MAPPER接口
 * @author lixinapp
 * @version 2019-10-16
 */
@MyBatisMapper
public interface ShopMapper extends BaseMapper<Shop> {

	List<Map<String, Object>> shopOption(@Param("shopId")String shopId, @Param("beginDate")String beginDate, @Param("endDate")String endDate);
}