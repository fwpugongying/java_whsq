/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.recharge.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 话费充值选项Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Recharge extends DataEntity<Recharge> {
	
	private static final long serialVersionUID = 1L;
	private String amount;		// 充值金额
	private String price;		// 支付金额
	private String point;		// 铜板
	private Integer sort;		// 排序
	
	public Recharge() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Recharge(String id){
		super(id);
	}

	@ExcelField(title="充值金额", align=2, sort=1)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="支付金额", align=2, sort=2)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@ExcelField(title="铜板", align=2, sort=3)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=4)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}