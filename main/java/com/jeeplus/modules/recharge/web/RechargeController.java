/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.recharge.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.recharge.entity.Recharge;
import com.jeeplus.modules.recharge.service.RechargeService;

/**
 * 话费充值选项Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/recharge/recharge")
public class RechargeController extends BaseController {

	@Autowired
	private RechargeService rechargeService;
	
	@ModelAttribute
	public Recharge get(@RequestParam(required=false) String id) {
		Recharge entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = rechargeService.get(id);
		}
		if (entity == null){
			entity = new Recharge();
		}
		return entity;
	}
	
	/**
	 * 话费充值选项列表页面
	 */
	@RequiresPermissions("recharge:recharge:list")
	@RequestMapping(value = {"list", ""})
	public String list(Recharge recharge, Model model) {
		model.addAttribute("recharge", recharge);
		return "modules/recharge/rechargeList";
	}
	
		/**
	 * 话费充值选项列表数据
	 */
	@ResponseBody
	@RequiresPermissions("recharge:recharge:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Recharge recharge, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Recharge> page = rechargeService.findPage(new Page<Recharge>(request, response), recharge); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑话费充值选项表单页面
	 */
	@RequiresPermissions(value={"recharge:recharge:view","recharge:recharge:add","recharge:recharge:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Recharge recharge, Model model) {
		model.addAttribute("recharge", recharge);
		return "modules/recharge/rechargeForm";
	}

	/**
	 * 保存话费充值选项
	 */
	@ResponseBody
	@RequiresPermissions(value={"recharge:recharge:add","recharge:recharge:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Recharge recharge, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(recharge);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		rechargeService.save(recharge);//保存
		j.setSuccess(true);
		j.setMsg("保存话费充值选项成功");
		return j;
	}
	
	/**
	 * 删除话费充值选项
	 */
	@ResponseBody
	@RequiresPermissions("recharge:recharge:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Recharge recharge) {
		AjaxJson j = new AjaxJson();
		rechargeService.delete(recharge);
		j.setMsg("删除话费充值选项成功");
		return j;
	}
	
	/**
	 * 批量删除话费充值选项
	 */
	@ResponseBody
	@RequiresPermissions("recharge:recharge:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			rechargeService.delete(rechargeService.get(id));
		}
		j.setMsg("删除话费充值选项成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("recharge:recharge:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Recharge recharge, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "话费充值选项"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Recharge> page = rechargeService.findPage(new Page<Recharge>(request, response, -1), recharge);
    		new ExportExcel("话费充值选项", Recharge.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出话费充值选项记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("recharge:recharge:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Recharge> list = ei.getDataList(Recharge.class);
			for (Recharge recharge : list){
				try{
					rechargeService.save(recharge);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条话费充值选项记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条话费充值选项记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入话费充值选项失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入话费充值选项数据模板
	 */
	@ResponseBody
	@RequiresPermissions("recharge:recharge:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "话费充值选项数据导入模板.xlsx";
    		List<Recharge> list = Lists.newArrayList(); 
    		new ExportExcel("话费充值选项数据", Recharge.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}