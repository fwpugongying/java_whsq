/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.recharge.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.recharge.entity.Recharge;
import com.jeeplus.modules.recharge.mapper.RechargeMapper;

/**
 * 话费充值选项Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class RechargeService extends CrudService<RechargeMapper, Recharge> {

	public Recharge get(String id) {
		return super.get(id);
	}
	
	public List<Recharge> findList(Recharge recharge) {
		return super.findList(recharge);
	}
	
	public Page<Recharge> findPage(Page<Recharge> page, Recharge recharge) {
		return super.findPage(page, recharge);
	}
	
	@Transactional(readOnly = false)
	public void save(Recharge recharge) {
		super.save(recharge);
	}
	
	@Transactional(readOnly = false)
	public void delete(Recharge recharge) {
		super.delete(recharge);
	}
	
}