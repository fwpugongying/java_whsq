/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shoucang.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.shoucang.entity.Shoucang;
import com.jeeplus.modules.shoucang.service.ShoucangService;

/**
 * 热门搜索关键词Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/shoucang/shoucang")
public class ShoucangController extends BaseController {

	@Autowired
	private ShoucangService shoucangService;
	
	@ModelAttribute
	public Shoucang get(@RequestParam(required=false) String id) {
		Shoucang entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shoucangService.get(id);
		}
		if (entity == null){
			entity = new Shoucang();
		}
		return entity;
	}
	
	/**
	 * 热门搜索关键词列表页面
	 */
	@RequiresPermissions("shoucang:shoucang:list")
	@RequestMapping(value = {"list", ""})
	public String list(Shoucang shoucang, Model model) {
		model.addAttribute("shoucang", shoucang);
		return "modules/shoucang/shoucangList";
	}
	
		/**
	 * 热门搜索关键词列表数据
	 */
	@ResponseBody
	@RequiresPermissions("shoucang:shoucang:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Shoucang shoucang, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Shoucang> page = shoucangService.findPage(new Page<Shoucang>(request, response), shoucang); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑热门搜索关键词表单页面
	 */
	@RequiresPermissions(value={"shoucang:shoucang:view","shoucang:shoucang:add","shoucang:shoucang:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Shoucang shoucang, Model model) {
		model.addAttribute("shoucang", shoucang);
		return "modules/shoucang/shoucangForm";
	}

	/**
	 * 保存热门搜索关键词
	 */
	@ResponseBody
	@RequiresPermissions(value={"shoucang:shoucang:add","shoucang:shoucang:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Shoucang shoucang, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(shoucang);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		shoucangService.save(shoucang);//保存
		j.setSuccess(true);
		j.setMsg("保存热门搜索关键词成功");
		return j;
	}
	
	/**
	 * 删除热门搜索关键词
	 */
	@ResponseBody
	@RequiresPermissions("shoucang:shoucang:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Shoucang shoucang) {
		AjaxJson j = new AjaxJson();
		shoucangService.delete(shoucang);
		j.setMsg("删除热门搜索关键词成功");
		return j;
	}
	
	/**
	 * 批量删除热门搜索关键词
	 */
	@ResponseBody
	@RequiresPermissions("shoucang:shoucang:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			shoucangService.delete(shoucangService.get(id));
		}
		j.setMsg("删除热门搜索关键词成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("shoucang:shoucang:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Shoucang shoucang, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "热门搜索关键词"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Shoucang> page = shoucangService.findPage(new Page<Shoucang>(request, response, -1), shoucang);
    		new ExportExcel("热门搜索关键词", Shoucang.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出热门搜索关键词记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("shoucang:shoucang:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Shoucang> list = ei.getDataList(Shoucang.class);
			for (Shoucang shoucang : list){
				try{
					shoucangService.save(shoucang);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条热门搜索关键词记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条热门搜索关键词记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入热门搜索关键词失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入热门搜索关键词数据模板
	 */
	@ResponseBody
	@RequiresPermissions("shoucang:shoucang:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "热门搜索关键词数据导入模板.xlsx";
    		List<Shoucang> list = Lists.newArrayList(); 
    		new ExportExcel("热门搜索关键词数据", Shoucang.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}