/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shoucang.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;
import com.jeeplus.modules.member.entity.Member;

import java.util.Date;

/**
 * 热门搜索关键词Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Shoucang extends DataEntity<Shoucang> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private Date createTime;		// 审核时间
	private int dpId;		// 审核时间
	private int status;		// 审核时间



	public Shoucang() {
		super();
	}
	public Shoucang(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public int getDpId() {
		return dpId;
	}

	public void setDpId(int dpId) {
		this.dpId = dpId;
	}
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}