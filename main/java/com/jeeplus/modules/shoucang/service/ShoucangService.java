/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shoucang.service;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.shoucang.entity.Shoucang;
import com.jeeplus.modules.shoucang.mapper.ShoucangMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 热门搜索关键词Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class ShoucangService extends CrudService<ShoucangMapper, Shoucang> {

	public Shoucang get(String id) {
		return super.get(id);
	}
	
	public List<Shoucang> findList(Shoucang shoucang) {
		return super.findList(shoucang);
	}
	
	public Page<Shoucang> findPage(Page<Shoucang> page, Shoucang shoucang) {
		return super.findPage(page, shoucang);
	}
	
	@Transactional(readOnly = false)
	public void save(Shoucang shoucang) {
		super.save(shoucang);
	}
	
	@Transactional(readOnly = false)
	public void delete(Shoucang shoucang) {
		super.delete(shoucang);
	}
	
}