/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.comment.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.comment.entity.Comment;
import com.jeeplus.modules.comment.service.CommentService;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;

/**
 * 商品评价Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/comment/comment")
public class CommentController extends BaseController {

	@Autowired
	private CommentService commentService;
	
	@ModelAttribute
	public Comment get(@RequestParam(required=false) String id) {
		Comment entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = commentService.get(id);
		}
		if (entity == null){
			entity = new Comment();
		}
		return entity;
	}
	
	/**
	 * 商品评价列表页面
	 */
	@RequiresPermissions("comment:comment:list")
	@RequestMapping(value = {"list", ""})
	public String list(Comment comment, Model model) {
		model.addAttribute("comment", comment);
		return "modules/comment/commentList";
	}
	
		/**
	 * 商品评价列表数据
	 */
	@ResponseBody
	@RequiresPermissions("comment:comment:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Comment comment, HttpServletRequest request, HttpServletResponse response, Model model) {
		// 判断当前登录用户
		User user = UserUtils.getUser();
		if (user.isShop()) {
			comment.setDataScope(" AND store.user_id = '"+user.getId()+"' ");
		}
		Page<Comment> page = commentService.findPage(new Page<Comment>(request, response), comment); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑商品评价表单页面
	 */
	@RequiresPermissions(value={"comment:comment:view","comment:comment:add","comment:comment:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Comment comment, Model model) {
		model.addAttribute("comment", comment);
		return "modules/comment/commentForm";
	}

	/**
	 * 保存商品评价
	 */
	@ResponseBody
	@RequiresPermissions(value={"comment:comment:add","comment:comment:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Comment comment, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(comment);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		commentService.save(comment);//保存
		j.setSuccess(true);
		j.setMsg("保存商品评价成功");
		return j;
	}
	
	/**
	 * 删除商品评价
	 */
	@ResponseBody
	@RequiresPermissions("comment:comment:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Comment comment) {
		AjaxJson j = new AjaxJson();
		commentService.delete(comment);
		j.setMsg("删除商品评价成功");
		return j;
	}
	
	
	/**
	 * 回复商品评价
	 */
	@ResponseBody
	@RequestMapping(value = "reply")
	public AjaxJson reply(Comment comment, String text) {
		AjaxJson j = new AjaxJson();
		if (comment != null && StringUtils.isNotBlank(comment.getId()) && "0".equals(comment.getState())) {
			comment.setState("1");
			comment.setReply(text);
			comment.setReplyDate(new Date());
			commentService.save(comment);
			j.setMsg("回复成功");
		} else {
			j.setSuccess(false);
			j.setMsg("回复失败，请刷新后重试！");
		}
		return j;
	}
	
	/**
	 * 批量删除商品评价
	 */
	@ResponseBody
	@RequiresPermissions("comment:comment:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			commentService.delete(commentService.get(id));
		}
		j.setMsg("删除商品评价成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("comment:comment:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Comment comment, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品评价"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Comment> page = commentService.findPage(new Page<Comment>(request, response, -1), comment);
    		new ExportExcel("商品评价", Comment.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出商品评价记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("comment:comment:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Comment> list = ei.getDataList(Comment.class);
			for (Comment comment : list){
				try{
					commentService.save(comment);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条商品评价记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条商品评价记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入商品评价失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入商品评价数据模板
	 */
	@ResponseBody
	@RequiresPermissions("comment:comment:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品评价数据导入模板.xlsx";
    		List<Comment> list = Lists.newArrayList(); 
    		new ExportExcel("商品评价数据", Comment.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}