/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.comment.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.store.entity.Store;
import com.jeeplus.modules.product.entity.Product;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品评价Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Comment extends DataEntity<Comment> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String orderId;		// 订单号
	private Store store;		// 店铺
	private Product product;		// 商品
	private String skuId;		// 规格
	private String productTitle;		// 商品名称
	private String productIcon;		// 商品图片
	private String skuTitle;		// 规格名称
	private String price;		// 商品单价
	private Integer qty;		// 数量
	private Integer score;		// 评分
	private String content;		// 评价内容
	private String images;		// 图片
	private String state;		// 回复状态 0未回复 1已回复
	private String reply;		// 回复内容
	private Date replyDate;		// 回复时间
	private String productId;		// 辅助字段-商品ID
	private String itemId;		// 辅助字段-商品订单项ID
	
	public Comment() {
		super();
	}

	public Comment(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="订单号", align=2, sort=2)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Store.class, value="store.title", align=2, sort=3)
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}
	
	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=4)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ExcelField(title="规格", align=2, sort=5)
	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	
	@ExcelField(title="商品名称", align=2, sort=6)
	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	
	@ExcelField(title="商品图片", align=2, sort=7)
	public String getProductIcon() {
		return productIcon;
	}

	public void setProductIcon(String productIcon) {
		this.productIcon = productIcon;
	}
	
	@ExcelField(title="规格名称", align=2, sort=8)
	public String getSkuTitle() {
		return skuTitle;
	}

	public void setSkuTitle(String skuTitle) {
		this.skuTitle = skuTitle;
	}
	
	@ExcelField(title="商品单价", align=2, sort=9)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@NotNull(message="数量不能为空")
	@ExcelField(title="数量", align=2, sort=10)
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@NotNull(message="评分不能为空")
	@ExcelField(title="评分", align=2, sort=11)
	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}
	
	@ExcelField(title="评价内容", align=2, sort=12)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="图片", align=2, sort=13)
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}
	
	@ExcelField(title="回复状态 0未回复 1已回复", dictType="comment_state", align=2, sort=15)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="回复内容", align=2, sort=16)
	public String getReply() {
		return reply;
	}

	public void setReply(String reply) {
		this.reply = reply;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="回复时间", align=2, sort=17)
	public Date getReplyDate() {
		return replyDate;
	}

	public void setReplyDate(Date replyDate) {
		this.replyDate = replyDate;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
}