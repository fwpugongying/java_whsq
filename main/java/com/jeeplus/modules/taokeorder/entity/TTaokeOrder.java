/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.taokeorder.entity;

import com.jeeplus.modules.member.entity.Member;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 淘客订单管理Entity
 * @author ws
 * @version 2019-11-09
 */
public class TTaokeOrder extends DataEntity<TTaokeOrder> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String alipayTotalPrice;		// 付款金额（不包含运费金额）
	private String itemTitle;		// 商品标题
	private String itemImg;		// 商品图片
	private Integer itemNum;		// 数量
	private String tradeParentId;		// 订单编号
	private String tkStatus;		// 3：订单结算，12：订单付款， 13：订单失效，14：订单成功
	private String itemPrice;		// 商品单价
	private String itemId;		// 商品id
	private String totalCommissionFee;		// 佣金金额=结算金额*佣金比率
	private Date tbPaidTime;		// 付款时间
	private Date tkCreateTime;		// 创建时间
	private Date beginTkCreateTime;		// 开始 创建时间
	private Date endTkCreateTime;		// 结束 创建时间
	private Date minTime;		// 订单最小时间
	private Date maxTime;		// 订单最大时间
	private String type;		// 类型：0-淘宝，1-拼多多，2-京东
	private String isJs;		// 是否已结算:0-否，1-是
	private String pubSharePreFee;		// 付款预估收入=付款金额*提成 (指买家付款金额为基数，预估您可能获得的收入。因买家退款等原因，可能与结算预估收入不一致)
	private String ygtb;		// 预估铜板=预估佣金*45%
	private String jstb;		// 结算铜板=结算佣金*45%
	private String goodsSign;		// 结算铜板=结算佣金*45%

	public String getGoodsSign() {
		return goodsSign;
	}
	public void setGoodsSign(String goodsSign) {
		this.goodsSign = goodsSign;
	}
	public TTaokeOrder() {
		super();
	}

	public TTaokeOrder(String id){
		super(id);
	}

	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="付款金额（不包含运费金额）", align=2, sort=2)
	public String getAlipayTotalPrice() {
		return alipayTotalPrice;
	}

	public void setAlipayTotalPrice(String alipayTotalPrice) {
		this.alipayTotalPrice = alipayTotalPrice;
	}
	
	@ExcelField(title="商品标题", align=2, sort=3)
	public String getItemTitle() {
		return itemTitle;
	}

	public void setItemTitle(String itemTitle) {
		this.itemTitle = itemTitle;
	}
	
	@ExcelField(title="商品图片", align=2, sort=4)
	public String getItemImg() {
		return itemImg;
	}

	public void setItemImg(String itemImg) {
		this.itemImg = itemImg;
	}
	
	@ExcelField(title="数量", align=2, sort=5)
	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}
	
	@ExcelField(title="订单编号", align=2, sort=6)
	public String getTradeParentId() {
		return tradeParentId;
	}

	public void setTradeParentId(String tradeParentId) {
		this.tradeParentId = tradeParentId;
	}
	
	@ExcelField(title="3：订单结算，12：订单付款， 13：订单失效，14：订单成功", dictType="tk_status", align=2, sort=7)
	public String getTkStatus() {
		return tkStatus;
	}

	public void setTkStatus(String tkStatus) {
		this.tkStatus = tkStatus;
	}
	
	@ExcelField(title="商品单价", align=2, sort=8)
	public String getItemPrice() {
		if(itemPrice!=null){
			return itemPrice;
		}else{
			return "0";
		}
	}

	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	@ExcelField(title="商品id", align=2, sort=9)
	public String getItemId() {
		if(itemId!=null){
			return itemId;
		}else{
			return "";
		}
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	@ExcelField(title="佣金金额=结算金额*佣金比率", align=2, sort=10)
	public String getTotalCommissionFee() {
		return totalCommissionFee;
	}

	public void setTotalCommissionFee(String totalCommissionFee) {
		this.totalCommissionFee = totalCommissionFee;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="付款时间", align=2, sort=11)
	public Date getTbPaidTime() {
		return tbPaidTime;
	}

	public void setTbPaidTime(Date tbPaidTime) {
		this.tbPaidTime = tbPaidTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="创建时间", align=2, sort=12)
	public Date getTkCreateTime() {
		return tkCreateTime;
	}

	public void setTkCreateTime(Date tkCreateTime) {
		this.tkCreateTime = tkCreateTime;
	}
	
	public Date getBeginTkCreateTime() {
		return beginTkCreateTime;
	}

	public void setBeginTkCreateTime(Date beginTkCreateTime) {
		this.beginTkCreateTime = beginTkCreateTime;
	}
	
	public Date getEndTkCreateTime() {
		return endTkCreateTime;
	}

	public void setEndTkCreateTime(Date endTkCreateTime) {
		this.endTkCreateTime = endTkCreateTime;
	}

	public Date getMinTime() {
		return minTime;
	}

	public void setMinTime(Date minTime) {
		this.minTime = minTime;
	}

	public Date getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(Date maxTime) {
		this.maxTime = maxTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPubSharePreFee() {
		return pubSharePreFee;
	}

	public void setPubSharePreFee(String pubSharePreFee) {
		this.pubSharePreFee = pubSharePreFee;
	}

	public String getIsJs() {
		return isJs;
	}

	public void setIsJs(String isJs) {
		this.isJs = isJs;
	}

	public String getYgtb() {
		return ygtb;
	}

	public void setYgtb(String ygtb) {
		this.ygtb = ygtb;
	}

	public String getJstb() {
		return jstb;
	}

	public void setJstb(String jstb) {
		this.jstb = jstb;
	}
		
}