/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.taokeorder.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.taokeorder.entity.TTaokeOrder;
import com.jeeplus.modules.taokeorder.mapper.TTaokeOrderMapper;

/**
 * 淘客订单管理Service
 * @author ws
 * @version 2019-11-09
 */
@Service
@Transactional(readOnly = true)
public class TTaokeOrderService extends CrudService<TTaokeOrderMapper, TTaokeOrder> {

	public TTaokeOrder get(String id) {
		return super.get(id);
	}
	
	public List<TTaokeOrder> findList(TTaokeOrder tTaokeOrder) {
		return super.findList(tTaokeOrder);
	}
	
	public Page<TTaokeOrder> findPage(Page<TTaokeOrder> page, TTaokeOrder tTaokeOrder) {
		return super.findPage(page, tTaokeOrder);
	}
	
	@Transactional(readOnly = false)
	public void save(TTaokeOrder tTaokeOrder) {
		super.save(tTaokeOrder);
	}
	
	@Transactional(readOnly = false)
	public void delete(TTaokeOrder tTaokeOrder) {
		super.delete(tTaokeOrder);
	}
	
}