/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.taokeorder.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.taokeorder.entity.TTaokeOrder;

/**
 * 淘客订单管理MAPPER接口
 * @author ws
 * @version 2019-11-09
 */
@MyBatisMapper
public interface TTaokeOrderMapper extends BaseMapper<TTaokeOrder> {
	
}