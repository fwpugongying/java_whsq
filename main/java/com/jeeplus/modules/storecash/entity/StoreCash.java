/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storecash.entity;

import com.jeeplus.modules.store.entity.Store;
import javax.validation.constraints.NotNull;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 厂家提现Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class StoreCash extends DataEntity<StoreCash> {
	
	private static final long serialVersionUID = 1L;
	private Store store;		// 店铺
	private String amount;		// 金额
	private String fee;		// 手续费
	private String username;		// 姓名
	private String bank;		// 银行
	private String account;		// 卡号
	private String orderno;		// 单号
	private String state;		// 状态 0待审核 1通过 2拒绝
	private String pay;		// 状态 0银行卡 1微信
	private Date auditDate;		// 审核时间
	private Date beginCreateDate; // 开始 下单时间
	private Date endCreateDate; // 结束 下单时间
	public StoreCash() {
		super();
	}

	public StoreCash(String id){
		super(id);
	}

	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Store.class, value="store.title", align=2, sort=1)
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}
	
	@ExcelField(title="金额", align=2, sort=2)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="姓名", align=2, sort=3)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="银行", align=2, sort=4)
	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}
	
	@ExcelField(title="卡号", align=2, sort=5)
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	@ExcelField(title="提现单号", align=2, sort=5)
	public String getOrderno() {
		return orderno;
	}

	public void setOrderno(String orderno) {
		this.orderno = orderno;
	}
	
	@ExcelField(title="状态 0待审核 1通过 2拒绝", dictType="cash_state", align=2, sort=7)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	@ExcelField(title="状态 0银行卡 1微信", dictType="storecashpay", align=2, sort=7)
	public String getPay() {
		return pay;
	}
	public void setPay(String pay) {
		this.pay = pay;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=8)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public Date getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(Date beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}

	public Date getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}
	
}