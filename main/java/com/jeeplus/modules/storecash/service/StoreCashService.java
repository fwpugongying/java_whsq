/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storecash.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.modules.api.utils.WxPayUtils;
import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.modules.store.entity.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.store.service.StoreService;
import com.jeeplus.modules.storecash.entity.StoreCash;
import com.jeeplus.modules.storecash.mapper.StoreCashMapper;

/**
 * 厂家提现Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class StoreCashService extends CrudService<StoreCashMapper, StoreCash> {
	@Autowired
	private StoreService storeService;

	public StoreCash get(String id) {
		return super.get(id);
	}

	public List<StoreCash> findList(StoreCash storeCash) {
		return super.findList(storeCash);
	}

	public Page<StoreCash> findPage(Page<StoreCash> page, StoreCash storeCash) {
		return super.findPage(page, storeCash);
	}

	@Transactional(readOnly = false)
	public void save(StoreCash storeCash) {
		super.save(storeCash);
	}

	@Transactional(readOnly = false)
	public void delete(StoreCash storeCash) {
		super.delete(storeCash);
	}

	/**
	 * 审核
	 * @param storeCash
	 * @return
	 */
	@Transactional(readOnly = false)
	public AjaxJson audit(StoreCash storeCash) throws Exception {
		AjaxJson j = new AjaxJson();
		if ("1".equals(storeCash.getState())) {
			super.save(storeCash);
		}
		if ("3".equals(storeCash.getState())) {
			String pay = storeCash.getPay();
			if ("1".equals(storeCash.getPay())) {
				//进行微信提现操作
				Store store = storeCash.getStore();
				String storeid = store.getId();
				Store store2 = storeService.findUniqueByProperty("id", storeid);
				Member member = store2.getMember();
				if (member != null) {
					if (member.getTxopenid() != null && !member.getTxopenid().isEmpty()) {
					String openid =member.getTxopenid();
//						String openid = "okob81ZY1kEUHibhhXn22MCg3-mU";
						DecimalFormat df = new DecimalFormat("#");
						String price = df.format((Double.valueOf(storeCash.getAmount())-Double.valueOf(storeCash.getFee())) * 100.0);
						String payMoney = String.valueOf(price);
						String orderId = storeCash.getOrderno();
						String desc = store.getTitle() + "厂家提现";
						Map<String, String> body = WxPayUtils.wxqyPay(openid, payMoney, orderId, desc);
						String status = body.get("status");
						String msg = body.get("msg");
						if ("1".equals(status)) {
							logger.error("提现失败：" + msg);
							j.setSuccess(false);
							j.setMsg("提现失败：" + msg);
							return j;
						} else {
							super.save(storeCash);
						}
					}
				} else {
					logger.error("会员信息获取失败，提现失败");
					j.setSuccess(false);
					j.setMsg("会员信息获取失败，提现失败");
					return j;
				}
			}
		}
		//处理拒绝
		if ("2".equals(storeCash.getState())) {
			super.save(storeCash);
			// 加钱
			storeService.updateBalance(storeCash.getStore(), "提现拒绝", storeCash.getAmount(), "1", null, null, null, null, null);
		}
		j.setSuccess(true);
		j.setMsg("审核厂家提现成功");
		return j;
	}

	/**
	 * 申请提现
	 * @param storeCash
	 */
	@Transactional(readOnly = false)
	public void add(StoreCash storeCash) {
		super.save(storeCash);
		if("1".equals(storeCash.getPay())){
			storeService.updateBalance(storeCash.getStore(), "微信提现", storeCash.getAmount(), "0", null, null, null, null, null);
		}else {
		// 减钱
		storeService.updateBalance(storeCash.getStore(), storeCash.getBank() + "-" + storeCash.getAccount().substring(storeCash.getAccount().length() - 4), storeCash.getAmount(), "0", null, null, null, null, null);
		}
	}


}