/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.brand.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.brand.entity.Brand;
import com.jeeplus.modules.brand.service.BrandService;

/**
 * 商品品牌Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/brand/brand")
public class BrandController extends BaseController {

	@Autowired
	private BrandService brandService;
	
	@ModelAttribute
	public Brand get(@RequestParam(required=false) String id) {
		Brand entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = brandService.get(id);
		}
		if (entity == null){
			entity = new Brand();
		}
		return entity;
	}
	
	/**
	 * 商品品牌列表页面
	 */
	@RequiresPermissions("brand:brand:list")
	@RequestMapping(value = {"list", ""})
	public String list(Brand brand, Model model) {
		model.addAttribute("brand", brand);
		return "modules/brand/brandList";
	}
	
		/**
	 * 商品品牌列表数据
	 */
	@ResponseBody
	//@RequiresPermissions("brand:brand:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Brand brand, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Brand> page = brandService.findPage(new Page<Brand>(request, response), brand); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑商品品牌表单页面
	 */
	@RequiresPermissions(value={"brand:brand:view","brand:brand:add","brand:brand:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Brand brand, Model model) {
		model.addAttribute("brand", brand);
		return "modules/brand/brandForm";
	}

	/**
	 * 保存商品品牌
	 */
	@ResponseBody
	@RequiresPermissions(value={"brand:brand:add","brand:brand:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Brand brand, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(brand);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		brandService.save(brand);//保存
		j.setSuccess(true);
		j.setMsg("保存商品品牌成功");
		return j;
	}
	
	/**
	 * 删除商品品牌
	 */
	@ResponseBody
	@RequiresPermissions("brand:brand:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Brand brand) {
		AjaxJson j = new AjaxJson();
		brandService.delete(brand);
		j.setMsg("删除商品品牌成功");
		return j;
	}
	
	/**
	 * 批量删除商品品牌
	 */
	@ResponseBody
	@RequiresPermissions("brand:brand:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			brandService.delete(brandService.get(id));
		}
		j.setMsg("删除商品品牌成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("brand:brand:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Brand brand, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品品牌"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Brand> page = brandService.findPage(new Page<Brand>(request, response, -1), brand);
    		new ExportExcel("商品品牌", Brand.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出商品品牌记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("brand:brand:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Brand> list = ei.getDataList(Brand.class);
			for (Brand brand : list){
				try{
					brandService.save(brand);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条商品品牌记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条商品品牌记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入商品品牌失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入商品品牌数据模板
	 */
	@ResponseBody
	@RequiresPermissions("brand:brand:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品品牌数据导入模板.xlsx";
    		List<Brand> list = Lists.newArrayList(); 
    		new ExportExcel("商品品牌数据", Brand.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}