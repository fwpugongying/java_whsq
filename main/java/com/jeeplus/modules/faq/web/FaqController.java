/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.faq.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.faq.entity.Faq;
import com.jeeplus.modules.faq.service.FaqService;

/**
 * 常见问题Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/faq/faq")
public class FaqController extends BaseController {

	@Autowired
	private FaqService faqService;
	
	@ModelAttribute
	public Faq get(@RequestParam(required=false) String id) {
		Faq entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = faqService.get(id);
		}
		if (entity == null){
			entity = new Faq();
		}
		return entity;
	}
	
	/**
	 * 常见问题列表页面
	 */
	@RequiresPermissions("faq:faq:list")
	@RequestMapping(value = {"list", ""})
	public String list(Faq faq, Model model) {
		model.addAttribute("faq", faq);
		return "modules/faq/faqList";
	}
	
		/**
	 * 常见问题列表数据
	 */
	@ResponseBody
	@RequiresPermissions("faq:faq:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Faq faq, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Faq> page = faqService.findPage(new Page<Faq>(request, response), faq); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑常见问题表单页面
	 */
	@RequiresPermissions(value={"faq:faq:view","faq:faq:add","faq:faq:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Faq faq, Model model) {
		model.addAttribute("faq", faq);
		return "modules/faq/faqForm";
	}

	/**
	 * 保存常见问题
	 */
	@ResponseBody
	@RequiresPermissions(value={"faq:faq:add","faq:faq:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Faq faq, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(faq);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		faqService.save(faq);//保存
		j.setSuccess(true);
		j.setMsg("保存常见问题成功");
		return j;
	}
	
	/**
	 * 删除常见问题
	 */
	@ResponseBody
	@RequiresPermissions("faq:faq:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Faq faq) {
		AjaxJson j = new AjaxJson();
		faqService.delete(faq);
		j.setMsg("删除常见问题成功");
		return j;
	}
	
	/**
	 * 批量删除常见问题
	 */
	@ResponseBody
	@RequiresPermissions("faq:faq:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			faqService.delete(faqService.get(id));
		}
		j.setMsg("删除常见问题成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("faq:faq:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Faq faq, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "常见问题"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Faq> page = faqService.findPage(new Page<Faq>(request, response, -1), faq);
    		new ExportExcel("常见问题", Faq.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出常见问题记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("faq:faq:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Faq> list = ei.getDataList(Faq.class);
			for (Faq faq : list){
				try{
					faqService.save(faq);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条常见问题记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条常见问题记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入常见问题失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入常见问题数据模板
	 */
	@ResponseBody
	@RequiresPermissions("faq:faq:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "常见问题数据导入模板.xlsx";
    		List<Faq> list = Lists.newArrayList(); 
    		new ExportExcel("常见问题数据", Faq.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}