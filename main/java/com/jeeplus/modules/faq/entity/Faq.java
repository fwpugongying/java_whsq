/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.faq.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 常见问题Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Faq extends DataEntity<Faq> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 标题
	private String content;		// 内容
	private Integer sort;		// 排序
	
	public Faq() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Faq(String id){
		super(id);
	}

	@ExcelField(title="标题", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="内容", align=2, sort=2)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=3)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	/**
	 * 富文本路径
	 * @return
	 */
	public String getUrl() {
		return Global.getConfig("filePath") + "/display/faq?id=" + id;
	}
}