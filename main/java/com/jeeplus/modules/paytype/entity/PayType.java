/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.paytype.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 支付方式Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class PayType extends DataEntity<PayType> {
	
	private static final long serialVersionUID = 1L;
	private String type;		// 付款方式 1支付宝 2微信 3
	private String state;		// 状态 0启用 1停用
	private Integer sort;		// 排序
	
	public PayType() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public PayType(String id){
		super(id);
	}

	@ExcelField(title="付款方式 1支付宝 2微信 3", dictType="pay_type", align=2, sort=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="状态 0启用 1停用", dictType="pay_type_state", align=2, sort=2)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=3)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}