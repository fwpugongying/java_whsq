/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.paa.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.paa.entity.Paa;
import com.jeeplus.modules.paa.mapper.PaaMapper;
/**
 * 平台区域Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class PaaService extends CrudService<PaaMapper, Paa> {


	@Autowired

	public Paa get(String id) {
		return super.get(id);
	}

	public List<Paa> findList(Paa paa) {
		return super.findList(paa);
	}
	public List<Paa> findUniqueByProperty(Paa paa) {
		return super.findList(paa);
	}

	public Paa getOrderno(String orderno) {
		return findUniqueByProperty("orderno", orderno);
	}

	public Paa getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Paa> findPage(Page<Paa> page, Paa paa) {
		return super.findPage(page, paa);
	}
	@Transactional(readOnly = false)
	public void save(Paa paa) {
		super.save(paa);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Paa paa) {
		super.save(paa);

//		// 调用三方
//		List<WohuiEntity> list = Lists.newArrayList();
//		list.add(new WohuiEntity("OrderNO", paa.getOrderNo()));
//		JSONObject object = WohuiUtils.send(WohuiUtils.LLBTPay, list);
//		if (!"0000".equals(object.getString("respCode"))) {
//			logger.error("开通流量补贴订单号"+paa.getOrderNo()+"失败：" + object.getString("respMsg"));
//		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Paa paa) {
		super.delete(paa);
	}

}