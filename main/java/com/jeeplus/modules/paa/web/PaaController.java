/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.paa.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.paa.entity.Paa;
import com.jeeplus.modules.paa.service.PaaService;

/**
 * 平台区域订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/paa/paa")
public class PaaController extends BaseController {

	@Autowired
	private PaaService paaService;
	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Paa get(@RequestParam(required=false) String id) {
		Paa entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = paaService.get(id);
		}
		if (entity == null){
			entity = new Paa();
		}
		return entity;
	}
	
	/**
	 * 平台区域订单列表页面
	 */
	@RequiresPermissions("paa:paa:list")
	@RequestMapping(value = {"list", ""})
	public String list(Paa paa, Model model) {
		model.addAttribute("paa", paa);
		return "modules/paa/paaList";
	}
	
		/**
	 * 平台区域订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("paa:paa:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Paa paa, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Paa> page = paaService.findPage(new Page<Paa>(request, response), paa); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑平台区域订单表单页面
	 */
	@RequiresPermissions(value={"paa:paa:view","paa:paa:add","paa:paa:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Paa paa, Model model) {
		model.addAttribute("paa", paa);
		return "modules/paa/paaForm";
	}

	/**
	 * 保存平台区域订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"paa:paa:add","paa:paa:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Paa paa, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(paa);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		paaService.save(paa);//保存
		j.setSuccess(true);
		j.setMsg("保存平台区域订单成功");
		return j;
	}
	/**
	 * 冻结解冻平台区域订单
	 */
	@ResponseBody
	@RequiresPermissions(value="paa:paa:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Paa paa, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(paa.getId())) {
			paa.setState(type);
			paaService.save(paa);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除平台区域订单
	 */
	@ResponseBody
	@RequiresPermissions("paa:paa:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Paa paa) {
		AjaxJson j = new AjaxJson();
		paaService.delete(paa);
		j.setMsg("删除平台区域订单成功");
		return j;
	}
	
	/**
	 * 批量删除平台区域订单
	 */
	@ResponseBody
	@RequiresPermissions("paa:paa:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			paaService.delete(paaService.get(id));
		}
		j.setMsg("删除平台区域订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("paa:paa:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Paa paa, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "平台区域订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Paa> page = paaService.findPage(new Page<Paa>(request, response, -1), paa);
    		new ExportExcel("平台区域订单", Paa.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出平台区域订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("paa:paa:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Paa> list = ei.getDataList(Paa.class);
			for (Paa paa : list){
				try{
					paaService.save(paa);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条平台区域订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条平台区域订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入平台区域订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入平台区域订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("paa:paa:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "平台区域订单数据导入模板.xlsx";
    		List<Paa> list = Lists.newArrayList(); 
    		new ExportExcel("平台区域订单数据", Paa.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}