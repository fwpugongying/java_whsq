/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productsku.entity;

import com.jeeplus.modules.product.entity.Product;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品规格Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class ProductSku extends DataEntity<ProductSku> {
	
	private static final long serialVersionUID = 1L;
	private Product product;		// 商品
	private String content;		// 规格内容
	private Integer stock;		// 库存
	private String oldPrice;		// 市场价
	private String discount;		// 优惠券
	private String price;		// 券后价
	private String point;		// 铜板
	private String sort;		// 排序
	private String amount;		// 结算价
	private String image;		// 图片
	private String title;// 展示标题
	private String groupPrice;// 拼团价
	private String subsidyprice;        // 补贴价格
	private String openprice;        // 预售价格

	public ProductSku() {
		super();
	}

	public ProductSku(String id){
		super(id);
	}

	public String getSubsidyprice() {
		return subsidyprice;
	}
	public void setSubsidyprice(String subsidyprice) {
		this.subsidyprice = subsidyprice;
	}

	public String getOpenprice() {
		return openprice;
	}
	public void setOpenprice(String openprice) {
		this.openprice = openprice;
	}
	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=1)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ExcelField(title="规格内容", align=2, sort=2)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@NotNull(message="库存不能为空")
	@ExcelField(title="库存", align=2, sort=3)
	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}
	
	@ExcelField(title="市场价", align=2, sort=4)
	public String getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(String oldPrice) {
		this.oldPrice = oldPrice;
	}
	
	@ExcelField(title="优惠券", align=2, sort=5)
	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	@ExcelField(title="券后价", align=2, sort=6)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@ExcelField(title="铜板", align=2, sort=6)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	@ExcelField(title="排序", align=2, sort=2)
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}
	
	@ExcelField(title="结算价", align=2, sort=7)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="图片", align=2, sort=8)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public String getGroupPrice() {
		return groupPrice;
	}

	public void setGroupPrice(String groupPrice) {
		this.groupPrice = groupPrice;
	}

	private List<Specification> specificationList = Lists.newArrayList();

	public List<Specification> getSpecificationList() {
		return specificationList;
	}

	public void setSpecificationList(List<Specification> specificationList) {
		this.specificationList = specificationList;
	}

	public String getTitle() {
		List<Specification> list = JSONObject.parseArray(content, Specification.class);
		List<String> titleList = Lists.newArrayList();
		for (Specification specification : list) {
			titleList.add(specification.getValue());
		}
		title = StringUtils.join(titleList, ",");
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public List<Specification> getSpecificationListByContent() {
		if (StringUtils.isBlank(content)) {
			return new ArrayList<Specification>();
		}
		return JSONObject.parseArray(content, Specification.class);
	}
}

