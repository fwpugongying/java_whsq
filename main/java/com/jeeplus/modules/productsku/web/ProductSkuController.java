/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productsku.web;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.product.entity.ProductSkuname;
import com.jeeplus.modules.product.service.ProductService;
import com.jeeplus.modules.productsku.entity.ProductSku;
import com.jeeplus.modules.productsku.entity.Specification;
import com.jeeplus.modules.productsku.service.ProductSkuService;

/**
 * 商品规格Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/productsku/productSku")
public class ProductSkuController extends BaseController {
	@Autowired
	private ProductService productService;
	@Autowired
	private ProductSkuService productSkuService;
	
	@ModelAttribute
	public ProductSku get(@RequestParam(required=false) String id) {
		ProductSku entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = productSkuService.get(id);
		}
		if (entity == null){
			entity = new ProductSku();
		}
		return entity;
	}
	
	/**
	 * 商品规格列表页面
	 */
	@RequiresPermissions("productsku:productSku:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProductSku productSku, Model model) {
		model.addAttribute("productSku", productSku);
		return "modules/productsku/productSkuList";
	}
	
		/**
	 * 商品规格列表数据
	 */
	@ResponseBody
	@RequiresPermissions("productsku:productSku:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProductSku productSku, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProductSku> page = productSkuService.findPage(new Page<ProductSku>(request, response), productSku); 
		return getBootstrapData(page);
	}
	
	/**
	 * 商品规格列表数据
	 */
	@ResponseBody
	@RequiresPermissions("productsku:productSku:list")
	@RequestMapping(value = "dataByProductId")
	public Map<String, Object> dataByProductId(ProductSku productSku, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProductSku> page = new Page<ProductSku>(request, response);
		if (productSku.getProduct() != null && StringUtils.isNotBlank(productSku.getProduct().getId())) {
			page = productSkuService.findPage(new Page<ProductSku>(request, response), productSku); 
		}
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑商品规格表单页面
	 */
	@RequiresPermissions(value={"productsku:productSku:view","productsku:productSku:add","productsku:productSku:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProductSku productSku, String productId, Model model) {
		Product product = productService.get(productId);
		if (product == null) {
			product = new Product();
		}
		List<Map<String, String>> skuNameList = Lists.newArrayList();
		List<ProductSkuname> productSkunameList = product.getProductSkunameList();
		List<Specification> specificationList = Lists.newArrayList();
		if (StringUtils.isNotBlank(productSku.getContent())) {
			specificationList = JSONObject.parseArray(productSku.getContent(), Specification.class);
		}
		for (ProductSkuname productSkuname : productSkunameList) {
			Map<String, String> map = Maps.newHashMap();
			map.put("id", productSkuname.getId());
			map.put("title", productSkuname.getTitle());
			for (Specification specification : specificationList) {
				if (productSkuname.getId().equals(specification.getId())) {
					map.put("value", specification.getValue());
				}
			}
			skuNameList.add(map);
		}
		model.addAttribute("skuNameList", skuNameList);
		productSku.setProduct(product);
		model.addAttribute("productSku", productSku);
		return "modules/productsku/productSkuForm";
	}

	/**
	 * 保存商品规格
	 */
	@ResponseBody
	@RequiresPermissions(value={"productsku:productSku:add","productsku:productSku:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProductSku productSku, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		
		BigDecimal price = new BigDecimal(productSku.getOldPrice()).subtract(new BigDecimal(StringUtils.isNotBlank(productSku.getDiscount())?productSku.getDiscount():"0"));
		//BigDecimal price = new BigDecimal(productSku.getOldPrice()).subtract(new BigDecimal(productSku.getDiscount()));
		if (price.compareTo(BigDecimal.ZERO) < 1) {
			j.setSuccess(false);
			j.setMsg("市场价必须大于优惠券金额");
			return j;
		}
		productSku.setPrice(price.toString());
		if (StringUtils.isBlank(productSku.getId())) {
			productSku.setGroupPrice(productSku.getPrice());
			productSku.setSubsidyprice(productSku.getPrice());
			productSku.setOpenprice(productSku.getPrice());
			//productSku.setAmount("0");
			productSku.setDiscount("0");
//			productSku.setPoint("0");
		}
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(productSku);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		
		productSku.setContent(JSONObject.toJSONString(productSku.getSpecificationList()));
		//新增或编辑表单保存
		productSkuService.save(productSku);//保存
		j.setSuccess(true);
		j.setMsg("保存商品规格成功");
		return j;
	}
	
	/**
	 * 删除商品规格
	 */
	@ResponseBody
	@RequiresPermissions("productsku:productSku:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProductSku productSku) {
		AjaxJson j = new AjaxJson();
		productSkuService.delete(productSku);
		j.setMsg("删除商品规格成功");
		return j;
	}
	
	/**
	 * 批量删除商品规格
	 */
	@ResponseBody
	@RequiresPermissions("productsku:productSku:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			productSkuService.delete(productSkuService.get(id));
		}
		j.setMsg("删除商品规格成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("productsku:productSku:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProductSku productSku, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品规格"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProductSku> page = productSkuService.findPage(new Page<ProductSku>(request, response, -1), productSku);
    		new ExportExcel("商品规格", ProductSku.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出商品规格记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("productsku:productSku:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProductSku> list = ei.getDataList(ProductSku.class);
			for (ProductSku productSku : list){
				try{
					productSkuService.save(productSku);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条商品规格记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条商品规格记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入商品规格失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入商品规格数据模板
	 */
	@ResponseBody
	@RequiresPermissions("productsku:productSku:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品规格数据导入模板.xlsx";
    		List<ProductSku> list = Lists.newArrayList(); 
    		new ExportExcel("商品规格数据", ProductSku.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}