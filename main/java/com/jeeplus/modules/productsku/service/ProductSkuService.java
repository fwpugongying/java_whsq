/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productsku.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.productsku.entity.ProductSku;
import com.jeeplus.modules.productsku.mapper.ProductSkuMapper;

/**
 * 商品规格Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class ProductSkuService extends CrudService<ProductSkuMapper, ProductSku> {

	public ProductSku get(String id) {
		return super.get(id);
	}
	
	public List<ProductSku> findList(ProductSku productSku) {
		return super.findList(productSku);
	}
	
	public Page<ProductSku> findPage(Page<ProductSku> page, ProductSku productSku) {
		return super.findPage(page, productSku);
	}
	
	@Transactional(readOnly = false)
	public void save(ProductSku productSku) {
		super.save(productSku);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProductSku productSku) {
		super.delete(productSku);
	}
	
}