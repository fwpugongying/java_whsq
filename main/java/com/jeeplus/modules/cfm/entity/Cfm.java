/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cfm.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import java.util.Date;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * Cfm开通申请Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Cfm extends DataEntity<Cfm> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String goodsnum;		// 商品数量
	private String uid;		// huiyuan
	private String money;		// 支付金额
	private Date createDate;		// 申请时间
	private Date payDate;		// 支付时间
	private Date finishdate;		// 完成时间
	private Date canceldate;		// 取消时间
	private String state;		// 状态
	private String payType;		// 付款方式 1支付宝 2微信
	private String month;		// 开通月份数

	public Cfm() {
		super();
	}

	public Cfm(String id){
		super(id);
	}

	@ExcelField(title="订单号", align=2, sort=1)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}

	@ExcelField(title="付款方式", dictType="pay_type", align=2, sort=18)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@ExcelField(title="uid", align=2, sort=2)
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}
	@ExcelField(title="商品数量", align=2, sort=2)
	public String getGoodsnum() {
		return goodsnum;
	}

	public void setGoodsnum(String goodsnum) {
		this.goodsnum = goodsnum;
	}
	
	@ExcelField(title="支付金额", align=2, sort=3)
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}
	

	@ExcelField(title="付款时间", align=2, sort=17)
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	@ExcelField(title="完成时间", align=2, sort=7)
	public Date getFinishdate() {
		return finishdate;
	}
	public void setFinishdate(Date finishdate) {
		this.finishdate = finishdate;
	}
	@ExcelField(title="取消时间", align=2, sort=8)
	public Date getCanceldate() {
		return canceldate;
	}
	public void setCanceldate(Date canceldate) {
		this.canceldate = canceldate;
	}
	@ExcelField(title="状态  0待支付 1已支付 2已取消", dictType="member_state", align=2, sort=18)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}