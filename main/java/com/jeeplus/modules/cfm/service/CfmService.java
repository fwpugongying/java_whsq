/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cfm.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.cfm.entity.Cfm;
import com.jeeplus.modules.cfm.mapper.CfmMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class CfmService extends CrudService<CfmMapper, Cfm> {


	@Autowired

	public Cfm get(String id) {
		return super.get(id);
	}

	public List<Cfm> findList(Cfm cfm) {
		return super.findList(cfm);
	}
	public List<Cfm> findUniqueByProperty(Cfm cfm) {
		return super.findList(cfm);
	}
	public Cfm getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Cfm> findPage(Page<Cfm> page, Cfm cfm) {
		return super.findPage(page, cfm);
	}
	@Transactional(readOnly = false)
	public void save(Cfm cfm) {
		super.save(cfm);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Cfm cfm) {
		super.save(cfm);
		String PayMethod="10";
		if("1".equals(cfm.getPayType())){//支付宝
			PayMethod="40";
		}else if("2".equals(cfm.getPayType())){//微信
			PayMethod="30";
		}else if("3".equals(cfm.getPayType())){//平台购物券支付
			PayMethod="10";
		}else if("4".equals(cfm.getPayType())){//商城购物券支付
			PayMethod="20";
		}

		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("CardGUID", cfm.getMember().getId()));
		list.add(new WohuiEntity("Margin", cfm.getMoney()));
		JSONObject object = WohuiUtils.send(WohuiUtils.CFMOpen, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("VIP支付"+"失败：" + object.getString("respMsg"));
		}
	}

	@Transactional(readOnly = false)
	public void delete(Cfm cfm) {
		super.delete(cfm);
	}

}