/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cfm.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.cfm.entity.Cfm;
import com.jeeplus.modules.cfm.service.CfmService;

/**
 * 订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/cfm/cfm")
public class CfmController extends BaseController {

	@Autowired
	private CfmService cfmService;
	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Cfm get(@RequestParam(required=false) String id) {
		Cfm entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = cfmService.get(id);
		}
		if (entity == null){
			entity = new Cfm();
		}
		return entity;
	}
	
	/**
	 * 订单列表页面
	 */
	@RequiresPermissions("cfm:cfm:list")
	@RequestMapping(value = {"list", ""})
	public String list(Cfm cfm, Model model) {
		model.addAttribute("cfm", cfm);
		return "modules/cfm/cfmList";
	}
	
		/**
	 * 订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("cfm:cfm:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Cfm cfm, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Cfm> page = cfmService.findPage(new Page<Cfm>(request, response), cfm);
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑订单表单页面
	 */
	@RequiresPermissions(value={"cfm:cfm:view","cfm:cfm:add","cfm:cfm:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Cfm cfm, Model model) {
		model.addAttribute("cfm", cfm);
		return "modules/cfm/cfmForm";
	}

	/**
	 * 保存订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"cfm:cfm:add","cfm:cfm:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Cfm cfm, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(cfm);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		cfmService.save(cfm);//保存
		j.setSuccess(true);
		j.setMsg("保存流量补贴订单成功");
		return j;
	}
	/**
	 * 冻结解冻订单
	 */
	@ResponseBody
	@RequiresPermissions(value="cfm:cfm:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Cfm cfm, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(cfm.getId())) {
			cfm.setState(type);
			cfmService.save(cfm);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除订单
	 */
	@ResponseBody
	@RequiresPermissions("cfm:cfm:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Cfm cfm) {
		AjaxJson j = new AjaxJson();
		cfmService.delete(cfm);
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 批量删除订单
	 */
	@ResponseBody
	@RequiresPermissions("cfm:cfm:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			cfmService.delete(cfmService.get(id));
		}
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("cfm:cfm:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Cfm cfm, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Cfm> page = cfmService.findPage(new Page<Cfm>(request, response, -1), cfm);
    		new ExportExcel("流量补贴订单", Cfm.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出流量补贴订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("cfm:cfm:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Cfm> list = ei.getDataList(Cfm.class);
			for (Cfm cfm : list){
				try{
					cfmService.save(cfm);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条流量补贴订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条流量补贴订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入流量补贴订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入流量补贴订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("cfm:cfm:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单数据导入模板.xlsx";
    		List<Cfm> list = Lists.newArrayList(); 
    		new ExportExcel("流量补贴订单数据", Cfm.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}