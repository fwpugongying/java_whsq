/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.groupbill.entity;

import com.jeeplus.modules.group.entity.Group;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 实体店账单Entity
 * @author lixinapp
 * @version 2019-10-17
 */
public class GroupBill extends DataEntity<GroupBill> {
	
	private static final long serialVersionUID = 1L;
	private Group group;		// 商家
	private String title;		// 标题
	private String amount;		// 金额
	private String type;		// 类型 0支出 1收入
	private String orderId;		// 订单号
	private String content;		// 描述
	private Integer qty;		// 数量
	private String username;	// 收货人
	private String phone;		// 收货电话
	private Date finishDate;//订单完成时间
	public GroupBill() {
		super();
	}

	public GroupBill(String id){
		super(id);
	}

	@NotNull(message="商家不能为空")
	@ExcelField(title="商家", fieldType=Group.class, value="group.title", align=2, sort=1)
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}
	
	@ExcelField(title="标题", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@ExcelField(title="金额", align=2, sort=3)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="类型 0支出 1收入", dictType="bill_type", align=2, sort=4)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
}