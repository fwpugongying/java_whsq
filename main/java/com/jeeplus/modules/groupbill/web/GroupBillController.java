/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.groupbill.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.groupbill.entity.GroupBill;
import com.jeeplus.modules.groupbill.service.GroupBillService;

/**
 * 实体店账单Controller
 * @author lixinapp
 * @version 2019-10-17
 */
@Controller
@RequestMapping(value = "${adminPath}/groupbill/groupBill")
public class GroupBillController extends BaseController {

	@Autowired
	private GroupBillService groupBillService;
	
	@ModelAttribute
	public GroupBill get(@RequestParam(required=false) String id) {
		GroupBill entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = groupBillService.get(id);
		}
		if (entity == null){
			entity = new GroupBill();
		}
		return entity;
	}
	
	/**
	 * 实体店账单列表页面
	 */
	@RequiresPermissions("groupbill:groupBill:list")
	@RequestMapping(value = {"list", ""})
	public String list(GroupBill groupBill, Model model) {
		model.addAttribute("groupBill", groupBill);
		return "modules/groupbill/groupBillList";
	}
	
		/**
	 * 实体店账单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("groupbill:groupBill:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(GroupBill groupBill, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<GroupBill> page = groupBillService.findPage(new Page<GroupBill>(request, response), groupBill); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑实体店账单表单页面
	 */
	@RequiresPermissions(value={"groupbill:groupBill:view","groupbill:groupBill:add","groupbill:groupBill:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(GroupBill groupBill, Model model) {
		model.addAttribute("groupBill", groupBill);
		return "modules/groupbill/groupBillForm";
	}

	/**
	 * 保存实体店账单
	 */
	@ResponseBody
	@RequiresPermissions(value={"groupbill:groupBill:add","groupbill:groupBill:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(GroupBill groupBill, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(groupBill);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		groupBillService.save(groupBill);//保存
		j.setSuccess(true);
		j.setMsg("保存实体店账单成功");
		return j;
	}
	
	/**
	 * 删除实体店账单
	 */
	@ResponseBody
	@RequiresPermissions("groupbill:groupBill:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(GroupBill groupBill) {
		AjaxJson j = new AjaxJson();
		groupBillService.delete(groupBill);
		j.setMsg("删除实体店账单成功");
		return j;
	}
	
	/**
	 * 批量删除实体店账单
	 */
	@ResponseBody
	@RequiresPermissions("groupbill:groupBill:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			groupBillService.delete(groupBillService.get(id));
		}
		j.setMsg("删除实体店账单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("groupbill:groupBill:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(GroupBill groupBill, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店账单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<GroupBill> page = groupBillService.findPage(new Page<GroupBill>(request, response, -1), groupBill);
    		new ExportExcel("实体店账单", GroupBill.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出实体店账单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("groupbill:groupBill:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<GroupBill> list = ei.getDataList(GroupBill.class);
			for (GroupBill groupBill : list){
				try{
					groupBillService.save(groupBill);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条实体店账单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条实体店账单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入实体店账单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入实体店账单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("groupbill:groupBill:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店账单数据导入模板.xlsx";
    		List<GroupBill> list = Lists.newArrayList(); 
    		new ExportExcel("实体店账单数据", GroupBill.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}