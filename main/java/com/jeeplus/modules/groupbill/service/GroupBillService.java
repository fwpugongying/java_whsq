/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.groupbill.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.groupbill.entity.GroupBill;
import com.jeeplus.modules.groupbill.mapper.GroupBillMapper;

/**
 * 实体店账单Service
 * @author lixinapp
 * @version 2019-10-17
 */
@Service
@Transactional(readOnly = true)
public class GroupBillService extends CrudService<GroupBillMapper, GroupBill> {

	public GroupBill get(String id) {
		return super.get(id);
	}
	
	public List<GroupBill> findList(GroupBill groupBill) {
		return super.findList(groupBill);
	}
	
	public Page<GroupBill> findPage(Page<GroupBill> page, GroupBill groupBill) {
		return super.findPage(page, groupBill);
	}
	
	@Transactional(readOnly = false)
	public void save(GroupBill groupBill) {
		super.save(groupBill);
	}
	
	@Transactional(readOnly = false)
	public void delete(GroupBill groupBill) {
		super.delete(groupBill);
	}
	
}