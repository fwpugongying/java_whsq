/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.vip.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.vip.entity.Vip;
import com.jeeplus.modules.vip.mapper.VipMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class VipService extends CrudService<VipMapper, Vip> {


	@Autowired

	public Vip get(String id) {
		return super.get(id);
	}

	public List<Vip> findList(Vip vip) {
		return super.findList(vip);
	}
	public List<Vip> findUniqueByProperty(Vip vip) {
		return super.findList(vip);
	}
	public Vip getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Vip> findPage(Page<Vip> page, Vip vip) {
		return super.findPage(page, vip);
	}
	@Transactional(readOnly = false)
	public void save(Vip vip) {
		super.save(vip);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Vip vip) {
		super.save(vip);
		String PayMethod="10";
		if("1".equals(vip.getPayType())){//支付宝
			PayMethod="40";
		}else if("2".equals(vip.getPayType())){//微信
			PayMethod="30";
		}else if("3".equals(vip.getPayType())){//平台购物券支付
			PayMethod="10";
		}else if("4".equals(vip.getPayType())){//商城购物券支付
			PayMethod="20";
		}

		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("OrderNO", vip.getId()));
		list.add(new WohuiEntity("CardGUID", vip.getMember().getId()));
		JSONObject object = WohuiUtils.send(WohuiUtils.BHVIPOpen, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("VIP支付"+"失败：" + object.getString("respMsg"));
		}
	}

	@Transactional(readOnly = false)
	public void delete(Vip vip) {
		super.delete(vip);
	}

}