/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.customer.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.customer.entity.Customer;

/**
 * 客服中心MAPPER接口
 * @author lixinapp
 * @version 2019-11-25
 */
@MyBatisMapper
public interface CustomerMapper extends BaseMapper<Customer> {
	
}