/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.groupcash.service;

import java.util.List;

import com.jeeplus.modules.group.entity.Group;
import com.jeeplus.modules.group.service.GroupService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
//import com.jeeplus.modules.group.service.GroupService;
import com.jeeplus.modules.groupcash.entity.GroupCash;
import com.jeeplus.modules.groupcash.mapper.GroupCashMapper;

/**
 * 厂家提现Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class GroupCashService extends CrudService<GroupCashMapper, GroupCash> {
	@Autowired
	private GroupService GroupService;
	
	public GroupCash get(String id) {
		return super.get(id);
	}
	
	public List<GroupCash> findList(GroupCash groupCash) {
		return super.findList(groupCash);
	}
	
	public Page<GroupCash> findPage(Page<GroupCash> page, GroupCash groupCash) {
		return super.findPage(page, groupCash);
	}
	
	@Transactional(readOnly = false)
	public void save(GroupCash groupCash) {
		super.save(groupCash);
	}
	
	@Transactional(readOnly = false)
	public void delete(GroupCash groupCash) {
		super.delete(groupCash);
	}
	
	/**
	 * 审核
	 * @param groupCash
	 */
	@Transactional(readOnly = false)
	public void audit(GroupCash groupCash) {
		super.save(groupCash);
		
		//处理拒绝
		if ("2".equals(groupCash.getState())) {
			// 加钱
			GroupService.updateBalance(groupCash.getGroup(), "提现拒绝", groupCash.getAmount(), "1", null, null, null, null, null);
		}
	}

	/**
	 * 申请提现
	 * @param groupCash
	 */
	@Transactional(readOnly = false)
	public void add(GroupCash groupCash) {
		super.save(groupCash);
		// 减钱
		GroupService.updateBalance(groupCash.getGroup(), groupCash.getBank() + "-" + groupCash.getAccount().substring(groupCash.getAccount().length() - 4), groupCash.getAmount(), "0", null, null, null, null, null);
	}
	
}