package com.jeeplus.modules.monitor.task;

import com.jeeplus.modules.shopbusorder.service.ShopbusOrderService;
import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.shopbusorder.entity.ShopbusOrder;
import com.jeeplus.modules.monitor.entity.Task;

/**
 * 取消超时未支付的优惠券订单
 * @author Administrator
 *
 */
@DisallowConcurrentExecution
public class CancelShopbusOrderTask extends Task {
	private static ShopbusOrderService shopbusorderService = SpringContextHolder.getBean(ShopbusOrderService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);

	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		ShopbusOrder shopbusorder = new ShopbusOrder();
		shopbusorder.setState("0");
		shopbusorder.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 24 HOUR) ");
		while (true) {
			Page<ShopbusOrder> page = shopbusorderService.findPage(new Page<ShopbusOrder>(1, 100), shopbusorder);
			if (!page.getList().isEmpty()) {
				for (ShopbusOrder order : page.getList()) {
					order.setState("5");
					order.setCancelDate(DateUtils.addHours(order.getCreateDate(), 24));
					shopbusorderService.save(order);
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}
}
