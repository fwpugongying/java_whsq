package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.proxyorder.service.ProxyOrderService;

/**
 * 取消超时未支付的单品代理申请单
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelProxyOrderTask extends Task {
	private static ProxyOrderService proxyOrderService = SpringContextHolder.getBean(ProxyOrderService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		proxyOrderService.cancel();
	}
}
