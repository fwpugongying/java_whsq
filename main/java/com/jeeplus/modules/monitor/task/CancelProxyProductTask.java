package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.proxyproduct.service.ProxyProductService;

/**
 * 取消超时未支付的增选单品代理产品订单
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelProxyProductTask extends Task {
	private static ProxyProductService proxyProductService = SpringContextHolder.getBean(ProxyProductService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		proxyProductService.cancel();
	}
}
