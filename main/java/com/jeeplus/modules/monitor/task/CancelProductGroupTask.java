package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.productgroup.entity.ProductGroup;
import com.jeeplus.modules.productgroup.service.ProductGroupService;

/**
 * 取消未成功的拼团
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelProductGroupTask extends Task {
	private static ProductGroupService productGroupService = SpringContextHolder.getBean(ProductGroupService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		ProductGroup productGroup = new ProductGroup();
		productGroup.setState("0");
		productGroup.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 48 HOUR) ");
		while (true) {
			Page<ProductGroup> page = productGroupService.findPage(new Page<ProductGroup>(1, 100), productGroup);
			if (!page.getList().isEmpty()) {
				for (ProductGroup group : page.getList()) {
					group.setState("2");
					group.setEndDate(DateUtils.addHours(group.getCreateDate(), 48));
					productGroupService.save(group);
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}
}
