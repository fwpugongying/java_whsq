package com.jeeplus.modules.monitor.task;

import java.io.IOException;

import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.monitor.utils.MySQLBackupUtil;

/**
 * 定时备份数据库
 * 
 * @author mall
 *
 */
@DisallowConcurrentExecution
public class BackupTask extends Task {

	@Override
	public void run() {
		try {
			MySQLBackupUtil.startBackUp();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
