package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.ordertask.entity.Ordertask;
import com.jeeplus.modules.ordertask.service.OrdertaskService;

import java.math.BigDecimal;

/**
 * 取消未成功的任务
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelOrdertask extends Task {
	private static OrdertaskService ordertaskService = SpringContextHolder.getBean(OrdertaskService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		Ordertask ordertask = new Ordertask();
		ordertask.setState("0");
		ordertask.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 24 HOUR) ");
		while (true) {
			Page<Ordertask> page = ordertaskService.findPage(new Page<Ordertask>(1, 100), ordertask);
			if (!page.getList().isEmpty()) {
				for (Ordertask task : page.getList()) {
					if(new BigDecimal(task.getNum()).compareTo(new BigDecimal(5)) > -1){
						task.setState("3");
						task.setFinishdate(DateUtils.addHours(task.getCreatedate(), 24));
						ordertaskService.save(task);
					}
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}
}
