package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.couponorder.entity.CouponOrder;
import com.jeeplus.modules.couponorder.service.CouponOrderService;
import com.jeeplus.modules.monitor.entity.Task;

/**
 * 取消超时未支付的优惠券订单
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelCouponOrderTask extends Task {
	private static CouponOrderService couponOrderService = SpringContextHolder.getBean(CouponOrderService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		CouponOrder couponOrder = new CouponOrder();
		couponOrder.setState("0");
		couponOrder.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 24 HOUR) ");
		while (true) {
			Page<CouponOrder> page = couponOrderService.findPage(new Page<CouponOrder>(1, 100), couponOrder);
			if (!page.getList().isEmpty()) {
				for (CouponOrder order : page.getList()) {
					order.setState("5");
					order.setCancelDate(DateUtils.addHours(order.getCreateDate(), 24));
					couponOrderService.save(order);
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}
}
