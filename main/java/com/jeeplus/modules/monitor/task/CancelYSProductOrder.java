package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.productorder.entity.ProductOrder;
import com.jeeplus.modules.productorder.service.ProductOrderService;

/**
 * 取消云店未付款订单
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelYSProductOrder extends Task {
	private static ProductOrderService productOrderService = SpringContextHolder.getBean(ProductOrderService.class);

	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	private static Logger logger = LoggerFactory.getLogger(CancelYSProductOrder.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		// 云店订单
		ProductOrder productOrder = new ProductOrder();
		productOrder.setState("8");
		productOrder.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 15 DAY) ");
		int pageNo = 1;
		while (true) {
			Page<ProductOrder> page = productOrderService.findPage(new Page<ProductOrder>(pageNo, 100), productOrder);
			if (!page.getList().isEmpty()) {
				for (ProductOrder order : page.getList()) {
					order.setState("1");
					try {
						productOrderService.saveYStask(order);
					} catch (Exception e) {
						logger.error("云店订单" + order.getId() + "预售完成：" + e.getMessage());
					}
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
			pageNo++;
		}
	}
}
