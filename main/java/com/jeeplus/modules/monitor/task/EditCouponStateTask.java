package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.shopcoupon.entity.ShopCoupon;
import com.jeeplus.modules.shopcoupon.service.ShopCouponService;

/**
 * 修改实体店铺优惠券过期状态
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class EditCouponStateTask extends Task {
	private static ShopCouponService shopCouponService = SpringContextHolder.getBean(ShopCouponService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		ShopCoupon shopCoupon = new ShopCoupon();
		shopCoupon.setState("1");
		shopCoupon.setDataScope(" AND a.end_date < NOW() ");
		while (true) {
			Page<ShopCoupon> page = shopCouponService.findPage(new Page<ShopCoupon>(1, 100), shopCoupon);
			if (!page.getList().isEmpty()) {
				for (ShopCoupon coupon : page.getList()) {
					coupon.setState("2");
					shopCouponService.save(coupon);
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}
}
