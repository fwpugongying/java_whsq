package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.productorder.entity.ProductOrder;
import com.jeeplus.modules.productorder.service.ProductOrderService;

/**
 * 取消云店未付款订单
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelProductOrderTask extends Task {
	private static ProductOrderService productOrderService = SpringContextHolder.getBean(ProductOrderService.class);

	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	private static Logger logger = LoggerFactory.getLogger(CancelProductOrderTask.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		// 云店订单
		ProductOrder productOrder = new ProductOrder();
		productOrder.setState("0");
		productOrder.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 24 HOUR) ");
		int pageNo = 1;
		while (true) {
			Page<ProductOrder> page = productOrderService.findPage(new Page<ProductOrder>(pageNo, 100), productOrder);
			if (!page.getList().isEmpty()) {
				for (ProductOrder order : page.getList()) {
					order.setState("5");
					order.setCancelReason("超时未付款");
					order.setCancelDate(DateUtils.addHours(order.getCreateDate(), 24));
					try {
						productOrderService.cancel(order);
					} catch (Exception e) {
						logger.error("云店订单" + order.getId() + "取消失败：" + e.getMessage());
					}
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
			pageNo++;
		}
	}
}
