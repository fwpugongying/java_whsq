package com.jeeplus.modules.monitor.task;

import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;

import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.couponorder.entity.CouponOrder;
import com.jeeplus.modules.couponorder.service.CouponOrderService;
import com.jeeplus.modules.monitor.entity.Task;

/**
 * 处理超时未使用优惠券订单
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class FinishCouponOrderTask extends Task {
	private static CouponOrderService couponOrderService = SpringContextHolder.getBean(CouponOrderService.class);
	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	
	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		
		CouponOrder couponOrder = new CouponOrder();
		couponOrder.setState("1");
		couponOrder.setDataScope(" AND a.end_date < NOW() ");
		while (true) {
			Page<CouponOrder> page = couponOrderService.findPage(new Page<CouponOrder>(1, 100), couponOrder);
			if (!page.getList().isEmpty()) {
				for (CouponOrder order : page.getList()) {
					order.setState("3");
					couponOrderService.save(order);
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}
}
