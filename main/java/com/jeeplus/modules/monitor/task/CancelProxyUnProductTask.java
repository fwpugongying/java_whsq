package com.jeeplus.modules.monitor.task;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.log.ThrottledSlf4jLogger;
import com.jeeplus.modules.api.service.ApiService;
import com.jeeplus.modules.api.utils.MiaoyouquanUtils;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import com.jeeplus.modules.monitor.entity.Task;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.product.service.ProductService;
import com.jeeplus.modules.proxyproduct.service.ProxyProductService;
import org.apache.shiro.util.ThreadContext;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * 下架单品代理产品
 * @author Administrator
 *
 */
@DisallowConcurrentExecution  
public class CancelProxyUnProductTask extends Task {
	private static ProxyProductService proxyProductService = SpringContextHolder.getBean(ProxyProductService.class);
	private static ProductService productService = SpringContextHolder.getBean(ProductService.class);
	private static ApiService apiService = SpringContextHolder.getBean(ApiService.class);

	private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder.getBean(org.apache.shiro.mgt.SecurityManager.class);
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run() {
		if (ThreadContext.getSecurityManager() == null) {
			ThreadContext.bind(MANAGER);
		}
		//获取所有的单品代理商品
		int PageIndex=1;//第几页
		int PageRows=20;//每页多少条
		while (true) {
			List<WohuiEntity> xjParam = Lists.newArrayList();//下架
			JSONObject info;
			List<WohuiEntity> param = Lists.newArrayList();//获取数据
			param.add(new WohuiEntity("PageIndex", String.valueOf(PageIndex)));//第几页
			param.add(new WohuiEntity("PageRows", String.valueOf(PageRows)));//多少条数据
			try {
				Thread.sleep(100);//设置暂停的时间 1 秒
			} catch (InterruptedException e2) {
				e2.printStackTrace();
			}
			JSONObject list = WohuiUtils.send(WohuiUtils.GetMSMGoodsDisList, param);
			int numsss=PageIndex*PageRows;
			int ss=Integer.valueOf(list.getString("totalCount"));
			if(PageIndex*PageRows>=Integer.valueOf(list.getString("totalCount"))){
				break;
			}
			if ("0000".equals(list.getString("respCode"))) {
				JSONArray dataArrays=list.getJSONArray("data");//获取所有的产品数据
				if(dataArrays==null){
					break;
				}
				for(int j = 0; j < dataArrays.size(); ++j) {
					JSONObject a = dataArrays.getJSONObject(j);
					String goodsid = a.getString("MSMG_GoodsID");
					String Source = a.getString("MSMG_Source");//来源
					//云店商品是否上架判断
					if("YD".equals(Source)){
						Product product = productService.get(goodsid);
						if (product == null || !"0".equals(product.getState())) {
							//下架接口
							xjParam.add(new WohuiEntity("Source",Source));//来源
							xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
							info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
							if (!"0000".equals(info.getString("respCode"))) {
								//下架成功
								System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
								System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
							}else{
								System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
								logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

							}
						}
					}else if("ALI".equals(Source)){
						Map<String, String> params = Maps.newHashMap();
						params.put("tao_id", goodsid);
						try {
							Thread.sleep(100);//设置暂停的时间 1 秒
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
						JSONObject object = MiaoyouquanUtils.zetaokeAPI(MiaoyouquanUtils.api_detail, params);
						if ("301".equals(object.getString("status"))) {
							//下架接口
							xjParam.add(new WohuiEntity("Source",Source));//来源
							xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
							info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
							if (!"0000".equals(info.getString("respCode"))) {
								//下架成功
								System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
								logger.error("下架成功goodsid："+goodsid+"来源"+Source);

							}else{
								System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
								logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

							}
						}
					}else if("JD".equals(Source)){
						Map<String, String> params = Maps.newHashMap();
						params.put("skuIds", goodsid);
						try {
							Thread.sleep(100);//设置暂停的时间 1 秒
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
						JSONObject jsonObject = MiaoyouquanUtils.miaoyouquanAPI(MiaoyouquanUtils.getJdUnionItems, params);
						if ("200".equals(jsonObject.getString("code"))){
							JSONArray dataArray = jsonObject.getJSONObject("data").getJSONArray("list");
							List<Map<String, Object>> categoryList = Lists.newArrayList();
							if (!dataArray.isEmpty()) {
								for(int i = 0; i < dataArray.size(); ++i) {
									JSONObject c = dataArray.getJSONObject(i);
									JSONObject data = c.getJSONObject("commissionInfo");
									categoryList.add(data);
								}
								Object commissionShare = ((Map)categoryList.get(0)).get("commissionShare");
								if (StringUtils.isBlank(String.valueOf(commissionShare))) {
									//下架接口
									xjParam.add(new WohuiEntity("Source",Source));//来源
									xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
									info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
									if (!"0000".equals(info.getString("respCode"))) {
										//下架成功
										System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
										logger.error("下架成功goodsid："+goodsid+"来源"+Source);

									}else{
										System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
										logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

									}
								}
							}else{
								//下架接口
								xjParam.add(new WohuiEntity("Source",Source));//来源
								xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
								info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
								if (!"0000".equals(info.getString("respCode"))) {
									//下架成功
									System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
									logger.error("下架成功goodsid："+goodsid+"来源"+Source);

								}else{
									System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
									logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

								}
							}
						}else{
							//下架接口
							xjParam.add(new WohuiEntity("Source",Source));//来源
							xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
							info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
							if (!"0000".equals(info.getString("respCode"))) {
								//下架成功
								System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
								logger.error("下架成功goodsid："+goodsid+"来源"+Source);

							}else{
								System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
								logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

							}
						}
					}else if("PDD".equals(Source)){
						//拼多多商品判断
						Map<String, String> params = Maps.newHashMap();
						params = Maps.newHashMap();
						String goods_sign="";//暂时没用
						params.put("goods_sign", "[" + goods_sign + "]");
						try {
							Thread.sleep(100);//设置暂停的时间 1 秒
						} catch (InterruptedException e2) {
							e2.printStackTrace();
						}
						JSONObject jsonObject = MiaoyouquanUtils.miaoyouquanAPI(MiaoyouquanUtils.getGoodsDetailInfo, params);
						if ("200".equals(jsonObject.getString("code")) ) {
							if (!jsonObject.getJSONArray("data").isEmpty()) {
								String rate = jsonObject.getJSONArray("data").getJSONObject(0).getString("promotion_rate");
								if (StringUtils.isBlank(rate)) {
									//下架接口
									xjParam.add(new WohuiEntity("Source",Source));//来源
									xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
									info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
									if (!"0000".equals(info.getString("respCode"))) {
										//下架成功
										System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
										logger.error("下架成功goodsid："+goodsid+"来源"+Source);

									}else{
										System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
										logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

									}
								}
							}else{
								//下架接口
								xjParam.add(new WohuiEntity("Source",Source));//来源
								xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
								info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
								if (!"0000".equals(info.getString("respCode"))) {
									//下架成功
									System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
									logger.error("下架成功goodsid："+goodsid+"来源"+Source);

								}else{
									System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
									logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

								}
							}
						}else{
							//下架接口
							xjParam.add(new WohuiEntity("Source",Source));//来源
							xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
							info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
							if (!"0000".equals(info.getString("respCode"))) {
								//下架成功
								System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
								logger.error("下架成功goodsid："+goodsid+"来源"+Source);

							}else{
								System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
								logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

							}
						}
					}	
				}
				PageIndex++;
			}else{
				break;
			}
		}
	}
}
