/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.group.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.group.entity.Group;

/**
 * 实体商家MAPPER接口
 * @author lixinapp
 * @version 2019-10-16
 */
@MyBatisMapper
public interface GroupMapper extends BaseMapper<Group> {

	List<Map<String, Object>> groupOption(@Param("groupId")String groupId, @Param("beginDate")String beginDate, @Param("endDate")String endDate);
	
}