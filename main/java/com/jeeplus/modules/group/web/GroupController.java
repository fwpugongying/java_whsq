/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.group.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.group.entity.Group;
import com.jeeplus.modules.group.service.GroupService;

/**
 * 实体商家Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/group/group")
public class GroupController extends BaseController {

	@Autowired
	private GroupService groupService;
	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Group get(@RequestParam(required=false) String id) {
		Group entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = groupService.get(id);
		}
		if (entity == null){
			entity = new Group();
		}
		return entity;
	}
	
	/**
	 * 实体商家列表页面
	 */
	@RequiresPermissions("group:group:list")
	@RequestMapping(value = {"list", ""})
	public String list(Group group, Model model) {
		model.addAttribute("group", group);
		return "modules/group/groupList";
	}
	
		/**
	 * 实体商家列表数据
	 */
	@ResponseBody
	@RequiresPermissions("group:group:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Group group, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Group> page = groupService.findPage(new Page<Group>(request, response), group); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑实体商家表单页面
	 */
	@RequiresPermissions(value={"group:group:view","group:group:add","group:group:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Group group, Model model) {
		model.addAttribute("group", group);
		return "modules/group/groupForm";
	}

	/**
	 * 保存实体商家
	 */
	@ResponseBody
	@RequiresPermissions(value={"group:group:add","group:group:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Group group, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(group);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		groupService.save(group);//保存
		j.setSuccess(true);
		j.setMsg("保存实体商家成功");
		return j;
	}
	/**
	 * 审核实体商家
	 */
	@ResponseBody
	@RequiresPermissions(value="group:group:edit")
	@RequestMapping(value = "audit")
	public AjaxJson audit(Group group, String type, String reason) throws Exception{
		AjaxJson j = new AjaxJson();
		group.setAuditState(type);
		group.setAuditDate(new Date());
		groupService.save(group);//保存
		
		// 拒绝后发消息
		if ("3".equals(type) && StringUtils.isNotBlank(reason)) {
			msgService.insert(group.getMember(), "系统消息", "您的实体店铺入驻申请被拒绝，原因：" + reason);
		}
		j.setSuccess(true);
		j.setMsg("审核成功");
		return j;
	}
	/**
	 * 冻结解冻实体商家
	 */
	@ResponseBody
	@RequiresPermissions(value="group:group:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Group group, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(group.getId())) {
			group.setState(type);
			groupService.save(group);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除实体商家
	 */
	@ResponseBody
	@RequiresPermissions("group:group:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Group group) {
		AjaxJson j = new AjaxJson();
		groupService.delete(group);
		j.setMsg("删除实体商家成功");
		return j;
	}
	
	/**
	 * 批量删除实体商家
	 */
	@ResponseBody
	@RequiresPermissions("group:group:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			groupService.delete(groupService.get(id));
		}
		j.setMsg("删除实体商家成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("group:group:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Group group, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体商家"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Group> page = groupService.findPage(new Page<Group>(request, response, -1), group);
    		new ExportExcel("实体商家", Group.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出实体商家记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("group:group:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Group> list = ei.getDataList(Group.class);
			for (Group group : list){
				try{
					groupService.save(group);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条实体商家记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条实体商家记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入实体商家失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入实体商家数据模板
	 */
	@ResponseBody
	@RequiresPermissions("group:group:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体商家数据导入模板.xlsx";
    		List<Group> list = Lists.newArrayList(); 
    		new ExportExcel("实体商家数据", Group.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}