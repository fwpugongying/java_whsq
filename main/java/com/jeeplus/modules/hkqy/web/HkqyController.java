/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.hkqy.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.hkqy.entity.Hkqy;
import com.jeeplus.modules.hkqy.service.HkqyService;

/**
 * 惠康区域订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/hkqy/hkqy")
public class HkqyController extends BaseController {

	@Autowired
	private HkqyService hkqyService;
	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Hkqy get(@RequestParam(required=false) String id) {
		Hkqy entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = hkqyService.get(id);
		}
		if (entity == null){
			entity = new Hkqy();
		}
		return entity;
	}
	
	/**
	 * 流量补贴订单列表页面
	 */
	@RequiresPermissions("hkqy:hkqy:list")
	@RequestMapping(value = {"list", ""})
	public String list(Hkqy hkqy, Model model) {
		model.addAttribute("hkqy", hkqy);
		return "modules/hkqy/hkqyList";
	}
	
		/**
	 * 流量补贴订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("hkqy:hkqy:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Hkqy hkqy, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Hkqy> page = hkqyService.findPage(new Page<Hkqy>(request, response), hkqy); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑流量补贴订单表单页面
	 */
	@RequiresPermissions(value={"hkqy:hkqy:view","hkqy:hkqy:add","hkqy:hkqy:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Hkqy hkqy, Model model) {
		model.addAttribute("hkqy", hkqy);
		return "modules/hkqy/hkqyForm";
	}

	/**
	 * 保存流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"hkqy:hkqy:add","hkqy:hkqy:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Hkqy hkqy, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(hkqy);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		hkqyService.save(hkqy);//保存
		j.setSuccess(true);
		j.setMsg("保存流量补贴订单成功");
		return j;
	}
	/**
	 * 冻结解冻流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions(value="hkqy:hkqy:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Hkqy hkqy, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(hkqy.getId())) {
			hkqy.setState(type);
			hkqyService.save(hkqy);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("hkqy:hkqy:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Hkqy hkqy) {
		AjaxJson j = new AjaxJson();
		hkqyService.delete(hkqy);
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 批量删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("hkqy:hkqy:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			hkqyService.delete(hkqyService.get(id));
		}
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("hkqy:hkqy:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Hkqy hkqy, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Hkqy> page = hkqyService.findPage(new Page<Hkqy>(request, response, -1), hkqy);
    		new ExportExcel("流量补贴订单", Hkqy.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出流量补贴订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("hkqy:hkqy:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Hkqy> list = ei.getDataList(Hkqy.class);
			for (Hkqy hkqy : list){
				try{
					hkqyService.save(hkqy);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条流量补贴订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条流量补贴订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入流量补贴订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入流量补贴订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("hkqy:hkqy:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单数据导入模板.xlsx";
    		List<Hkqy> list = Lists.newArrayList(); 
    		new ExportExcel("流量补贴订单数据", Hkqy.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}