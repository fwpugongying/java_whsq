/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.hkqy.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.hkqy.entity.Hkqy;
import com.jeeplus.modules.hkqy.mapper.HkqyMapper;
/**
 * 惠康区域Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class HkqyService extends CrudService<HkqyMapper, Hkqy> {


	@Autowired

	public Hkqy get(String id) {
		return super.get(id);
	}

	public List<Hkqy> findList(Hkqy hkqy) {
		return super.findList(hkqy);
	}
	public List<Hkqy> findUniqueByProperty(Hkqy hkqy) {
		return super.findList(hkqy);
	}

	public Hkqy getOrderno(String orderno) {
		return findUniqueByProperty("orderno", orderno);
	}

	public Hkqy getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Hkqy> findPage(Page<Hkqy> page, Hkqy hkqy) {
		return super.findPage(page, hkqy);
	}
	@Transactional(readOnly = false)
	public void save(Hkqy hkqy) {
		super.save(hkqy);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Hkqy hkqy) {
		super.save(hkqy);

// 调用三方接口
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("CardGUID", hkqy.getMember().getId()));
		list.add(new WohuiEntity("Province", hkqy.getProvince()));
		list.add(new WohuiEntity("City", hkqy.getCity()));
		list.add(new WohuiEntity("District",hkqy.getArea(),false));
		list.add(new WohuiEntity("OrderNO", hkqy.getId()));
		list.add(new WohuiEntity("Role", hkqy.getType()));
		list.add(new WohuiEntity("Type", hkqy.getTztype()));
		JSONObject object = WohuiUtils.send(WohuiUtils.HKQYAOpen, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("惠康区域代理订单号"+hkqy.getId()+"失败：" + object.getString("respMsg"));
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Hkqy hkqy) {
		super.delete(hkqy);
	}

}