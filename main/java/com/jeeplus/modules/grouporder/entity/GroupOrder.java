/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouporder.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.store.entity.Store;
import com.jeeplus.modules.sys.entity.Area;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.google.common.collect.Lists;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 拼团订单Entity
 * @author lixinapp
 * @version 2019-10-26
 */
public class GroupOrder extends DataEntity<GroupOrder> {
	
	private static final long serialVersionUID = 1L;
	private String id;// 订单号
	private Member member;		// 用户
	private Member daimai;		// 代买 
	private String groupId;		// 拼团
	private Product product;		// 商品
	private String skuId;		// 规格
	private Store store;		// 店铺
	private String productCode;		// 商品编码
	private String productTitle;		// 商品名称
	private String productIcon;		// 商品图片
	private String skuName;		// 规格名称
	private Integer qty;		// 数量
	private String price;		// 价格
	private String amount;		// 订单总额
	private String money;		// 订单结算金额
	private String freight;		// 运费
	private String point;		// 铜板
	private String proxyType;		// 申请代理 0否 1是
	private String proxyPrice;		// 代理费用
	private String discount;		// 优惠金额
	private String receipt;		// 发票抬头
	private Area area;		// 代理区域
	private String username;		// 收货人
	private String phone;		// 收货电话
	private String address;		// 收货地址
	private Date payDate;		// 付款时间
	private String payType;		// 付款方式 1支付宝 2微信
	private String state;		// 订单状态 0待付款 
	private Date sendDate;		// 发货时间
	private String expressCode;		// 物流编码
	private String expressName;		// 物流公司
	private String expressNo;		// 物流单号
	private Date cancelDate;		// 取消时间
	private Date expireDate;		// 过期时间
	private String payNo;		// 付款单号
	private Date finishDate;		// 收货时间
	private Date refundDate;		// 退款时间
	private String cancelReason;		// 取消原因
	private Date beginCreateDate; // 开始 下单时间
	private Date endCreateDate; // 结束 下单时间
	private Date createDate; // 下单时间
	private String tradeType; // 下单时间
	private String PtFwmoney; // 下单时间
	private String ThCode; // 提货码
	private String dlbt; // 提货码
	private String province;		// 省份
	private String city;		// 城市
	private String marea;		// 区县
	private String pgId;		// 拼购表id
	private String pgOrderstatus;		// 是否已经参与拼购0未参与1参与2退出
	private String isPgzj;		// 0中二等奖1中一等奖
	private String jrzjNum;		// 今日抽中二等奖参抽奖次数
	private String pgOrder;		// 是否是拼购订单

	public String getPgId() {
		return pgId;
	}

	public void setPgId(String pgId) {
		this.pgId = pgId;
	}

	public String getPgOrderstatus() {
		return pgOrderstatus;
	}

	public void setPgOrderstatus(String pgOrderstatus) {
		this.pgOrderstatus = pgOrderstatus;
	}

	public String getIsPgzj() {
		return isPgzj;
	}

	public void setIsPgzj(String isPgzj) {
		this.isPgzj = isPgzj;
	}

	public String getJrzjNum() {
		return jrzjNum;
	}

	public void setJrzjNum(String jrzjNum) {
		this.jrzjNum = jrzjNum;
	}

	public String getPgOrder() {
		return pgOrder;
	}

	public void setPgOrder(String pgOrder) {
		this.pgOrder = pgOrder;
	}



	private List<OrderGift> orderGiftList = Lists.newArrayList();		// 子表列表

	public GroupOrder() {
		super();
	}
	public GroupOrder(String id){
		super(id);
	}

	@ExcelField(title="订单号", align=2, sort=1)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPt_fwmoney(String pt_fwmoney) {
		this.PtFwmoney = pt_fwmoney;
	}
	@ExcelField(title="拼团服务费", align=2, sort=18)
	public String getPt_fwmoney() {
		return PtFwmoney;
	}

	@ExcelField(title="代理补贴", align=20, sort=1)
	public String getDlbt() {
		return dlbt;
	}

	public void setDlbt(String dlbt) {
		this.dlbt = dlbt;
	}
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="下单时间", align=2, sort=17)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=2)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	@NotNull(message="商品不能为空")
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public String getSkuId() {
		return skuId;
	}

	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	
	@ExcelField(title="店铺", fieldType=Store.class, value="store.title", align=2, sort=3)
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}
	
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	@ExcelField(title="商品名称", align=2, sort=4)
	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	
	public String getProductIcon() {
		return productIcon;
	}

	public void setProductIcon(String productIcon) {
		this.productIcon = productIcon;
	}
	
	@ExcelField(title="规格名称", align=2, sort=5)
	public String getSkuName() {
		return skuName;
	}

	public void setSkuName(String skuName) {
		this.skuName = skuName;
	}
	
	@NotNull(message="数量不能为空")
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@ExcelField(title="数量", align=2, sort=6)
	public String getQty1() {
		return qty+"";
	}

	@ExcelField(title="商品价格", align=2, sort=7)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@ExcelField(title="订单总额", align=2, sort=8)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="运费", align=2, sort=10)
	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}
	
	@ExcelField(title="铜板", align=2, sort=12)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	public String getProxyType() {
		return proxyType;
	}

	public void setProxyType(String proxyType) {
		this.proxyType = proxyType;
	}
	
	public String getProxyPrice() {
		return proxyPrice;
	}

	public void setProxyPrice(String proxyPrice) {
		this.proxyPrice = proxyPrice;
	}
	
	@ExcelField(title="优惠金额", align=2, sort=9)
	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	@ExcelField(title="发票抬头", align=2, sort=13)
	public String getReceipt() {
		return receipt;
	}

	public void setReceipt(String receipt) {
		this.receipt = receipt;
	}
	
	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	@ExcelField(title="收货人", align=2, sort=14)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="收货电话", align=2, sort=15)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@ExcelField(title="收货地址", align=2, sort=16)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@ExcelField(title="省份", align=2, sort=4)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@ExcelField(title="城市", align=2, sort=5)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@ExcelField(title="区县", align=2, sort=6)
	public String getMarea() {
		return marea;
	}

	public void setMarea(String marea) {
		this.marea = marea;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="付款时间", align=2, sort=18)
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	
	@ExcelField(title="付款方式", dictType="pay_type", align=2, sort=19)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	@ExcelField(title="订单状态", dictType="product_order_state", align=2, sort=20)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="发货时间", align=2, sort=23)
	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	
	public String getExpressCode() {
		return expressCode;
	}

	public void setExpressCode(String expressCode) {
		this.expressCode = expressCode;
	}
	
	@ExcelField(title="物流公司", align=2, sort=21)
	public String getExpressName() {
		return expressName;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}
	
	@ExcelField(title="物流单号", align=2, sort=22)
	public String getExpressNo() {
		return expressNo;
	}

	public void setExpressNo(String expressNo) {
		this.expressNo = expressNo;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
	
	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getFinishDate() {
		return finishDate;
	}

	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}
	
	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	
	public List<OrderGift> getOrderGiftList() {
		return orderGiftList;
	}

	public void setOrderGiftList(List<OrderGift> orderGiftList) {
		this.orderGiftList = orderGiftList;
	}

	public Member getDaimai() {
		return daimai;
	}

	public void setDaimai(Member daimai) {
		this.daimai = daimai;
	}

	@ExcelField(title="结算金额", align=2, sort=11)
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}
	public Date getBeginCreateDate() {
		return beginCreateDate;
	}

	public void setBeginCreateDate(Date beginCreateDate) {
		this.beginCreateDate = beginCreateDate;
	}

	public Date getEndCreateDate() {
		return endCreateDate;
	}

	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}

    public void setTrade_type(String trade_type) {
        this.tradeType = trade_type;
    }

    public String getTrade_type() {
        return tradeType;
    }
	public String getThCode() {
		return ThCode;
	}
	public void setThCode(String thCode) {
		ThCode = thCode;
	}
}