/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouporder.entity;

import com.jeeplus.modules.product.entity.Product;
import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 拼团订单赠品Entity
 * @author lixinapp
 * @version 2019-10-26
 */
public class OrderGift extends DataEntity<OrderGift> {
	
	private static final long serialVersionUID = 1L;
	private GroupOrder order;		// 订单号 父类
	private Product product;		// 商品
	private String productTitle;		// 商品名称
	private String productIcon;		// 商品图片
	private Integer qty;		// 数量
	private String price;		// 价格
	
	public OrderGift() {
		super();
	}

	public OrderGift(String id){
		super(id);
	}

	public OrderGift(GroupOrder order){
		this.order = order;
	}

	public GroupOrder getOrder() {
		return order;
	}

	public void setOrder(GroupOrder order) {
		this.order = order;
	}
	
	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=2)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ExcelField(title="商品名称", align=2, sort=3)
	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	
	@ExcelField(title="商品图片", align=2, sort=4)
	public String getProductIcon() {
		return productIcon;
	}

	public void setProductIcon(String productIcon) {
		this.productIcon = productIcon;
	}
	
	@NotNull(message="数量不能为空")
	@ExcelField(title="数量", align=2, sort=5)
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@ExcelField(title="价格", align=2, sort=6)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
}