/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.llbt.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import com.jeeplus.modules.proxyproduct.entity.ProxyProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.llbt.entity.Llbt;
import com.jeeplus.modules.llbt.mapper.LlbtMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class LlbtService extends CrudService<LlbtMapper, Llbt> {


	@Autowired

	public Llbt get(String id) {
		return super.get(id);
	}

	public List<Llbt> findList(Llbt llbt) {
		return super.findList(llbt);
	}
	public List<Llbt> findUniqueByProperty(Llbt llbt) {
		return super.findList(llbt);
	}

	public Llbt getOrderno(String orderno) {
		return findUniqueByProperty("orderno", orderno);
	}

	public Llbt getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Llbt> findPage(Page<Llbt> page, Llbt llbt) {
		return super.findPage(page, llbt);
	}
	@Transactional(readOnly = false)
	public void save(Llbt llbt) {
		super.save(llbt);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Llbt llbt) {
		super.save(llbt);

		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("OrderNO", llbt.getOrderNo()));
		JSONObject object = WohuiUtils.send(WohuiUtils.LLBTPay, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("开通流量补贴订单号"+llbt.getOrderNo()+"失败：" + object.getString("respMsg"));
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Llbt llbt) {
		super.delete(llbt);
	}

}