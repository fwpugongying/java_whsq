/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouprefund.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 拼团订单退款Entity
 * @author lixinapp
 * @version 2019-10-26
 */
public class GroupRefund extends DataEntity<GroupRefund> {
	
	private static final long serialVersionUID = 1L;
	private String orderId;		// 订单号
	private String reason;		// 退款原因
	private String content;		// 描述
	private String images;		// 图片
	private String amount;		// 申请金额
	private String state;		// 状态 0待审核 1通过 2拒绝
	private Date auditDate;		// 审核时间
	private String orderState;		// 订单状态
	
	public GroupRefund() {
		super();
	}

	public GroupRefund(String id){
		super(id);
	}

	@ExcelField(title="订单号", align=2, sort=1)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
	@ExcelField(title="退款原因", align=2, sort=2)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@ExcelField(title="描述", align=2, sort=3)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="图片", align=2, sort=4)
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}
	
	@ExcelField(title="申请金额", align=2, sort=5)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="状态 0待审核 1通过 2拒绝", dictType="order_refund_state", align=2, sort=7)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=8)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	
	@ExcelField(title="订单状态", align=2, sort=10)
	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}
	
}