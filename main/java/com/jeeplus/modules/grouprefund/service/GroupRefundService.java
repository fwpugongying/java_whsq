/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouprefund.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.grouprefund.entity.GroupRefund;
import com.jeeplus.modules.grouprefund.mapper.GroupRefundMapper;

/**
 * 拼团订单退款Service
 * @author lixinapp
 * @version 2019-10-26
 */
@Service
@Transactional(readOnly = true)
public class GroupRefundService extends CrudService<GroupRefundMapper, GroupRefund> {

	public GroupRefund get(String id) {
		return super.get(id);
	}
	
	public List<GroupRefund> findList(GroupRefund groupRefund) {
		return super.findList(groupRefund);
	}
	
	public Page<GroupRefund> findPage(Page<GroupRefund> page, GroupRefund groupRefund) {
		return super.findPage(page, groupRefund);
	}
	
	@Transactional(readOnly = false)
	public void save(GroupRefund groupRefund) {
		super.save(groupRefund);
	}
	
	@Transactional(readOnly = false)
	public void delete(GroupRefund groupRefund) {
		super.delete(groupRefund);
	}
	
}