/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.grouprefund.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.grouporder.entity.GroupOrder;
import com.jeeplus.modules.grouporder.service.GroupOrderService;
import com.jeeplus.modules.grouprefund.entity.GroupRefund;
import com.jeeplus.modules.grouprefund.service.GroupRefundService;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;

/**
 * 拼团订单退款Controller
 * @author lixinapp
 * @version 2019-10-26
 */
@Controller
@RequestMapping(value = "${adminPath}/grouprefund/groupRefund")
public class GroupRefundController extends BaseController {

	@Autowired
	private GroupRefundService groupRefundService;
	@Autowired
	private GroupOrderService groupOrderService;
	
	@ModelAttribute
	public GroupRefund get(@RequestParam(required=false) String id) {
		GroupRefund entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = groupRefundService.get(id);
		}
		if (entity == null){
			entity = new GroupRefund();
		}
		return entity;
	}
	
	/**
	 * 拼团订单退款列表页面
	 */
	@RequiresPermissions("grouprefund:groupRefund:list")
	@RequestMapping(value = {"list", ""})
	public String list(GroupRefund groupRefund, Model model) {
		model.addAttribute("groupRefund", groupRefund);
		return "modules/grouprefund/groupRefundList";
	}
	
		/**
	 * 拼团订单退款列表数据
	 */
	@ResponseBody
	@RequiresPermissions("grouprefund:groupRefund:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(GroupRefund groupRefund, HttpServletRequest request, HttpServletResponse response, Model model) {
		// 判断当前登录用户
		User user = UserUtils.getUser();
		if (user.isShop()) {
			groupRefund.setDataScope(" AND store.user_id = '"+user.getId()+"' ");
		}
		Page<GroupRefund> page = groupRefundService.findPage(new Page<GroupRefund>(request, response), groupRefund); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑拼团订单退款表单页面
	 */
	@RequiresPermissions(value={"grouprefund:groupRefund:view","grouprefund:groupRefund:add","grouprefund:groupRefund:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(GroupRefund groupRefund, Model model) {
		model.addAttribute("groupRefund", groupRefund);
		return "modules/grouprefund/groupRefundForm";
	}

	/**
	 * 保存拼团订单退款
	 */
	@ResponseBody
	@RequiresPermissions(value={"grouprefund:groupRefund:add","grouprefund:groupRefund:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(GroupRefund groupRefund, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(groupRefund);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		groupRefundService.save(groupRefund);//保存
		j.setSuccess(true);
		j.setMsg("保存拼团订单退款成功");
		return j;
	}
	
	/**
	 * 退款审核
	 */
	@ResponseBody
	@RequestMapping(value = "audit")
	public AjaxJson audit(GroupRefund groupRefund, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (!"0".equals(groupRefund.getState())) {
			j.setSuccess(false);
			j.setMsg("只能操作退款中的订单");
			return j;
		}
		
		//orderRefund.setState(type);
		//orderRefund.setAuditDate(new Date());
		//orderRefundService.refund(orderRefund);//保存
		GroupOrder order = groupOrderService.get(groupRefund.getOrderId());
		if (order != null) {
			groupOrderService.refund(order, type);
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除拼团订单退款
	 */
	@ResponseBody
	@RequiresPermissions("grouprefund:groupRefund:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(GroupRefund groupRefund) {
		AjaxJson j = new AjaxJson();
		groupRefundService.delete(groupRefund);
		j.setMsg("删除拼团订单退款成功");
		return j;
	}
	
	/**
	 * 批量删除拼团订单退款
	 */
	@ResponseBody
	@RequiresPermissions("grouprefund:groupRefund:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			groupRefundService.delete(groupRefundService.get(id));
		}
		j.setMsg("删除拼团订单退款成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("grouprefund:groupRefund:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(GroupRefund groupRefund, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "拼团订单退款"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<GroupRefund> page = groupRefundService.findPage(new Page<GroupRefund>(request, response, -1), groupRefund);
    		new ExportExcel("拼团订单退款", GroupRefund.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出拼团订单退款记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("grouprefund:groupRefund:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<GroupRefund> list = ei.getDataList(GroupRefund.class);
			for (GroupRefund groupRefund : list){
				try{
					groupRefundService.save(groupRefund);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条拼团订单退款记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条拼团订单退款记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入拼团订单退款失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入拼团订单退款数据模板
	 */
	@ResponseBody
	@RequiresPermissions("grouprefund:groupRefund:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "拼团订单退款数据导入模板.xlsx";
    		List<GroupRefund> list = Lists.newArrayList(); 
    		new ExportExcel("拼团订单退款数据", GroupRefund.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}