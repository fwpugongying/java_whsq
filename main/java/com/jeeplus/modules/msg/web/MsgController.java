/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.msg.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.entity.Msg;
import com.jeeplus.modules.msg.service.MsgService;

/**
 * 用户消息Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/msg/msg")
public class MsgController extends BaseController {

	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Msg get(@RequestParam(required=false) String id) {
		Msg entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = msgService.get(id);
		}
		if (entity == null){
			entity = new Msg();
		}
		return entity;
	}
	
	/**
	 * 用户消息列表页面
	 */
	@RequiresPermissions("msg:msg:list")
	@RequestMapping(value = {"list", ""})
	public String list(Msg msg, Model model) {
		model.addAttribute("msg", msg);
		return "modules/msg/msgList";
	}
	
		/**
	 * 用户消息列表数据
	 */
	@ResponseBody
	@RequiresPermissions("msg:msg:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Msg msg, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Msg> page = msgService.findPage(new Page<Msg>(request, response), msg); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑用户消息表单页面
	 */
	@RequiresPermissions(value={"msg:msg:view","msg:msg:add","msg:msg:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Msg msg, Model model) {
		model.addAttribute("msg", msg);
		return "modules/msg/msgForm";
	}

	/**
	 * 保存用户消息
	 */
	@ResponseBody
	@RequiresPermissions(value={"msg:msg:add","msg:msg:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Msg msg, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(msg);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		msgService.save(msg);//保存
		j.setSuccess(true);
		j.setMsg("保存用户消息成功");
		return j;
	}
	
	/**
	 * 删除用户消息
	 */
	@ResponseBody
	@RequiresPermissions("msg:msg:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Msg msg) {
		AjaxJson j = new AjaxJson();
		msgService.delete(msg);
		j.setMsg("删除用户消息成功");
		return j;
	}
	
	/**
	 * 批量删除用户消息
	 */
	@ResponseBody
	@RequiresPermissions("msg:msg:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			msgService.delete(msgService.get(id));
		}
		j.setMsg("删除用户消息成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("msg:msg:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Msg msg, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "用户消息"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Msg> page = msgService.findPage(new Page<Msg>(request, response, -1), msg);
    		new ExportExcel("用户消息", Msg.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出用户消息记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("msg:msg:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Msg> list = ei.getDataList(Msg.class);
			for (Msg msg : list){
				try{
					msgService.save(msg);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条用户消息记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条用户消息记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入用户消息失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入用户消息数据模板
	 */
	@ResponseBody
	@RequiresPermissions("msg:msg:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "用户消息数据导入模板.xlsx";
    		List<Msg> list = Lists.newArrayList(); 
    		new ExportExcel("用户消息数据", Msg.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}