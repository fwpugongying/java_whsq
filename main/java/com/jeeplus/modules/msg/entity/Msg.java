/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.msg.entity;

import com.jeeplus.modules.member.entity.Member;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 用户消息Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Msg extends DataEntity<Msg> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String title;		// 标题
	private String content;		// 内容
	private String type;		// 类型
	private String productId;		// 商品
	private String state;		// 状态 0未读 1已读
	
	public Msg() {
		super();
	}

	public Msg(String id){
		super(id);
	}

	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="标题", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="内容", align=2, sort=3)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="类型", dictType="msg_type", align=2, sort=4)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="商品", align=2, sort=5)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	@ExcelField(title="状态 0未读 1已读", dictType="msg_state", align=2, sort=6)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}