/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.xfq.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.xfq.entity.Xfq;
import com.jeeplus.modules.xfq.service.XfqService;

/**
 * 流量补贴订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/xfq/xfq")
public class XfqController extends BaseController {

	@Autowired
	private XfqService xfqService;
	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Xfq get(@RequestParam(required=false) String id) {
		Xfq entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = xfqService.get(id);
		}
		if (entity == null){
			entity = new Xfq();
		}
		return entity;
	}
	
	/**
	 * 流量补贴订单列表页面
	 */
	@RequiresPermissions("xfq:xfq:list")
	@RequestMapping(value = {"list", ""})
	public String list(Xfq xfq, Model model) {
		model.addAttribute("xfq", xfq);
		return "modules/xfq/xfqList";
	}
	
		/**
	 * 流量补贴订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("xfq:xfq:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Xfq xfq, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Xfq> page = xfqService.findPage(new Page<Xfq>(request, response), xfq);
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑流量补贴订单表单页面
	 */
	@RequiresPermissions(value={"xfq:xfq:view","xfq:xfq:add","xfq:xfq:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Xfq xfq, Model model) {
		model.addAttribute("xfq", xfq);
		return "modules/xfq/xfqForm";
	}

	/**
	 * 保存流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"xfq:xfq:add","xfq:xfq:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Xfq xfq, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(xfq);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		xfqService.save(xfq);//保存
		j.setSuccess(true);
		j.setMsg("保存流量补贴订单成功");
		return j;
	}
	/**
	 * 冻结解冻流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions(value="xfq:xfq:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Xfq xfq, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(xfq.getId())) {
			xfq.setState(type);
			xfqService.save(xfq);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("xfq:xfq:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Xfq xfq) {
		AjaxJson j = new AjaxJson();
		xfqService.delete(xfq);
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 批量删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("xfq:xfq:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			xfqService.delete(xfqService.get(id));
		}
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("xfq:xfq:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Xfq xfq, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Xfq> page = xfqService.findPage(new Page<Xfq>(request, response, -1), xfq);
    		new ExportExcel("流量补贴订单", Xfq.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出流量补贴订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("xfq:xfq:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Xfq> list = ei.getDataList(Xfq.class);
			for (Xfq xfq : list){
				try{
					xfqService.save(xfq);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条流量补贴订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条流量补贴订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入流量补贴订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入流量补贴订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("xfq:xfq:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单数据导入模板.xlsx";
    		List<Xfq> list = Lists.newArrayList(); 
    		new ExportExcel("流量补贴订单数据", Xfq.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}