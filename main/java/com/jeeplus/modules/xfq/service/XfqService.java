/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.xfq.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.xfq.entity.Xfq;
import com.jeeplus.modules.xfq.mapper.XfqMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class XfqService extends CrudService<XfqMapper, Xfq> {


	@Autowired

	public Xfq get(String id) {
		return super.get(id);
	}

	public List<Xfq> findList(Xfq xfq) {
		return super.findList(xfq);
	}
	public List<Xfq> findUniqueByProperty(Xfq xfq) {
		return super.findList(xfq);
	}
	public Xfq getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Xfq> findPage(Page<Xfq> page, Xfq xfq) {
		return super.findPage(page, xfq);
	}
	@Transactional(readOnly = false)
	public void save(Xfq xfq) {
		super.save(xfq);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Xfq xfq) {
		super.save(xfq);
		String PayMethod="10";
		if("1".equals(xfq.getPayType())){//支付宝
			PayMethod="40";
		}else if("2".equals(xfq.getPayType())){//微信
			PayMethod="30";
		}else if("3".equals(xfq.getPayType())){//平台购物券支付
			PayMethod="10";
		}else if("4".equals(xfq.getPayType())){//商城购物券支付
			PayMethod="20";
		}

		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("OrderNO", xfq.getId()));
		list.add(new WohuiEntity("CardGUID", xfq.getMember().getId()));
		list.add(new WohuiEntity("Money", xfq.getMoney()));
		JSONObject object = WohuiUtils.send(WohuiUtils.XFQBuy, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("消费券支付"+"失败：" + object.getString("respMsg"));
		}
	}

	@Transactional(readOnly = false)
	public void delete(Xfq xfq) {
		super.delete(xfq);
	}

}