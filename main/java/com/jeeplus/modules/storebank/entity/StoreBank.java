/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storebank.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jeeplus.modules.store.entity.Store;
import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

import java.util.Date;

/**
 * 厂家提现Entity
 * @author lixinapp
 * @version 2019-10-14storebank
 */
public class StoreBank extends DataEntity<StoreBank> {
	
	private static final long serialVersionUID = 1L;
	private Store store;		// 店铺
	private String uid;		//
	private Date createDate;		//
	private String username;		// 姓名
	private String bank;		// 银行
	private String account;		// 卡号
	private String state;		// 状态 0待审核 1通过 2拒绝
	public StoreBank() {
		super();
	}

	public StoreBank(String id){
		super(id);
	}

	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Store.class, value="store.title", align=2, sort=1)
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}
	
	@ExcelField(title="姓名", align=2, sort=3)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="银行", align=2, sort=4)
	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}
	
	@ExcelField(title="卡号", align=2, sort=5)
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	@ExcelField(title="状态 0待审核 1通过 2拒绝", dictType="cash_state", align=2, sort=7)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}