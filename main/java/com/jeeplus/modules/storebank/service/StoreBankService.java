/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.storebank.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.storebank.entity.StoreBank;
import com.jeeplus.modules.storebank.mapper.StoreBankMapper;

/**
 * 实体店账单Service
 * @author lixinapp
 * @version 2019-10-17
 */
@Service
@Transactional(readOnly = true)
public class StoreBankService extends CrudService<StoreBankMapper, StoreBank> {

	public StoreBank get(String id) {
		return super.get(id);
	}

	public List<StoreBank> findList(StoreBank shopBill) {
		return super.findList(shopBill);
	}

	public Page<StoreBank> findPage(Page<StoreBank> page, StoreBank shopBill) {
		return super.findPage(page, shopBill);
	}

	@Transactional(readOnly = false)
	public void save(StoreBank shopBill) {
		super.save(shopBill);
	}

	@Transactional(readOnly = false)
	public void delete(StoreBank shopBill) {
		super.delete(shopBill);
	}

}