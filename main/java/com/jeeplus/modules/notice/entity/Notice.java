/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.notice.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 平台公告Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Notice extends DataEntity<Notice> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 标题
	private String content;		// 内容
	private Integer sort;		// 置顶状态 0否 1是
	private String type;		// 类型 1用户 2商家
	
	public Notice() {
		super();
	}

	public Notice(String id){
		super(id);
	}

	@ExcelField(title="标题", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="内容", align=2, sort=2)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="置顶状态 0否 1是", dictType="yes_no", align=2, sort=3)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	@ExcelField(title="类型 1用户 2商家", dictType="notice_type", align=2, sort=4)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * 富文本路径
	 * @return
	 */
	public String getUrl() {
		return Global.getConfig("filePath") + "/display/notice?id=" + id;
	}
}