/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.notice.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.notice.entity.Notice;

/**
 * 平台公告MAPPER接口
 * @author lixinapp
 * @version 2019-10-14
 */
@MyBatisMapper
public interface NoticeMapper extends BaseMapper<Notice> {
	
}