/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.jdcategory.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.jdcategory.entity.JdCategory;
import com.jeeplus.modules.jdcategory.service.JdCategoryService;

/**
 * 京东分类Controller
 * @author lixinapp
 * @version 2019-11-22
 */
@Controller
@RequestMapping(value = "${adminPath}/jdcategory/jdCategory")
public class JdCategoryController extends BaseController {

	@Autowired
	private JdCategoryService jdCategoryService;
	
	@ModelAttribute
	public JdCategory get(@RequestParam(required=false) String id) {
		JdCategory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = jdCategoryService.get(id);
		}
		if (entity == null){
			entity = new JdCategory();
		}
		return entity;
	}
	
	/**
	 * 京东分类列表页面
	 */
	@RequiresPermissions("jdcategory:jdCategory:list")
	@RequestMapping(value = {"list", ""})
	public String list(JdCategory jdCategory, Model model) {
		model.addAttribute("jdCategory", jdCategory);
		return "modules/jdcategory/jdCategoryList";
	}
	
		/**
	 * 京东分类列表数据
	 */
	@ResponseBody
	@RequiresPermissions("jdcategory:jdCategory:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(JdCategory jdCategory, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<JdCategory> page = jdCategoryService.findPage(new Page<JdCategory>(request, response), jdCategory); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑京东分类表单页面
	 */
	@RequiresPermissions(value={"jdcategory:jdCategory:view","jdcategory:jdCategory:add","jdcategory:jdCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(JdCategory jdCategory, Model model) {
		model.addAttribute("jdCategory", jdCategory);
		return "modules/jdcategory/jdCategoryForm";
	}

	/**
	 * 保存京东分类
	 */
	@ResponseBody
	@RequiresPermissions(value={"jdcategory:jdCategory:add","jdcategory:jdCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(JdCategory jdCategory, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(jdCategory);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		jdCategoryService.save(jdCategory);//保存
		j.setSuccess(true);
		j.setMsg("保存京东分类成功");
		return j;
	}
	
	/**
	 * 删除京东分类
	 */
	@ResponseBody
	@RequiresPermissions("jdcategory:jdCategory:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(JdCategory jdCategory) {
		AjaxJson j = new AjaxJson();
		jdCategoryService.delete(jdCategory);
		j.setMsg("删除京东分类成功");
		return j;
	}
	
	/**
	 * 批量删除京东分类
	 */
	@ResponseBody
	@RequiresPermissions("jdcategory:jdCategory:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			jdCategoryService.delete(jdCategoryService.get(id));
		}
		j.setMsg("删除京东分类成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("jdcategory:jdCategory:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(JdCategory jdCategory, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "京东分类"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<JdCategory> page = jdCategoryService.findPage(new Page<JdCategory>(request, response, -1), jdCategory);
    		new ExportExcel("京东分类", JdCategory.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出京东分类记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("jdcategory:jdCategory:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<JdCategory> list = ei.getDataList(JdCategory.class);
			for (JdCategory jdCategory : list){
				try{
					jdCategoryService.save(jdCategory);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条京东分类记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条京东分类记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入京东分类失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入京东分类数据模板
	 */
	@ResponseBody
	@RequiresPermissions("jdcategory:jdCategory:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "京东分类数据导入模板.xlsx";
    		List<JdCategory> list = Lists.newArrayList(); 
    		new ExportExcel("京东分类数据", JdCategory.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}