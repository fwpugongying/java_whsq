/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.jdcategory.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.jdcategory.entity.JdCategory;
import com.jeeplus.modules.jdcategory.mapper.JdCategoryMapper;

/**
 * 京东分类Service
 * @author lixinapp
 * @version 2019-11-22
 */
@Service
@Transactional(readOnly = true)
public class JdCategoryService extends CrudService<JdCategoryMapper, JdCategory> {

	public JdCategory get(String id) {
		return super.get(id);
	}
	
	public List<JdCategory> findList(JdCategory jdCategory) {
		return super.findList(jdCategory);
	}
	
	public Page<JdCategory> findPage(Page<JdCategory> page, JdCategory jdCategory) {
		return super.findPage(page, jdCategory);
	}
	
	@Transactional(readOnly = false)
	public void save(JdCategory jdCategory) {
		super.save(jdCategory);
	}
	
	@Transactional(readOnly = false)
	public void delete(JdCategory jdCategory) {
		super.delete(jdCategory);
	}
	
}