/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.groupsys.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.groupsys.entity.Groupsys;
import com.jeeplus.modules.groupsys.service.GroupsysService;

/**
 * 支付方式Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/groupsys/groupsys")
public class GroupsysController extends BaseController {

	@Autowired
	private GroupsysService groupsysService;

	@ModelAttribute
	public Groupsys get(@RequestParam(required=false) String id) {
		Groupsys entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = groupsysService.get(id);
		}
		if (entity == null){
			entity = new Groupsys();
		}
		return entity;
	}

	/**
	 * 支付方式列表页面
	 */
	@RequiresPermissions("groupsys:groupsys:list")
	@RequestMapping(value = {"list", ""})
	public String list(Groupsys groupsys, Model model) {
		model.addAttribute("groupsys", groupsys);
		return "modules/groupsys/groupsysList";
	}

	/**
	 * 支付方式列表数据
	 */
	@ResponseBody
	@RequiresPermissions("groupsys:groupsys:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Groupsys groupsys, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Groupsys> page = groupsysService.findPage(new Page<Groupsys>(request, response), groupsys);
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑支付方式表单页面
	 */
	@RequiresPermissions(value={"groupsys:groupsys:view","groupsys:groupsys:add","groupsys:groupsys:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Groupsys groupsys, Model model) {
		model.addAttribute("groupsys", groupsys);
		return "modules/groupsys/groupsysForm";
	}

	/**
	 * 保存支付方式
	 */
	@ResponseBody
	@RequiresPermissions(value={"groupsys:groupsys:add","groupsys:groupsys:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Groupsys groupsys, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(groupsys);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		groupsysService.save(groupsys);//保存
		j.setSuccess(true);
		j.setMsg("保存支付方式成功");
		return j;
	}

	/**
	 * 删除支付方式
	 */
	@ResponseBody
	@RequiresPermissions("groupsys:groupsys:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Groupsys groupsys) {
		AjaxJson j = new AjaxJson();
		groupsysService.delete(groupsys);
		j.setMsg("删除支付方式成功");
		return j;
	}

	/**
	 * 批量删除支付方式
	 */
	@ResponseBody
	@RequiresPermissions("groupsys:groupsys:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			groupsysService.delete(groupsysService.get(id));
		}
		j.setMsg("删除支付方式成功");
		return j;
	}

	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("groupsys:groupsys:export")
	@RequestMapping(value = "export")
	public AjaxJson exportFile(Groupsys groupsys, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
			String fileName = "支付方式"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
			Page<Groupsys> page = groupsysService.findPage(new Page<Groupsys>(request, response, -1), groupsys);
			new ExportExcel("支付方式", Groupsys.class).setDataList(page.getList()).write(response, fileName).dispose();
			j.setSuccess(true);
			j.setMsg("导出成功！");
			return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出支付方式记录失败！失败信息："+e.getMessage());
		}
		return j;
	}

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("groupsys:groupsys:import")
	@RequestMapping(value = "import")
	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Groupsys> list = ei.getDataList(Groupsys.class);
			for (Groupsys groupsys : list){
				try{
					groupsysService.save(groupsys);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条支付方式记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条支付方式记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入支付方式失败！失败信息："+e.getMessage());
		}
		return j;
	}

	/**
	 * 下载导入支付方式数据模板
	 */
	@ResponseBody
	@RequiresPermissions("groupsys:groupsys:import")
	@RequestMapping(value = "import/template")
	public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
			String fileName = "支付方式数据导入模板.xlsx";
			List<Groupsys> list = Lists.newArrayList();
			new ExportExcel("支付方式数据", Groupsys.class, 1).setDataList(list).write(response, fileName).dispose();
			return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
	}

}