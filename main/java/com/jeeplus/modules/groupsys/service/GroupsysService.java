/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.groupsys.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.groupsys.entity.Groupsys;
import com.jeeplus.modules.groupsys.mapper.GroupsysMapper;

/**
 * 支付方式Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class GroupsysService extends CrudService<GroupsysMapper, Groupsys> {

	public Groupsys get(String id) {
		return super.get(id);
	}
	
	public List<Groupsys> findList(Groupsys payType) {
		return super.findList(payType);
	}
	
	public Page<Groupsys> findPage(Page<Groupsys> page, Groupsys payType) {
		return super.findPage(page, payType);
	}
	
	@Transactional(readOnly = false)
	public void save(Groupsys payType) {
		super.save(payType);
	}
	
	@Transactional(readOnly = false)
	public void delete(Groupsys payType) {
		super.delete(payType);
	}
	
}