/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.groupsys.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 支付方式Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Groupsys extends DataEntity<Groupsys> {
	
	private static final long serialVersionUID = 1L;
	private String state;		// 状态 0启用 1停用
	private Integer num;		// 排序
	
	public Groupsys() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Groupsys(String id){
		super(id);
	}

	@ExcelField(title="状态 0启用 1停用", dictType="pay_type_state", align=2, sort=2)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@NotNull(message="次数")
	@ExcelField(title="次数", align=2, sort=3)
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}
	
}