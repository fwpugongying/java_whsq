/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.producttag.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.producttag.entity.ProductTag;
import com.jeeplus.modules.producttag.service.ProductTagService;

/**
 * 商品标签Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/producttag/productTag")
public class ProductTagController extends BaseController {

	@Autowired
	private ProductTagService productTagService;
	
	@ModelAttribute
	public ProductTag get(@RequestParam(required=false) String id) {
		ProductTag entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = productTagService.get(id);
		}
		if (entity == null){
			entity = new ProductTag();
		}
		return entity;
	}
	
	/**
	 * 商品标签列表页面
	 */
	@RequiresPermissions("producttag:productTag:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProductTag productTag, Model model) {
		model.addAttribute("productTag", productTag);
		return "modules/producttag/productTagList";
	}
	
		/**
	 * 商品标签列表数据
	 */
	@ResponseBody
	@RequiresPermissions("producttag:productTag:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProductTag productTag, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProductTag> page = productTagService.findPage(new Page<ProductTag>(request, response), productTag); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑商品标签表单页面
	 */
	@RequiresPermissions(value={"producttag:productTag:view","producttag:productTag:add","producttag:productTag:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProductTag productTag, Model model) {
		model.addAttribute("productTag", productTag);
		return "modules/producttag/productTagForm";
	}

	/**
	 * 保存商品标签
	 */
	@ResponseBody
	@RequiresPermissions(value={"producttag:productTag:add","producttag:productTag:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProductTag productTag, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(productTag);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		productTagService.save(productTag);//保存
		j.setSuccess(true);
		j.setMsg("保存商品标签成功");
		return j;
	}
	
	/**
	 * 删除商品标签
	 */
	@ResponseBody
	@RequiresPermissions("producttag:productTag:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProductTag productTag) {
		AjaxJson j = new AjaxJson();
		productTagService.delete(productTag);
		j.setMsg("删除商品标签成功");
		return j;
	}
	
	/**
	 * 批量删除商品标签
	 */
	@ResponseBody
	@RequiresPermissions("producttag:productTag:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			productTagService.delete(productTagService.get(id));
		}
		j.setMsg("删除商品标签成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("producttag:productTag:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProductTag productTag, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品标签"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProductTag> page = productTagService.findPage(new Page<ProductTag>(request, response, -1), productTag);
    		new ExportExcel("商品标签", ProductTag.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出商品标签记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("producttag:productTag:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProductTag> list = ei.getDataList(ProductTag.class);
			for (ProductTag productTag : list){
				try{
					productTagService.save(productTag);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条商品标签记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条商品标签记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入商品标签失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入商品标签数据模板
	 */
	@ResponseBody
	@RequiresPermissions("producttag:productTag:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品标签数据导入模板.xlsx";
    		List<ProductTag> list = Lists.newArrayList(); 
    		new ExportExcel("商品标签数据", ProductTag.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}