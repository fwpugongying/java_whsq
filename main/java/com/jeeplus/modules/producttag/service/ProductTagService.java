/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.producttag.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.producttag.entity.ProductTag;
import com.jeeplus.modules.producttag.mapper.ProductTagMapper;

/**
 * 商品标签Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class ProductTagService extends CrudService<ProductTagMapper, ProductTag> {

	public ProductTag get(String id) {
		return super.get(id);
	}
	
	public List<ProductTag> findList(ProductTag productTag) {
		return super.findList(productTag);
	}
	
	public Page<ProductTag> findPage(Page<ProductTag> page, ProductTag productTag) {
		return super.findPage(page, productTag);
	}
	
	@Transactional(readOnly = false)
	public void save(ProductTag productTag) {
		super.save(productTag);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProductTag productTag) {
		super.delete(productTag);
	}
	
}