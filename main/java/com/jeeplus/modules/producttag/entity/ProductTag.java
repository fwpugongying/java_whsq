/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.producttag.entity;

import com.jeeplus.modules.product.entity.Product;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.tags.entity.Tags;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品标签Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class ProductTag extends DataEntity<ProductTag> {
	
	private static final long serialVersionUID = 1L;
	private Product product;		// 商品
	private Tags tags;		// 标签
	
	public ProductTag() {
		super();
	}

	public ProductTag(String id){
		super(id);
	}

	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=1)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@NotNull(message="标签不能为空")
	@ExcelField(title="标签", fieldType=Tags.class, value="tags.title", align=2, sort=2)
	public Tags getTags() {
		return tags;
	}

	public void setTags(Tags tags) {
		this.tags = tags;
	}
	
}