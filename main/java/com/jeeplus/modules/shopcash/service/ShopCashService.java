/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcash.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.shop.service.ShopService;
import com.jeeplus.modules.shopcash.entity.ShopCash;
import com.jeeplus.modules.shopcash.mapper.ShopCashMapper;

/**
 * 实体商家提现Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class ShopCashService extends CrudService<ShopCashMapper, ShopCash> {
	@Autowired
	private ShopService shopService;
	
	public ShopCash get(String id) {
		return super.get(id);
	}
	
	public List<ShopCash> findList(ShopCash shopCash) {
		return super.findList(shopCash);
	}
	
	public Page<ShopCash> findPage(Page<ShopCash> page, ShopCash shopCash) {
		return super.findPage(page, shopCash);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopCash shopCash) {
		super.save(shopCash);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopCash shopCash) {
		super.delete(shopCash);
	}

	/**
	 * 审核
	 * @param shopCash
	 */
	@Transactional(readOnly = false)
	public void audit(ShopCash shopCash) {
		super.save(shopCash);
		
		// 处理拒绝
		if ("2".equals(shopCash.getState())) {
			// 加钱
			shopService.updateBalance(shopCash.getShop(), "提现拒绝", shopCash.getAmount(), "1", null, null, null, null, null);
		}
	}

	/**
	 * 提现
	 * @param sc
	 */
	@Transactional(readOnly = false)
	public void add(ShopCash shopCash) {
		super.save(shopCash);
		// 减钱
		shopService.updateBalance(shopCash.getShop(), shopCash.getBank() + StringUtils.right(shopCash.getAccount(), 4), shopCash.getAmount(), "0", null, null, null, null, null);
		
	}
	
}