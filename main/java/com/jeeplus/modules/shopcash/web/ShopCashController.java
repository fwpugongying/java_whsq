/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcash.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.shopcash.entity.ShopCash;
import com.jeeplus.modules.shopcash.service.ShopCashService;

/**
 * 实体商家提现Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/shopcash/shopCash")
public class ShopCashController extends BaseController {

	@Autowired
	private ShopCashService shopCashService;
	
	@ModelAttribute
	public ShopCash get(@RequestParam(required=false) String id) {
		ShopCash entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopCashService.get(id);
		}
		if (entity == null){
			entity = new ShopCash();
		}
		return entity;
	}
	
	/**
	 * 实体商家提现列表页面
	 */
	@RequiresPermissions("shopcash:shopCash:list")
	@RequestMapping(value = {"list", ""})
	public String list(ShopCash shopCash, Model model) {
		model.addAttribute("shopCash", shopCash);
		return "modules/shopcash/shopCashList";
	}
	
		/**
	 * 实体商家提现列表数据
	 */
	@ResponseBody
	@RequiresPermissions("shopcash:shopCash:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ShopCash shopCash, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopCash> page = shopCashService.findPage(new Page<ShopCash>(request, response), shopCash); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑实体商家提现表单页面
	 */
	@RequiresPermissions(value={"shopcash:shopCash:view","shopcash:shopCash:add","shopcash:shopCash:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ShopCash shopCash, Model model) {
		model.addAttribute("shopCash", shopCash);
		return "modules/shopcash/shopCashForm";
	}

	/**
	 * 保存实体商家提现
	 */
	@ResponseBody
	@RequiresPermissions(value={"shopcash:shopCash:add","shopcash:shopCash:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ShopCash shopCash, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(shopCash);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		shopCashService.save(shopCash);//保存
		j.setSuccess(true);
		j.setMsg("保存实体商家提现成功");
		return j;
	}
	/**
	 * 提现审核
	 */
	@ResponseBody
	@RequiresPermissions(value="shopcash:shopCash:edit")
	@RequestMapping(value = "audit")
	public AjaxJson audit(ShopCash shopCash, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (!"0".equals(shopCash.getState())) {
			j.setSuccess(false);
			j.setMsg("只能审核待审核状态的申请");
			return j;
		}
		shopCash.setState(type);
		shopCash.setAuditDate(new Date());
		shopCashService.audit(shopCash);//审核
		j.setSuccess(true);
		j.setMsg("审核提现成功");
		return j;
	}
	
	/**
	 * 删除实体商家提现
	 */
	@ResponseBody
	@RequiresPermissions("shopcash:shopCash:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ShopCash shopCash) {
		AjaxJson j = new AjaxJson();
		shopCashService.delete(shopCash);
		j.setMsg("删除实体商家提现成功");
		return j;
	}
	
	/**
	 * 批量删除实体商家提现
	 */
	@ResponseBody
	@RequiresPermissions("shopcash:shopCash:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			shopCashService.delete(shopCashService.get(id));
		}
		j.setMsg("删除实体商家提现成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("shopcash:shopCash:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ShopCash shopCash, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体商家提现"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ShopCash> page = shopCashService.findPage(new Page<ShopCash>(request, response, -1), shopCash);
    		new ExportExcel("实体商家提现", ShopCash.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出实体商家提现记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("shopcash:shopCash:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ShopCash> list = ei.getDataList(ShopCash.class);
			for (ShopCash shopCash : list){
				try{
					shopCashService.save(shopCash);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条实体商家提现记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条实体商家提现记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入实体商家提现失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入实体商家提现数据模板
	 */
	@ResponseBody
	@RequiresPermissions("shopcash:shopCash:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体商家提现数据导入模板.xlsx";
    		List<ShopCash> list = Lists.newArrayList(); 
    		new ExportExcel("实体商家提现数据", ShopCash.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}