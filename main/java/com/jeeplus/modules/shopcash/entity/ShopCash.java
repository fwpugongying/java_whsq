/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcash.entity;

import com.jeeplus.modules.shop.entity.Shop;
import javax.validation.constraints.NotNull;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 实体商家提现Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class ShopCash extends DataEntity<ShopCash> {
	
	private static final long serialVersionUID = 1L;
	private Shop shop;		// 店铺
	private String amount;		// 金额
	private String fee;		// 手续费
	private String username;		// 姓名
	private String bank;		// 银行
	private String account;		// 卡号
	private String state;		// 状态 0待审核 1通过 2拒绝
	private Date auditDate;		// 审核时间
	
	public ShopCash() {
		super();
	}

	public ShopCash(String id){
		super(id);
	}

	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Shop.class, value="shop.title", align=2, sort=1)
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	@ExcelField(title="金额", align=2, sort=2)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="姓名", align=2, sort=3)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="银行", align=2, sort=4)
	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}
	
	@ExcelField(title="卡号", align=2, sort=5)
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	
	@ExcelField(title="状态 0待审核 1通过 2拒绝", dictType="cash_state", align=2, sort=7)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=8)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}
	
}