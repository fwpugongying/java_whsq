/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tsupport.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.tsupport.entity.TSupport;
import com.jeeplus.modules.tsupport.mapper.TSupportMapper;

/**
 * 扶贫信息管理Service
 * @author ws
 * @version 2019-10-21
 */
@Service
@Transactional(readOnly = true)
public class TSupportService extends CrudService<TSupportMapper, TSupport> {

	public TSupport get(String id) {
		return super.get(id);
	}
	
	public List<TSupport> findList(TSupport tSupport) {
		return super.findList(tSupport);
	}
	
	public Page<TSupport> findPage(Page<TSupport> page, TSupport tSupport) {
		return super.findPage(page, tSupport);
	}
	
	@Transactional(readOnly = false)
	public void save(TSupport tSupport) {
		super.save(tSupport);
	}
	
	@Transactional(readOnly = false)
	public void delete(TSupport tSupport) {
		super.delete(tSupport);
	}
	
}