/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tsupport.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.tsupport.entity.TSupport;

/**
 * 扶贫信息管理MAPPER接口
 * @author ws
 * @version 2019-10-21
 */
@MyBatisMapper
public interface TSupportMapper extends BaseMapper<TSupport> {
	
}