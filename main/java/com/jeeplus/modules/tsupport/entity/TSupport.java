/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tsupport.entity;

import com.jeeplus.modules.sys.entity.Area;
import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 扶贫信息管理Entity
 * @author ws
 * @version 2019-10-21
 */
public class TSupport extends DataEntity<TSupport> {
	
	private static final long serialVersionUID = 1L;
	private Area area;		// 区县ID
	private String title;		// 标题
	private String image;		// 图片
	private String video;		// 视频
	private String videoImage;		// 视频封面图
	private String content;		// 内容
	
	public TSupport() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public TSupport(String id){
		super(id);
	}

	@NotNull(message="区县ID不能为空")
	@ExcelField(title="区县ID", fieldType=Area.class, value="area.name", align=2, sort=1)
	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	@ExcelField(title="标题", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="图片", align=2, sort=3)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	@ExcelField(title="视频", align=2, sort=4)
	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}
	
	@ExcelField(title="视频封面图", align=2, sort=5)
	public String getVideoImage() {
		return videoImage;
	}

	public void setVideoImage(String videoImage) {
		this.videoImage = videoImage;
	}
	
	@ExcelField(title="内容", align=2, sort=6)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * 富文本路径
	 * @return
	 */
	public String getUrl() {
		return Global.getConfig("filePath") + "/display/support?id=" + id;
	}
}