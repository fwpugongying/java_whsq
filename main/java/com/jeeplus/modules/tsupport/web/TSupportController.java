/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tsupport.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.tsupport.entity.TSupport;
import com.jeeplus.modules.tsupport.service.TSupportService;

/**
 * 扶贫信息管理Controller
 * @author ws
 * @version 2019-10-21
 */
@Controller
@RequestMapping(value = "${adminPath}/tsupport/tSupport")
public class TSupportController extends BaseController {

	@Autowired
	private TSupportService tSupportService;
	
	@ModelAttribute
	public TSupport get(@RequestParam(required=false) String id) {
		TSupport entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tSupportService.get(id);
		}
		if (entity == null){
			entity = new TSupport();
		}
		return entity;
	}
	
	/**
	 * 扶贫信息管理列表页面
	 */
	@RequiresPermissions("tsupport:tSupport:list")
	@RequestMapping(value = {"list", ""})
	public String list(TSupport tSupport, Model model) {
		model.addAttribute("tSupport", tSupport);
		return "modules/tsupport/tSupportList";
	}
	
		/**
	 * 扶贫信息管理列表数据
	 */
	@ResponseBody
	@RequiresPermissions("tsupport:tSupport:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(TSupport tSupport, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TSupport> page = tSupportService.findPage(new Page<TSupport>(request, response), tSupport); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑扶贫信息管理表单页面
	 */
	@RequiresPermissions(value={"tsupport:tSupport:view","tsupport:tSupport:add","tsupport:tSupport:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(TSupport tSupport, Model model) {
		model.addAttribute("tSupport", tSupport);
		return "modules/tsupport/tSupportForm";
	}

	/**
	 * 保存扶贫信息管理
	 */
	@ResponseBody
	@RequiresPermissions(value={"tsupport:tSupport:add","tsupport:tSupport:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(TSupport tSupport, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(tSupport);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		tSupportService.save(tSupport);//保存
		j.setSuccess(true);
		j.setMsg("保存扶贫信息管理成功");
		return j;
	}
	
	/**
	 * 删除扶贫信息管理
	 */
	@ResponseBody
	@RequiresPermissions("tsupport:tSupport:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(TSupport tSupport) {
		AjaxJson j = new AjaxJson();
		tSupportService.delete(tSupport);
		j.setMsg("删除扶贫信息管理成功");
		return j;
	}
	
	/**
	 * 批量删除扶贫信息管理
	 */
	@ResponseBody
	@RequiresPermissions("tsupport:tSupport:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			tSupportService.delete(tSupportService.get(id));
		}
		j.setMsg("删除扶贫信息管理成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("tsupport:tSupport:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(TSupport tSupport, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "扶贫信息管理"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<TSupport> page = tSupportService.findPage(new Page<TSupport>(request, response, -1), tSupport);
    		new ExportExcel("扶贫信息管理", TSupport.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出扶贫信息管理记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("tsupport:tSupport:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<TSupport> list = ei.getDataList(TSupport.class);
			for (TSupport tSupport : list){
				try{
					tSupportService.save(tSupport);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条扶贫信息管理记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条扶贫信息管理记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入扶贫信息管理失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入扶贫信息管理数据模板
	 */
	@ResponseBody
	@RequiresPermissions("tsupport:tSupport:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "扶贫信息管理数据导入模板.xlsx";
    		List<TSupport> list = Lists.newArrayList(); 
    		new ExportExcel("扶贫信息管理数据", TSupport.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}