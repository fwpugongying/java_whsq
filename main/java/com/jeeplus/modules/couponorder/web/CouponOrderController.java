/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.couponorder.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.couponorder.entity.CouponOrder;
import com.jeeplus.modules.couponorder.service.CouponOrderService;

/**
 * 实体店优惠券订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/couponorder/couponOrder")
public class CouponOrderController extends BaseController {

	@Autowired
	private CouponOrderService couponOrderService;
	
	@ModelAttribute
	public CouponOrder get(@RequestParam(required=false) String id) {
		CouponOrder entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = couponOrderService.get(id);
		}
		if (entity == null){
			entity = new CouponOrder();
		}
		return entity;
	}
	
	/**
	 * 实体店优惠券订单列表页面
	 */
	@RequiresPermissions("couponorder:couponOrder:list")
	@RequestMapping(value = {"list", ""})
	public String list(CouponOrder couponOrder, Model model) {
		model.addAttribute("couponOrder", couponOrder);
		return "modules/couponorder/couponOrderList";
	}
	
		/**
	 * 实体店优惠券订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("couponorder:couponOrder:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(CouponOrder couponOrder, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CouponOrder> page = couponOrderService.findPage(new Page<CouponOrder>(request, response), couponOrder); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑实体店优惠券订单表单页面
	 */
	@RequiresPermissions(value={"couponorder:couponOrder:view","couponorder:couponOrder:add","couponorder:couponOrder:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(CouponOrder couponOrder, Model model) {
		model.addAttribute("couponOrder", couponOrder);
		return "modules/couponorder/couponOrderForm";
	}

	/**
	 * 保存实体店优惠券订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"couponorder:couponOrder:add","couponorder:couponOrder:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(CouponOrder couponOrder, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(couponOrder);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		couponOrderService.save(couponOrder);//保存
		j.setSuccess(true);
		j.setMsg("保存实体店优惠券订单成功");
		return j;
	}
	
	/**
	 * 删除实体店优惠券订单
	 */
	@ResponseBody
	@RequiresPermissions("couponorder:couponOrder:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(CouponOrder couponOrder) {
		AjaxJson j = new AjaxJson();
		couponOrderService.delete(couponOrder);
		j.setMsg("删除实体店优惠券订单成功");
		return j;
	}
	
	/**
	 * 批量删除实体店优惠券订单
	 */
	@ResponseBody
	@RequiresPermissions("couponorder:couponOrder:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			couponOrderService.delete(couponOrderService.get(id));
		}
		j.setMsg("删除实体店优惠券订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("couponorder:couponOrder:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(CouponOrder couponOrder, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店优惠券订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<CouponOrder> page = couponOrderService.findPage(new Page<CouponOrder>(request, response, -1), couponOrder);
    		new ExportExcel("实体店优惠券订单", CouponOrder.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出实体店优惠券订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("couponorder:couponOrder:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<CouponOrder> list = ei.getDataList(CouponOrder.class);
			for (CouponOrder couponOrder : list){
				try{
					couponOrderService.save(couponOrder);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条实体店优惠券订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条实体店优惠券订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入实体店优惠券订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入实体店优惠券订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("couponorder:couponOrder:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店优惠券订单数据导入模板.xlsx";
    		List<CouponOrder> list = Lists.newArrayList(); 
    		new ExportExcel("实体店优惠券订单数据", CouponOrder.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}