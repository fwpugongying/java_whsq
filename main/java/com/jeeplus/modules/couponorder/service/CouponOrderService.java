/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.couponorder.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.api.service.ApiService;
import com.jeeplus.modules.couponorder.entity.CouponOrder;
import com.jeeplus.modules.couponorder.mapper.CouponOrderMapper;
import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.modules.member.service.MemberService;
import com.jeeplus.modules.shop.service.ShopService;

/**
 * 实体店优惠券订单Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class CouponOrderService extends CrudService<CouponOrderMapper, CouponOrder> {
	@Autowired
	private ApiService apiService;
	@Autowired
	private ShopService shopService;
	@Autowired
	private MemberService memberService;
	
	public CouponOrder get(String id) {
		return super.get(id);
	}
	
	public List<CouponOrder> findList(CouponOrder couponOrder) {
		return super.findList(couponOrder);
	}
	
	public Page<CouponOrder> findPage(Page<CouponOrder> page, CouponOrder couponOrder) {
		return super.findPage(page, couponOrder);
	}
	
	@Transactional(readOnly = false)
	public void save(CouponOrder couponOrder) {
		super.save(couponOrder);
	}
	
	@Transactional(readOnly = false)
	public void delete(CouponOrder couponOrder) {
		super.delete(couponOrder);
	}

	/**
	 * 支付处理
	 * @param couponOrder
	 */
	@Transactional(readOnly = false)
	public void pay(CouponOrder couponOrder) {
		super.save(couponOrder);
		
		// 调用三方
		Member member = memberService.get(couponOrder.getMember());
		if (member != null) {
		}
	}

	/**
	 * 退款
	 * @param
	 */
	@Transactional(readOnly = false)
	public void refund(CouponOrder couponOrder) {
		super.save(couponOrder);
		
		// 调用退款
		apiService.refund(couponOrder.getPayNo(), couponOrder.getPrice(), couponOrder.getPrice(), couponOrder.getPayType(),couponOrder.getTrade_type());
		
		// 调用三方失效订单
		JSONObject object = apiService.orderInvalid(couponOrder.getId());
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("我惠会员系统请求失效订单失败：" + object.getString("respMsg"));
		}
	}

	/**
	 * 核销
	 * @param couponOrder
	 */
	@Transactional(readOnly = false)
	public void hexiao(CouponOrder couponOrder) {
		super.save(couponOrder);
		
		// 商家加钱
		// 获取结算价
		//ShopCoupon shopCoupon = shopCouponMapper.get(couponOrder.getCouponId());
		//if (shopCoupon != null) {
			shopService.updateBalance(couponOrder.getShop(), "订单" + couponOrder.getId(), couponOrder.getAmount(), "1", couponOrder.getId(), "优惠券", couponOrder.getMember().getNickname(), couponOrder.getMember().getPhone(), new Date());
		//}
		// 用户加铜板
		memberService.updatePoint(couponOrder.getMember(), couponOrder.getPoint(), "1");
		
		// 调用三方结算
		JSONObject object = apiService.orderSettle(couponOrder.getId(), couponOrder.getPrice(), couponOrder.getPoint());
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
		}
	}
	
}