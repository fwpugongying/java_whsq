/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.couponorder.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.shop.entity.Shop;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 实体店优惠券订单Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class CouponOrder extends DataEntity<CouponOrder> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private Shop shop;		// 商家
	private String couponId;		// 优惠券
	private String code;		// 编码
	private String price;		// 售价
	private String money;		// 可抵金额
	private String amount;		// 结算价
	private Date startDate;		// 有效开始日期
	private Date endDate;		// 有效截止日期
	private Date payDate;		// 支付时间
	private String payType;		// 支付方式 1支付宝 2微信
	private String point;		// 铜板
	private String state;		// 状态 0待付 1待使用 2已使用 3已过期 4已退款
	private Date useDate;		// 使用时间
	private Date refundDate;		// 退款时间
	private Date cancelDate;		// 取消时间
	private String cancelReason;	// 取消原因
	private String trade_type;		 // 支付类型
	private String payNo;		     // 付款单号
	
	public CouponOrder() {
		super();
	}

	public CouponOrder(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@NotNull(message="商家不能为空")
	@ExcelField(title="商家", fieldType=Shop.class, value="shop.title", align=2, sort=2)
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	@ExcelField(title="优惠券", align=2, sort=3)
	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	
	@ExcelField(title="编码", align=2, sort=4)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="售价", align=2, sort=5)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@ExcelField(title="可抵金额", align=2, sort=6)
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="有效开始日期不能为空")
	@ExcelField(title="有效开始日期", align=2, sort=7)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="有效截止日期不能为空")
	@ExcelField(title="有效截止日期", align=2, sort=8)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="支付时间", align=2, sort=10)
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	
	@ExcelField(title="支付方式 1支付宝 2微信", dictType="pay_type", align=2, sort=11)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	@ExcelField(title="铜板", align=2, sort=12)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	@ExcelField(title="状态 0待付 1待使用 2已使用 3已过期 4已退款", dictType="coupon_order_state", align=2, sort=13)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="使用时间", align=2, sort=14)
	public Date getUseDate() {
		return useDate;
	}

	public void setUseDate(Date useDate) {
		this.useDate = useDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="退款时间", align=2, sort=15)
	public Date getRefundDate() {
		return refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTrade_type() {
		return trade_type;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
    }
}