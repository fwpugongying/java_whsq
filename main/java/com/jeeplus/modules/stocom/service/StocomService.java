/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.stocom.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.stocom.entity.Stocom;
import com.jeeplus.modules.stocom.mapper.StocomMapper;
/**
 * 云店版块套餐Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class StocomService extends CrudService<StocomMapper, Stocom> {


	@Autowired

	public Stocom get(String id) {
		return super.get(id);
	}

	public List<Stocom> findList(Stocom stocom) {
		return super.findList(stocom);
	}
	public List<Stocom> findUniqueByProperty(Stocom stocom) {
		return super.findList(stocom);
	}
	public Stocom getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Stocom> findPage(Page<Stocom> page, Stocom stocom) {
		return super.findPage(page, stocom);
	}
	@Transactional(readOnly = false)
	public void save(Stocom stocom) {
		super.save(stocom);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Stocom stocom) {
		super.save(stocom);
//		// 调用三方
//		List<WohuiEntity> list = Lists.newArrayList();
//		list.add(new WohuiEntity("CardGUID", stocom.getMember().getId()));
//		list.add(new WohuiEntity("Money", stocom.getMoney()));
//		JSONObject object = WohuiUtils.send(WohuiUtils.CSOpen, list);
//		if (!"0000".equals(object.getString("respCode"))) {
//			logger.error("开通云店版块云店铺订单号"+stocom.getId()+"失败：" + object.getString("respMsg"));
//		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Stocom stocom) {
		super.delete(stocom);
	}

}