/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.stocom.web;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jeeplus.modules.api.utils.KuaidiEntity;
import com.jeeplus.modules.api.utils.ToolsUtil;
import com.jeeplus.modules.delivery.entity.Delivery;
import com.jeeplus.modules.delivery.service.DeliveryService;
import com.jeeplus.modules.stocom.entity.Stocom;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.stocom.entity.Stocom;
import com.jeeplus.modules.stocom.service.StocomService;

/**
 * 云店铺订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/stocom/stocom")
public class StocomController extends BaseController {

	@Autowired
	private StocomService stocomService;
	@Autowired
	private MsgService msgService;
	@Autowired
	private DeliveryService deliveryService;


	@ModelAttribute
	public Stocom get(@RequestParam(required=false) String id) {
		Stocom entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = stocomService.get(id);
		}
		if (entity == null){
			entity = new Stocom();
		}
		return entity;
	}
	
	/**
	 * 云店铺订单列表页面
	 */
	@RequiresPermissions("stocom:stocom:list")
	@RequestMapping(value = {"list", ""})
	public String list(Stocom stocom, Model model) {
		model.addAttribute("stocom", stocom);
		return "modules/stocom/stocomList";
	}
	
		/**
	 * 云店铺订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("stocom:stocom:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Stocom stocom, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Stocom> page = stocomService.findPage(new Page<Stocom>(request, response), stocom);
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑云店铺订单表单页面
	 */
	@RequiresPermissions(value={"stocom:stocom:view","stocom:stocom:add","stocom:stocom:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Stocom stocom, Model model) {
		model.addAttribute("stocom", stocom);
		return "modules/stocom/stocomForm";
	}

	/**
	 * 保存云店铺
	 */
	@ResponseBody
	@RequiresPermissions(value={"stocom:stocom:add","stocom:stocom:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Stocom stocom, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(stocom);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		stocomService.save(stocom);//保存
		j.setSuccess(true);
		j.setMsg("保存云店铺订单成功");
		return j;
	}
	/**
	 * 冻结解冻流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions(value="stocom:stocom:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Stocom stocom, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(stocom.getId())) {
			stocom.setState(type);
			stocomService.save(stocom);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("stocom:stocom:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Stocom stocom) {
		AjaxJson j = new AjaxJson();
		stocomService.delete(stocom);
		j.setMsg("删除云店铺订单成功");
		return j;
	}
	
	/**
	 * 批量删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("stocom:stocom:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			stocomService.delete(stocomService.get(id));
		}
		j.setMsg("删除云店铺订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("stocom:stocom:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Stocom stocom, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "云店铺订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Stocom> page = stocomService.findPage(new Page<Stocom>(request, response, -1), stocom);
    		new ExportExcel("云店铺订单", Stocom.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出云店铺订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("stocom:stocom:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Stocom> list = ei.getDataList(Stocom.class);
			for (Stocom stocom : list){
				try{
					stocomService.save(stocom);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条云店铺订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条云店铺订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入云店铺订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入流量补贴订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("stocom:stocom:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单数据导入模板.xlsx";
    		List<Stocom> list = Lists.newArrayList();
    		new ExportExcel("流量补贴订单数据", Stocom.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}