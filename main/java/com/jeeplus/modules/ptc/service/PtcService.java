/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.ptc.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.ptc.entity.Ptc;
import com.jeeplus.modules.ptc.mapper.PtcMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class PtcService extends CrudService<PtcMapper, Ptc> {


	@Autowired

	public Ptc get(String id) {
		return super.get(id);
	}

	public List<Ptc> findList(Ptc ptc) {
		return super.findList(ptc);
	}
	public List<Ptc> findUniqueByProperty(Ptc ptc) {
		return super.findList(ptc);
	}

	public Ptc getOrderno(String orderno) {
		return findUniqueByProperty("orderno", orderno);
	}

	public Ptc getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Ptc> findPage(Page<Ptc> page, Ptc ptc) {
		return super.findPage(page, ptc);
	}
	@Transactional(readOnly = false)
	public void save(Ptc ptc) {
		super.save(ptc);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Ptc ptc) {
		super.save(ptc);

//		// 调用三方
//		List<WohuiEntity> list = Lists.newArrayList();
//		list.add(new WohuiEntity("OrderNO", ptc.getOrderNo()));
//		JSONObject object = WohuiUtils.send(WohuiUtils.LLBTPay, list);
//		if (!"0000".equals(object.getString("respCode"))) {
//			logger.error("开通流量补贴订单号"+ptc.getOrderNo()+"失败：" + object.getString("respMsg"));
//		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Ptc ptc) {
		super.delete(ptc);
	}

}