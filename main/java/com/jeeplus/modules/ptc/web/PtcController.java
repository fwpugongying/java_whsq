/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.ptc.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.ptc.entity.Ptc;
import com.jeeplus.modules.ptc.service.PtcService;

/**
 * 流量补贴订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/ptc/ptc")
public class PtcController extends BaseController {

	@Autowired
	private PtcService ptcService;
	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Ptc get(@RequestParam(required=false) String id) {
		Ptc entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = ptcService.get(id);
		}
		if (entity == null){
			entity = new Ptc();
		}
		return entity;
	}
	
	/**
	 * 流量补贴订单列表页面
	 */
	@RequiresPermissions("ptc:ptc:list")
	@RequestMapping(value = {"list", ""})
	public String list(Ptc ptc, Model model) {
		model.addAttribute("ptc", ptc);
		return "modules/ptc/ptcList";
	}
	
		/**
	 * 流量补贴订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("ptc:ptc:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Ptc ptc, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Ptc> page = ptcService.findPage(new Page<Ptc>(request, response), ptc); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑流量补贴订单表单页面
	 */
	@RequiresPermissions(value={"ptc:ptc:view","ptc:ptc:add","ptc:ptc:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Ptc ptc, Model model) {
		model.addAttribute("ptc", ptc);
		return "modules/ptc/ptcForm";
	}

	/**
	 * 保存流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"ptc:ptc:add","ptc:ptc:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Ptc ptc, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(ptc);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		ptcService.save(ptc);//保存
		j.setSuccess(true);
		j.setMsg("保存流量补贴订单成功");
		return j;
	}
	/**
	 * 冻结解冻流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions(value="ptc:ptc:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Ptc ptc, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(ptc.getId())) {
			ptc.setState(type);
			ptcService.save(ptc);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("ptc:ptc:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Ptc ptc) {
		AjaxJson j = new AjaxJson();
		ptcService.delete(ptc);
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 批量删除流量补贴订单
	 */
	@ResponseBody
	@RequiresPermissions("ptc:ptc:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			ptcService.delete(ptcService.get(id));
		}
		j.setMsg("删除流量补贴订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("ptc:ptc:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Ptc ptc, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Ptc> page = ptcService.findPage(new Page<Ptc>(request, response, -1), ptc);
    		new ExportExcel("流量补贴订单", Ptc.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出流量补贴订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("ptc:ptc:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Ptc> list = ei.getDataList(Ptc.class);
			for (Ptc ptc : list){
				try{
					ptcService.save(ptc);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条流量补贴订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条流量补贴订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入流量补贴订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入流量补贴订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("ptc:ptc:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "流量补贴订单数据导入模板.xlsx";
    		List<Ptc> list = Lists.newArrayList(); 
    		new ExportExcel("流量补贴订单数据", Ptc.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}