/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.gift.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.gift.entity.Gift;
import com.jeeplus.modules.gift.mapper.GiftMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class GiftService extends CrudService<GiftMapper, Gift> {


	@Autowired

	public Gift get(String id) {
		return super.get(id);
	}

	public List<Gift> findList(Gift gift) {
		return super.findList(gift);
	}
	public List<Gift> findUniqueByProperty(Gift gift) {
		return super.findList(gift);
	}
	public Gift getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Gift> findPage(Page<Gift> page, Gift gift) {
		return super.findPage(page, gift);
	}
	@Transactional(readOnly = false)
	public void save(Gift gift) {
		super.save(gift);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Gift gift) {
		super.save(gift);
		String PayMethod="10";
		if("1".equals(gift.getPayType())){//支付宝
			PayMethod="40";
		}else if("2".equals(gift.getPayType())){//微信
			PayMethod="30";
		}else if("3".equals(gift.getPayType())){//平台购物券支付
			PayMethod="10";
		}else if("4".equals(gift.getPayType())){//商城购物券支付
			PayMethod="20";
		}

		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("GiftGUID", gift.getGguid()));
		list.add(new WohuiEntity("PayMethod", PayMethod));
		JSONObject object = WohuiUtils.send(WohuiUtils.GiftSurePay, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("赠品支付"+gift.getGguid()+"失败：" + object.getString("respMsg"));
		}
	}

	@Transactional(readOnly = false)
	public void delete(Gift gift) {
		super.delete(gift);
	}

}