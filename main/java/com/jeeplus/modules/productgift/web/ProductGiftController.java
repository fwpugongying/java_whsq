/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productgift.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.productgift.entity.ProductGift;
import com.jeeplus.modules.productgift.service.ProductGiftService;

/**
 * 拼团商品赠品Controller
 * @author lixinapp
 * @version 2019-10-26
 */
@Controller
@RequestMapping(value = "${adminPath}/productgift/productGift")
public class ProductGiftController extends BaseController {

	@Autowired
	private ProductGiftService productGiftService;
	
	@ModelAttribute
	public ProductGift get(@RequestParam(required=false) String id) {
		ProductGift entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = productGiftService.get(id);
		}
		if (entity == null){
			entity = new ProductGift();
		}
		return entity;
	}
	
	/**
	 * 拼团商品赠品列表页面
	 */
	@RequiresPermissions("productgift:productGift:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProductGift productGift, Model model) {
		model.addAttribute("productGift", productGift);
		return "modules/productgift/productGiftList";
	}
	
		/**
	 * 拼团商品赠品列表数据
	 */
	@ResponseBody
	@RequiresPermissions("productgift:productGift:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProductGift productGift, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProductGift> page = productGiftService.findPage(new Page<ProductGift>(request, response), productGift); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑拼团商品赠品表单页面
	 */
	@RequiresPermissions(value={"productgift:productGift:view","productgift:productGift:add","productgift:productGift:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProductGift productGift, Model model) {
		model.addAttribute("productGift", productGift);
		return "modules/productgift/productGiftForm";
	}

	/**
	 * 保存拼团商品赠品
	 */
	@ResponseBody
	@RequiresPermissions(value={"productgift:productGift:add","productgift:productGift:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProductGift productGift, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(productGift);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		productGiftService.save(productGift);//保存
		j.setSuccess(true);
		j.setMsg("保存拼团商品赠品成功");
		return j;
	}
	
	/**
	 * 删除拼团商品赠品
	 */
	@ResponseBody
	@RequiresPermissions("productgift:productGift:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProductGift productGift) {
		AjaxJson j = new AjaxJson();
		productGiftService.delete(productGift);
		j.setMsg("删除拼团商品赠品成功");
		return j;
	}
	
	/**
	 * 批量删除拼团商品赠品
	 */
	@ResponseBody
	@RequiresPermissions("productgift:productGift:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			productGiftService.delete(productGiftService.get(id));
		}
		j.setMsg("删除拼团商品赠品成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("productgift:productGift:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProductGift productGift, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "拼团商品赠品"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProductGift> page = productGiftService.findPage(new Page<ProductGift>(request, response, -1), productGift);
    		new ExportExcel("拼团商品赠品", ProductGift.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出拼团商品赠品记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("productgift:productGift:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProductGift> list = ei.getDataList(ProductGift.class);
			for (ProductGift productGift : list){
				try{
					productGiftService.save(productGift);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条拼团商品赠品记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条拼团商品赠品记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入拼团商品赠品失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入拼团商品赠品数据模板
	 */
	@ResponseBody
	@RequiresPermissions("productgift:productGift:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "拼团商品赠品数据导入模板.xlsx";
    		List<ProductGift> list = Lists.newArrayList(); 
    		new ExportExcel("拼团商品赠品数据", ProductGift.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}