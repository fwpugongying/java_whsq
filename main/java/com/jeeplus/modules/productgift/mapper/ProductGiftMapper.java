/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productgift.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.productgift.entity.ProductGift;

/**
 * 拼团商品赠品MAPPER接口
 * @author lixinapp
 * @version 2019-10-26
 */
@MyBatisMapper
public interface ProductGiftMapper extends BaseMapper<ProductGift> {
	
}