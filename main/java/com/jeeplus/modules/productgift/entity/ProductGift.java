/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productgift.entity;

import com.jeeplus.modules.product.entity.Product;
import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 拼团商品赠品Entity
 * @author lixinapp
 * @version 2019-10-26
 */
public class ProductGift extends DataEntity<ProductGift> {
	
	private static final long serialVersionUID = 1L;
	private Product product;		// 拼团商品
	private Product gift;		// 赠品
	private Integer qty;		// 数量
	
	public ProductGift() {
		super();
	}

	public ProductGift(String id){
		super(id);
	}

	@NotNull(message="拼团商品不能为空")
	@ExcelField(title="拼团商品", fieldType=Product.class, value="product.title", align=2, sort=1)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@NotNull(message="赠品不能为空")
	@ExcelField(title="赠品", fieldType=Product.class, value="gift.title", align=2, sort=2)
	public Product getGift() {
		return gift;
	}

	public void setGift(Product gift) {
		this.gift = gift;
	}
	
	@NotNull(message="数量不能为空")
	@ExcelField(title="数量", align=2, sort=3)
	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
}