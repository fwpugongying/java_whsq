/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productgift.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.productgift.entity.ProductGift;
import com.jeeplus.modules.productgift.mapper.ProductGiftMapper;

/**
 * 拼团商品赠品Service
 * @author lixinapp
 * @version 2019-10-26
 */
@Service
@Transactional(readOnly = true)
public class ProductGiftService extends CrudService<ProductGiftMapper, ProductGift> {

	public ProductGift get(String id) {
		return super.get(id);
	}
	
	public List<ProductGift> findList(ProductGift productGift) {
		return super.findList(productGift);
	}
	
	public Page<ProductGift> findPage(Page<ProductGift> page, ProductGift productGift) {
		return super.findPage(page, productGift);
	}
	
	@Transactional(readOnly = false)
	public void save(ProductGift productGift) {
		super.save(productGift);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProductGift productGift) {
		super.delete(productGift);
	}
	
}