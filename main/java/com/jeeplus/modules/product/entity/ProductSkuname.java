/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.product.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品规格名称Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class ProductSkuname extends DataEntity<ProductSkuname> {
	
	private static final long serialVersionUID = 1L;
	private Product product;		// 商品 父类
	private String title;		// 规格名称

	public ProductSkuname() {
		super();
	}

	public ProductSkuname(String id){
		super(id);
	}

	public ProductSkuname(Product product){
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	@ExcelField(title="规格名称", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}