/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.product.entity;

import com.jeeplus.modules.store.entity.Store;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.brand.entity.Brand;
import com.jeeplus.modules.productcategory.entity.ProductCategory;
import com.jeeplus.modules.sys.entity.Area;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.google.common.collect.Lists;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 云店商品Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Product extends DataEntity<Product> {
	
	private static final long serialVersionUID = 1L;
	private String code;		// 商品编码
	private Store store;		// 店铺
	private String title;		// 商品名称
	private String subtitle;		// 副标题
	private Brand brand;		// 品牌
	private String icon;		// 列表图
	private String images;		// 轮播图
	private ProductCategory category;		// 分类
	private Integer sales;		// 销量
	private Integer hits;		// 浏览量
	private String point;		// 铜板
	private String state;		// 状态 0正常 1下架
	private String content;		// 详情
	private String oldPrice;		// 市场价
	private String discount;		// 优惠券
	private String price;		// 券后价
	private String amount;		// 结算价
	private Area area;		// 区县
	private String auditState;		// 审核状态 0待审核 1审核通过 2审核拒绝
	private String isGroup;		// 是否拼团：0-否，1-是
	private String isHot;		// 是否爆品:0-否，1-是
	private String isNew;		// 是否新品:0-否，1-是
	private String isFupin;		// 是否扶贫：0-否，1-是
	private String isteprice;		// 是否特价商品：0-否，1-是
	private Integer tepricenum;        // 特价商品限购0是不限购，1是限购
	private String groupPrice;        // 拼团价格
	private String giftprice;        // 赠品需支付价格默认60
	private Date auditDate;        // 审核时间
	private String province;
	private String city;
	private String district;
	private String isTuijian;// 推荐
	private String ptFwmoney;// 推荐
	private String nodelarea;// 新加 不发货区域提示
	private Area distant;        //
	private String times;    //拼团开始多久结束小时
	private String isMradd;// 是否可以自己选择地址
	private String ptRenNum;// 拼团人数
	private String dlbt;// 代理补贴
	private String isDl;// 是否是拼购商品
	private String isbd;// 是否为补单商品0否1是
	private String isYg;// 几个订单为一组抽奖开始抽奖
	private String iscombo;// 套餐专区0否1是
	private String isshow;// 1隐藏，正常搜索不显示 0默认显示
	private String diffprice;//差额

	private String subsidyprice;        // 补贴价格
	private String openprice;        // 预售价格

	public String getSubsidyprice() {
		return subsidyprice;
	}
	public void setSubsidyprice(String subsidyprice) {
		this.subsidyprice = subsidyprice;
	}

	public String getOpenprice() {
		return openprice;
	}
	public void setOpenprice(String openprice) {
		this.openprice = openprice;
	}

	public String getDiffprice() {
		return diffprice;
	}
	public void setDiffprice(String diffprice) {
		this.diffprice = diffprice;
	}

	public String getIsshow() {
		return isshow;
	}
	public void setIsshow(String isshow) {
		this.isshow = isshow;
	}

	public String getDlbt() {
		return dlbt;
	}

	public void setDlbt(String dlbt) {
		this.dlbt = dlbt;
	}

	public String getIsDl() {
		return isDl;
	}

	public void setIsDl(String isDl) {
		this.isDl = isDl;
	}
	public String getIsbd() {
		return isbd;
	}

	public void setIsbd(String isbd) {
		this.isbd = isbd;
	}

	public String getIsYg() {
		return isYg;
	}

	public void setIsYg(String isYg) {
		this.isYg = isYg;
	}

	public String getIscombo() {
		return iscombo;
	}

	public void setIscombo(String iscombo) {
		this.iscombo = iscombo;
	}

	public String getIsMradd() {
		return isMradd;
	}

	public void setIsMradd(String isMradd) {
		this.isMradd = isMradd;
	}

	public String getPtRenNum() {
		return ptRenNum;
	}

	public void setPtRenNum(String ptRenNum) {
		this.ptRenNum = ptRenNum;
	}


	public String getTimes() {
		return times;
	}

	public void setTimes(String times) {
		this.times = times;
	}

	private List<ProductSkuname> productSkunameList = Lists.newArrayList();		// 子表列表

	public Product() {
		super();
	}

	public Product(String id){
		super(id);
	}

	@ExcelField(title="商品编码", align=2, sort=1)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPtFwmoney() {
		return ptFwmoney;
	}

	public void setPtFwmoney(String ptFwmoney) {
		this.ptFwmoney = ptFwmoney;
	}

	public String getNodelarea() {
		return nodelarea;
	}

	public void setNodelarea(String nodelarea) {
		this.nodelarea = nodelarea;
	}
	public Area getDistant() {
		return distant;
	}

	public void setDistant(Area distant) {
		this.distant = distant;
	}
	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Store.class, value="store.title", align=2, sort=2)
	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}
	
	@ExcelField(title="商品名称", align=2, sort=3)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="副标题", align=2, sort=4)
	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	
	@NotNull(message="品牌不能为空")
	@ExcelField(title="品牌", fieldType=Brand.class, value="brand.title", align=2, sort=5)
	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	
	@ExcelField(title="列表图", align=2, sort=6)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@ExcelField(title="轮播图", align=2, sort=7)
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}
	
	@NotNull(message="分类不能为空")
	@ExcelField(title="分类", fieldType=ProductCategory.class, value="category.name", align=2, sort=8)
	public ProductCategory getCategory() {
		return category;
	}

	public void setCategory(ProductCategory category) {
		this.category = category;
	}
	
	@NotNull(message="销量不能为空")
	@ExcelField(title="销量", align=2, sort=9)
	public Integer getSales() {
		return sales;
	}

	public void setSales(Integer sales) {
		this.sales = sales;
	}
	
	@NotNull(message="浏览量不能为空")
	@ExcelField(title="浏览量", align=2, sort=10)
	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}
	
	@ExcelField(title="铜板", align=2, sort=11)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	@ExcelField(title="状态 0正常 1下架", dictType="product_state", align=2, sort=12)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="详情", align=2, sort=13)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="市场价", align=2, sort=14)
	public String getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(String oldPrice) {
		this.oldPrice = oldPrice;
	}
	
	@ExcelField(title="优惠券", align=2, sort=15)
	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}
	
	@ExcelField(title="券后价", align=2, sort=16)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@ExcelField(title="结算价", align=2, sort=17)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="区县", fieldType=Area.class, value="area.name", align=2, sort=18)
	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	@ExcelField(title="审核状态 0待审核 1审核通过 2审核拒绝", dictType="product_audit_state", align=2, sort=19)
	public String getAuditState() {
		return auditState;
	}

	public void setAuditState(String auditState) {
		this.auditState = auditState;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=20)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	
	public List<ProductSkuname> getProductSkunameList() {
		return productSkunameList;
	}

	public void setProductSkunameList(List<ProductSkuname> productSkunameList) {
		this.productSkunameList = productSkunameList;
	}

	public String getIsGroup() {
		return isGroup;
	}

	public void setIsGroup(String isGroup) {
		this.isGroup = isGroup;
	}

	public String getGroupPrice() {
		return groupPrice;
	}

	public void setGroupPrice(String groupPrice) {
		this.groupPrice = groupPrice;
	}

	public String getGiftprice() {
		return giftprice;
	}

	public void setGiftprice(String giftprice) {
		this.giftprice = giftprice;
	}

	public String getIsHot() {
		return isHot;
	}

	public void setIsHot(String isHot) {
		this.isHot = isHot;
	}

	public String getIsNew() {
		return isNew;
	}

	public void setIsNew(String isNew) {
		this.isNew = isNew;
	}

	public String getIsFupin() {
		return isFupin;
	}

	public void setIsFupin(String isFupin) {
		this.isFupin = isFupin;
	}

	public String getIsteprice() {
		return isteprice;
	}

	public void setIsteprice(String isteprice) {
		this.isteprice = isteprice;
	}

	public Integer getTepricenum() {
		return tepricenum;
	}

	public void setTepricenum(Integer tepricenum) {
		this.tepricenum = tepricenum;
	}

	/**
	 * 富文本路径
	 * @return
	 */
	public String getUrl() {
		return Global.getConfig("filePath") + "/display/product?id=" + id;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getIsTuijian() {
		return isTuijian;
	}

	public void setIsTuijian(String isTuijian) {
		this.isTuijian = isTuijian;
	}
	
}