/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.product.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jeeplus.common.utils.IdGen;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.product.mapper.ProductMapper;
import com.jeeplus.modules.product.entity.ProductSkuname;
import com.jeeplus.modules.product.mapper.ProductSkunameMapper;
import com.jeeplus.modules.productsku.entity.ProductSku;
import com.jeeplus.modules.productsku.mapper.ProductSkuMapper;

/**
 * 云店商品Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class ProductService extends CrudService<ProductMapper, Product> {

	@Autowired
	private ProductSkunameMapper productSkunameMapper;
	@Autowired
	private ProductSkuMapper productSkuMapper;
	
	public Product get(String id) {
		Product product = super.get(id);
		if (product != null) {
			product.setProductSkunameList(productSkunameMapper.findList(new ProductSkuname(product)));
		}
		return product;
	}
	
	public List<Product> findList(Product product) {
		return super.findList(product);
	}
	
	public Page<Product> findPage(Page<Product> page, Product product) {
		return super.findPage(page, product);
	}
	
	@Transactional(readOnly = false)
	public void save(Product product) {
		super.save(product);
		for (ProductSkuname productSkuname : product.getProductSkunameList()){
			if (productSkuname.getId() == null){
				continue;
			}
			if (ProductSkuname.DEL_FLAG_NORMAL.equals(productSkuname.getDelFlag())){
				if (StringUtils.isBlank(productSkuname.getId())){
					productSkuname.setProduct(product);
					productSkuname.preInsert();
					productSkunameMapper.insert(productSkuname);
				}else{
					productSkuname.preUpdate();
					productSkunameMapper.update(productSkuname);
				}
			}else{
				productSkunameMapper.delete(productSkuname);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Product product) {
		super.delete(product);
		productSkunameMapper.delete(new ProductSkuname(product));
	}
	
	/**
	 * 删除商品规格名称
	 * @param product
	 */
	@Transactional(readOnly = false)
	public void deleteSkuname(Product product) {
		productSkunameMapper.delete(new ProductSkuname(product));
	}

	/**
	 * 保存商品规格
	 * @param product 商品
	 * @param skuList 规格列表
	 * @param skuNameList 规格名称列表
	 */
	@Transactional(readOnly = false)
	public void saveSku(Product product, JSONArray skuList, JSONArray skuNameList) {
		// 删除现有规格名称
		deleteSkuname(product);
		// 删除现有规格
		executeDeleteSql("DELETE FROM t_product_sku WHERE product_id = '"+product.getId()+"'");
		
		for (int i = 0; i < skuNameList.size(); i++) {
			JSONObject object = skuNameList.getJSONObject(i);
			// 判断规格名称ID，为空则为新增，手动添加ID
			if (StringUtils.isBlank(object.getString("id"))) {
				object.put("id", IdGen.uuid());
			}
			
			// 插入
			ProductSkuname productSkuname = new ProductSkuname();
			productSkuname.setId(object.getString("id"));
			productSkuname.setIsNewRecord(true);
			productSkuname.preInsert();
			productSkuname.setProduct(product);
			productSkuname.setTitle(object.getString("name"));
			productSkunameMapper.insert(productSkuname);
		}
		
		for (int i = 0; i < skuList.size(); i++) {
			JSONObject object = skuList.getJSONObject(i);
			// 判断规格名称ID，为空则为新增，手动添加ID
			if (StringUtils.isBlank(object.getString("id"))) {
				object.put("id", IdGen.uuid());
			}
			
			ProductSku productSku = new ProductSku();
			productSku.setId(object.getString("id"));
			productSku.setIsNewRecord(true);
			productSku.preInsert();
			productSku.setProduct(product);
			productSku.setStock(object.getIntValue("stock"));
			productSku.setOldPrice(object.getString("oldPrice"));
			productSku.setSort(object.getString("sort"));
			productSku.setDiscount(object.getString("discount"));
			BigDecimal price = object.getBigDecimal("oldPrice").subtract(object.getBigDecimal("discount"));
			if (price.compareTo(BigDecimal.ZERO) < 0) {
				price = BigDecimal.ZERO;
			}
			productSku.setPrice(price.toString());
			
			BigDecimal groupPrice = object.getBigDecimal("groupPrice");
			if (groupPrice.compareTo(BigDecimal.ZERO) < 1) {
				groupPrice = price;
			}
			BigDecimal subsidyprice = object.getBigDecimal("subsidyprice");
			if (subsidyprice.compareTo(BigDecimal.ZERO) < 1) {
				subsidyprice = price;
			}
			BigDecimal openprice = object.getBigDecimal("openprice");
			if (openprice.compareTo(BigDecimal.ZERO) < 1) {
				openprice = price;
			}
			productSku.setGroupPrice(groupPrice.toString());
			productSku.setSubsidyprice(subsidyprice.toString());
			productSku.setOpenprice(openprice.toString());
			productSku.setAmount(object.getString("amount"));
			productSku.setPoint(object.getString("point"));
			productSku.setImage(object.getString("image"));
			
			JSONArray content = new JSONArray();
			JSONArray ggList = object.getJSONArray("gg");
			for (int j = 0; j < ggList.size(); j++) {
				JSONObject json = new JSONObject(true);
				json.put("id", skuNameList.getJSONObject(j).getString("id"));
				json.put("value", ggList.getString(j));
				content.add(json);
			}
			
			productSku.setContent(content.toString());
			productSkuMapper.insert(productSku);
		}
	}
	
}