/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.reason.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.reason.entity.Reason;
import com.jeeplus.modules.reason.service.ReasonService;

/**
 * 订单原因Controller
 * @author lixinapp
 * @version 2019-10-17
 */
@Controller
@RequestMapping(value = "${adminPath}/reason/reason")
public class ReasonController extends BaseController {

	@Autowired
	private ReasonService reasonService;
	
	@ModelAttribute
	public Reason get(@RequestParam(required=false) String id) {
		Reason entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = reasonService.get(id);
		}
		if (entity == null){
			entity = new Reason();
		}
		return entity;
	}
	
	/**
	 * 订单原因列表页面
	 */
	@RequiresPermissions("reason:reason:list")
	@RequestMapping(value = {"list", ""})
	public String list(Reason reason, Model model) {
		model.addAttribute("reason", reason);
		return "modules/reason/reasonList";
	}
	
		/**
	 * 订单原因列表数据
	 */
	@ResponseBody
	@RequiresPermissions("reason:reason:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Reason reason, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Reason> page = reasonService.findPage(new Page<Reason>(request, response), reason); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑订单原因表单页面
	 */
	@RequiresPermissions(value={"reason:reason:view","reason:reason:add","reason:reason:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Reason reason, Model model) {
		model.addAttribute("reason", reason);
		return "modules/reason/reasonForm";
	}

	/**
	 * 保存订单原因
	 */
	@ResponseBody
	@RequiresPermissions(value={"reason:reason:add","reason:reason:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Reason reason, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(reason);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		reasonService.save(reason);//保存
		j.setSuccess(true);
		j.setMsg("保存订单原因成功");
		return j;
	}
	
	/**
	 * 删除订单原因
	 */
	@ResponseBody
	@RequiresPermissions("reason:reason:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Reason reason) {
		AjaxJson j = new AjaxJson();
		reasonService.delete(reason);
		j.setMsg("删除订单原因成功");
		return j;
	}
	
	/**
	 * 批量删除订单原因
	 */
	@ResponseBody
	@RequiresPermissions("reason:reason:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			reasonService.delete(reasonService.get(id));
		}
		j.setMsg("删除订单原因成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("reason:reason:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Reason reason, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "订单原因"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Reason> page = reasonService.findPage(new Page<Reason>(request, response, -1), reason);
    		new ExportExcel("订单原因", Reason.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出订单原因记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("reason:reason:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Reason> list = ei.getDataList(Reason.class);
			for (Reason reason : list){
				try{
					reasonService.save(reason);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条订单原因记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条订单原因记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入订单原因失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入订单原因数据模板
	 */
	@ResponseBody
	@RequiresPermissions("reason:reason:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "订单原因数据导入模板.xlsx";
    		List<Reason> list = Lists.newArrayList(); 
    		new ExportExcel("订单原因数据", Reason.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}