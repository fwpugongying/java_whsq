/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.reason.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 订单原因Entity
 * @author lixinapp
 * @version 2019-10-17
 */
public class Reason extends DataEntity<Reason> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 名称
	private String type;		// 类型 1取消订单 2订单退款
	private Integer sort;		// 排序
	
	public Reason() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Reason(String id){
		super(id);
	}

	@ExcelField(title="名称", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="类型 1取消订单 2订单退款", dictType="reason_type", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=3)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}