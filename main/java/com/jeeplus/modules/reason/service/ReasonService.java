/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.reason.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.reason.entity.Reason;
import com.jeeplus.modules.reason.mapper.ReasonMapper;

/**
 * 订单原因Service
 * @author lixinapp
 * @version 2019-10-17
 */
@Service
@Transactional(readOnly = true)
public class ReasonService extends CrudService<ReasonMapper, Reason> {

	public Reason get(String id) {
		return super.get(id);
	}
	
	public List<Reason> findList(Reason reason) {
		return super.findList(reason);
	}
	
	public Page<Reason> findPage(Page<Reason> page, Reason reason) {
		return super.findPage(page, reason);
	}
	
	@Transactional(readOnly = false)
	public void save(Reason reason) {
		super.save(reason);
	}
	
	@Transactional(readOnly = false)
	public void delete(Reason reason) {
		super.delete(reason);
	}
	
}