/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tags.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.tags.entity.Tags;
import com.jeeplus.modules.tags.service.TagsService;

/**
 * 标签Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/tags/tags")
public class TagsController extends BaseController {

	@Autowired
	private TagsService tagsService;
	
	@ModelAttribute
	public Tags get(@RequestParam(required=false) String id) {
		Tags entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tagsService.get(id);
		}
		if (entity == null){
			entity = new Tags();
		}
		return entity;
	}
	
	/**
	 * 标签列表页面
	 */
	@RequiresPermissions("tags:tags:list")
	@RequestMapping(value = {"list", ""})
	public String list(Tags tags, Model model) {
		model.addAttribute("tags", tags);
		return "modules/tags/tagsList";
	}
	
		/**
	 * 标签列表数据
	 */
	@ResponseBody
	@RequiresPermissions("tags:tags:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Tags tags, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Tags> page = tagsService.findPage(new Page<Tags>(request, response), tags); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑标签表单页面
	 */
	@RequiresPermissions(value={"tags:tags:view","tags:tags:add","tags:tags:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Tags tags, Model model) {
		model.addAttribute("tags", tags);
		return "modules/tags/tagsForm";
	}

	/**
	 * 保存标签
	 */
	@ResponseBody
	@RequiresPermissions(value={"tags:tags:add","tags:tags:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Tags tags, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(tags);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		tagsService.save(tags);//保存
		j.setSuccess(true);
		j.setMsg("保存标签成功");
		return j;
	}
	
	/**
	 * 删除标签
	 */
	@ResponseBody
	@RequiresPermissions("tags:tags:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Tags tags) {
		AjaxJson j = new AjaxJson();
		tagsService.delete(tags);
		j.setMsg("删除标签成功");
		return j;
	}
	
	/**
	 * 批量删除标签
	 */
	@ResponseBody
	@RequiresPermissions("tags:tags:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			tagsService.delete(tagsService.get(id));
		}
		j.setMsg("删除标签成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("tags:tags:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Tags tags, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "标签"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Tags> page = tagsService.findPage(new Page<Tags>(request, response, -1), tags);
    		new ExportExcel("标签", Tags.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出标签记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("tags:tags:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Tags> list = ei.getDataList(Tags.class);
			for (Tags tags : list){
				try{
					tagsService.save(tags);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条标签记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条标签记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入标签失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入标签数据模板
	 */
	@ResponseBody
	@RequiresPermissions("tags:tags:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "标签数据导入模板.xlsx";
    		List<Tags> list = Lists.newArrayList(); 
    		new ExportExcel("标签数据", Tags.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}