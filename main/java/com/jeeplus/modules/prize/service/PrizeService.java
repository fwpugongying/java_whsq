/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.prize.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.prize.entity.Prize;
import com.jeeplus.modules.prize.mapper.PrizeMapper;
/**
 * 实体商家Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class PrizeService extends CrudService<PrizeMapper, Prize> {


	@Autowired

	public Prize get(String id) {
		return super.get(id);
	}

	public List<Prize> findList(Prize prize) {
		return super.findList(prize);
	}
	public List<Prize> findUniqueByProperty(Prize prize) {
		return super.findList(prize);
	}
	public Prize getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Prize getGguid(String gguid) {
		return findUniqueByProperty("gguid", gguid);
	}
	public Page<Prize> findPage(Page<Prize> page, Prize prize) {
		return super.findPage(page, prize);
	}
	@Transactional(readOnly = false)
	public void save(Prize prize) {
		super.save(prize);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Prize prize) {
		super.save(prize);
		String PayMethod="10";
		if("1".equals(prize.getPayType())){//支付宝
			PayMethod="40";
		}else if("2".equals(prize.getPayType())){//微信
			PayMethod="30";
		}else if("3".equals(prize.getPayType())){//平台购物券支付
			PayMethod="10";
		}else if("4".equals(prize.getPayType())){//商城购物券支付
			PayMethod="20";
		}

		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("PGUID", prize.getGguid()));
		list.add(new WohuiEntity("PayMethod", PayMethod));
		JSONObject object = WohuiUtils.send(WohuiUtils.PrizePay, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("赠品支付"+prize.getGguid()+"失败：" + object.getString("respMsg"));
		}
	}

	@Transactional(readOnly = false)
	public void delete(Prize prize) {
		super.delete(prize);
	}

}