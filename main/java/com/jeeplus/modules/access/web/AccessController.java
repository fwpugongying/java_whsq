/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.access.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.CacheUtils;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.access.entity.Access;
import com.jeeplus.modules.access.service.AccessService;
import com.jeeplus.modules.api.CacheUtil;

/**
 * 功能入口图片Controller
 * @author lixinapp
 * @version 2019-10-15
 */
@Controller
@RequestMapping(value = "${adminPath}/access/access")
public class AccessController extends BaseController {

	@Autowired
	private AccessService accessService;
	
	@ModelAttribute
	public Access get(@RequestParam(required=false) String id) {
		Access entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = accessService.get(id);
		}
		if (entity == null){
			entity = new Access();
		}
		return entity;
	}
	
	/**
	 * 功能入口图片列表页面
	 */
	@RequiresPermissions("access:access:list")
	@RequestMapping(value = {"list", ""})
	public String list(Access access, Model model) {
		model.addAttribute("access", access);
		return "modules/access/accessList";
	}
	
		/**
	 * 功能入口图片列表数据
	 */
	@ResponseBody
	@RequiresPermissions("access:access:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Access access, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Access> page = accessService.findPage(new Page<Access>(request, response), access); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑功能入口图片表单页面
	 */
	@RequiresPermissions(value={"access:access:view","access:access:add","access:access:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Access access, Model model) {
		model.addAttribute("access", access);
		return "modules/access/accessForm";
	}

	/**
	 * 保存功能入口图片
	 */
	@ResponseBody
	@RequiresPermissions(value={"access:access:add","access:access:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Access access, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(access);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		accessService.save(access);//保存
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_ACCESS_LIST);
		j.setSuccess(true);
		j.setMsg("保存功能入口图片成功");
		return j;
	}
	
	/**
	 * 删除功能入口图片
	 */
	@ResponseBody
	@RequiresPermissions("access:access:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Access access) {
		AjaxJson j = new AjaxJson();
		accessService.delete(access);
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_ACCESS_LIST);
		j.setMsg("删除功能入口图片成功");
		return j;
	}
	
	/**
	 * 批量删除功能入口图片
	 */
	@ResponseBody
	@RequiresPermissions("access:access:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			accessService.delete(accessService.get(id));
		}
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_ACCESS_LIST);
		j.setMsg("删除功能入口图片成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("access:access:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Access access, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "功能入口图片"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Access> page = accessService.findPage(new Page<Access>(request, response, -1), access);
    		new ExportExcel("功能入口图片", Access.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出功能入口图片记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("access:access:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Access> list = ei.getDataList(Access.class);
			for (Access access : list){
				try{
					accessService.save(access);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条功能入口图片记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条功能入口图片记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入功能入口图片失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入功能入口图片数据模板
	 */
	@ResponseBody
	@RequiresPermissions("access:access:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "功能入口图片数据导入模板.xlsx";
    		List<Access> list = Lists.newArrayList(); 
    		new ExportExcel("功能入口图片数据", Access.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}