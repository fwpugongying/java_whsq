/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.access.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.access.entity.Access;
import com.jeeplus.modules.access.mapper.AccessMapper;

/**
 * 功能入口图片Service
 * @author lixinapp
 * @version 2019-10-15
 */
@Service
@Transactional(readOnly = true)
public class AccessService extends CrudService<AccessMapper, Access> {

	public Access get(String id) {
		return super.get(id);
	}
	
	public List<Access> findList(Access access) {
		return super.findList(access);
	}
	
	public Page<Access> findPage(Page<Access> page, Access access) {
		return super.findPage(page, access);
	}
	
	@Transactional(readOnly = false)
	public void save(Access access) {
		super.save(access);
	}
	
	@Transactional(readOnly = false)
	public void delete(Access access) {
		super.delete(access);
	}
	
}