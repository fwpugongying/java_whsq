/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.access.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 功能入口图片Entity
 * @author lixinapp
 * @version 2019-10-15
 */
public class Access extends DataEntity<Access> {
	
	private static final long serialVersionUID = 1L;
	private String type;		// 类型 1限时秒杀 2-9.9包邮 3品牌闪购 4抖券 5跨境优选
	private String image;		// 图片
	
	public Access() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Access(String id){
		super(id);
	}

	@ExcelField(title="类型 1限时秒杀 2-9.9包邮 3品牌闪购 4抖券 5跨境优选", dictType="access_type", align=2, sort=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="图片", align=2, sort=2)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}