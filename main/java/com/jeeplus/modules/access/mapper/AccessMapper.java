/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.access.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.access.entity.Access;

/**
 * 功能入口图片MAPPER接口
 * @author lixinapp
 * @version 2019-10-15
 */
@MyBatisMapper
public interface AccessMapper extends BaseMapper<Access> {
	
}