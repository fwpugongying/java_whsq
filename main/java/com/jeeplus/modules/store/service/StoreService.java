/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.store.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.common.utils.IdGen;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import com.jeeplus.modules.shopbill.entity.ShopBill;
import com.jeeplus.modules.store.entity.Store;
import com.jeeplus.modules.store.mapper.StoreMapper;
import com.jeeplus.modules.storebill.entity.StoreBill;
import com.jeeplus.modules.storebill.mapper.StoreBillMapper;
import com.jeeplus.modules.sys.entity.Role;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.mapper.UserMapper;

/**
 * 厂家店铺Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class StoreService extends CrudService<StoreMapper, Store> {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private StoreBillMapper storeBillMapper;
	
	public Store get(String id) {
		return super.get(id);
	}
	
	public List<Store> findList(Store store) {
		return super.findList(store);
	}
	
	public Page<Store> findPage(Page<Store> page, Store store) {
		return super.findPage(page, store);
	}
	
	@Transactional(readOnly = false)
	public void save(Store store) {
		super.save(store);
	}
	
	@Transactional(readOnly = false)
	public void delete(Store store) {
		super.delete(store);
	}

	@Transactional(readOnly = false)
	public void pay(Store store) {
		super.save(store);
		// 其他逻辑处理
		
	}

	/**
	 * 审核
	 * @param store
	 */
	@Transactional(readOnly = false)
	public void audit(Store store) {
		if ("2".equals(store.getAuditState())) {
			// 添加系统用户
			User user = userMapper.getByLoginName(new User(null, store.getMember().getPhone()));
			if (null == user) {
				user = new User();
				user.setId(IdGen.uuid());
				user.setLoginName(store.getMember().getPhone());
				user.setPassword(store.getMember().getPassword());
				user.setName(store.getTitle());
				user.setLoginFlag("1");
				user.setCreateBy(new User("1"));
				user.setCreateDate(new Date());
				user.setUpdateBy(new User("1"));
				user.setUpdateDate(new Date());
				List<Role> roleList = Lists.newArrayList();
				roleList.add(new Role("1"));
				user.setRoleList(roleList);
				userMapper.insert(user);
				userMapper.insertUserRole(user);
			}
			store.setUser(user);
			String sqtype="MR";
			String paymoney="0";
			if("0".equals(store.getXhtype())){
				sqtype= "MRXHBT";
				paymoney=store.getPaymoney();
			}
			if("2".equals(store.getXhtype())){
				sqtype= "MRPFDPDL";
				paymoney=store.getPaymoney();
			}
			// 调用三方
			List<WohuiEntity> list = Lists.newArrayList();
			list.add(new WohuiEntity("Type", sqtype));
			list.add(new WohuiEntity("ShopName", store.getTitle()));
			list.add(new WohuiEntity("CardGUID", store.getMember().getId()));
			list.add(new WohuiEntity("RealName", store.getUsername()));
			list.add(new WohuiEntity("Phone", store.getPhone()));
			list.add(new WohuiEntity("SignGUID", store.getInvite().getId()));
//			list.add(new WohuiEntity("SignRealName", store.getInviteName()));
			list.add(new WohuiEntity("PayGUID", ""));
			//list.add(new WohuiEntity("PayRealName", ""));
			list.add(new WohuiEntity("UnionCode", store.getStoreCode()));
			//list.add(new WohuiEntity("GoodsCode", store.getProductCode()));
			list.add(new WohuiEntity("Fee", "0"));
			list.add(new WohuiEntity("FeeYear", "1"));
			list.add(new WohuiEntity("PayMoney", paymoney));
			list.add(new WohuiEntity("Remark", ""));
			JSONObject object = WohuiUtils.send(WohuiUtils.SupplierOpen, list);
			if (!"0000".equals(object.getString("respCode"))) {
				logger.error("调用会员系统开通供应商失败：" + object.getString("respMsg"));
			}
		}
		
		super.save(store);
	}

	/**
	 * 改余额
	 * @param shop 商家
	 * @param title 标题
	 * @param amount 金额
	 * @param type 0支出 1收入
	 * @param orderId 订单号
	 * @param fee 运费
	 * @param username 收货人
	 * @param phone 收货电话
	 * @param finishDate 订单完成时间
	 */
	@Transactional(readOnly = false)
	public synchronized void updateBalance(Store store, String title, String amount, String type, String orderId, String fee, String username, String phone, Date finishDate) {
		// 添加账单记录
		StoreBill storeBill = new StoreBill();
		storeBill.preInsert();
		storeBill.setStore(store);
		storeBill.setTitle(title);
		storeBill.setAmount(amount);
		storeBill.setType(type);
		storeBill.setOrderId(orderId);
		storeBill.setFee(fee);
		storeBill.setUsername(username);
		storeBill.setPhone(phone);
		storeBill.setFinishDate(finishDate);
		storeBillMapper.insert(storeBill);
		
		// 修改余额
		if ("0".equals(type)) {
			mapper.execUpdateSql("UPDATE t_store SET balance = balance - "+amount+" WHERE id = '"+store.getId()+"'");
		} else {
			mapper.execUpdateSql("UPDATE t_store SET balance = balance + "+amount+" WHERE id = '"+store.getId()+"'");
		}
	}

	/**
	 * 统计数据
	 * @param storeId 店铺ID
	 * @param beginDate 开始时间
	 * @param endDate 结束时间
	 * @return
	 */
	public List<Map<String, Object>> storeOption(String storeId, String beginDate, String endDate) {
		return mapper.storeOption(storeId, beginDate, endDate);
	}

	public Object totalJieSuan(String storeId, String beginDate, String endDate) {
		return mapper.totalJieSuan(storeId, beginDate, endDate);
	}

	public Object totalWeiJieSuan(String storeId, String beginDate, String endDate) {
		return mapper.totalWeiJieSuan(storeId, beginDate, endDate);
	}
	
}