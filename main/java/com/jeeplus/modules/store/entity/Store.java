/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.store.entity;

import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.modules.sys.entity.Area;
import com.jeeplus.modules.sys.entity.User;

import javax.validation.constraints.NotNull;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 厂家店铺Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Store extends DataEntity<Store> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String storeCode;		// 店铺编码
	private String productCode;		// 商品编码
	private String title;		// 商户名称
	private String icon;		// 店铺LOGO
	private String category;		// 主营项目
	private String city;		// 城市
	private String address;		// 地址
	private String username;		// 联系人
	private String phone;		// 手机号
	private Member invite;		// 签约人
	private String licence;		// 营业执照
	private String idcard;		// 身份证
	private String others;		// 其他证书
	private String state;		// 状态 0正常 1冻结
	private String auditState;		// 审核状态 0待支付 1待审核 2审核通过 3审核拒绝 4已退款
	private String amount;		// 入驻金额
	private String orderNo;		// 支付单号
	private String payType;		// 支付方式 1支付宝 2微信
	private Date auditDate;		// 审核时间
	private String balance;		// 余额
	private String huitian;		// 回填状态 0需回填 1已回填
	private String freight;		// 偏远运费
	private String distant;		// 偏远省份
	private String nodelarea;		// 不发货区域提示
	private String shangqiao;// 百度商桥
	private User user;// 系统用户
	private Area area;//
	private Area areatwo;//
	private String invitePhone;// 签约人手机号
	private String inviteName;// 签约人姓名
	private String content;// 店铺简介
	private String trade_type;
	private String xhtype;// 0选择的200款差额 1是1000元的押金
	private String paymoney;		// 付款200款的差额

	public Store() {
		super();
	}

	public Store(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@ExcelField(title="0选择的200款差额 1是1000元的押金", align=2, sort=2)
	public String getXhtype() {
		return xhtype;
	}

	public void setXhtype(String xhtype) {
		this.xhtype = xhtype;
	}

	@ExcelField(title="付款200款的差额", align=2, sort=20)
	public String getPaymoney() {
		return paymoney;
	}

	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}
	
	@ExcelField(title="店铺编码", align=2, sort=2)
	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}
	
	@ExcelField(title="商品编码", align=2, sort=3)
	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	@ExcelField(title="商户名称", align=2, sort=4)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="店铺LOGO", align=2, sort=5)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@ExcelField(title="主营项目", align=2, sort=6)
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	@ExcelField(title="城市", align=2, sort=7)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@ExcelField(title="地址", align=2, sort=8)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@ExcelField(title="联系人", align=2, sort=9)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@ExcelField(title="手机号", align=2, sort=10)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@NotNull(message="签约人不能为空")
	@ExcelField(title="签约人", fieldType=Member.class, value="invite.phone", align=2, sort=11)
	public Member getInvite() {
		return invite;
	}

	public void setInvite(Member invite) {
		this.invite = invite;
	}
	
	@ExcelField(title="营业执照", align=2, sort=12)
	public String getLicence() {
		return licence;
	}

	public void setLicence(String licence) {
		this.licence = licence;
	}
	
	@ExcelField(title="身份证", align=2, sort=13)
	public String getIdcard() {
		return idcard;
	}

	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	
	@ExcelField(title="其他证书", align=2, sort=14)
	public String getOthers() {
		return others;
	}

	public void setOthers(String others) {
		this.others = others;
	}
	
	@ExcelField(title="状态 0正常 1冻结", dictType="member_state", align=2, sort=16)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="审核状态 0待支付 1已支付 2审核通过 3审核拒绝 4已退款", dictType="shop_audit", align=2, sort=17)
	public String getAuditState() {
		return auditState;
	}

	public void setAuditState(String auditState) {
		this.auditState = auditState;
	}
	
	@ExcelField(title="入驻金额", align=2, sort=18)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@ExcelField(title="支付单号", align=2, sort=19)
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	
	@ExcelField(title="支付方式 1支付宝 2微信", dictType="pay_type", align=2, sort=20)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=21)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	
	@ExcelField(title="余额", align=2, sort=23)
	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	@ExcelField(title="回填状态 0需回填 1已回填", dictType="huitian", align=2, sort=24)
	public String getHuitian() {
		return huitian;
	}

	public void setHuitian(String huitian) {
		this.huitian = huitian;
	}
	
	@ExcelField(title="偏远运费", align=2, sort=25)
	public String getFreight() {
		return freight;
	}

	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getNodelarea() {
		return nodelarea;
	}

	public void setNodelarea(String nodelarea) {
		this.nodelarea = nodelarea;
	}
	
	@ExcelField(title="偏远省份", align=2, sort=26)
	public String getDistant() {
		return distant;
	}

	public void setDistant(String distant) {
		this.distant = distant;
	}

	public String getShangqiao() {
		return shangqiao;
	}

	public void setShangqiao(String shangqiao) {
		this.shangqiao = shangqiao;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Area getAreatwo() {
		return areatwo;
	}

	public void setAreatwo(Area areatwo) {
		this.areatwo = areatwo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getInvitePhone() {
		return invitePhone;
	}

	public void setInvitePhone(String invitePhone) {
		this.invitePhone = invitePhone;
	}

	public String getInviteName() {
		return inviteName;
	}

	public void setInviteName(String inviteName) {
		this.inviteName = inviteName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setTrade_type(String trade_type) {
		this.trade_type = trade_type;
	}

	public String getTrade_type() {
		return trade_type;
	}
}