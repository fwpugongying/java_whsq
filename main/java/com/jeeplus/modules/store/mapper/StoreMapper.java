/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.store.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.store.entity.Store;

/**
 * 厂家店铺MAPPER接口
 * @author lixinapp
 * @version 2019-10-14
 */
@MyBatisMapper
public interface StoreMapper extends BaseMapper<Store> {

	List<Map<String, Object>> storeOption(@Param("storeId")String storeId, @Param("beginDate")String beginDate, @Param("endDate")String endDate);

	Object totalJieSuan(@Param("storeId")String storeId, @Param("beginDate")String beginDate, @Param("endDate")String endDate);

	Object totalWeiJieSuan(@Param("storeId")String storeId, @Param("beginDate")String beginDate, @Param("endDate")String endDate);
	
}