/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tamoybrand.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.tamoybrand.entity.TAmoyBrand;
import com.jeeplus.modules.tamoybrand.mapper.TAmoyBrandMapper;

/**
 * 淘客品牌Service
 * @author ws
 * @version 2019-10-31
 */
@Service
@Transactional(readOnly = true)
public class TAmoyBrandService extends CrudService<TAmoyBrandMapper, TAmoyBrand> {

	public TAmoyBrand get(String id) {
		return super.get(id);
	}
	
	public List<TAmoyBrand> findList(TAmoyBrand tAmoyBrand) {
		return super.findList(tAmoyBrand);
	}
	
	public Page<TAmoyBrand> findPage(Page<TAmoyBrand> page, TAmoyBrand tAmoyBrand) {
		return super.findPage(page, tAmoyBrand);
	}
	
	@Transactional(readOnly = false)
	public void save(TAmoyBrand tAmoyBrand) {
		super.save(tAmoyBrand);
	}
	
	@Transactional(readOnly = false)
	public void delete(TAmoyBrand tAmoyBrand) {
		super.delete(tAmoyBrand);
	}
	
}