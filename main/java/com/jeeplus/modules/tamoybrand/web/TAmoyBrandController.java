/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tamoybrand.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.tamoybrand.entity.TAmoyBrand;
import com.jeeplus.modules.tamoybrand.service.TAmoyBrandService;

/**
 * 淘客品牌Controller
 * @author ws
 * @version 2019-10-31
 */
@Controller
@RequestMapping(value = "${adminPath}/tamoybrand/tAmoyBrand")
public class TAmoyBrandController extends BaseController {

	@Autowired
	private TAmoyBrandService tAmoyBrandService;
	
	@ModelAttribute
	public TAmoyBrand get(@RequestParam(required=false) String id) {
		TAmoyBrand entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = tAmoyBrandService.get(id);
		}
		if (entity == null){
			entity = new TAmoyBrand();
		}
		return entity;
	}
	
	/**
	 * 淘客品牌列表页面
	 */
	@RequiresPermissions("tamoybrand:tAmoyBrand:list")
	@RequestMapping(value = {"list", ""})
	public String list(TAmoyBrand tAmoyBrand, Model model) {
		model.addAttribute("tAmoyBrand", tAmoyBrand);
		return "modules/tamoybrand/tAmoyBrandList";
	}
	
		/**
	 * 淘客品牌列表数据
	 */
	@ResponseBody
	@RequiresPermissions("tamoybrand:tAmoyBrand:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(TAmoyBrand tAmoyBrand, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<TAmoyBrand> page = tAmoyBrandService.findPage(new Page<TAmoyBrand>(request, response), tAmoyBrand); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑淘客品牌表单页面
	 */
	@RequiresPermissions(value={"tamoybrand:tAmoyBrand:view","tamoybrand:tAmoyBrand:add","tamoybrand:tAmoyBrand:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(TAmoyBrand tAmoyBrand, Model model) {
		model.addAttribute("tAmoyBrand", tAmoyBrand);
		return "modules/tamoybrand/tAmoyBrandForm";
	}

	/**
	 * 保存淘客品牌
	 */
	@ResponseBody
	@RequiresPermissions(value={"tamoybrand:tAmoyBrand:add","tamoybrand:tAmoyBrand:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(TAmoyBrand tAmoyBrand, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(tAmoyBrand);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		tAmoyBrandService.save(tAmoyBrand);//保存
		j.setSuccess(true);
		j.setMsg("保存淘客品牌成功");
		return j;
	}
	
	/**
	 * 删除淘客品牌
	 */
	@ResponseBody
	@RequiresPermissions("tamoybrand:tAmoyBrand:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(TAmoyBrand tAmoyBrand) {
		AjaxJson j = new AjaxJson();
		tAmoyBrandService.delete(tAmoyBrand);
		j.setMsg("删除淘客品牌成功");
		return j;
	}
	
	/**
	 * 批量删除淘客品牌
	 */
	@ResponseBody
	@RequiresPermissions("tamoybrand:tAmoyBrand:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			tAmoyBrandService.delete(tAmoyBrandService.get(id));
		}
		j.setMsg("删除淘客品牌成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("tamoybrand:tAmoyBrand:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(TAmoyBrand tAmoyBrand, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "淘客品牌"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<TAmoyBrand> page = tAmoyBrandService.findPage(new Page<TAmoyBrand>(request, response, -1), tAmoyBrand);
    		new ExportExcel("淘客品牌", TAmoyBrand.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出淘客品牌记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("tamoybrand:tAmoyBrand:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<TAmoyBrand> list = ei.getDataList(TAmoyBrand.class);
			for (TAmoyBrand tAmoyBrand : list){
				try{
					tAmoyBrandService.save(tAmoyBrand);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条淘客品牌记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条淘客品牌记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入淘客品牌失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入淘客品牌数据模板
	 */
	@ResponseBody
	@RequiresPermissions("tamoybrand:tAmoyBrand:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "淘客品牌数据导入模板.xlsx";
    		List<TAmoyBrand> list = Lists.newArrayList(); 
    		new ExportExcel("淘客品牌数据", TAmoyBrand.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}