/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tamoybrand.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 淘客品牌Entity
 * @author ws
 * @version 2019-10-31
 */
public class TAmoyBrand extends DataEntity<TAmoyBrand> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 名称
	private String icon;		// 图标
	private String image;		// 图片
	private String sort;		// 排序
	
	public TAmoyBrand() {
		super();
	}

	public TAmoyBrand(String id){
		super(id);
	}

	@ExcelField(title="名称", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="图片", align=2, sort=2)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	@ExcelField(title="排序", align=2, sort=3)
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
}