/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.tamoybrand.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.tamoybrand.entity.TAmoyBrand;

/**
 * 淘客品牌MAPPER接口
 * @author ws
 * @version 2019-10-31
 */
@MyBatisMapper
public interface TAmoyBrandMapper extends BaseMapper<TAmoyBrand> {
	
}