/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.keyword.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.keyword.entity.Keyword;
import com.jeeplus.modules.keyword.mapper.KeywordMapper;

/**
 * 热门搜索关键词Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class KeywordService extends CrudService<KeywordMapper, Keyword> {

	public Keyword get(String id) {
		return super.get(id);
	}
	
	public List<Keyword> findList(Keyword keyword) {
		return super.findList(keyword);
	}
	
	public Page<Keyword> findPage(Page<Keyword> page, Keyword keyword) {
		return super.findPage(page, keyword);
	}
	
	@Transactional(readOnly = false)
	public void save(Keyword keyword) {
		super.save(keyword);
	}
	
	@Transactional(readOnly = false)
	public void delete(Keyword keyword) {
		super.delete(keyword);
	}
	
}