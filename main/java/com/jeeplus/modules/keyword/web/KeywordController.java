/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.keyword.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.keyword.entity.Keyword;
import com.jeeplus.modules.keyword.service.KeywordService;

/**
 * 热门搜索关键词Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/keyword/keyword")
public class KeywordController extends BaseController {

	@Autowired
	private KeywordService keywordService;
	
	@ModelAttribute
	public Keyword get(@RequestParam(required=false) String id) {
		Keyword entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = keywordService.get(id);
		}
		if (entity == null){
			entity = new Keyword();
		}
		return entity;
	}
	
	/**
	 * 热门搜索关键词列表页面
	 */
	@RequiresPermissions("keyword:keyword:list")
	@RequestMapping(value = {"list", ""})
	public String list(Keyword keyword, Model model) {
		model.addAttribute("keyword", keyword);
		return "modules/keyword/keywordList";
	}
	
		/**
	 * 热门搜索关键词列表数据
	 */
	@ResponseBody
	@RequiresPermissions("keyword:keyword:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Keyword keyword, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Keyword> page = keywordService.findPage(new Page<Keyword>(request, response), keyword); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑热门搜索关键词表单页面
	 */
	@RequiresPermissions(value={"keyword:keyword:view","keyword:keyword:add","keyword:keyword:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Keyword keyword, Model model) {
		model.addAttribute("keyword", keyword);
		return "modules/keyword/keywordForm";
	}

	/**
	 * 保存热门搜索关键词
	 */
	@ResponseBody
	@RequiresPermissions(value={"keyword:keyword:add","keyword:keyword:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Keyword keyword, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(keyword);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		keywordService.save(keyword);//保存
		j.setSuccess(true);
		j.setMsg("保存热门搜索关键词成功");
		return j;
	}
	
	/**
	 * 删除热门搜索关键词
	 */
	@ResponseBody
	@RequiresPermissions("keyword:keyword:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Keyword keyword) {
		AjaxJson j = new AjaxJson();
		keywordService.delete(keyword);
		j.setMsg("删除热门搜索关键词成功");
		return j;
	}
	
	/**
	 * 批量删除热门搜索关键词
	 */
	@ResponseBody
	@RequiresPermissions("keyword:keyword:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			keywordService.delete(keywordService.get(id));
		}
		j.setMsg("删除热门搜索关键词成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("keyword:keyword:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Keyword keyword, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "热门搜索关键词"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Keyword> page = keywordService.findPage(new Page<Keyword>(request, response, -1), keyword);
    		new ExportExcel("热门搜索关键词", Keyword.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出热门搜索关键词记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("keyword:keyword:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Keyword> list = ei.getDataList(Keyword.class);
			for (Keyword keyword : list){
				try{
					keywordService.save(keyword);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条热门搜索关键词记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条热门搜索关键词记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入热门搜索关键词失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入热门搜索关键词数据模板
	 */
	@ResponseBody
	@RequiresPermissions("keyword:keyword:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "热门搜索关键词数据导入模板.xlsx";
    		List<Keyword> list = Lists.newArrayList(); 
    		new ExportExcel("热门搜索关键词数据", Keyword.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}