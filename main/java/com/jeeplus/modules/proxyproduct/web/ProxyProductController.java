/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyproduct.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.proxyproduct.entity.ProxyProduct;
import com.jeeplus.modules.proxyproduct.service.ProxyProductService;

/**
 * 单品代理联盟商品Controller
 * @author lixinapp
 * @version 2019-11-08
 */
@Controller
@RequestMapping(value = "${adminPath}/proxyproduct/proxyProduct")
public class ProxyProductController extends BaseController {

	@Autowired
	private ProxyProductService proxyProductService;
	
	@ModelAttribute
	public ProxyProduct get(@RequestParam(required=false) String id) {
		ProxyProduct entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = proxyProductService.get(id);
		}
		if (entity == null){
			entity = new ProxyProduct();
		}
		return entity;
	}
	
	/**
	 * 单品代理联盟商品列表页面
	 */
	@RequiresPermissions("proxyproduct:proxyProduct:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProxyProduct proxyProduct, Model model) {
		model.addAttribute("proxyProduct", proxyProduct);
		return "modules/proxyproduct/proxyProductList";
	}
	
		/**
	 * 单品代理联盟商品列表数据
	 */
	@ResponseBody
	@RequiresPermissions("proxyproduct:proxyProduct:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProxyProduct proxyProduct, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProxyProduct> page = proxyProductService.findPage(new Page<ProxyProduct>(request, response), proxyProduct); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑单品代理联盟商品表单页面
	 */
	@RequiresPermissions(value={"proxyproduct:proxyProduct:view","proxyproduct:proxyProduct:add","proxyproduct:proxyProduct:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProxyProduct proxyProduct, Model model) {
		model.addAttribute("proxyProduct", proxyProduct);
		return "modules/proxyproduct/proxyProductForm";
	}

	/**
	 * 保存单品代理联盟商品
	 */
	@ResponseBody
	@RequiresPermissions(value={"proxyproduct:proxyProduct:add","proxyproduct:proxyProduct:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProxyProduct proxyProduct, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(proxyProduct);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		proxyProductService.save(proxyProduct);//保存
		j.setSuccess(true);
		j.setMsg("保存单品代理联盟商品成功");
		return j;
	}
	
	/**
	 * 删除单品代理联盟商品
	 */
	@ResponseBody
	@RequiresPermissions("proxyproduct:proxyProduct:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProxyProduct proxyProduct) {
		AjaxJson j = new AjaxJson();
		proxyProductService.delete(proxyProduct);
		j.setMsg("删除单品代理联盟商品成功");
		return j;
	}
	
	/**
	 * 批量删除单品代理联盟商品
	 */
	@ResponseBody
	@RequiresPermissions("proxyproduct:proxyProduct:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			proxyProductService.delete(proxyProductService.get(id));
		}
		j.setMsg("删除单品代理联盟商品成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("proxyproduct:proxyProduct:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProxyProduct proxyProduct, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "增选单品代理商品"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProxyProduct> page = proxyProductService.findPage(new Page<ProxyProduct>(request, response, -1), proxyProduct);
    		new ExportExcel("增选单品代理商品", ProxyProduct.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出增选单品代理商品记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("proxyproduct:proxyProduct:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProxyProduct> list = ei.getDataList(ProxyProduct.class);
			for (ProxyProduct proxyProduct : list){
				try{
					proxyProductService.save(proxyProduct);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条单品代理联盟商品记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条单品代理联盟商品记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入单品代理联盟商品失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入单品代理联盟商品数据模板
	 */
	@ResponseBody
	@RequiresPermissions("proxyproduct:proxyProduct:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "单品代理联盟商品数据导入模板.xlsx";
    		List<ProxyProduct> list = Lists.newArrayList(); 
    		new ExportExcel("单品代理联盟商品数据", ProxyProduct.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}