/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyproduct.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 单品代理联盟商品Entity
 * @author lixinapp
 * @version 2019-11-08
 */
public class ProxyProduct extends DataEntity<ProxyProduct> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String code;		// 代理编码
	private String proxyId;		// 代理ID
	private String productId;		// 商品ID
	private String goodssign;		// 拼多多加密商品ID
	private String productName;		// 名称
	private String productImage;		// 图片
	private String productPrice;		// 价格
	private String productPoint;		// 铜板
	private Date endDate;		// 优惠券到期时间
	private String type;		// 类型 1淘客 2京东 3拼多多
	private String state;		// 状态 0正常 1过期
	private Date payDate;		// 付款时间
	private String payType;		// 付款方式 1支付宝 2微信
	private String fee;		// 服务费
	private String payNo;		// 支付单号
	private String orderState;		// 订单状态 0待付款 1已付款 2已取消
	private String province;		// 省
	private String city;		// 市
	private String area;		// 区
	private String trade_type;

	public ProxyProduct() {
		super();
	}

	public ProxyProduct(String id){
		super(id);
	}

	//@ExcelField(title="代理编码", align=2, sort=1)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	//@ExcelField(title="商品ID", align=2, sort=2)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	//@ExcelField(title="拼多多加密商品ID", align=2, sort=2)
	public String getGoodssign() {
		return goodssign;
	}

	public void setGoodssign(String goodssign) {
		this.goodssign = goodssign;
	}
	
	@ExcelField(title="商品名称", align=2, sort=5)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	//@ExcelField(title="图片", align=2, sort=4)
	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
	
	//@ExcelField(title="价格", align=2, sort=5)
	public String getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
	
	//@ExcelField(title="铜板", align=2, sort=6)
	public String getProductPoint() {
		return productPoint;
	}

	public void setProductPoint(String productPoint) {
		this.productPoint = productPoint;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	//@ExcelField(title="优惠券到期时间", align=2, sort=8)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@ExcelField(title="类型", align=2, sort=6)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="状态", align=2, sort=7)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="付款时间", align=2, sort=9)
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	@ExcelField(title="费用", align=2, sort=8)
	public String getFee() {
		return fee;
	}

	public void setFee(String fee) {
		this.fee = fee;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}

	public String getProxyId() {
		return proxyId;
	}

	public void setProxyId(String proxyId) {
		this.proxyId = proxyId;
	}

	@ExcelField(title="省", align=2, sort=2)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	@ExcelField(title="市", align=2, sort=3)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@ExcelField(title="区", align=2, sort=4)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getTrade_type() {
        return trade_type;
    }
}