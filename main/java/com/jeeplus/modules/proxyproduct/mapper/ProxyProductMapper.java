/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyproduct.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.proxyproduct.entity.ProxyProduct;

/**
 * 单品代理联盟商品MAPPER接口
 * @author lixinapp
 * @version 2019-11-08
 */
@MyBatisMapper
public interface ProxyProductMapper extends BaseMapper<ProxyProduct> {
	
}