/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyproduct.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import com.jeeplus.modules.proxyorder.entity.ProxyOrder;
import com.jeeplus.modules.proxyproduct.entity.ProxyProduct;
import com.jeeplus.modules.proxyproduct.mapper.ProxyProductMapper;

/**
 * 单品代理联盟商品Service
 * @author lixinapp
 * @version 2019-11-08
 */
@Service
@Transactional(readOnly = true)
public class ProxyProductService extends CrudService<ProxyProductMapper, ProxyProduct> {

	public ProxyProduct get(String id) {
		return super.get(id);
	}
	
	public List<ProxyProduct> findList(ProxyProduct proxyProduct) {
		return super.findList(proxyProduct);
	}
	
	public Page<ProxyProduct> findPage(Page<ProxyProduct> page, ProxyProduct proxyProduct) {
		return super.findPage(page, proxyProduct);
	}
	
	@Transactional(readOnly = false)
	public void save(ProxyProduct proxyProduct) {
		super.save(proxyProduct);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProxyProduct proxyProduct) {
		super.delete(proxyProduct);
	}

	/**
	 * 生成订单
	 * @param proxyProduct
	 */
	@Transactional(readOnly = false)
	public void insert(ProxyProduct proxyProduct) {
		super.save(proxyProduct);
		
		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("CardGUID", proxyProduct.getMember().getId()));
		list.add(new WohuiEntity("OrderNO", proxyProduct.getId()));
		list.add(new WohuiEntity("Province", proxyProduct.getProvince()));
		list.add(new WohuiEntity("City", proxyProduct.getCity()));
		list.add(new WohuiEntity("District", proxyProduct.getArea(), false));
		list.add(new WohuiEntity("Source", proxyProduct.getType()));
		list.add(new WohuiEntity("GoodsID", proxyProduct.getProductId()));
		list.add(new WohuiEntity("GoodsSign", proxyProduct.getGoodssign()));
		list.add(new WohuiEntity("GoodsName", proxyProduct.getProductName(), false));
		list.add(new WohuiEntity("ImgUrl", proxyProduct.getProductImage(), false));
		list.add(new WohuiEntity("Fee", proxyProduct.getFee()));
		JSONObject object = WohuiUtils.send(WohuiUtils.MSMGoodsApply, list);
		if (!"0000".equals(object.getString("respCode"))) {
			throw new RuntimeException(object.getString("respMsg"));
		}
	}
	
	/**
	 * 取消超时未付款的订单
	 */
	@Transactional(readOnly = false)
	public void cancel() {
		ProxyProduct proxyProduct = new ProxyProduct();
		proxyProduct.setOrderState("0");
		proxyProduct.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 15 MINUTE) ");
		while (true) {
			Page<ProxyProduct> page = super.findPage(new Page<ProxyProduct>(1, 100), proxyProduct);
			if (!page.getList().isEmpty()) {
				for (ProxyProduct order : page.getList()) {
					order.setOrderState("2");
					super.save(order);
					
					try {
						// 调用三方取消订单
						List<WohuiEntity> list = Lists.newArrayList();
						list.add(new WohuiEntity("OrderNO", order.getId()));
						JSONObject object = WohuiUtils.send(WohuiUtils.MSMGoodsCancel, list);
						if (!"0000".equals(object.getString("respCode"))) {
							logger.error("取消增选产品订单号"+order.getId()+"失败：" + object.getString("respMsg"));
						}
					} catch (Exception e) {
						logger.error("取消增选产品订单号"+order.getId()+"失败：" + e.getMessage());
					}
					
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}

	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(ProxyProduct proxyProduct) {
		super.save(proxyProduct);
		
		// 调用三方
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("OrderNO", proxyProduct.getId()));
		JSONObject object = WohuiUtils.send(WohuiUtils.MSMGoodsOpen, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("开通增选产品订单号"+proxyProduct.getId()+"失败：" + object.getString("respMsg"));
		}
	}

	/**
	 * 判断产品是否被代理
	 * @param province
	 * @param city
	 * @param area
	 * @param type
	 * @param productId
	 * @return
	 */
	public boolean exist(String province, String city, String area, String type, String productId) {
		return !"0".equals(mapper.executeGetSql("SELECT COUNT(*) FROM t_proxy_product WHERE province = '"+province+"' AND city = '"+city+"' AND area = '"+area+"' AND type = '"+type+"' AND product_id = '"+productId+"' AND order_state != '2'").toString());
	}
	
}