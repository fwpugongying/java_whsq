/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productgroup.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.productgroup.entity.ProductGroup;
import com.jeeplus.modules.productgroup.mapper.ProductGroupMapper;

/**
 * 商品拼团Service
 * @author lixinapp
 * @version 2019-10-26
 */
@Service
@Transactional(readOnly = true)
public class ProductGroupService extends CrudService<ProductGroupMapper, ProductGroup> {

	public ProductGroup get(String id) {
		return super.get(id);
	}
	
	public List<ProductGroup> findList(ProductGroup productGroup) {
		return super.findList(productGroup);
	}
	
	public Page<ProductGroup> findPage(Page<ProductGroup> page, ProductGroup productGroup) {
		return super.findPage(page, productGroup);
	}
	
	@Transactional(readOnly = false)
	public void save(ProductGroup productGroup) {
		super.save(productGroup);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProductGroup productGroup) {
		super.delete(productGroup);
	}
	
}