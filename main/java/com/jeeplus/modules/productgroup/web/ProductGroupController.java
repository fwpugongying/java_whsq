/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productgroup.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.productgroup.entity.ProductGroup;
import com.jeeplus.modules.productgroup.service.ProductGroupService;

/**
 * 商品拼团Controller
 * @author lixinapp
 * @version 2019-10-26
 */
@Controller
@RequestMapping(value = "${adminPath}/productgroup/productGroup")
public class ProductGroupController extends BaseController {

	@Autowired
	private ProductGroupService productGroupService;
	
	@ModelAttribute
	public ProductGroup get(@RequestParam(required=false) String id) {
		ProductGroup entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = productGroupService.get(id);
		}
		if (entity == null){
			entity = new ProductGroup();
		}
		return entity;
	}
	
	/**
	 * 商品拼团列表页面
	 */
	@RequiresPermissions("productgroup:productGroup:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProductGroup productGroup, Model model) {
		model.addAttribute("productGroup", productGroup);
		return "modules/productgroup/productGroupList";
	}
	
		/**
	 * 商品拼团列表数据
	 */
	@ResponseBody
	@RequiresPermissions("productgroup:productGroup:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProductGroup productGroup, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProductGroup> page = productGroupService.findPage(new Page<ProductGroup>(request, response), productGroup);
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑商品拼团表单页面
	 */
	@RequiresPermissions(value={"productgroup:productGroup:view","productgroup:productGroup:add","productgroup:productGroup:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProductGroup productGroup, Model model) {
		model.addAttribute("productGroup", productGroup);
		return "modules/productgroup/productGroupForm";
	}

	/**
	 * 保存商品拼团
	 */
	@ResponseBody
	@RequiresPermissions(value={"productgroup:productGroup:add","productgroup:productGroup:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProductGroup productGroup, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(productGroup);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		productGroupService.save(productGroup);//保存
		j.setSuccess(true);
		j.setMsg("保存商品拼团成功");
		return j;
	}
	
	/**
	 * 删除商品拼团
	 */
	@ResponseBody
	@RequiresPermissions("productgroup:productGroup:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProductGroup productGroup) {
		AjaxJson j = new AjaxJson();
		productGroupService.delete(productGroup);
		j.setMsg("删除商品拼团成功");
		return j;
	}
	
	/**
	 * 批量删除商品拼团
	 */
	@ResponseBody
	@RequiresPermissions("productgroup:productGroup:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			productGroupService.delete(productGroupService.get(id));
		}
		j.setMsg("删除商品拼团成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("productgroup:productGroup:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProductGroup productGroup, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品拼团"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProductGroup> page = productGroupService.findPage(new Page<ProductGroup>(request, response, -1), productGroup);
    		new ExportExcel("商品拼团", ProductGroup.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出商品拼团记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("productgroup:productGroup:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProductGroup> list = ei.getDataList(ProductGroup.class);
			for (ProductGroup productGroup : list){
				try{
					productGroupService.save(productGroup);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条商品拼团记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条商品拼团记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入商品拼团失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入商品拼团数据模板
	 */
	@ResponseBody
	@RequiresPermissions("productgroup:productGroup:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "商品拼团数据导入模板.xlsx";
    		List<ProductGroup> list = Lists.newArrayList(); 
    		new ExportExcel("商品拼团数据", ProductGroup.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}