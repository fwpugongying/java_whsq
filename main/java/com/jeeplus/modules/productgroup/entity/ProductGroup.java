/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.productgroup.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import com.jeeplus.modules.product.entity.Product;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 商品拼团Entity
 * @author lixinapp
 * @version 2019-10-26
 */
public class ProductGroup extends DataEntity<ProductGroup> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 发起人
	private Product product;		// 商品
	private String state;		// 状态 0进行中 1拼团成功 2拼团失败
	private Date endDate;		// 成功/失败时间
	private String times;	//拼团开始多久结束小时
	private String isMradd;// 是否可以自己选择地址
	private String ptRenNum;// 拼团人数
	private String canNum;// 参团人数
	private String type; // 开团类型
	private String isPingou;// 是否是拼购商品


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCanNum() {
		return canNum;
	}

	public void setCanNum(String canNum) {
		this.canNum = canNum;
	}

	public String getIsMradd() {
		return isMradd;
	}

	public void setIsMradd(String isMradd) {
		this.isMradd = isMradd;
	}

	public String getPtRenNum() {
		return ptRenNum;
	}

	public void setPtRenNum(String ptRenNum) {
		this.ptRenNum = ptRenNum;
	}


	public String getTimes() {
		return times;
	}

	public void setTimes(String times) {
		this.times = times;
	}

	public ProductGroup() {
		super();
	}

	public ProductGroup(String id){
		super(id);
	}

	@NotNull(message="发起人不能为空")
	@ExcelField(title="发起人", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@NotNull(message="商品不能为空")
	@ExcelField(title="商品", fieldType=Product.class, value="product.title", align=2, sort=2)
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@ExcelField(title = "状态 0进行中 1拼团成功 2拼团失败", dictType = "productGroup_state", align = 2, sort = 4)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title = "成功/失败时间", align = 2, sort = 5)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getIsPingou() {
		return isPingou;
	}

	public void setIsPingou(String isPingou) {
		this.isPingou = isPingou;
	}
}