/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcoupon.entity;

import com.jeeplus.modules.shop.entity.Shop;
import javax.validation.constraints.NotNull;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 实体店优惠券Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class ShopCoupon extends DataEntity<ShopCoupon> {
	
	private static final long serialVersionUID = 1L;
	private Shop shop;		// 店铺
	private String price;		// 售价
	private String money;		// 抵用金额
	private String amount;		// 结算价
	private Date startDate;		// 有效起始日期
	private Date endDate;		// 有效截止日期
	private String content;		// 使用规则
	private String state;		// 状态 0未上架 1已上架 2已过期
	private String code;		// 编码
	private String point;		// 铜板
	private String audit;		// 审核状态 0待审核 1审核通过 2审核拒绝
	private Date auditDate;		// 审核时间
	private String group;// 分组查询
	public ShopCoupon() {
		super();
	}

	public ShopCoupon(String id){
		super(id);
	}

	@NotNull(message="店铺不能为空")
	@ExcelField(title="店铺", fieldType=Shop.class, value="shop.title", align=2, sort=1)
	public Shop getShop() {
		return shop;
	}

	public void setShop(Shop shop) {
		this.shop = shop;
	}
	
	@ExcelField(title="售价", align=2, sort=2)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@ExcelField(title="抵用金额", align=2, sort=3)
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}
	
	@ExcelField(title="结算价", align=2, sort=4)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="有效起始日期不能为空")
	@ExcelField(title="有效起始日期", align=2, sort=5)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@NotNull(message="有效截止日期不能为空")
	@ExcelField(title="有效截止日期", align=2, sort=6)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	@ExcelField(title="使用规则", align=2, sort=7)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="状态 0未上架 1已上架 2已过期", dictType="shopcoupon_state", align=2, sort=8)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="编码", align=2, sort=9)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="铜板", align=2, sort=10)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	@ExcelField(title="审核状态 0待审核 1审核通过 2审核拒绝", dictType="shopcoupon_audit", align=2, sort=11)
	public String getAudit() {
		return audit;
	}

	public void setAudit(String audit) {
		this.audit = audit;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=14)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
}