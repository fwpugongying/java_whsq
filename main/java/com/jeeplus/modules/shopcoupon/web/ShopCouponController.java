/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcoupon.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.common.utils.time.DateUtil;
import com.jeeplus.modules.shopcoupon.entity.ShopCoupon;
import com.jeeplus.modules.shopcoupon.service.ShopCouponService;

/**
 * 实体店优惠券Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/shopcoupon/shopCoupon")
public class ShopCouponController extends BaseController {

	@Autowired
	private ShopCouponService shopCouponService;
	
	@ModelAttribute
	public ShopCoupon get(@RequestParam(required=false) String id) {
		ShopCoupon entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopCouponService.get(id);
		}
		if (entity == null){
			entity = new ShopCoupon();
		}
		return entity;
	}
	
	/**
	 * 实体店优惠券列表页面
	 */
	@RequiresPermissions("shopcoupon:shopCoupon:list")
	@RequestMapping(value = {"list", ""})
	public String list(ShopCoupon shopCoupon, Model model) {
		model.addAttribute("shopCoupon", shopCoupon);
		return "modules/shopcoupon/shopCouponList";
	}
	
		/**
	 * 实体店优惠券列表数据
	 */
	@ResponseBody
	@RequiresPermissions("shopcoupon:shopCoupon:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ShopCoupon shopCoupon, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopCoupon> page = shopCouponService.findPage(new Page<ShopCoupon>(request, response), shopCoupon); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑实体店优惠券表单页面
	 */
	@RequiresPermissions(value={"shopcoupon:shopCoupon:view","shopcoupon:shopCoupon:add","shopcoupon:shopCoupon:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ShopCoupon shopCoupon, Model model) {
		model.addAttribute("shopCoupon", shopCoupon);
		return "modules/shopcoupon/shopCouponForm";
	}

	/**
	 * 保存实体店优惠券
	 */
	@ResponseBody
	@RequiresPermissions(value={"shopcoupon:shopCoupon:add","shopcoupon:shopCoupon:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ShopCoupon shopCoupon, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(shopCoupon);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		shopCoupon.setStartDate(DateUtil.beginOfDate(shopCoupon.getStartDate()));
		shopCoupon.setEndDate(new Date(DateUtil.nextDate(shopCoupon.getEndDate()).getTime() - 1000));
		//新增或编辑表单保存
		shopCouponService.save(shopCoupon);//保存
		j.setSuccess(true);
		j.setMsg("保存实体店优惠券成功");
		return j;
	}
	
	/**
	 * 删除实体店优惠券
	 */
	@ResponseBody
	@RequiresPermissions("shopcoupon:shopCoupon:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ShopCoupon shopCoupon) {
		AjaxJson j = new AjaxJson();
		shopCouponService.delete(shopCoupon);
		j.setMsg("删除实体店优惠券成功");
		return j;
	}
	
	
	/**
	 * 审核实体店优惠券
	 */
	@ResponseBody
	@RequiresPermissions("shopcoupon:shopCoupon:edit")
	@RequestMapping(value = "audit")
	public AjaxJson audit(ShopCoupon shopCoupon, String type) {
		AjaxJson j = new AjaxJson();
		if (!"1".equals(shopCoupon.getState()) || !"0".equals(shopCoupon.getAudit())) {
			j.setSuccess(false);
			j.setMsg("只能操作已上架未审核的优惠券");
			return j;
		}
		shopCoupon.setAudit(type);
		shopCoupon.setAuditDate(new Date());
		shopCouponService.save(shopCoupon);
		j.setMsg("审核实体店优惠券成功");
		return j;
	}
	
	/**
	 * 批量删除实体店优惠券
	 */
	@ResponseBody
	@RequiresPermissions("shopcoupon:shopCoupon:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			shopCouponService.delete(shopCouponService.get(id));
		}
		j.setMsg("删除实体店优惠券成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("shopcoupon:shopCoupon:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ShopCoupon shopCoupon, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店优惠券"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ShopCoupon> page = shopCouponService.findPage(new Page<ShopCoupon>(request, response, -1), shopCoupon);
    		new ExportExcel("实体店优惠券", ShopCoupon.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出实体店优惠券记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("shopcoupon:shopCoupon:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ShopCoupon> list = ei.getDataList(ShopCoupon.class);
			for (ShopCoupon shopCoupon : list){
				try{
					shopCouponService.save(shopCoupon);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条实体店优惠券记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条实体店优惠券记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入实体店优惠券失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入实体店优惠券数据模板
	 */
	@ResponseBody
	@RequiresPermissions("shopcoupon:shopCoupon:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体店优惠券数据导入模板.xlsx";
    		List<ShopCoupon> list = Lists.newArrayList(); 
    		new ExportExcel("实体店优惠券数据", ShopCoupon.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}