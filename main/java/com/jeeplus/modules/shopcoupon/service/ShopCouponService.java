/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcoupon.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.shopcoupon.entity.ShopCoupon;
import com.jeeplus.modules.shopcoupon.mapper.ShopCouponMapper;

/**
 * 实体店优惠券Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class ShopCouponService extends CrudService<ShopCouponMapper, ShopCoupon> {

	public ShopCoupon get(String id) {
		return super.get(id);
	}
	
	public List<ShopCoupon> findList(ShopCoupon shopCoupon) {
		return super.findList(shopCoupon);
	}
	
	public Page<ShopCoupon> findPage(Page<ShopCoupon> page, ShopCoupon shopCoupon) {
		return super.findPage(page, shopCoupon);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopCoupon shopCoupon) {
		super.save(shopCoupon);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopCoupon shopCoupon) {
		super.delete(shopCoupon);
	}
	
}