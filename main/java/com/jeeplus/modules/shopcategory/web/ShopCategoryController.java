/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcategory.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.shopcategory.entity.ShopCategory;
import com.jeeplus.modules.shopcategory.service.ShopCategoryService;

/**
 * 实体商家行业Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/shopcategory/shopCategory")
public class ShopCategoryController extends BaseController {

	@Autowired
	private ShopCategoryService shopCategoryService;
	
	@ModelAttribute
	public ShopCategory get(@RequestParam(required=false) String id) {
		ShopCategory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = shopCategoryService.get(id);
		}
		if (entity == null){
			entity = new ShopCategory();
		}
		return entity;
	}
	
	/**
	 * 实体商家行业列表页面
	 */
	@RequiresPermissions("shopcategory:shopCategory:list")
	@RequestMapping(value = {"list", ""})
	public String list(ShopCategory shopCategory, Model model) {
		model.addAttribute("shopCategory", shopCategory);
		return "modules/shopcategory/shopCategoryList";
	}
	
		/**
	 * 实体商家行业列表数据
	 */
	@ResponseBody
	@RequiresPermissions("shopcategory:shopCategory:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ShopCategory shopCategory, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ShopCategory> page = shopCategoryService.findPage(new Page<ShopCategory>(request, response), shopCategory); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑实体商家行业表单页面
	 */
	@RequiresPermissions(value={"shopcategory:shopCategory:view","shopcategory:shopCategory:add","shopcategory:shopCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ShopCategory shopCategory, Model model) {
		model.addAttribute("shopCategory", shopCategory);
		return "modules/shopcategory/shopCategoryForm";
	}

	/**
	 * 保存实体商家行业
	 */
	@ResponseBody
	@RequiresPermissions(value={"shopcategory:shopCategory:add","shopcategory:shopCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ShopCategory shopCategory, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(shopCategory);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		shopCategoryService.save(shopCategory);//保存
		j.setSuccess(true);
		j.setMsg("保存实体商家行业成功");
		return j;
	}
	
	/**
	 * 删除实体商家行业
	 */
	@ResponseBody
	@RequiresPermissions("shopcategory:shopCategory:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ShopCategory shopCategory) {
		AjaxJson j = new AjaxJson();
		shopCategoryService.delete(shopCategory);
		j.setMsg("删除实体商家行业成功");
		return j;
	}
	
	/**
	 * 批量删除实体商家行业
	 */
	@ResponseBody
	@RequiresPermissions("shopcategory:shopCategory:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			shopCategoryService.delete(shopCategoryService.get(id));
		}
		j.setMsg("删除实体商家行业成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("shopcategory:shopCategory:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ShopCategory shopCategory, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体商家行业"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ShopCategory> page = shopCategoryService.findPage(new Page<ShopCategory>(request, response, -1), shopCategory);
    		new ExportExcel("实体商家行业", ShopCategory.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出实体商家行业记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("shopcategory:shopCategory:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ShopCategory> list = ei.getDataList(ShopCategory.class);
			for (ShopCategory shopCategory : list){
				try{
					shopCategoryService.save(shopCategory);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条实体商家行业记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条实体商家行业记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入实体商家行业失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入实体商家行业数据模板
	 */
	@ResponseBody
	@RequiresPermissions("shopcategory:shopCategory:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "实体商家行业数据导入模板.xlsx";
    		List<ShopCategory> list = Lists.newArrayList(); 
    		new ExportExcel("实体商家行业数据", ShopCategory.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}