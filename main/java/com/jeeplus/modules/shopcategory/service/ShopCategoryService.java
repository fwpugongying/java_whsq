/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcategory.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.shopcategory.entity.ShopCategory;
import com.jeeplus.modules.shopcategory.mapper.ShopCategoryMapper;

/**
 * 实体商家行业Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class ShopCategoryService extends CrudService<ShopCategoryMapper, ShopCategory> {

	public ShopCategory get(String id) {
		return super.get(id);
	}
	
	public List<ShopCategory> findList(ShopCategory shopCategory) {
		return super.findList(shopCategory);
	}
	
	public Page<ShopCategory> findPage(Page<ShopCategory> page, ShopCategory shopCategory) {
		return super.findPage(page, shopCategory);
	}
	
	@Transactional(readOnly = false)
	public void save(ShopCategory shopCategory) {
		super.save(shopCategory);
	}
	
	@Transactional(readOnly = false)
	public void delete(ShopCategory shopCategory) {
		super.delete(shopCategory);
	}
	
}