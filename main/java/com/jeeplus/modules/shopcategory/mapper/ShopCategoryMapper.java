/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcategory.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.shopcategory.entity.ShopCategory;

/**
 * 实体商家行业MAPPER接口
 * @author lixinapp
 * @version 2019-10-16
 */
@MyBatisMapper
public interface ShopCategoryMapper extends BaseMapper<ShopCategory> {
	
}