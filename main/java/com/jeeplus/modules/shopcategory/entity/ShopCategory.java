/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.shopcategory.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 实体商家行业Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class ShopCategory extends DataEntity<ShopCategory> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 名称
	private Integer sort;		// 排序
	private String state;		// 状态 0启用 1停用
	
	public ShopCategory() {
		super();
	}

	public ShopCategory(String id){
		super(id);
	}

	@ExcelField(title="名称", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=2)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	@ExcelField(title="状态 0启用 1停用", dictType="shop_category_state", align=2, sort=3)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}