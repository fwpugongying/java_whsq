/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.activity.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 首页按钮Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Activity extends DataEntity<Activity> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 名称
	private String type;		// 类型
	private String icon;		// 图标
	private Integer sort;		// 排序
	private String link;// 外部链接
	private String actid;// 活动iD
	private String state;// // 状态 0启用 1停用
	
	public Activity() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Activity(String id){
		super(id);
	}

	@ExcelField(title="状态 0启用 1停用", dictType="pay_type_state", align=2, sort=2)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@ExcelField(title="名称", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="类型", dictType="button_type", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="图标", align=2, sort=3)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=4)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@ExcelField(title="外链", align=2, sort=3)
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@ExcelField(title="iD", align=2, sort=3)
	public String getActid() {
		return actid;
	}

	public void setActid(String actid) {
		this.actid = actid;
	}

	
}