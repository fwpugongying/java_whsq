/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.activity.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.CacheUtils;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.api.CacheUtil;
import com.jeeplus.modules.activity.entity.Activity;
import com.jeeplus.modules.activity.service.ActivityService;

/**
 * 首页按钮Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/activity/activity")
public class ActivityController extends BaseController {

	@Autowired
	private ActivityService activityService;
	
	@ModelAttribute
	public Activity get(@RequestParam(required=false) String id) {
		Activity entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = activityService.get(id);
		}
		if (entity == null){
			entity = new Activity();
		}
		return entity;
	}
	
	/**
	 * 首页按钮列表页面
	 */
	@RequiresPermissions("activity:activity:list")
	@RequestMapping(value = {"list", ""})
	public String list(Activity activity, Model model) {
		model.addAttribute("activity", activity);
		return "modules/activity/activityList";
	}
	
		/**
	 * 首页按钮列表数据
	 */
	@ResponseBody
	@RequiresPermissions("activity:activity:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Activity activity, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Activity> page = activityService.findPage(new Page<Activity>(request, response), activity); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑首页按钮表单页面
	 */
	@RequiresPermissions(value={"activity:activity:view","activity:activity:add","activity:activity:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Activity activity, Model model) {
		model.addAttribute("activity", activity);
		return "modules/activity/activityForm";
	}

	/**
	 * 保存首页按钮
	 */
	@ResponseBody
	@RequiresPermissions(value={"activity:activity:add","activity:activity:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Activity activity, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(activity);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		activityService.save(activity);//保存
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_BUTTON_LIST);
		j.setSuccess(true);
		j.setMsg("保存首页按钮成功");
		return j;
	}
	
	/**
	 * 删除首页按钮
	 */
	@ResponseBody
	@RequiresPermissions("activity:activity:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Activity activity) {
		AjaxJson j = new AjaxJson();
		activityService.delete(activity);
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_BUTTON_LIST);
		j.setMsg("删除首页按钮成功");
		return j;
	}
	
	/**
	 * 批量删除首页按钮
	 */
	@ResponseBody
	@RequiresPermissions("activity:activity:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			activityService.delete(activityService.get(id));
		}
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_BUTTON_LIST);
		j.setMsg("删除首页按钮成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("activity:activity:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Activity activity, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "首页按钮"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Activity> page = activityService.findPage(new Page<Activity>(request, response, -1), activity);
    		new ExportExcel("首页按钮", Activity.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出首页按钮记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("activity:activity:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Activity> list = ei.getDataList(Activity.class);
			for (Activity activity : list){
				try{
					activityService.save(activity);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条首页按钮记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条首页按钮记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入首页按钮失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入首页按钮数据模板
	 */
	@ResponseBody
	@RequiresPermissions("activity:activity:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "首页按钮数据导入模板.xlsx";
    		List<Activity> list = Lists.newArrayList(); 
    		new ExportExcel("首页按钮数据", Activity.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}