/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcardorder.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.creditcardorder.entity.CreditCardOrder;
import com.jeeplus.modules.creditcardorder.service.CreditCardOrderService;

/**
 * 信用卡订单Controller
 * @author lixinapp
 * @version 2019-11-01
 */
@Controller
@RequestMapping(value = "${adminPath}/creditcardorder/creditCardOrder")
public class CreditCardOrderController extends BaseController {

	@Autowired
	private CreditCardOrderService creditCardOrderService;
	
	@ModelAttribute
	public CreditCardOrder get(@RequestParam(required=false) String id) {
		CreditCardOrder entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = creditCardOrderService.get(id);
		}
		if (entity == null){
			entity = new CreditCardOrder();
		}
		return entity;
	}
	
	/**
	 * 信用卡订单列表页面
	 */
	@RequiresPermissions("creditcardorder:creditCardOrder:list")
	@RequestMapping(value = {"list", ""})
	public String list(CreditCardOrder creditCardOrder, Model model) {
		model.addAttribute("creditCardOrder", creditCardOrder);
		return "modules/creditcardorder/creditCardOrderList";
	}
	
		/**
	 * 信用卡订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("creditcardorder:creditCardOrder:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(CreditCardOrder creditCardOrder, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CreditCardOrder> page = creditCardOrderService.findPage(new Page<CreditCardOrder>(request, response), creditCardOrder); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑信用卡订单表单页面
	 */
	@RequiresPermissions(value={"creditcardorder:creditCardOrder:view","creditcardorder:creditCardOrder:add","creditcardorder:creditCardOrder:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(CreditCardOrder creditCardOrder, Model model) {
		model.addAttribute("creditCardOrder", creditCardOrder);
		return "modules/creditcardorder/creditCardOrderForm";
	}

	/**
	 * 保存信用卡订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"creditcardorder:creditCardOrder:add","creditcardorder:creditCardOrder:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(CreditCardOrder creditCardOrder, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(creditCardOrder);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		creditCardOrderService.save(creditCardOrder);//保存
		j.setSuccess(true);
		j.setMsg("保存信用卡订单成功");
		return j;
	}
	
	/**
	 * 删除信用卡订单
	 */
	@ResponseBody
	@RequiresPermissions("creditcardorder:creditCardOrder:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(CreditCardOrder creditCardOrder) {
		AjaxJson j = new AjaxJson();
		creditCardOrderService.delete(creditCardOrder);
		j.setMsg("删除信用卡订单成功");
		return j;
	}
	
	/**
	 * 批量删除信用卡订单
	 */
	@ResponseBody
	@RequiresPermissions("creditcardorder:creditCardOrder:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			creditCardOrderService.delete(creditCardOrderService.get(id));
		}
		j.setMsg("删除信用卡订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("creditcardorder:creditCardOrder:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(CreditCardOrder creditCardOrder, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "信用卡订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<CreditCardOrder> page = creditCardOrderService.findPage(new Page<CreditCardOrder>(request, response, -1), creditCardOrder);
    		new ExportExcel("信用卡订单", CreditCardOrder.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出信用卡订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("creditcardorder:creditCardOrder:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<CreditCardOrder> list = ei.getDataList(CreditCardOrder.class);
			for (CreditCardOrder creditCardOrder : list){
				try{
					creditCardOrderService.save(creditCardOrder);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条信用卡订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条信用卡订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入信用卡订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入信用卡订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("creditcardorder:creditCardOrder:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "信用卡订单数据导入模板.xlsx";
    		List<CreditCardOrder> list = Lists.newArrayList(); 
    		new ExportExcel("信用卡订单数据", CreditCardOrder.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}