/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcardorder.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.creditcardorder.entity.CreditCardOrder;

/**
 * 信用卡订单MAPPER接口
 * @author lixinapp
 * @version 2019-11-01
 */
@MyBatisMapper
public interface CreditCardOrderMapper extends BaseMapper<CreditCardOrder> {
	
}