/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcardorder.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.creditcardorder.entity.CreditCardOrder;
import com.jeeplus.modules.creditcardorder.mapper.CreditCardOrderMapper;

/**
 * 信用卡订单Service
 * @author lixinapp
 * @version 2019-11-01
 */
@Service
@Transactional(readOnly = true)
public class CreditCardOrderService extends CrudService<CreditCardOrderMapper, CreditCardOrder> {

	public CreditCardOrder get(String id) {
		return super.get(id);
	}
	
	public List<CreditCardOrder> findList(CreditCardOrder creditCardOrder) {
		return super.findList(creditCardOrder);
	}
	
	public Page<CreditCardOrder> findPage(Page<CreditCardOrder> page, CreditCardOrder creditCardOrder) {
		return super.findPage(page, creditCardOrder);
	}
	
	@Transactional(readOnly = false)
	public void save(CreditCardOrder creditCardOrder) {
		super.save(creditCardOrder);
	}
	
	@Transactional(readOnly = false)
	public void delete(CreditCardOrder creditCardOrder) {
		super.delete(creditCardOrder);
	}
	
}