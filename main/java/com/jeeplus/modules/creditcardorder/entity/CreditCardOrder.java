/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.creditcardorder.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 信用卡订单Entity
 * @author lixinapp
 * @version 2019-11-01
 */
public class CreditCardOrder extends DataEntity<CreditCardOrder> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String code;		// 信用卡编码
	private String title;		// 名称
	private String icon;		// 图片
	private String url;		// 接口落地页
	private String point;		// 铜板
	private String orderStatus;		// 结算状态 0： 推广订单， 1：成功订单 2：失效订单
	private String cardStatus;		//  信用卡状态0 ： 进件 1：初审 2：核卡 3：激活 4： 首刷
	private String money;		// 结算金额
	
	public CreditCardOrder() {
		super();
	}

	public CreditCardOrder(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="信用卡编码", align=2, sort=2)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="名称", align=2, sort=3)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="图片", align=2, sort=4)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@ExcelField(title="接口落地页", align=2, sort=5)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@ExcelField(title="铜板", align=2, sort=6)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}
	
	@ExcelField(title="结算状态 0： 推广订单， 1：成功订单 2：失效订单", dictType="credit_order_state", align=2, sort=7)
	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	
	@ExcelField(title=" 信用卡状态0 ： 进件 1：初审 2：核卡 3：激活 4： 首刷", dictType="credit_order_card_state", align=2, sort=8)
	public String getCardStatus() {
		return cardStatus;
	}

	public void setCardStatus(String cardStatus) {
		this.cardStatus = cardStatus;
	}
	
	@ExcelField(title="结算金额", align=2, sort=9)
	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}
	
}