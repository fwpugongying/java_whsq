/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cashprop.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.cashprop.entity.CashProp;
import com.jeeplus.modules.cashprop.mapper.CashPropMapper;

/**
 * 提现费率Service
 * @author lixinapp
 * @version 2019-12-17
 */
@Service
@Transactional(readOnly = true)
public class CashPropService extends CrudService<CashPropMapper, CashProp> {

	public CashProp get(String id) {
		return super.get(id);
	}
	
	public List<CashProp> findList(CashProp cashProp) {
		return super.findList(cashProp);
	}
	
	public Page<CashProp> findPage(Page<CashProp> page, CashProp cashProp) {
		return super.findPage(page, cashProp);
	}
	
	@Transactional(readOnly = false)
	public void save(CashProp cashProp) {
		super.save(cashProp);
	}
	
	@Transactional(readOnly = false)
	public void delete(CashProp cashProp) {
		super.delete(cashProp);
	}
	
}