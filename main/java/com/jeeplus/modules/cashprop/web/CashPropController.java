/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cashprop.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.cashprop.entity.CashProp;
import com.jeeplus.modules.cashprop.service.CashPropService;

/**
 * 提现费率Controller
 * @author lixinapp
 * @version 2019-12-17
 */
@Controller
@RequestMapping(value = "${adminPath}/cashprop/cashProp")
public class CashPropController extends BaseController {

	@Autowired
	private CashPropService cashPropService;
	
	@ModelAttribute
	public CashProp get(@RequestParam(required=false) String id) {
		CashProp entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = cashPropService.get(id);
		}
		if (entity == null){
			entity = new CashProp();
		}
		return entity;
	}
	
	/**
	 * 提现费率列表页面
	 */
	@RequiresPermissions("cashprop:cashProp:list")
	@RequestMapping(value = {"list", ""})
	public String list(CashProp cashProp, Model model) {
		model.addAttribute("cashProp", cashProp);
		return "modules/cashprop/cashPropList";
	}
	
		/**
	 * 提现费率列表数据
	 */
	@ResponseBody
	@RequiresPermissions("cashprop:cashProp:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(CashProp cashProp, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<CashProp> page = cashPropService.findPage(new Page<CashProp>(request, response), cashProp); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑提现费率表单页面
	 */
	@RequiresPermissions(value={"cashprop:cashProp:view","cashprop:cashProp:add","cashprop:cashProp:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(CashProp cashProp, Model model) {
		model.addAttribute("cashProp", cashProp);
		return "modules/cashprop/cashPropForm";
	}

	/**
	 * 保存提现费率
	 */
	@ResponseBody
	@RequiresPermissions(value={"cashprop:cashProp:add","cashprop:cashProp:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(CashProp cashProp, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(cashProp);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		cashPropService.save(cashProp);//保存
		j.setSuccess(true);
		j.setMsg("保存提现费率成功");
		return j;
	}
	
	/**
	 * 删除提现费率
	 */
	@ResponseBody
	@RequiresPermissions("cashprop:cashProp:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(CashProp cashProp) {
		AjaxJson j = new AjaxJson();
		cashPropService.delete(cashProp);
		j.setMsg("删除提现费率成功");
		return j;
	}
	
	/**
	 * 批量删除提现费率
	 */
	@ResponseBody
	@RequiresPermissions("cashprop:cashProp:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			cashPropService.delete(cashPropService.get(id));
		}
		j.setMsg("删除提现费率成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("cashprop:cashProp:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(CashProp cashProp, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "提现费率"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<CashProp> page = cashPropService.findPage(new Page<CashProp>(request, response, -1), cashProp);
    		new ExportExcel("提现费率", CashProp.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出提现费率记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("cashprop:cashProp:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<CashProp> list = ei.getDataList(CashProp.class);
			for (CashProp cashProp : list){
				try{
					cashPropService.save(cashProp);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条提现费率记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条提现费率记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入提现费率失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入提现费率数据模板
	 */
	@ResponseBody
	@RequiresPermissions("cashprop:cashProp:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "提现费率数据导入模板.xlsx";
    		List<CashProp> list = Lists.newArrayList(); 
    		new ExportExcel("提现费率数据", CashProp.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}