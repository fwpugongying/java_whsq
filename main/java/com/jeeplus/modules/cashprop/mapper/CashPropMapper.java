/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cashprop.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.cashprop.entity.CashProp;

/**
 * 提现费率MAPPER接口
 * @author lixinapp
 * @version 2019-12-17
 */
@MyBatisMapper
public interface CashPropMapper extends BaseMapper<CashProp> {
	
}