/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.cashprop.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 提现费率Entity
 * @author lixinapp
 * @version 2019-12-17
 */
public class CashProp extends DataEntity<CashProp> {
	
	private static final long serialVersionUID = 1L;
	private String prop;		// 费率
	private String type;		// 类型 1云店 2实体店
	
	public CashProp() {
		super();
	}

	public CashProp(String id){
		super(id);
	}

	@ExcelField(title="费率", align=2, sort=1)
	public String getProp() {
		return prop;
	}

	public void setProp(String prop) {
		this.prop = prop;
	}
	
	@ExcelField(title="类型 1云店 2实体店", dictType="cash_prop_type", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}