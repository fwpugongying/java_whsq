/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.college.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.college.entity.College;
import com.jeeplus.modules.college.service.CollegeService;

/**
 * 我惠学院Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/college/college")
public class CollegeController extends BaseController {

	@Autowired
	private CollegeService collegeService;
	
	@ModelAttribute
	public College get(@RequestParam(required=false) String id) {
		College entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = collegeService.get(id);
		}
		if (entity == null){
			entity = new College();
		}
		return entity;
	}
	
	/**
	 * 我惠学院列表页面
	 */
	@RequiresPermissions("college:college:list")
	@RequestMapping(value = {"list", ""})
	public String list(College college, Model model) {
		model.addAttribute("college", college);
		return "modules/college/collegeList";
	}
	
		/**
	 * 我惠学院列表数据
	 */
	@ResponseBody
	@RequiresPermissions("college:college:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(College college, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<College> page = collegeService.findPage(new Page<College>(request, response), college); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑我惠学院表单页面
	 */
	@RequiresPermissions(value={"college:college:view","college:college:add","college:college:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(College college, Model model) {
		model.addAttribute("college", college);
		return "modules/college/collegeForm";
	}

	/**
	 * 保存我惠学院
	 */
	@ResponseBody
	@RequiresPermissions(value={"college:college:add","college:college:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(College college, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(college);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		collegeService.save(college);//保存
		j.setSuccess(true);
		j.setMsg("保存我惠学院成功");
		return j;
	}
	
	/**
	 * 删除我惠学院
	 */
	@ResponseBody
	@RequiresPermissions("college:college:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(College college) {
		AjaxJson j = new AjaxJson();
		collegeService.delete(college);
		j.setMsg("删除我惠学院成功");
		return j;
	}
	
	/**
	 * 批量删除我惠学院
	 */
	@ResponseBody
	@RequiresPermissions("college:college:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			collegeService.delete(collegeService.get(id));
		}
		j.setMsg("删除我惠学院成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("college:college:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(College college, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "我惠学院"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<College> page = collegeService.findPage(new Page<College>(request, response, -1), college);
    		new ExportExcel("我惠学院", College.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出我惠学院记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("college:college:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<College> list = ei.getDataList(College.class);
			for (College college : list){
				try{
					collegeService.save(college);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条我惠学院记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条我惠学院记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入我惠学院失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入我惠学院数据模板
	 */
	@ResponseBody
	@RequiresPermissions("college:college:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "我惠学院数据导入模板.xlsx";
    		List<College> list = Lists.newArrayList(); 
    		new ExportExcel("我惠学院数据", College.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}