/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.college.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.college.entity.College;
import com.jeeplus.modules.college.mapper.CollegeMapper;

/**
 * 我惠学院Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class CollegeService extends CrudService<CollegeMapper, College> {

	public College get(String id) {
		return super.get(id);
	}
	
	public List<College> findList(College college) {
		return super.findList(college);
	}
	
	public Page<College> findPage(Page<College> page, College college) {
		return super.findPage(page, college);
	}
	
	@Transactional(readOnly = false)
	public void save(College college) {
		super.save(college);
	}
	
	@Transactional(readOnly = false)
	public void delete(College college) {
		super.delete(college);
	}
	
}