/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.college.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 我惠学院Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class College extends DataEntity<College> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 标题
	private String image;		// 图片
	private String content;		// 内容
	private Integer sort;// 排序
	
	public College() {
		super();
	}

	public College(String id){
		super(id);
	}

	@ExcelField(title="标题", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="图片", align=2, sort=2)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	@ExcelField(title="内容", align=2, sort=3)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * 富文本路径
	 * @return
	 */
	public String getUrl() {
		return Global.getConfig("httpsPath") + "/display/college?id=" + id;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}