/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.ktq.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.ktq.entity.Ktq;
import com.jeeplus.modules.ktq.mapper.KtqMapper;
/**
 * 开通权购买Service
 * @author lixinapp
 * @version 2019-10-16
 */
@Service
@Transactional(readOnly = true)
public class KtqService extends CrudService<KtqMapper, Ktq> {


	@Autowired

	public Ktq get(String id) {
		return super.get(id);
	}

	public List<Ktq> findList(Ktq ktq) {
		return super.findList(ktq);
	}
	public List<Ktq> findUniqueByProperty(Ktq ktq) {
		return super.findList(ktq);
	}
	public Ktq getUid(String uid) {
		return findUniqueByProperty("uid", uid);
	}
	public Page<Ktq> findPage(Page<Ktq> page, Ktq ktq) {
		return super.findPage(page, ktq);
	}
	@Transactional(readOnly = false)
	public void save(Ktq ktq) {
		super.save(ktq);
	}
	/**
	 * 支付处理
	 */
	@Transactional(readOnly = false)
	public void pay(Ktq ktq) {
		super.save(ktq);
	}

	@Transactional(readOnly = false)
	public void delete(Ktq ktq) {
		super.delete(ktq);
	}

}