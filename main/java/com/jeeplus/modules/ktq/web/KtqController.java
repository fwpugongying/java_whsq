/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.ktq.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.msg.service.MsgService;
import com.jeeplus.modules.ktq.entity.Ktq;
import com.jeeplus.modules.ktq.service.KtqService;

/**
 * 开通权购买订单Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/ktq/ktq")
public class KtqController extends BaseController {

	@Autowired
	private KtqService ktqService;
	@Autowired
	private MsgService msgService;
	
	@ModelAttribute
	public Ktq get(@RequestParam(required=false) String id) {
		Ktq entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = ktqService.get(id);
		}
		if (entity == null){
			entity = new Ktq();
		}
		return entity;
	}
	
	/**
	 * 开通权购买订单列表页面
	 */
	@RequiresPermissions("ktq:ktq:list")
	@RequestMapping(value = {"list", ""})
	public String list(Ktq ktq, Model model) {
		model.addAttribute("ktq", ktq);
		return "modules/ktq/ktqList";
	}
	
		/**
	 * 开通权购买订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("ktq:ktq:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Ktq ktq, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Ktq> page = ktqService.findPage(new Page<Ktq>(request, response), ktq);
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑开通权购买订单表单页面
	 */
	@RequiresPermissions(value={"ktq:ktq:view","ktq:ktq:add","ktq:ktq:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Ktq ktq, Model model) {
		model.addAttribute("ktq", ktq);
		return "modules/ktq/ktqForm";
	}

	/**
	 * 保存开通权购买订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"ktq:ktq:add","ktq:ktq:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Ktq ktq, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(ktq);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		ktqService.save(ktq);//保存
		j.setSuccess(true);
		j.setMsg("保存开通权购买订单成功");
		return j;
	}
	/**
	 * 冻结解冻开通权购买订单
	 */
	@ResponseBody
	@RequiresPermissions(value="ktq:ktq:edit")
	@RequestMapping(value = "updateState")
	public AjaxJson updateState(Ktq ktq, String type) throws Exception{
		AjaxJson j = new AjaxJson();
		if (StringUtils.isNotBlank(ktq.getId())) {
			ktq.setState(type);
			ktqService.save(ktq);//保存
		}
		j.setSuccess(true);
		j.setMsg("操作成功");
		return j;
	}
	
	/**
	 * 删除开通权购买订单
	 */
	@ResponseBody
	@RequiresPermissions("ktq:ktq:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Ktq ktq) {
		AjaxJson j = new AjaxJson();
		ktqService.delete(ktq);
		j.setMsg("删除开通权购买订单成功");
		return j;
	}
	
	/**
	 * 批量删除开通权购买订单
	 */
	@ResponseBody
	@RequiresPermissions("ktq:ktq:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			ktqService.delete(ktqService.get(id));
		}
		j.setMsg("删除开通权购买订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("ktq:ktq:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Ktq ktq, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "开通权购买订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Ktq> page = ktqService.findPage(new Page<Ktq>(request, response, -1), ktq);
    		new ExportExcel("开通权购买订单", Ktq.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出开通权购买订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("ktq:ktq:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Ktq> list = ei.getDataList(Ktq.class);
			for (Ktq ktq : list){
				try{
					ktqService.save(ktq);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条开通权购买订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条开通权购买订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入开通权购买订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入开通权购买订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("ktq:ktq:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "开通权购买订单数据导入模板.xlsx";
    		List<Ktq> list = Lists.newArrayList(); 
    		new ExportExcel("开通权购买订单数据", Ktq.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}