/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.member.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 用户Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Member extends DataEntity<Member> {
	
	private static final long serialVersionUID = 1L;
	private String phone;		// 账号
	private String Openid;      //openid
	private String txopenid;      //商家提现openid
	private String wxAppOpenid;      //小程序openid
	private String txname;      //商家提现微信昵称
	private String nickname;		// 昵称
	private String icon;		// 头像
	private String password;		// 密码
	private String province;		// 省份
	private String city;		// 城市
	private String area;		// 区县
	private String state;		// 状态 0正常 1冻结
	private String wxid;		// 微信ID
	private String qqid;		// QQID
	private String aliid;		// 支付宝ID
	private String point;		// point
	private String token;		// 推送标识
	private Member invite;		// 邀请人
	private String number;// 我惠卡号
	private String roles;// 有效身份
	private String relationId;// 渠道ID
	private String pddId;// 拼多多所需的推广位ID
	private String jdId;// 京东所需的推广位ID
	private String wphId;// 唯品会所需的推广位ID

	public Member() {
		super();
	}

	public Member(String id){
		super(id);
	}

	@ExcelField(title="Openid", align=2, sort=1)
	public String getOpenid() {
		return Openid;
	}
	public void setOpenid(String Openid) {
		this.Openid = Openid;
	}

	@ExcelField(title="txopenid", align=2, sort=1)
	public String getTxopenid() {
		return txopenid;
	}
	public void setTxopenid(String txopenid) {
		this.txopenid = txopenid;
	}
	@ExcelField(title="wxAppOpenid", align=2, sort=1)
	public String getWxAppOpenid() {
		return wxAppOpenid;
	}

	public void setWxAppOpenid(String wxAppOpenid) {
		this.wxAppOpenid = wxAppOpenid;
	}

	@ExcelField(title="txname", align=2, sort=1)
	public String getTxname() {
		return txname;
	}
	public void setTxname(String txname) {
		this.txname = txname;
	}

	@ExcelField(title="账号", align=2, sort=1)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@ExcelField(title="昵称", align=2, sort=2)
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	@ExcelField(title="头像", align=2, sort=3)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@ExcelField(title="密码", align=2, sort=4)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@ExcelField(title="省份", align=2, sort=5)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@ExcelField(title="城市", align=2, sort=6)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@ExcelField(title="区县", fieldType=String.class, value="", align=2, sort=7)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	@ExcelField(title="状态 0正常 1冻结", dictType="member_state", align=2, sort=8)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="微信ID", align=2, sort=9)
	public String getWxid() {
		return wxid;
	}

	public void setWxid(String wxid) {
		this.wxid = wxid;
	}
	
	@ExcelField(title="QQID", align=2, sort=10)
	public String getQqid() {
		return qqid;
	}

	public void setQqid(String qqid) {
		this.qqid = qqid;
	}
	
	@ExcelField(title="支付宝ID", align=2, sort=11)
	public String getAliid() {
		return aliid;
	}

	public void setAliid(String aliid) {
		this.aliid = aliid;
	}
	
	@ExcelField(title="point", align=2, sort=12)
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	@ExcelField(title="推送标识", align=2, sort=13)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	@ExcelField(title="邀请人", fieldType=Member.class, value="invite.phone", align=2, sort=14)
	public Member getInvite() {
		return invite;
	}

	public void setInvite(Member invite) {
		this.invite = invite;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public String getRelationId() {
		return relationId;
	}

	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

	public String getPddId() {
		return pddId;
	}

	public void setPddId(String pddId) {
		this.pddId = pddId;
	}

	public String getJdId() {
		return jdId;
	}

	public void setJdId(String jdId) {
		this.jdId = jdId;
	}

	public String getWphId() {
		return wphId;
	}
	public void setWphId(String wphId) {
		this.wphId = wphId;
	}
}