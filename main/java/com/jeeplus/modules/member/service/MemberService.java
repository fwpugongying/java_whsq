/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.member.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.modules.member.mapper.MemberMapper;

/**
 * 用户Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class MemberService extends CrudService<MemberMapper, Member> {

	public Member get(String id) {
		return super.get(id);
	}
	
	public List<Member> findList(Member member) {
		return super.findList(member);
	}
	
	public Page<Member> findPage(Page<Member> page, Member member) {
		return super.findPage(page, member);
	}
	
	@Transactional(readOnly = false)
	public void save(Member member) {
		super.save(member);
	}
	
	@Transactional(readOnly = false)
	public void delete(Member member) {
		super.delete(member);
	}

	/**
	 * 根据手机号查询用户
	 * @param string
	 * @return
	 */
	public Member getByPhone(String phone) {
		return findUniqueByProperty("phone", phone);
	}

	/**
	 * 修改铜板
	 * @param member 用户
	 * @param point 铜板
	 * @param type 0减少 1增加
	 */
	@Transactional(readOnly = false)
	public synchronized void updatePoint(Member member, String point, String type) {
		if ("1".equals(type)) {
			mapper.execUpdateSql("UPDATE t_member SET point = point + "+point+" WHERE id = '"+member.getId()+"'");
		} else {
			mapper.execUpdateSql("UPDATE t_member SET point = point - "+point+" WHERE id = '"+member.getId()+"'");
		}
	}
	/**
	 * 修改注册人
	 * @param id 用户ID
	 * @param invite 推荐人GUID
	 */
	@Transactional(readOnly = false)
	public synchronized void updateInvite(String id, String invite) {
			mapper.execUpdateSql("UPDATE t_member SET invite_id = '"+invite+"' WHERE id = '"+id+"'");
	}

}