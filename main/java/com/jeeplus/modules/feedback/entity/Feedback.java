/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.feedback.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;
import com.jeeplus.modules.member.entity.Member;

import javax.validation.constraints.NotNull;

/**
 * 意见反馈后Entity
 * @author lixinapp
 * @version 2019-10-16
 */
public class Feedback extends DataEntity<Feedback> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String reason;		// 退款原因
	private String content;		// 描述
	private String images;		// 图片
	private String state;		// 状态 0待审核 1通过 2拒绝
	private Date auditDate;		// 审核时间

	public Feedback() {
		super();
	}

	public Feedback(String id){
		super(id);
	}
	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@ExcelField(title="原因", align=2, sort=2)
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@ExcelField(title="描述", align=2, sort=3)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@ExcelField(title="图片", align=2, sort=4)
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	@ExcelField(title="状态 0待审核 1通过 2拒绝", dictType="order_refund_state", align=2, sort=7)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="审核时间", align=2, sort=8)
	public Date getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	
}