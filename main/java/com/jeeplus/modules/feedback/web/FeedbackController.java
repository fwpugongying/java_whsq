/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.feedback.web;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.feedback.entity.Feedback;
import com.jeeplus.modules.feedback.service.FeedbackService;
import com.jeeplus.modules.sys.entity.User;
import com.jeeplus.modules.sys.utils.UserUtils;

/**
 * 意见反馈Controller
 * @author lixinapp
 * @version 2019-10-16
 */
@Controller
@RequestMapping(value = "${adminPath}/feedback/feedback")
public class FeedbackController extends BaseController {

	@Autowired
	private FeedbackService feedbackService;

	@ModelAttribute
	public Feedback get(@RequestParam(required=false) String id) {
		Feedback entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = feedbackService.get(id);
		}
		if (entity == null){
			entity = new Feedback();
		}
		return entity;
	}
	
	/**
	 * 意见反馈列表页面
	 */
	@RequiresPermissions("feedback:feedback:list")
	@RequestMapping(value = {"list", ""})
	public String list(Feedback feedback, Model model) {
		model.addAttribute("feedback", feedback);
		return "modules/feedback/feedbackList";
	}
	
		/**
	 * 意见反馈列表数据
	 */
	@ResponseBody
	@RequiresPermissions("feedback:feedback:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Feedback feedback, HttpServletRequest request, HttpServletResponse response, Model model) {
		// 判断当前登录用户
		User user = UserUtils.getUser();
		if (user.isShop()) {
			feedback.setDataScope(" AND store.user_id = '"+user.getId()+"' ");
		}
		Page<Feedback> page = feedbackService.findPage(new Page<Feedback>(request, response), feedback); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑意见反馈表单页面
	 */
	@RequiresPermissions(value={"feedback:feedback:view","feedback:feedback:add","feedback:feedback:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Feedback feedback, Model model) {
		model.addAttribute("feedback", feedback);
		return "modules/feedback/feedbackForm";
	}

	/**
	 * 保存意见反馈
	 */
	@ResponseBody
	@RequiresPermissions(value={"feedback:feedback:add","feedback:feedback:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Feedback feedback, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(feedback);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		feedbackService.save(feedback);//保存
		j.setSuccess(true);
		j.setMsg("保存意见反馈成功");
		return j;
	}
	
	/**
	 * 删除意见反馈
	 */
	@ResponseBody
	@RequiresPermissions("feedback:feedback:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Feedback feedback) {
		AjaxJson j = new AjaxJson();
		feedbackService.delete(feedback);
		j.setMsg("删除意见反馈成功");
		return j;
	}
	
	/**
	 * 批量删除意见反馈
	 */
	@ResponseBody
	@RequiresPermissions("feedback:feedback:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			feedbackService.delete(feedbackService.get(id));
		}
		j.setMsg("删除意见反馈成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("feedback:feedback:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Feedback feedback, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "意见反馈"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Feedback> page = feedbackService.findPage(new Page<Feedback>(request, response, -1), feedback);
    		new ExportExcel("意见反馈", Feedback.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出意见反馈记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("feedback:feedback:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Feedback> list = ei.getDataList(Feedback.class);
			for (Feedback feedback : list){
				try{
					feedbackService.save(feedback);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条意见反馈记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条意见反馈记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入意见反馈失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入意见反馈数据模板
	 */
	@ResponseBody
	@RequiresPermissions("feedback:feedback:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "意见反馈数据导入模板.xlsx";
    		List<Feedback> list = Lists.newArrayList(); 
    		new ExportExcel("意见反馈数据", Feedback.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}