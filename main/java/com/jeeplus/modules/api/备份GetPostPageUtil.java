package com.jeeplus.modules.api;

import com.jeeplus.modules.api.utils.KuaidiUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class 备份GetPostPageUtil {
    // param:请求参数，格式应该满足name1=value1&name2=value2的形式。
    public static String sendGet(String url, String param) {
        String result = "";
        if (param != null) {
            url = url + "?" + param;
        }
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            HttpURLConnection conn = getHttpURLConnection(realUrl);
            // 打印头信息
            printHeader(conn);
            // 获取响应
            result = getResponse(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    // param:请求参数，格式应该满足name1=value1&name2=value2的形式。
    public static String sendPost(String url, String param) {
        String result = "";
        try {
            URL realUrl = new URL(url);
            HttpURLConnection conn = getHttpURLConnection(realUrl);
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            {// Post发送参数：
                // 获取HttpURLConnection 对象对应的输出流
                PrintWriter out = new PrintWriter(conn.getOutputStream());
                // 发送请求参数
                out.print(param);
                out.close();
            }
            // 获取响应
            result = getResponse(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    // 提供主方法，测试发送GET请求和POST请求
    public static void Oldmain(String args[]) {
        String url = "http://www.shicimingju.com/book/xiyouji.html";
        // "https://zhuanlan.zhihu.com/hulaoshi";
        String param = null;
        // 也可以自己写个Servlet测试是否接收到参数
        // url = "http://localhost:8080/TestJavaWeb/AHServlet";
        // param = "uname=tiger";
        // ----------------------------
        String s;
        // 发送GET请求
        s = 备份GetPostPageUtil.sendGet(url, param);
        write("http_get.html", s);
        System.out.println("-----------------------------------------------");
        // 发送POST请求
        s = 备份GetPostPageUtil.sendPost(url, param);
        write("http_post.html", s);
    }
    // 提供主方法，测试发送GET请求和POST请求
    public static void main(String args[]) {
        String url = KuaidiUtils.URL_KD100;

        // "https://zhuanlan.zhihu.com/hulaoshi";
        String param = null;
        // 也可以自己写个Servlet测试是否接收到参数
        // url = "http://localhost:8080/TestJavaWeb/AHServlet";
        // param = "uname=tiger";
        // ----------------------------
        param = "cb=jQuery110205216993417576254_1555296096417&appid=4001&vcode=&com=youzhengguonei&nu=9897470864967&token=&_=1555296096639";
        String s;
        // 发送GET请求
        s = 备份GetPostPageUtil.sendGet(url, param);
        write("http_get.html", s);
        System.out.println("-----------------------------------------------");
        // 发送POST请求
        s = 备份GetPostPageUtil.sendPost(url, param);
        write("http_post.html", s);
    }
    private static HttpURLConnection getHttpURLConnection(URL realUrl) {
        StringBuilder sb = new StringBuilder();
        sb.append("Mozilla/5.0 (Windows NT 10.0; Win64; x64)");
        sb.append(" AppleWrbKit/537.36(KHTML, like Gecko)");
        sb.append(" Chrome/72.0.3626.119 Safari/537.36");
        HttpURLConnection conn = null;
        try {
            // 打开和URL之间的连接
            conn = (HttpURLConnection) realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", sb.toString());
            conn.setRequestProperty("Cookie", "BAIDUID=B8ECE33041E289F9ABE1F5E2A5F85597:FG=1; BIDUPSID=B8ECE33041E289F9ABE1F5E2A5F85597; PSTM=1554963132; H_PS_PSSID=1437_21095_28771_28720_28557_28835_28585_28519_28625_28605; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598; delPer=0; PSINO=7");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return conn;
    }
    private static String getResponse(HttpURLConnection conn) {
        // 读取URL的响应
        String result = "";
        try (InputStream is = conn.getInputStream();
             InputStreamReader isr = new InputStreamReader(is, "utf-8");
             BufferedReader in = new BufferedReader(isr)) {
            String line;
            while ((line = in.readLine()) != null) {
                result += "\n" + line;
            }
        } catch (Exception e) {
            System.out.println("Err:getResponse()");
            e.printStackTrace();
        } finally {
            conn.disconnect();
        }
        System.out.println("getResponse()：" + result.length());
        return result;
    }
    private static void printHeader(HttpURLConnection conn) {
        System.out.println("---↓↓↓响应头字段---");
        Map<String, List<String>> map = conn.getHeaderFields();
        for (String key : map.keySet()) {
            System.out.println(key + "--->" + map.get(key));
        }
        System.out.println("---↑↑↑响应头字段---");
    }
    private static void write(String fileName, String text) {
        File f = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(f);
             OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
             BufferedWriter bw = new BufferedWriter(osw);) {
            bw.write(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}