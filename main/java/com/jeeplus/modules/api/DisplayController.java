package com.jeeplus.modules.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.modules.agreement.entity.Agreement;
import com.jeeplus.modules.agreement.service.AgreementService;
import com.jeeplus.modules.banner.entity.Banner;
import com.jeeplus.modules.banner.service.BannerService;
import com.jeeplus.modules.college.entity.College;
import com.jeeplus.modules.college.service.CollegeService;
import com.jeeplus.modules.faq.entity.Faq;
import com.jeeplus.modules.faq.service.FaqService;
import com.jeeplus.modules.notice.entity.Notice;
import com.jeeplus.modules.notice.service.NoticeService;
import com.jeeplus.modules.product.entity.Product;
import com.jeeplus.modules.product.service.ProductService;
import com.jeeplus.modules.tsupport.entity.TSupport;
import com.jeeplus.modules.tsupport.service.TSupportService;

/**
 * 富文本内容展示Controller
 * 
 * @author mall
 * @version 2018-08-09
 */
@Controller
@RequestMapping(value = "/display")
public class DisplayController extends BaseController {
	@Autowired
	private CollegeService collegeService;
	@Autowired
	private AgreementService agreementService;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private FaqService faqService;
	@Autowired
	private ProductService productService;
	@Autowired
	private TSupportService tSupportService;
	@Autowired
	private BannerService bannerService;
	
	/**
	 * 轮播图富文本
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/banner")
	public String banner(HttpServletRequest request, String id) {
		String content = "";
		Banner banner = bannerService.get(id);
		if(banner != null) {
			content = banner.getContent();
		}
		request.setAttribute("content", content);
		return "modules/app/displayContent";
	}
	
	/**
	 * 扶贫信息
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/support")
	public String support(HttpServletRequest request, String id) {
		String content = "";
		TSupport support = tSupportService.get(id);
		if(support != null) {
			content = support.getContent();
		}
		request.setAttribute("content", content);
		return "modules/app/displayContent";
	}
	
	/**
	 * 商品详情
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/product")
	public String product(HttpServletRequest request, String id) {
		String content = "";
		Product product = productService.get(id);
		if(product != null) {
			content = product.getContent();
		}
		request.setAttribute("content", content);
		return "modules/app/displayContent";
	}
	
	/**
	 * 常见问题
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/faq")
	public String faq(HttpServletRequest request, String id) {
		String content = "";
		String title = "";
		Faq faq = faqService.get(id);
		if(faq != null) {
			content = faq.getContent();
			title = faq.getTitle();
		}
		request.setAttribute("content", content);
		request.setAttribute("title", title);
		return "modules/app/displayContent";
	}
	
	/**
	 * 平台公告
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/notice")
	public String notice(HttpServletRequest request, String id) {
		String content = "";
		String title = "";
		Notice notice = noticeService.get(id);
		if(notice != null) {
			content = notice.getContent();
			title = notice.getTitle();
		}
		request.setAttribute("content", content);
		request.setAttribute("title", title);
		return "modules/app/displayContent";
	}
	
	/**
	 * 我惠学院
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/college")
	public String college(HttpServletRequest request, String id) {
		String content = "";
		String title = "";
		College college = collegeService.get(id);
		if(college != null) {
			content = college.getContent();
			title = college.getTitle();
		}
		request.setAttribute("content", content);
		request.setAttribute("title", title);
		return "modules/app/displayContent";
	}
	
	/**
	 * 协议
	 * 
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("/agreement")
	public String agreement(HttpServletRequest request, String id) {
		String content = "";
		String title = "";
		Agreement agreement = agreementService.get(id);
		if(agreement != null) {
			content = agreement.getContent();
			title = agreement.getTitle();
		}
		request.setAttribute("content", content);
		request.setAttribute("title", title);
		return "modules/app/displayContent";
	}
}
