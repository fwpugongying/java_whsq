package com.jeeplus.modules.api;

public class CacheUtil {
	/**
	 * APP缓存
	 */
	public static final String APP_CACHE = "appCache";
	/**
	 * 入口图列表缓存
	 */
	public static final String CACHE_ACCESS_LIST = "accessList";
	/**
	 * 首页推荐商品列表缓存
	 */
	public static final String CACHE_RECOMMEND_LIST = "recommentList";
	/**
	 * 按钮列表缓存
	 */
	public static final String CACHE_BUTTON_LIST = "buttonList";
	/**
	 * 淘客分类
	 */
	public static final String CACHE_TB_CATEGORY_LIST = "tbCategoryList";
}
