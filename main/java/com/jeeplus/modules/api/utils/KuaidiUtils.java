package com.jeeplus.modules.api.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class KuaidiUtils {
    private static Logger logger = LoggerFactory.getLogger(KuaidiUtils.class);
     /*
     * 定义订单查询方法
     */
     public static final String  getExpressList = "/getExpressList";
     /**
      * 定义处理菜鸟的auth_key的地址
      */
     public static final String  URL = "https://www.so.com/s?ie=utf-8&src=360se7_addr&q=%E8%8F%9C%E9%B8%9F%E8%A3%B9%E8%A3%B9";
     //快递100
     public static final String  URL_KD100="https://sp0.baidu.com/9_Q4sjW91Qh3otqbppnN2DJv/pae/channel/data/asyncqury";
    /**
     * 发送模拟请求
     * @return
     */
    public static String send() {
        StringBuilder apiUrl = new StringBuilder();
        StringBuilder sb = new StringBuilder();
        JSONObject jsonObject = new JSONObject();//数据接收对象


        JSONObject res= new JSONObject();//物流接口返回结果
        String result = "";//返回结果集
        try {
            sb.append("cb=jQuery110205216993417576254_1555296096417&appid=4001&vcode=&"); //参数拼装
            sb.append("com=").append(KuaidiEntity.getExpresssn());
            sb.append("&");
            sb.append("nu=").append(KuaidiEntity.getExpress());
            sb.append("&token=&_=1555296096639");
            result = GetPostPageUtil.sendGet(KuaidiUtils.URL_KD100, sb.toString()); //发送请求
            result=result.replace("\n/**/jQuery110205216993417576254_1555296096417(","");//结果处理
            result=result.substring(0,result.length()-1);
            jsonObject = JSONObject.parseObject(result);
            if(jsonObject.getString("status").equals("0")){
                JSONArray jsonArr=jsonObject.getJSONObject("data").getJSONObject("info").getJSONArray("context");
                JSONArray  array = new JSONArray();//数据返回数组
                for(Object row :jsonArr){
                    JSONObject s = (JSONObject) row;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//时间戳转换
                    StringBuilder gettime=new StringBuilder();
                    long l = Long.parseLong(String.valueOf(s.getString("time")));
                    long timeLong = Long.valueOf(l);
                    String dateTime = sdf.format(new Date(timeLong * 1000L));
                    JSONObject kuaiDi = new JSONObject();//数据转换
                    kuaiDi.put("time",dateTime);
                    kuaiDi.put("step",s.getString("desc"));
                    array.add(kuaiDi);
                }
                res.put("code","0000");//结果返回
                res.put("data",array);
            }else{
                res.put("code","0003");
                res.put("data","快递查询错误!");//结果返回
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.toJSONString(res);
    }

    public static void main(String[] args) {
        new KuaidiEntity("yunda","4601960768664");
        System.out.println(send());
    }
}
