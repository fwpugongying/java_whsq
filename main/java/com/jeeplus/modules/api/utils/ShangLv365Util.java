package com.jeeplus.modules.api.utils;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import org.apache.http.Consts;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;
import com.jeeplus.common.utils.MD5Util;
import com.jeeplus.common.utils.time.DateFormatUtil;

public class ShangLv365Util {
//	public static final String APIKEY = "b421b9a21075a359c09a36db79325d5b";
//	public static final String SECRETKEY = "d3243685d9922998ddd4232f38b53555";
//	public static final String API_URL = "http://apitest.99263.com";
	public static final String APIKEY = "4d2ce2184af0602376d52c124f6195f9";
	public static final String SECRETKEY = "20b4fcbf57727e0eee17d1b61219f2b9";
	public static final String API_URL = "http://api.51jp.cn";

	/**国内机票航班查询*/
	public static final String FlightQuery = "/Flight/Query";
	/**获取所有机场城市*/
	public static final String FlightGetAllAirportCity = "/Flight/GetAllAirportCity";
	/**获取所有航司*/
	public static final String FlightGetAllAirCompany = "/Flight/GetAllAirCompany";
	/**国内机票实时验价*/
	public static final String FlightVerifyCabin = "/Flight/VerifyCabin";
	/**获取实时政策*/
	public static final String FlightGetFlightPolicy = "/Flight/GetFlightPolicy";
	/**获取国内机票下单所需参数*/
	public static final String FlightGetFlightBookPara = "/Flight/GetFlightBookPara";
	/**创建国内机票订单*/
	public static final String FlightCreateOrder = "/Flight/CreateOrder";
	/**获取国内机票订单详情*/
	public static final String FlightGetOrderDetail = "/Flight/GetOrderDetail";
	/**获取国内机票订单列表*/
	public static final String FlightGetOrderList = "/Flight/GetOrderList";
	/**取消国内机票订单*/
	public static final String FlightCancelOrder = "/Flight/CancelOrder";
	/**国内机票申请退票*/
	public static final String FlightApplyRefund = "/Flight/ApplyRefund";
	/**自动支付*/
	public static final String CommonPay = "/Common/Pay";
	
	/**
	 * POST请求
	 * @param url 路径
	 * @param data 参数
	 * @return
	 */
	public static JSONObject doPost(String url, Map<String, Object> data) {
		JSONObject param = new JSONObject();
		param.put("ApiKey", APIKEY);
		String timestamp = DateFormatUtil.DEFAULT_ON_SECOND_FORMAT.format(new Date());
		param.put("Timestamp", timestamp);
		param.put("Sign", MD5Util.md5("apikey" + APIKEY + "secretkey" + SECRETKEY + "timestamp" + timestamp, Consts.UTF_8).toLowerCase());
		param.put("Data", data);
		
		// 创建Httpclient对象
		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		String resultString = "";
		try {
			// 创建Http Post请求
			HttpPost httpPost = new HttpPost(API_URL + url);
			// 设置超时时间
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectionRequestTimeout(10000).setConnectTimeout(60000).build();//设置请求和传输超时时间
			httpPost.setConfig(requestConfig);
			
			// 创建参数列表
			StringEntity entity = new StringEntity(param.toString(), ContentType.APPLICATION_JSON);
			httpPost.setEntity(entity);
			// 执行http请求
			response = httpClient.execute(httpPost);
			resultString = EntityUtils.toString(response.getEntity(), Consts.UTF_8);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return JSONObject.parseObject(resultString);
	}
	
	/**
	 * 验证签名
	 * @param sign 签名
	 * @param timestamp 时间戳
	 * @return
	 */
	public static boolean verifySign(String sign, String timestamp) {
		try {
			return MD5Util.md5(SECRETKEY + timestamp, Consts.UTF_8).equals(sign);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
