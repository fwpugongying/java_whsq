package com.jeeplus.modules.api.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneUtils {
	/**
	 * 手机号码:	13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[1, 6, 7, 8], 18[0-9], 170[0-9], 19[8,9]
	 * /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/
	 */
	private static final String MOBILE_PATTERN = "^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\\d{8}$";
	//	private static final String MOBILE_PATTERN = "^1(3[0-9]|4[57]|5[0-35-9]|66|7[16-8]|8[0-9]|70|9[89])\\d{8}$";
	/**
	 * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1703,1705,1706,198
	 */
	private static final String CHINA_MOBILE_PATTERN = "(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478]|98)\\d{8}$)|(^170[356]\\d{7}$)";
	/**
	 * 联通号段: 130,131,132,155,156,185,186,145,176,1704,1707,1708,1709,171,166
	 */
	private static final String CHINA_UNICOM_PATTERN = "(^1(3[0-2]|4[5]|5[56]|6[6]|7[16]|8[56])\\d{8}$)|(^170[47-9]\\d{7}$)";
	/**
	 * 电信号段: 133,153,180,181,189,177,1700,1701,1702,199,1349
	 */
	private static final String CHINA_TELECOM_PATTERN = "^1((33|53|77|8[019]|99)[0-9]|349|70[0-2])\\d{7}$";
	
	/**
	 * 根据手机号获取运营商编号
	 * @param mobile 手机号
	 * @return 1-移动 2-联通 3-电信
	 */
	public static String getIspCode(String mobile) {
		if (Pattern.matches(CHINA_MOBILE_PATTERN, mobile)) {
			return "1";
		} else if (Pattern.matches(CHINA_UNICOM_PATTERN, mobile)) {
			return "2";
		} else if (Pattern.matches(CHINA_TELECOM_PATTERN, mobile)) {
			return "3";
		}
		return "";
	}
	
	/**
	 * 根据手机号获取运营商名称
	 * @param mobile 手机号
	 * @return 移动 联通 电信
	 */
	public static String getIsp(String mobile) {
		if (Pattern.matches(CHINA_MOBILE_PATTERN, mobile)) {
			return "移动";
		} else if (Pattern.matches(CHINA_UNICOM_PATTERN, mobile)) {
			return "联通";
		} else if (Pattern.matches(CHINA_TELECOM_PATTERN, mobile)) {
			return "电信";
		}
		return "";
	}
	
	/** 
     * 手机号验证 
     *  
     * @param  mobile 手机号
     * @return 验证通过返回true 
     */  
    public static boolean isMobile(String mobile) {  
    	return Pattern.matches(MOBILE_PATTERN, mobile);
    }  
    
	 /**
	  * 电话号码验证
	  * @param str
	  * @return 验证通过返回true
	  */
	 public static boolean isPhone(final String str) {
	   Pattern p1 = null, p2 = null;
	   Matcher m = null;
	   boolean b = false;
	   p1 = Pattern.compile("^[0][1-9]{2,3}-[0-9]{5,10}$"); // 验证带区号的
	   p2 = Pattern.compile("^[1-9]{1}[0-9]{5,8}$");     // 验证没有区号的
	   if (str.length() > 9) {
	     m = p1.matcher(str);
	     b = m.matches();
	   } else {
	     m = p2.matcher(str);
	     b = m.matches();
	   }
	   return b;
	 }
}
