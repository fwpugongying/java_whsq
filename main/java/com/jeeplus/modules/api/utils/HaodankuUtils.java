package com.jeeplus.modules.api.utils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.net.HttpClientUtil;

public class HaodankuUtils {
	
	private static final String apikey = "ws";// 秘钥
	public static final String super_classify = "http://v2.api.haodanku.com/super_classify/apikey/";// 超级分类API
	public static final String column = "http://v2.api.haodanku.com/column/apikey/";// 商品筛选API
	public static final String get_trill_data = "http://v2.api.haodanku.com/get_trill_data/apikey/";// 抖货商品API
	
	public static void main(String[] args) {
		JSONObject result =haodankuAPI(super_classify, "");
		System.out.println(result);
		System.out.println(result.getJSONArray("general_classify").getJSONObject(1).getString("main_name"));
		System.out.println(haodankuAPI(column, "/type/9/back/10/min_id/1/cid/2"));
	}
	
	
	/**
	 * 订单侠接口
	 * @param url API接口路径
	 * @param params 业务参数
	 * @return
	 */
	public static JSONObject haodankuAPI(String url,String params) {
		String	resultString = HttpClientUtil.doGet(url+apikey+params);
		return StringUtils.isNotBlank(resultString) ? JSONObject.parseObject(resultString) : new JSONObject();
	}
	
}
