package com.jeeplus.modules.api.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Formatter;

import org.apache.commons.lang3.StringUtils;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.common.utils.IOUtils;
import com.aliyun.oss.model.GenericResult;
import com.aliyun.oss.model.GetObjectRequest;
import com.aliyun.oss.model.ProcessObjectRequest;


public class AliOSSUtil {
	private static final String endpoint = "oss-cn-beijing.aliyuncs.com";
	private static final String accessKeyId = "LTAI2q9joL0nnMTV";
	private static final String accessKeySecret = "seReO6SrqTUfgTehfEc7LOq5ICTqp4";
	private static final String bucketName = "wohuiyd";
	//public static final String url = "https://wohuiyd.oss-cn-beijing.aliyuncs.com/";
	public static final String url = "https://oss.whsq365.com/";

	public static void upload(File file, String fileName) {
		// 创建OSSClient实例。
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		// 上传文件。<yourLocalFile>由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
		ossClient.putObject(bucketName, fileName, file);
		/*String style = "image/resize,w_100";
		GetObjectRequest request = new GetObjectRequest(bucketName, fileName);
		request.setProcess(style);
		ossClient.getObject(request, new File("example-crop.jpg"));*/
		// 关闭OSSClient。
		ossClient.shutdown();
		edit(fileName);
	}
	
	public static void upload(InputStream inputStream, String fileName) {
		// 创建OSSClient实例。
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		
		// 上传文件流。
		ossClient.putObject(bucketName, fileName, inputStream);
		/*String style = "image/resize,w_100";
		GetObjectRequest request = new GetObjectRequest(bucketName, fileName);
		request.setProcess(style);
		ossClient.getObject(request, new File("example-crop.jpg"));*/
		// 关闭OSSClient。
		ossClient.shutdown();
		edit(fileName);
	}
	
	public static void delete(String fileName) {
		// 创建OSSClient实例。
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		// 删除文件
		ossClient.deleteObject(bucketName, fileName);
		// 关闭OSSClient。
		ossClient.shutdown();
	}
	
	public static void edit(String targetImage) {
		try {
			if (StringUtils.endsWithIgnoreCase(targetImage, ".jpg") || StringUtils.endsWithIgnoreCase(targetImage, ".png") || StringUtils.endsWithIgnoreCase(targetImage, ".jpeg") || StringUtils.endsWithIgnoreCase(targetImage, ".bmp")) {
				OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
				StringBuilder sbStyle = new StringBuilder();
			    Formatter styleFormatter = new Formatter(sbStyle);
			    String styleType = "image/resize,w_800";
			    styleFormatter.format("%s|sys/saveas,o_%s,b_%s", styleType,
			            BinaryUtil.toBase64String(targetImage.getBytes()),
			            BinaryUtil.toBase64String(bucketName.getBytes()));
			    System.out.println(sbStyle.toString());
			    ProcessObjectRequest request = new ProcessObjectRequest(bucketName, targetImage, sbStyle.toString());
			    GenericResult processResult = ossClient.processObject(request);
		    
				String json = IOUtils.readStreamAsString(processResult.getResponse().getContent(), "UTF-8");
				processResult.getResponse().getContent().close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 通过文件名判断并获取OSS服务文件上传时文件的contentType
	 * 
	 * @param fileName
	 *            文件名
	 * @return 文件的contentType
	 */
	public static final String getContentType(String fileName) {
		String fileExtension = fileName.substring(fileName.lastIndexOf("."));
		if ("bmp".equalsIgnoreCase(fileExtension))
			return "image/bmp";
		if ("gif".equalsIgnoreCase(fileExtension))
			return "image/gif";
		if ("jpeg".equalsIgnoreCase(fileExtension) || "jpg".equalsIgnoreCase(fileExtension)
				|| "png".equalsIgnoreCase(fileExtension))
			return "image/jpeg";
		if ("html".equalsIgnoreCase(fileExtension))
			return "text/html";
		if ("txt".equalsIgnoreCase(fileExtension))
			return "text/plain";
		if ("vsd".equalsIgnoreCase(fileExtension))
			return "application/vnd.visio";
		if ("ppt".equalsIgnoreCase(fileExtension) || "pptx".equalsIgnoreCase(fileExtension))
			return "application/vnd.ms-powerpoint";
		if ("doc".equalsIgnoreCase(fileExtension) || "docx".equalsIgnoreCase(fileExtension))
			return "application/msword";
		if ("xml".equalsIgnoreCase(fileExtension))
			return "text/xml";
		return "text/html";
	}
}
