package com.jeeplus.modules.api.utils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.net.HttpClientUtil;

public class DingdanxiaUtils {
	
	private static final String APIKEY = "OTdgRBnEQFpEH44v021b3ai30aL283YN";// 秘钥
	private static final String TOKEN = "3ebc5294bffba6441384533ed8ae7311";//根据apikey进行算法加密得出，不可更改
	public static final String topURL = "http://api.tbk.dingdanxia.com/spk/top";// 淘宝客top100商品人气榜
	public static final String qiangURL = "http://api.tbk.dingdanxia.com/spk/qiang";// 限时整点抢购api接口,最近三天时间段的抢购商品
	public static final String jiukuaijiuURL = "http://api.tbk.dingdanxia.com/spk/jiukuaijiu";// 9.9元包邮/九块九包邮api接口
	public static final String super_search = "http://api.tbk.dingdanxia.com/tbk/super_search";// 淘宝联盟商品库（双11定金预售商品API）-超级搜索api接口
	public static final String goodcopy = "http://api.tbk.dingdanxia.com/spk/goodcopy";// 淘宝客精选文案api接口
	public static final String cate = "http://api.tbk.dingdanxia.com/spk/cate";// 商品多级分类-带图标api接口
	public static final String optimus = "http://api.tbk.dingdanxia.com/spk/optimus";// 淘宝客官方推荐商品库大全API
	public static final String good_info = "http://api.tbk.dingdanxia.com/shop/good_info";// 淘宝全网商品详情v2.0版
	public static final String product_get = "http://api.tbk.dingdanxia.com/shop/product_get";// 淘宝全网商品详情api接口-高级电商
	public static final String item_info = "http://api.tbk.dingdanxia.com/tbk/item_info";// 淘宝客商品详情api接口-简版
	public static final String wdetail = "http://api.tbk.dingdanxia.com/shop/wdetail";// 淘宝商品详情api接口-标准版
	public static final String id_privilege = "http://api.tbk.dingdanxia.com/tbk/id_privilege";// 淘宝客高佣转链api（商品id版）(如果您的淘宝联盟账号是初级，请升级到高级后再调用高佣转链接口，否则转链后依然是低佣金，如需快速代做升级请联系客服。)
	
	public static void main(String[] args) {
		Map<String, String> params = Maps.newHashMap();
		//淘宝客top100商品人气榜
		/*params.put("sale_type", "1");
		params.put("cid", "0");
		params.put("min_id", "2");
		params.put("back", "1");
		JSONObject result = DingdanxiaUtils.dingdanxiaAPI(topURL,params);
		System.out.println(result.toString());
		String code= JSONObject.parseObject(result.toString()).get("code").toString();
		System.out.println("code:"+code);
		JSONArray products = result.getJSONArray("data");
		JSONObject obj = products.getJSONObject(0);
		System.out.println(obj.get("discount"))*/
		
		//限时整点抢购api接口,最近三天时间段的抢购商品
		/*params.put("hour_type", "8");
		params.put("min_id", "1");
		JSONObject result = DingdanxiaUtils.dingdanxiaAPI(qiangURL,params);
		System.out.println(result.toString());
		String code= JSONObject.parseObject(result.toString()).get("code").toString();
		System.out.println("code:"+code);
		JSONArray products = result.getJSONObject("data").getJSONArray("data");
		JSONObject obj = products.getJSONObject(0);
		System.out.println(obj.get("itemendprice"));*/
		
		//限时整点抢购api接口,最近三天时间段的抢购商品
		/*params.put("nav", "3");
		params.put("page_size", "2");
		params.put("min_id", "1");
		JSONObject result = DingdanxiaUtils.dingdanxiaAPI(jiukuaijiuURL,params);
		System.out.println(result.toString());
		String code= JSONObject.parseObject(result.toString()).get("code").toString();
		System.out.println("code:"+code);
		JSONArray products = result.getJSONObject("data").getJSONArray("data");
		JSONObject obj = products.getJSONObject(0);
		System.out.println(obj.get("itemendprice"));*/
		params.put("num_iids", "554427772541");
		JSONObject result = DingdanxiaUtils.dingdanxiaAPI(item_info,params);
		System.out.println(result.toString());
		System.out.println(result.getJSONArray("data").get(0));
	}
	
	
	/**
	 * 订单侠接口
	 * @param url API接口路径
	 * @param params 业务参数
	 * @return
	 */
	public static JSONObject dingdanxiaAPI(String url,Map<String, String> params) {
		params.put("apikey", APIKEY);// 接口秘钥
		params.put("signature", getSign(params));//接口签名，（可在后台开启或关闭，关闭状态可不需要此参数）
		String resultString = HttpClientUtil.doPost(url, params);
		return StringUtils.isNotBlank(resultString) ? JSONObject.parseObject(resultString) : new JSONObject();
	}
	
	/**
	 * 订单侠专用生成签名
	 * @param map
	 * @return
	 */
	public static String getSign(Map<String, String> map) {
		String result = "";
		try {
			TreeMap<String, String> treeMap = new TreeMap<>(map);

			// 构造签名键值对的格式
			StringBuilder sb = new StringBuilder();
			Iterator<Entry<String, String>> iterator = treeMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, String> entry = iterator.next();
				String key = entry.getKey();
				String val = URLEncoder.encode(entry.getValue(), "utf8");
				sb.append(key).append("=").append(val).append("&");
			}
			result = sb.append("token=").append(TOKEN).toString();
			System.out.println("sb:" + result);
			// 进行MD5加密
			result = DigestUtils.md5Hex(result);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
