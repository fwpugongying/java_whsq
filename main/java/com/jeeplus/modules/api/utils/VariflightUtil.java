package com.jeeplus.modules.api.utils;

import com.jeeplus.common.utils.MD5Util;
import com.jeeplus.common.utils.net.HttpClientUtil;
/**
 * 飞常准航班动态查询工具类
 * @author Administrator
 *
 */
public class VariflightUtil {
	private static final String appid = "10729";
	private static final String appsecurity = "5d09ee8a10720";
	private static final String url = "http://open-al.variflight.com/api/flight?";
	
	/**
	 * 按航班号+日期方式查询
	 * @param fnum 航班号
	 * @param date 日期
	 * @return
	 */
	public static String flightByNum(String fnum, String date) {
		StringBuilder sb = new StringBuilder();
		sb.append("appid=").append(appid);
		sb.append("&date=").append(date);
		sb.append("&fnum=").append(fnum);
		String token = sb.toString() + appsecurity;
		sb.append("&token=").append(MD5Util.md5(MD5Util.md5(token)));
		
		return HttpClientUtil.doGet(url + sb.toString());
	}
	
	/**
	 * 按航段（出发到达机场三字码）+日期方式查询
	 * @param depcity 出发城市三字码
	 * @param arrcity 到达城市三字码
	 * @param date 日期
	 * @return
	 */
	public static String flightByCity(String depcity, String arrcity, String date) {
		StringBuilder sb = new StringBuilder();
		sb.append("appid=").append(appid);
		sb.append("&arrcity=").append(arrcity);
		sb.append("&date=").append(date);
		sb.append("&depcity=").append(depcity);
		String token = sb.toString() + appsecurity;
		sb.append("&token=").append(MD5Util.md5(MD5Util.md5(token)));
		
		return HttpClientUtil.doGet(url + sb.toString());
	}
}
