package com.jeeplus.modules.api.utils;

public class WohuiEntity {
	private String name;
	private String value;
	private boolean flag;

	public WohuiEntity() {
		this.flag = true;
	}

	public WohuiEntity(String name, String value, boolean flag) {
		super();
		this.name = name;
		this.value = value;
		this.flag = flag;
	}

	public WohuiEntity(String name, String value) {
		super();
		this.name = name;
		this.value = value;
		this.flag = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}
