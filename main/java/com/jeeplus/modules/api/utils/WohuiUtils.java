package com.jeeplus.modules.api.utils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.common.utils.MD5Util;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.net.HttpClientUtil;

public class WohuiUtils {
	private static Logger logger = LoggerFactory.getLogger(WohuiUtils.class);
	/**
	 * 请求地址
	 */
//	private static final String URL = "http://vip.pc.whyd365.com/API/APPAPI.asmx";
	/**
	 * 请求地址（不需签名版）
	 */
	private static final String URL = "http://39.103.238.207:8001/API/APPAPI.asmx";
	private static final String KEY = "SK@wohui.com-!@#123";

	/* 签到列表 -大健康
	 */
	public static final String BHSignList = "/BHSignList";
	/* 签到 -大健康
	 */
	public static final String BHSignAdd = "/BHSignAdd";
	/* 是否已签到-大健康
	 */
	public static final String BHIsSigned = "/BHIsSigned";
	/* 云店铺-开通
	 */
	public static final String CFMOpen = "/CFMOpen";
	/* 云店铺产品-列表
	 */
	public static final String CFMGoodsList = "/CFMGoodsList";
	/* 云店铺产品-添加
	 */
	public static final String CFMGoodsAdd = "/CFMGoodsAdd";
	/* 云店铺产品-删除
	 */
	public static final String CFMGoodsDelete = "/CFMGoodsDelete";
	/* 云店铺产品-置顶
	 */
	public static final String CFMGoodsSetTop = "/CFMGoodsSetTop";
	/* 云店铺产品-排序
	 */
	public static final String CFMGoodsSetSort = "/CFMGoodsSetSort";
	/* 云店铺产品-设置销售标签
	 */
	public static final String CFMGoodsSetTag = "/CFMGoodsSetTag";
	/* 购买产品优惠券(开通VIP) 接口
	 */
	public static final String BHVIPOpen = "/BHVIPOpen";
	/* 惠康可复购数量 -
	 */
	public static final String HKRebuyNum = "/HKRebuyNum";
	/* 惠康区域代理-开通 接口
	 */
	public static final String HKQYAOpen = "/HKQYAOpen";
	/* 消费券-支付接口
	 */
	public static final String XFQPay = "/XFQPay";
	/* 消费券-退款接口
	 */
	public static final String XFQRefund = "/XFQRefund";
	/* 消费券-支付接口
	 */
	public static final String XFQBuy = "/XFQBuy";
	/* 专属券-支付接口
	 */
	public static final String ZSQPay = "/ZSQPay";
	/* 专属券-退款接口
	 */
	public static final String ZSQRefund = "/ZSQRefund";

	/* 全国厂家总店产品列表接口  2021/11/30
	 */
	public static final String NFHGoodsAllList = "/NFHGoodsAllList";
	/* 全国厂家总店产品添加接口  2021/11/30
	 */
	public static final String NFHGoodsAdd = "/NFHGoodsAdd";
	/* 全国厂家总店产品更换接口  2021/11/30
	 */
	public static final String NFHGoodsChange = "/NFHGoodsChange";
	/* 城市合伙人-空开 接口  2021/11/30
	 */
	public static final String CFDMEmptyOpen = "/CFDMEmptyOpen";
	/* 城市合伙人-非空开 接口  2022/3/4
	 */
	public static final String CFDMOpen = "/CFDMOpen";
	/* 城市合伙人-可申请类型列表接口  2021/11/30
	 */
	public static final String NFHCFDMList = "/NFHCFDMList";
	/* 全国厂家总店-可选货数量接口  2021/11/30
	 */
	public static final String NFHCanXHNum = "/NFHCanXHNum";


	/* 根据产品ID获取产品周转中心信息  2021/10/17
	 */
	public static final String GetHKYPConfigByGoodsID = "/GetHKYPConfigByGoodsID";
	/* 产品服务站-开通云店铺接口 2021/9/04
	 */
	public static final String PSSOpenHKYD = "/PSSOpenHKYD";
	/* 云端快递超市-开通云店铺接口 2021/9/04
	 */
	public static final String CESOpenCS = "/CESOpenCS";
	/* 云店铺购物券明细接口 2021/9/04
	 */
	public static final String CESQLogList = "/CESQLogList";
	/* 专属产品(鬼谷回春堂/血德平)购物券明细接口
	 */
	public static final String PSSQLogList = "/PSSQLogList";
	/* 惠康云店分-兑换产品 接口 2021/8/27
	 */
	public static final String HKYDScoreExchangeProduct = "/HKYDScoreExchangeProduct";

	/* 惠康云店分-兑换奖金 接口 2021/8/27
	 */
	public static final String HKYDScoreExchangeMoney = "/HKYDScoreExchangeMoney";

	/* 奖品列表接口-开通 接口 2021/8/6
	 */
	public static final String PrizeList = "/PrizeList";

	/* 奖品领取-发货接口 2021/8/6
	 */
	public static final String PrizeToGetSend = "/PrizeToGetSend";
	/* 奖品领取-回购接口 2021/8/6
	 */
	public static final String PrizeToGetBack = "/PrizeToGetBack";
	/* 奖品签收接口-开通 接口 2021/8/6
	 */
	public static final String PrizeSureSign = "/PrizeSureSign";

	/* 奖品支付接口 2021/8/6
	 */
	public static final String PrizePay = "/PrizePay";
	/* 奖品领取-转免费接口 2021/8/6
	 */
	public static final String PrizeToFree = "/PrizeToFree";


	/* 云店铺开通接口 接口 2021/5/25   云店铺开通接口
	 */
	public static final String CSOpen = "/CSOpen";
	/* 爆品奖励-获取状态接口 2021/5/26  爆品奖励-获取状态接口
	 */
	public static final String BPPrizeGetStatus = "/BPPrizeGetStatus";

	/* 云店铺销售列表接口-云店铺销售列表接口 2021/5/26  云店铺销售列表接口-云店铺销售列表接口
	 */
	public static final String CSSaleLog = "/CSSaleLog";

	/* 平台代理-开通 接口 2021/4/12  平台代理 平台销售红包（平台代理）
	 */
	public static final String PTAOpen = "/PTAOpen";
	/* 版块代理-开通 接口 2021/3/20  版块代理
	 */
	public static final String BKAOpen = "/BKAOpen";
	/*爆品奖励-领取  2021/4/01  爆品奖励-领取
	 */
	public static final String BPPrizeToGet = "/BPPrizeToGet";
	/* 购物币支付接口 2021/3/23
	 */
	public static final String BPQPay = "/BPQPay";
	/* 砍一刀接口-列表接口 2020/12/31  旅游列表接口
	 */
	public static final String TravelList = "/TravelList";
	/* 砍一刀接口-列表接口 2020/12/31  奖品列表-全部
	 */
	public static final String KYDPrizeList = "/KYDPrizeList";
	/* 砍一刀接口-列表接口 2020/12/31  奖品列表-今天
	 */
	public static final String KYDPrizeListToday = "/KYDPrizeListToday";
	/* 砍一刀接口-列表接口 2020/12/31  奖品列表-领取(砍一刀)
	 */
	public static final String KYDPrizeListToGet = "/KYDPrizeListToGet";
	/* 砍一刀接口-列表接口 2020/12/31  砍一刀系统配置信息接口
	 */
	public static final String KYDConfig = "/KYDConfig";
	/* 产品周转中心-开通 接口
	 */
	public static final String PTCOpen = "/PTCOpen";

	/* 赠品支付接口-列表接口 2020/9/20
	 */
	public static final String GiftSurePay = "/GiftSurePay";
	/* 赠品列表接口-列表接口 2020/9/20
	 */
	public static final String GiftList = "/GiftList";
	/* 赠品领取接口-列表接口 2020/9/20
	 */
	public static final String GiftGoGet = "/GiftGoGet";

	/* 赠品签收接口-添加 接口 2020/9/10
	 */
	public static final String GiftSureSign = "/GiftSureSign";

	/* 选获权申请接口-列表接口 2020/9/10
	 */
	public static final String XHQApply = "/XHQApply";
	/* 选获权转让列表-列表接口 2020/9/10
	 */
	public static final String XHQTurnList = "/XHQTurnList";

	/* 会员提现银行卡-添加 接口 2020/9/10
	 */
	public static final String XHQPay = "/XHQPay";
	/* 选获权取消申请接口-列表接口 2020/9/10
	 */
	public static final String XHQCancel = "/XHQCancel";

	/* 选获权转让接口 接口 2020/9/10
	 */
	public static final String XHQTurn = "/XHQTurn";
	/* 单品代理权限转让接口 接口 2022/7/30
	 */
	public static final String PCANumTurn = "/PCANumTurn";


	/* 会员提现银行卡-列表接口 2020/7/8
	 */
	public static final String UserBankList = "/UserBankList";

	/* 会员提现银行卡-添加 接口 2020/7/8
	 */
	public static final String UserBankAdd = "/UserBankAdd";

	/* 会员提现银行卡-删除 接口 2020/7/8
	 */
	public static final String UserBankDelete = "/UserBankDelete";

	/* 注册会员列表接口 2020/7/8
	 */
	public static final String RegCardList = "/RegCardList";

	/* 会员收入列表接口 2020/7/8
	 */
	public static final String CardIncomeList = "/CardIncomeList";

	/* 会员提现 接口 2020/7/8
	 */
	public static final String WithdrawalsAdd = "/WithdrawalsAdd";

	/* 扶贫-贫困户列表
	 */
	public static final String HP_UserList = "/HP_UserList";
	/* 零售云店铺开通接口
	 */
	public static final String RCSOpen = "/RCSOpen";
	/**
	 * 云店铺产品-是否已有产品代理接口
	 */
	public static final String MSMGoodsIsHas = "/MSMGoodsIsHas";
	/**
	 * 会员流量补贴申请接口
	 */
	public static final String LLBTApply = "/LLBTApply";
	/**
	 * 会员流量补贴支付接口
	 */
	public static final String LLBTPay = "/LLBTPay";
	/**
	 * 会员流量补贴取消申请接口
	 */
	public static final String LLBTCancel = "/LLBTCancel";
	/**
	 * 设置联系信息接口
	 */
	public static final String SetContact = "/SetContact";
	/**
	 * 登录
	 */
	public static final String ULogin = "/ULogin";
	/**
	 * 注册
	 */
	public static final String PhoneReg = "/PhoneReg";
	/**
	 * 修改密码
	 */
	public static final String UpdatePwd = "/UpdatePwd";
	/**
	 * 我惠卡支付
	 */
	public static final String WHKPay = "/WHKPay";
	/**
	 * 我惠卡支付-退款
	 */
	public static final String WHKPayRefund = "/WHKPayRefund";
	/**
	 * 代金券支付
	 */
	public static final String DJQPay = "/DJQPay";
	/**
	 * 代金券退款
	 */
	public static final String DJQRefund = "/DJQRefund";
	/**
	 * 获取预成长值
	 */
	public static final String Get3DaySGV = "/Get3DaySGV";
	/**
	 * 根据GUID获取会员所有信息
	 */
	public static final String GetUInfoByGUID = "/GetUInfoByGUID";
	/**
	 * 获取单品代理信息
	 */
	public static final String GetMSM = "/GetMSM";
	/**
	 * 会员注销接口
	 */
	public static final String Logout = "/Logout";
	/**
	 * 获取厂家代理信息
	 */
	public static final String GetSHO = "/GetSHO";
	/**
	 * 获取供应商信息
	 */
	public static final String GetSupplier = "/GetSupplier";
	/**
	 * 批发云店铺代理产品更换
	 */
	public static final String MSMGoodsChange = "/MSMGoodsChange";
	/**
	 * 云店铺产品列表
	 */
	public static final String GetMSMGoodsList = "/GetMSMGoodsList";
	/**
	 *
	 *     批发云店铺产品-列表(去重后)
	 *     去掉重复的产品信息，确保只有一个产品信息。适用于分享我的店铺后展示数据
	 */
	public static final String GetMSMGoodsMyList = "/GetMSMGoodsMyList";
	/**
	 *
	 *     批发云店铺产品-列表(全部)
	 *    展示全部代理产品信息。适用于会员自己查看自己代理的产品信息（包含代理区域）
	 */
	public static final String GetMSMGoodsAllList = "/GetMSMGoodsAllList";
	/**
	 * 批发云店铺申请接口
	 */
	public static final String MSMApply = "/MSMApply";
	/**
	 * 批发云店铺取消申请接口
	 */
	public static final String MSMCancel = "/MSMCancel";
	/**
	 * 批发云店铺开通接口
	 */
	public static final String MSMOpen = "/MSMOpen";
	/**
	 * 单品代理-开通
	 */
	public static final String PCAOpen = "/PCAOpen";

	/**
	 * 单品代理-赠送资格券列表
	 */
	public static final String MSMCouponList = "/MSMCouponList";
	/**
	 * 全国厂家总店-开通
	 */
	public static final String NMGSOpen = "/NMGSOpen";
	/**
	 * 供应商-开通
	 */
	public static final String SupplierOpen = "/SupplierOpen";
	/**
	 * 供应商-回填
	 */
	public static final String SupplierBackfill = "/SupplierBackfill";
	/**
	 * 找回密码
	 */
	public static final String FindPwd = "/FindPwd";
	/**
	 * 通过手机号获取会员信息
	 */
	public static final String GetUInfoByPhone = "/GetUInfoByPhone";
	/**
	 * 修改手机号
	 */
	public static final String EditPhone = "/EditPhone";
	/**
	 * 修改真实姓名
	 */
	public static final String EditRealName = "/EditRealName";
	/**
	 * 更改注册人
	 */
	public static final String ChangeSender = "/ChangeSender";
	/**
	 * 订单-已付款
	 */
	public static final String OrderPay = "/OrderPay";
	/**
	 * 订单-已结算
	 */
	public static final String OrderSettle = "/OrderSettle";
	/**
	 * 订单-已失效
	 */
	public static final String OrderInvalid = "/OrderInvalid";
	/**
	 * 联盟订单-已付款 接口
	 */
	public static final String UnionOrderPay = "/UnionOrderPay";
	/**
	 * 联盟订单-已结算 接口
	 */
	public static final String UnionOrderSettle = "/UnionOrderSettle";
	/**
	 * 联盟订单-已失效 接口
	 */
	public static final String UnionOrderInvalid = "/UnionOrderInvalid";
	/**
	 * 获取单品代理产品信息
	 */
	public static final String GetMSMGoods = "/GetMSMGoods";
	/**
	 * 批发云店铺产品申请接口
	 */
	public static final String MSMGoodsApply = "/MSMGoodsApply";
	/**
	 * 批发云店铺产品-取消申请
	 */
	public static final String MSMGoodsCancel = "/MSMGoodsCancel";
	/**
	 * 批发云店铺产品-开通
	 */
	public static final String MSMGoodsOpen = "/MSMGoodsOpen";
	/**
	 * 云店铺产品-下架 接口
	 */
	public static final String MSMGoodsOff = "/MSMGoodsOff";
	/**
	 * 批发云店铺产品列表接口
	 */
	public static final String GetMSMGoodsDisList = "/GetMSMGoodsDisList";
	/**
	 * 发送请求
	 * @return
	 */
	public static JSONObject send(String method, List<WohuiEntity> list) {
		StringBuilder apiUrl = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		JSONObject jsonObject = new JSONObject();
		jsonObject = JSONObject.parseObject("{\"respCode\":\"0002\",\"respMsg\":\"信息错误-超时！\"}");
		try {
			/*apiUrl.append(URL).append(method).append("?");
			for (WohuiEntity entity : list) {
				apiUrl.append(entity.getName()).append("=").append(entity.getValue()).append("&");
				if (entity.isFlag()) {
					sb.append(entity.getValue());
				}
			}
			apiUrl.append("Sign").append("=").append(getSign(sb.toString()));
			logger.debug("我惠365会员系统请求数据：" + apiUrl.toString());
			String resultString = HttpClientUtil.doGet(apiUrl.toString());
			logger.debug("我惠365会员系统返回数据：" + resultString);*/
			apiUrl.append(URL).append(method);
			Map<String, String> map = new LinkedHashMap<String, String>();
			
			for (WohuiEntity entity : list) {
				map.put(entity.getName(), entity.getValue());
				if (entity.isFlag()) {
					sb.append(entity.getValue());
				}
			}
			map.put("Sign", getSign(sb.toString()));
			logger.debug("我惠365会员系统请求数据：" + apiUrl.toString() + "：" + (map.toString()));
			String resultString = HttpClientUtil.doPost(apiUrl.toString(), map);
			
			logger.debug("我惠365会员系统返回数据：" + resultString);
			if (StringUtils.isNotBlank(resultString)) {
				jsonObject = JSONObject.parseObject(resultString);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("会员数据接口"+method+"错误信息："+e.getMessage());//
		}
		return jsonObject;
	}
	
	/**
	 * 计算签名
	 * @param map
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private static String getSign(String values) {
		return MD5Util.md5(values + KEY, "GB2312").toUpperCase();
	}
	
	public static void main(String[] args) {
		//订单支付Start
//				List<WohuiEntity> list = Lists.newArrayList();
//				list.add(new WohuiEntity("OrderNO", "1176536322893566063"));
//				list.add(new WohuiEntity("CardGUID", "EE11F8933F504BD1B9BF40A04A268D90"));
//				list.add(new WohuiEntity("Province", "河北省"));
//				list.add(new WohuiEntity("City", "邯郸市"));
//				list.add(new WohuiEntity("District", "魏县",false));
//				list.add(new WohuiEntity("GoodsID", "587246122685"));
//				list.add(new WohuiEntity("GYSCode", ""));
//				list.add(new WohuiEntity("Money", "58.80"));
//				list.add(new WohuiEntity("PTFL", "0.61"));
//				list.add(new WohuiEntity("HYFL", "0"));
//				list.add(new WohuiEntity("PayTime", "2020-08-12 21:05:06"));
//				list.add(new WohuiEntity("ImportTime", "2020-08-12 21:05:06"));
//				list.add(new WohuiEntity("IsRefund", "False"));
//				list.add(new WohuiEntity("Source", "ALI "));
//				list.add(new WohuiEntity("Platform", "阿里"));
//				list.add(new WohuiEntity("Area", ""));
//				list.add(new WohuiEntity("TZBT", "0"));
//				list.add(new WohuiEntity("DLBT", "0"));
//				System.out.println(send( OrderPay, list).toString());

		//订单支付end
		//流量补贴
//		List<WohuiEntity> list = Lists.newArrayList();
//		list.add(new WohuiEntity("OrderNO", "LLBT2020050507020001"));
//		list.add(new WohuiEntity("CardGUID", "F3CDF699D0B14C508EBEDF7B0B9C9177"));
//		list.add(new WohuiEntity("GoodsNum", "20"));
//		list.add(new WohuiEntity("Money", "40"));
//		JSONObject object = WohuiUtils.send(WohuiUtils.LLBTApply, list);
//		System.out.println(object);
		//订单结算
//		List<WohuiEntity> list1 = Lists.newArrayList();
//		list1.add(new WohuiEntity("OrderNO", "200512-360705229532350"));
//		list1.add(new WohuiEntity("Money", "21.66"));
//		list1.add(new WohuiEntity("PTFL", "1.51"));
//		System.out.println(send(OrderSettle, list1).toString());
		//订单结算

//				List<WohuiEntity> list = Lists.newArrayList();
//				list.add(new WohuiEntity("Name", "15225518841"));
//				list.add(new WohuiEntity("Pwd", "wohui365"));
//				list.add(new WohuiEntity("IP", "127.0.0.1"));
//				System.out.println(send(ULogin, list).toString());
//		String goodsid="16047177876";
//		String Source="ALI";
//		List<WohuiEntity> xjParam = Lists.newArrayList();//下架
//		xjParam.add(new WohuiEntity("Source",Source));//来源
//		xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
//		JSONObject info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
//		if (!"0000".equals(info.getString("respCode"))) {
//			//下架成功
//			System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
//		}else{
//			System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
//		}
	}
}
