package com.jeeplus.modules.api.utils.Esaipay;

import com.jeeplus.common.config.Global;

public class Esaipay {
	// 正式
	//public static final String URL = "http://api.saiheyi.com/S/";
	// 测试
	public static final String URL = "http://api.saiheyi.com/STest/";
	// 商家编号
	public static final String PARTNERID = "12345";
	// 商家编号
	public static final String SIGNKEY = "testkey";
	// 异同通知地址
	public static final String NOTIFYURL = Global.getConfig("filePath") + "/api/esaipay/callback";
	
}
