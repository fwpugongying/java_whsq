package com.jeeplus.modules.api.utils.Esaipay;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jeeplus.modules.api.utils.Esaipay.Http.HttpUtil;
import com.jeeplus.modules.api.utils.Esaipay.Http.ParameterBuilder;
import com.jeeplus.modules.api.utils.Esaipay.Http.Response;


/**
 * 分销商辅助查询
 * @author Administrator
 *
 */
public class AssistQuery {

	public void test() {
		fund();
		QueryPhoneProperty();
		phoneProducts();
		oilProducts();
		flowProducts();
		gameProducts();
		wecProducts();
		cardProducts();
	}

	/**
	 * 1. 分销商查询余额接口
	 */
	public void fund() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("timestamp", sdf.format(new Date()));
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/fund", params);
		System.out.println(response);
	}

	/**
	 * 2. 话费归属地查询接口
	 */
	public void QueryPhoneProperty() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("phonenumber", "13877421141");
		params.put("timestamp", sdf.format(new Date()));
		// params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/QueryPhoneProperty", params);
		System.out.println(response);
	}

	/**
	 * 3. 话费对接产品查询接口
	 */
	public void phoneProducts() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("timestamp", sdf.format(new Date()));
		//params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/phoneproducts", params);
		System.out.println(response);
	}
	
	/**
	 * 4.	加油卡对接产品查询接口
	 */
	public void oilProducts(){
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/oilproducts", params);
		System.out.println(response);
	}
	
	/**
	 * 5.	流量包对接产品查询接口
	 */
	public void flowProducts() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/flowproducts", params);
		System.out.println(response);
	}
	
	/**
	 * 6.	游戏对接产品查询接口
	 */
	public void gameProducts() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/gameproducts", params);
		System.out.println(response);
	}
	
	/**
	 * 7.	水电煤对接产品查询接口
	 */
	public void wecProducts() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/wecproducts", params);
		System.out.println(response);
	}
	
	/**
	 * 8.	卡密对接产品查询接口
	 */
	public void cardProducts() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://api.saiheyi.com/QTest/cardproducts", params);
		System.out.println(response);
	}
}
