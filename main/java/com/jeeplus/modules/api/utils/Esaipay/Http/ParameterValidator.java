package com.jeeplus.modules.api.utils.Esaipay.Http;

import javax.servlet.http.HttpServletRequest;

import com.jeeplus.modules.api.utils.Esaipay.Esaipay;

public class ParameterValidator {
	private ParameterBuilder pb = new ParameterBuilder();
	private HttpServletRequest request;
	
	public ParameterValidator(HttpServletRequest request) {
		this.request = request;
	}
	
	public void Put(String key) {
		System.out.println(key + "=" +  request.getParameter(key));
		pb.put(key, request.getParameter(key));
	}
	
	public boolean Validate(String signKey) {
		return pb.sign(Esaipay.SIGNKEY).equals(request.getParameter(signKey));
	}
}
