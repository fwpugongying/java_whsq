package com.jeeplus.modules.api.utils.Esaipay;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jeeplus.modules.api.utils.Esaipay.Http.HttpUtil;
import com.jeeplus.modules.api.utils.Esaipay.Http.ParameterBuilder;
import com.jeeplus.modules.api.utils.Esaipay.Http.Response;

/**
 * 异步通知 回调接口测试
 * @author Administrator
 *
 */
public class NotifyCallback {

	public void test(){
		oilRechargeNotify();
		gameRechargeNotify();
		flowRechargeNotify();
		phoneRechargeNotify();
		wecRechargeNotify();
	}

	/**
	 *  油卡充值  异步通知接口  调用测试
	 */
	public static void oilRechargeNotify() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("ordernumber", "12345");
		params.put("esupordernumber", "235234524");
		params.put("oilnumber", "345124563356745");
		params.put("status", "21");
		params.put("custom", "rqwe sd");
		params.put("finishedtime", sdf.format(new Date()));
		params.put("timestamp", sdf.format(new Date()));
		//params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://localhost:8090/NotifyCallbackWeb/Notify/oilrecharge", params);
		System.out.println(response);
	}
	
	/**
	 *  游戏充值  异步通知接口  调用测试
	 */
	public static void gameRechargeNotify() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("ordernumber", "12345");
		params.put("esupordernumber", "235234524");
		//params.put("oilnumber", "345124563356745");
		params.put("status", "21");
		params.put("custom", "rqwe sd");
		params.put("finishedtime", sdf.format(new Date()));
		params.put("timestamp", sdf.format(new Date()));
		//params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://localhost:8090/NotifyCallbackWeb/Notify/gamerecharge", params);
		System.out.println(response);
	}

	/**
	 *  游戏充值  异步通知接口  调用测试
	 */
	public static void flowRechargeNotify() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("ordernumber", "12345");
		params.put("esupordernumber", "235234524");
		//params.put("oilnumber", "345124563356745");
		params.put("status", "21");
		params.put("custom", "rqwe sd");
		params.put("finishedtime", sdf.format(new Date()));
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://localhost:8090/NotifyCallbackWeb/Notify/flowrecharge", params);
		System.out.println(response);
	}

	/**
	 *  话费充值  异步通知接口  调用测试
	 */
	public static void phoneRechargeNotify() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("ordernumber", "12345");
		params.put("esupordernumber", "235234524");
		//params.put("oilnumber", "345124563356745");
		params.put("status", "21");
		params.put("custom", "rqwe sd");
		params.put("finishedtime", sdf.format(new Date()));
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://localhost:8090/NotifyCallbackWeb/Notify/phonerecharge", params);
		System.out.println(response);
	}
	
	/**
	 *  话费充值  异步通知接口  调用测试
	 */
	public static void wecRechargeNotify() {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", "12345");
		params.put("ordernumber", "12345");
		params.put("esupordernumber", "235234524");
		//params.put("oilnumber", "345124563356745");
		params.put("status", "21");
		params.put("custom", "rqwe sd");
		params.put("finishedtime", sdf.format(new Date()));
		params.put("timestamp", sdf.format(new Date()));
		//params.put("format", "json");
		params.put("sign", params.sign("testkey"));
		Response response = HttpUtil.DoPost("http://localhost:8090/NotifyCallbackWeb/Notify/wecrecharge", params);
		System.out.println(response);
	}
}
