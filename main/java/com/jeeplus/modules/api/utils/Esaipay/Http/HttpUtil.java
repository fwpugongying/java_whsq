package com.jeeplus.modules.api.utils.Esaipay.Http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpUtil {
	private static int defaultTimeout = 10000;
	
	
	
	public static Response DoGet(String urlStr) {
		return DoGet(urlStr, defaultTimeout, null);
	}

	public static Response DoGet(String urlStr, int timeout) {
		return DoGet(urlStr, timeout, null);
	}

	public static Response DoGet(String urlStr, int timeout,  String charsetName) {
		System.out.println(String.format("request:%s",  urlStr));
		if (charsetName == null || charsetName.trim().equals("")) {
			charsetName = "utf-8";
		}
		StringBuilder sb = new StringBuilder();
		int code = 0;
		String responseMsg = "";
		HttpURLConnection connection = null;

		try {
			URL url = new URL(urlStr);
			connection = (HttpURLConnection) url.openConnection();
			// 设置是否向HttpURLConnection输出
			connection.setDoOutput(false);
			// 设置是否从httpUrlConnection读入
			connection.setDoInput(true);
			connection.setRequestMethod("GET");
			// 设置是否使用缓存
			connection.setUseCaches(false);
			// 设置此 HttpURLConnection 实例是否应该自动执行 HTTP 重定向
			connection.setInstanceFollowRedirects(true);

			connection.setConnectTimeout(timeout);
			connection.connect();
			code = connection.getResponseCode();
			responseMsg = connection.getResponseMessage();
			switch (code) {
			case 200:
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(connection.getInputStream(), charsetName));

				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line).append("\r\n");
				}
				reader.close();
				break;

			default:

				break;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return new Response(code, responseMsg, sb.toString());
	}
	
	
	
	public static Response DoPost(String urlStr, ParameterBuilder params) {
		return DoPost(urlStr, params, defaultTimeout, null);
	}
	
	public static Response DoPost(String urlStr, ParameterBuilder params, int timeout) {
		return DoPost(urlStr, params, timeout, null);
	}

	public static Response DoPost(String urlStr, ParameterBuilder params, int timeout, String charsetName) {
		System.out.println(String.format("request:%s",  urlStr));
		if (charsetName == null || charsetName.trim().equals("")) {
			charsetName = "utf-8";
		}
		
		//params.setCharsetName(charsetName);
		StringBuilder sb = new StringBuilder();
		int code = 0;
		String responseMsg = "";
		HttpURLConnection connection = null;

		try {
			URL url = new URL(urlStr);
			connection = (HttpURLConnection) url.openConnection();
			// 设置是否向HttpURLConnection输出
			connection.setDoOutput(true);
			// 设置是否从httpUrlConnection读入
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			// 设置是否使用缓存
			connection.setUseCaches(false);
			// 设置此 HttpURLConnection 实例是否应该自动执行 HTTP 重定向
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Accept-Charset", charsetName);
			// 设置使用标准编码格式编码参数的名-值对
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charsetName);
			connection.setConnectTimeout(timeout);
			connection.connect();

			DataOutputStream out = new DataOutputStream(connection.getOutputStream());// 打开输出流往对端服务器写数据  
	        out.writeBytes(params.toString());// 写数据,也就是提交你的表单 name=xxx&pwd=xxx  
			//OutputStream out = connection.getOutputStream();
			//out.write(params.getBytes());
			out.flush();
			out.close();

			code = connection.getResponseCode();
			responseMsg = connection.getResponseMessage();

			switch (code) {
			case 200:
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(connection.getInputStream(), charsetName));

				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line).append("\r\n");
				}
				reader.close();
				break;

			default:

				break;
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return new Response(code, responseMsg, sb.toString());
	}
}
