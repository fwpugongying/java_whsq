package com.jeeplus.modules.api.utils.Esaipay;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.jeeplus.modules.api.utils.Esaipay.Http.HttpUtil;
import com.jeeplus.modules.api.utils.Esaipay.Http.ParameterBuilder;
import com.jeeplus.modules.api.utils.Esaipay.Http.Response;

/**
 * 话费充值分销商接入
 * @author Administrator
 *
 */
public class PhoneRecharge {

	/**
	 * 1.	充值接口
	 * @param ordernumber 订单号
	 * @param phonenumber 手机号
	 * @param coid 运营商 1-移动 2-联通 3-电信
	 * @param value 充值金额 正整数 单位元
	 * @return
	 */
	public String recharge(String ordernumber, String phonenumber, String coid, String value) {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		ParameterBuilder params = new ParameterBuilder();
		params.put("partnerid", Esaipay.PARTNERID);
		params.put("ordernumber", ordernumber);
		params.put("phonenumber", phonenumber);
		params.put("pid", "0");
		params.put("cid", "0");
		params.put("coid", coid);
		params.put("value", value);
		params.put("timelimit", "600");
		params.put("timestamp", sdf.format(new Date()));
		params.put("notifyurl", Esaipay.NOTIFYURL);
		//params.put("custom", "custom info");
		params.put("format", "json");
		params.put("sign", params.sign(Esaipay.SIGNKEY));
		Response response = HttpUtil.DoPost(Esaipay.URL + "recharge", params);
		System.out.println(response);
		return response.getData();
	}
	
	/**
	 * 2.	充值结果查询接口（单条）
	 * @param ordernumber 订单号
	 * @param date 订单时间
	 * @return
	 */
	public String querySingle(String ordernumber, Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
		SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd");
		ParameterBuilder params = new ParameterBuilder();		
		params.put("partnerid", Esaipay.PARTNERID);
		params.put("ordernumber", ordernumber);
		params.put("ordertime", df.format(date));
		params.put("timestamp", sdf.format(new Date()));
		params.put("format", "json");
		params.put("sign", params.sign(Esaipay.SIGNKEY));
		Response response = HttpUtil.DoPost(Esaipay.URL + "querysingle", params);
		System.out.println(response);
		return response.getData();
	}
	
	
}
