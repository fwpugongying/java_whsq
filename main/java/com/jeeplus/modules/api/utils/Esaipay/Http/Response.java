package com.jeeplus.modules.api.utils.Esaipay.Http;

public class Response {
	private int Code;

	public int getCode() {
		return Code;
	}
	
	private String Data;
	

	public String getData() {
		return Data;
	}

	private String ResponseMsg;

	public String getResponseMsg() {
		return ResponseMsg;
	}

	public  Response(int code,String responseMsg, String data) {
		this.Code = code;
		this.ResponseMsg = responseMsg;
		this.Data = data;
	}
	
	public String toString() {
		return String.format("Code:\t\t%s;%nResponseMsg:\t%s;%nData:\t\t%s", this.Code, this.ResponseMsg, this.Data);
	}
}
