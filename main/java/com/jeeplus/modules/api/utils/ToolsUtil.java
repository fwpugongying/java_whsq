package com.jeeplus.modules.api.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Maps;
import com.google.gson.JsonObject;
import com.jeeplus.common.utils.MD5Util;
import org.apache.commons.lang3.RandomStringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class ToolsUtil {
    //快递100
    public static final String  URL="https://wuliu.market.alicloudapi.com/kdi";
    //签名
    public static final String  KEY="MWpGXhCygkCky7%4";
    //快递查询
    public static String send() {
        StringBuilder apiUrl = new StringBuilder();
        StringBuilder sb = new StringBuilder();
        JSONObject jsonObject = new JSONObject();//数据接收对象
        JSONObject res= new JSONObject();//物流接口返回结果
        String result = "";//返回结果集
        try {
            sb.append("no=").append(KuaidiEntity.getExpresssn());
            sb.append("&");
            sb.append("type=").append(KuaidiEntity.getExpress());
            result = GetPostPageUtil.sendGet(ToolsUtil.URL, sb.toString()); //发送请求
            jsonObject = JSONObject.parseObject(result);
            if(jsonObject.getString("status").equals("0")){
                JSONArray jsonArr=jsonObject.getJSONObject("result").getJSONArray("list");
                JSONArray  array = new JSONArray();//数据返回数组
                for(Object row :jsonArr){
                    JSONObject s = (JSONObject) row;
                    JSONObject kuaiDi = new JSONObject();//数据转换
                    kuaiDi.put("time",s.getString("time"));
                    kuaiDi.put("step",s.getString("status"));
                    array.add(kuaiDi);
                }
                res.put("code","0000");//结果返回
                res.put("data",array);
            }else{
                res.put("code","0003");
                res.put("data","快递查询错误!");//结果返回
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.toJSONString(res);
    }
    //获取签名
    private static String getSign(String values) {
        return MD5Util.md5(values + KEY, "UTF-8").toUpperCase();
    }
    //签名验证
    public static boolean VerfySign(String strSign, String strSigned) {
        String Str_Sign=getSign(strSign);
        if(!Str_Sign.equals(strSigned)){
           return false;
        }
        return true;
    }
    public static void main(String[] args) {
//        new KuaidiEntity("780098068058","zto");
//        System.out.println(send());
        //生成指定长度的随机字符串
//        String str= RandomStringUtils.randomAlphanumeric(5);
//        System.out.println(str);


    }

}
