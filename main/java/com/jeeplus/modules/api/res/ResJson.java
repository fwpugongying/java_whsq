package com.jeeplus.modules.api.res;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class ResJson extends HashMap<String, Object> implements Serializable {
	private static final long serialVersionUID = 1L;

	public ResJson() {
		put("result", "1");// 0 成功 1 失败
		put("resultNote", "系统繁忙");
	}

	/**
	 * 总条数
	 * @param totalCount
	 */
	public void setTotalCount(long totalCount) {
		put("totalCount", totalCount);
	}

	/**
	 * 状态 0 成功 1 失败
	 * @param result
	 */
	public void setResult(String result) {
		put("result", result);
	}

	/**
	 * 总页数
	 * @param totalPage
	 */
	public void setTotalPage(int totalPage) {
		put("totalPage", totalPage);
	}

	/**
	 * 提示语
	 * @param resultNote
	 */
	public void setResultNote(String resultNote) {
		put("resultNote", resultNote);
	}

	/**
	 * 数据列表
	 * @param <T>
	 * @param dataList
	 */
	public <T> void setDataList(List<T> dataList) {
		put("dataList", dataList);
	}

}
