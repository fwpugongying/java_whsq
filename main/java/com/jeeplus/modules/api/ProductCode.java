package com.jeeplus.modules.api;

public class ProductCode {
	/**
	 * 机票 固定编码
	 */
	public static final String flight = "CX00001";
	/**
	 * 酒店 固定编码
	 */
	public static final String hotel = "CX00002";
	/**
	 * 火车票 固定编码
	 */
	public static final String train = "CX00003";
	/**
	 * 信用卡 固定编码
	 */
	public static final String card = "CC00001";
	/**
	 * 话费充值 固定编码
	 */
	public static final String recharge = "HF00001";
	/**
	 * 联盟商品（淘客）LM开头+8位数字
	 */
	public static final String alliance = "LM";
	/**
	 * 实体商家优惠券 MD开头+5位数字
	 */
	public static final String coupon = "MD";
	/**
	 * 云店商品 CM开头+5位数字
	 */
	public static final String product = "CM";
	/**
	 * 供应商编码 GYS开头+7位数字
	 */
	public static final String store = "GYS";
	/**
	 * 实体商家编码 SQ开头+7位数字
	 */
	public static final String shop = "SQ";
}
