package com.jeeplus.modules.api;

public class OrderNumPrefix {
	/**
	 * 云店商品订单合并订单号
	 */
	public static final String totalId = "ZO";
	/**
	 * 充值订单号
	 */
	public static final String chongzhi = "CZ";
	/**
	 * 实体店代金券订单号
	 */
	public static final String couponOrder = "CO";
	/**
	 * 拼团订单号
	 */
	public static final String groupOrder = "PT";
	/**
	 * 实体商家入驻订单号
	 */
	public static final String shopOrder = "ST";
	/**
	 * 商圈线下付款订单号
	 */
	public static final String BusinessOrder = "SQ";
	
	/**
	 * 厂家入驻订单号
	 */
	public static final String storeOrder = "SO";
	
	/**
	 * 批发云店铺订单号
	 */
	public static final String proxyOrder = "PO";
	
	/**
	 * 全国总店订单号
	 */
	public static final String nationalOrder = "NO";

	/**
	 * 增选代理产品单号
	 */
	public static final String proxyProductOrder = "PP";
	/**
	 * 流量产品单号
	 */
	public static final String llbt = "LLBT";
	/**
	 * 选货权单号
	 */
	public static final String xhq = "XHQ";
	/**
	 * 赠品权单号
	 */
	public static final String gift = "GIFT";
	/**
	 * 消费券
	 */
	public static final String XFQ = "XFQ";
	/**
	 * VIP
	 */
	public static final String VIP = "VIP";
	/**
	 * 云店版块奖品支付
	 */
	public static final String JP = "JP";
	/**
	 * 开通权购买
	 */
	public static final String KTQ = "KTQ";
	/**
	 * 惠康区域代理开通
	 */
	public static final String HKQY = "HKQY";
	/**
	 * 云店铺 开通
	 */
	public static final String CFM = "CFM";
	
}
