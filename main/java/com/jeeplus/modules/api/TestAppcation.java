package com.jeeplus.modules.api;
import java.util.*;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.utils.SpringContextHolder;
import com.jeeplus.common.utils.log.ThrottledSlf4jLogger;
import com.jeeplus.common.utils.net.HttpClientUtil;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.modules.api.req.ReqJson;
import com.jeeplus.modules.api.res.ResJson;
import com.jeeplus.modules.api.service.ApiService;
import com.jeeplus.modules.api.utils.*;
import com.jeeplus.modules.member.entity.Member;
import com.jeeplus.modules.member.service.MemberService;
import com.jeeplus.modules.taokeorder.entity.TTaokeOrder;
import com.jeeplus.modules.taokeorder.service.TTaokeOrderService;
import com.jeeplus.modules.xhq.entity.Xhq;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping(value = "/api")
public class TestAppcation extends BaseController {
    @Autowired
    private MemberService memberService;
    @Autowired
    private ApiService apiService;
    @Autowired
    private TTaokeOrderService tTaokeOrderService;
    private static final org.apache.shiro.mgt.SecurityManager MANAGER = SpringContextHolder
            .getBean(org.apache.shiro.mgt.SecurityManager.class);

    /**
     * 淘宝
     *
     * @param req
     * @return
     */
    @PostMapping("/taobao")
    @ResponseBody
    public ResJson taobao(@RequestBody ReqJson req)     {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Integer page_no = 1;
        String position_index = "";
        // 开始时间
        Date now = new Date();
        ResJson res = new ResJson();
        if (StringUtils.isBlank(req.getString("startTime"))) {
            res.setResultNote("开始时间");
            return res;
        }
        if (StringUtils.isBlank(req.getString("endTime"))) {
            res.setResultNote("结束时间");
            return res;
        }
        String startTime = req.getString("startTime"); // 10分钟前的时间;
        String endTime = req.getString("endTime");
        // 结束时间
        while (true) {
            Map<String, String> params = Maps.newHashMap();
            params.put("sid", "22375");// 对应的淘客账号授权ID
            try {
                params.put("start_time", URLEncoder.encode(startTime, "utf-8"));// 订单查询开始时间，2019-04-05 12:18:22
                params.put("end_time", URLEncoder.encode(endTime, "utf-8"));// 订单查询结束时间，2019-04-25
                // 15:18:22，目前最大可查3个小时内的数据
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            params.put("page_no", page_no.toString());// 第几页，默认1，1~100
            params.put("page_size", "100");// 页大小，默认20，1~100
//            params.put("member_type", "2");// 推广者角色类型,2:二方，3:三方，不传，表示所有角色
            params.put("order_scene", "2");// 场景订单场景类型，1:常规订单，2:渠道订单，3:会员运营订单，默认为1
            params.put("signurl", "1");// 页大小，默认20，1~100
            if (StringUtils.isNotBlank(position_index)) {
                params.put("position_index", position_index);// 位点，除第一页之外，都需要传递；前端原样返回。注意：从第二页开始，位点必须传递前一页返回的值，否则翻页无效。
                params.put("jump_type", "1");// 跳转类型，当向前或者向后翻页必须提供,-1:
                // 向前翻页,1：向后翻页。
            }
            JSONObject jsonObject = MiaoyouquanUtils.zetaokeAPI(MiaoyouquanUtils.open_dingdanchaxun2, params);
            System.out.println("折淘客返回数据：" + jsonObject.toString());
            JSONObject result = JSONObject.parseObject(HttpClientUtil.doGet(jsonObject.getString("url")));
            if (result != null && StringUtils.isNotBlank(result.getString("tbk_sc_order_details_get_response"))) {
                JSONObject dataObject = result.getJSONObject("tbk_sc_order_details_get_response").getJSONObject("data");
                if (dataObject != null) {
                    JSONObject results = JSONObject.parseObject(dataObject.getString("results"));
                    if (results != null) {
                        JSONArray orderArr = results.getJSONArray("publisher_order_dto");
                        if (orderArr != null) {
                            for (int i = 0; i < orderArr.size(); i++) {
                                JSONObject o = orderArr.getJSONObject(i);
                                TTaokeOrder taokeOrder = tTaokeOrderService.get(o.getString("trade_id"));
                                if(taokeOrder!=null){
                                    if(true){
                                        taokeOrder.setAlipayTotalPrice(o.getString("alipay_total_price"));
                                        taokeOrder.setItemTitle(o.getString("item_title"));
                                        taokeOrder.setItemImg(o.getString("item_img"));
                                        taokeOrder.setItemNum(o.getInteger("item_num"));
                                        taokeOrder.setTradeParentId(o.getString("trade_parent_id"));
                                        taokeOrder.setTkStatus(o.getString("tk_status"));
                                        taokeOrder.setItemPrice(o.getString("item_price"));
                                        taokeOrder.setItemId(o.getString("item_id"));
                                        taokeOrder.setPubSharePreFee(o.getString("pub_share_pre_fee"));
                                        String commission_fee="";
                                        if("饿了么".equals(o.getString("order_type"))){
                                            float   total_commission_fee =   Float.parseFloat(o.getString("total_commission_fee"));
                                            float   subsidy_fee   =   Float.parseFloat(o.getString("subsidy_fee"));
                                            float   zmoney   =     subsidy_fee+total_commission_fee;
                                            commission_fee  =  Float.toString(zmoney);
                                        }else{
                                            commission_fee=o.getString("total_commission_fee");
                                        }
                                        taokeOrder.setTotalCommissionFee(commission_fee);

                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date date = null;
                                        try {
                                            date = simpleDateFormat.parse(o.getString("tk_create_time"));
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        long ts = date.getTime();
                                        Date current= null;
                                        try {
                                            current = simpleDateFormat.parse("2020-06-01 00:00:00");
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        long currents = current.getTime();
                                        //时间转换结束
                                        String fl;
                                        if(ts>=currents){
                                            fl="0.3";
                                        }else{
                                            fl="0.35";
                                        }
                                        tTaokeOrderService.save(taokeOrder);
                                        if(taokeOrder.getMember()!=null && StringUtils.isNotBlank(taokeOrder.getMember().getId())){
                                            if("3".equals(o.getString("tk_status")) && "0".equals(taokeOrder.getIsJs())){
                                                // 用户加铜板
                                                memberService.updatePoint(taokeOrder.getMember(), (new BigDecimal(taokeOrder.getTotalCommissionFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");
                                                //错误日志
                                                logger.error("淘宝结算订单号"+taokeOrder.getId());
                                                // 调用三方会员系统
                                                //JSONObject object = apiService.UnionOrderSettle(taokeOrder.getId(), taokeOrder.getAlipayTotalPrice(), taokeOrder.getTotalCommissionFee());
                                                JSONObject object = apiService.UnionOrderSettle(taokeOrder.getId(), taokeOrder.getAlipayTotalPrice(), (new BigDecimal(taokeOrder.getTotalCommissionFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                                if (!"0000".equals(object.getString("respCode"))) {
                                                    System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                    logger.error("我惠会员系统请求结算订单失败：" + object.getString("respMsg")+"淘宝结算订单号"+taokeOrder.getId());
                                                }
                                                taokeOrder.setIsJs("1");
                                                tTaokeOrderService.save(taokeOrder);
                                            }
                                            if("13".equals(o.getString("tk_status"))){
                                                // 调用三方失效订单
                                                logger.error("淘宝取消订单号"+taokeOrder.getId());
                                                JSONObject object = apiService.UnionOrderInvalid(taokeOrder.getId());
                                                if (!"0000".equals(object.getString("respCode"))) {
                                                    System.out.println("我惠会员系统请求失效订单失败：" + object.getString("respMsg"));
                                                    logger.error("我惠会员系统请求取消订单失败：" + object.getString("respMsg")+"淘宝取消订单号"+taokeOrder.getId());
                                                }
                                            }
                                        }
                                    }
                                }else{//插入一条数据
                                    taokeOrder = new TTaokeOrder();
                                    taokeOrder.setIsNewRecord(true);
                                    taokeOrder.setType("0");
                                    System.out.println("插入淘宝订单：" + o.getString("trade_id"));
                                    taokeOrder.setId(o.getString("trade_id"));
                                    // 根据relation_id查询用户
                                    Member member = memberService.findUniqueByProperty("relation_id",
                                            o.getString("relation_id"));
                                    if (member != null) {
                                        taokeOrder.setMember(member);
                                    }
                                    taokeOrder.setAlipayTotalPrice(o.getString("alipay_total_price"));
                                    taokeOrder.setItemTitle(o.getString("item_title"));
                                    taokeOrder.setItemImg(o.getString("item_img"));
                                    taokeOrder.setItemNum(o.getInteger("item_num"));
                                    taokeOrder.setTradeParentId(o.getString("trade_parent_id"));
                                    taokeOrder.setTkStatus(o.getString("tk_status"));
                                    taokeOrder.setItemPrice(o.getString("item_price"));
                                    taokeOrder.setItemId(o.getString("item_id"));
                                    //类型end
                                    String commission_fee="";
                                    if("饿了么".equals(o.getString("order_type"))){
                                        float   total_commission_fee =   Float.parseFloat(o.getString("total_commission_fee"));
                                        float   subsidy_fee   =   Float.parseFloat(o.getString("subsidy_fee"));
                                        float   zmoney   =     subsidy_fee+total_commission_fee;
                                        commission_fee  =  Float.toString(zmoney);
                                    }else{
                                        commission_fee=o.getString("total_commission_fee");
                                    }
                                    taokeOrder.setTotalCommissionFee(commission_fee);
                                    taokeOrder.setPubSharePreFee(o.getString("pub_share_pre_fee"));
                                    taokeOrder.setIsJs("0");
                                    try {
                                        taokeOrder.setTbPaidTime(StringUtils.isNotBlank(o.getString("tb_paid_time"))
                                                ? sdf.parse(o.getString("tb_paid_time")) : null);
                                        taokeOrder.setTkCreateTime(StringUtils.isNotBlank(o.getString("tk_create_time"))
                                                ? sdf.parse(o.getString("tk_create_time")) : null);
                                        //时间转换开始
                                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date date = simpleDateFormat.parse(o.getString("tk_create_time"));
                                        long ts = date.getTime();
                                        Date current=simpleDateFormat.parse("2020-06-01 00:00:00");
                                        long currents = current.getTime();
                                        //时间转换结束
                                        String fl;
                                        if(ts>=currents){
                                            fl="0.3";
                                        }else{
                                            fl="0.35";
                                        }
                                        //费率

                                        //类型
                                        String taobaoSource="";
                                        if("饿了么".equals(o.getString("order_type"))){
                                            taobaoSource="ELEME";
                                        }else if("阿里".equals(o.getString("order_type"))){
                                            taobaoSource="ALI";
                                        }else if("淘宝".equals(o.getString("order_type"))){
                                            taobaoSource="TAOBAO";
                                        }else if("天猫".equals(o.getString("order_type"))){
                                            taobaoSource="TMALL";
                                        }else if("天猫国际".equals(o.getString("order_type"))){
                                            taobaoSource="TMALLHK";
                                        }else if("聚划算".equals(o.getString("order_type"))){
                                            taobaoSource="JHS";
                                        }else if("".equals(taobaoSource)){
                                            taobaoSource="ALI";
                                        }
                                        //类型end

                                        tTaokeOrderService.save(taokeOrder);
                                        if(StringUtils.isNotBlank(taokeOrder.getId())){
                                            if(member!=null){
                                                TTaokeOrder to=tTaokeOrderService.get(taokeOrder.getId());
                                                if(to==null){
                                                    continue;
                                                }
                                                if("12".equals(to.getTkStatus())){
                                                    logger.error("淘宝支付订单号"+taokeOrder.getId());
                                                    JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(),  taobaoSource,  taokeOrder.getItemId(), taokeOrder.getItemTitle());
                                                    if (!"0000".equals(object.getString("respCode"))) {
                                                        System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                        logger.error("我惠会员系统请求支付订单失败：" + object.getString("respMsg")+"淘宝支付订单号"+taokeOrder.getId());
                                                    }
                                                }else if("3".equals(to.getTkStatus())){
                                                    logger.error("淘宝支付订单号"+taokeOrder.getId());
                                                    JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(),  taobaoSource, taokeOrder.getItemId(),taokeOrder.getItemTitle());
                                                    if (!"0000".equals(object.getString("respCode"))) {
                                                        System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                        logger.error("我惠会员系统请求支付订单失败：" + object.getString("respMsg")+"淘宝支付订单号"+taokeOrder.getId());
                                                    }
                                                    // 用户加铜板
                                                    memberService.updatePoint(to.getMember(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                                    // 调用三方会员系统
                                                    logger.error("淘宝结算订单号"+taokeOrder.getId());
                                                    object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                                    if (!"0000".equals(object.getString("respCode"))) {
                                                        System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                        logger.error("我惠会员系统请求结算订单失败：" + object.getString("respMsg")+"淘宝结算订单号"+taokeOrder.getId());
                                                    }
                                                    to.setIsJs("1");
                                                    tTaokeOrderService.save(to);
                                                }
                                            }else{
                                                logger.info("申请支付失败会员信息", member,"淘宝订单信息",taokeOrder);//
                                            }
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }
                    boolean has_next = dataObject.getBoolean("has_next");
                    if (has_next) {
                        page_no = page_no + 1;
                        position_index = dataObject.getString("position_index");
                    } else {
                        break;
                    }
                }
            } else {
                break;
            }
        }
        res.setResult("0");
        res.setResultNote("查询成功");
        return res;
    }

    /**
     * 拼多多
     *
     * @param req
     * @return
     */
    @PostMapping("/pdd")
    @ResponseBody
    public ResJson pdd(@RequestBody ReqJson req) {
        Date now = new Date();
        ResJson res = new ResJson();
        if (StringUtils.isBlank(req.getString("startTime"))) {
            res.setResultNote("开始时间");
            return res;
        }
        if (StringUtils.isBlank(req.getString("endTime"))) {
            res.setResultNote("结束时间");
            return res;
        }
        String last_order_id="";
        while (true) {
            Map<String, String> params = Maps.newHashMap();
            params.put("start_time", req.getString("startTime"));// 订单开始时间
            params.put("end_time", req.getString("endTime"));// 订单结束时间
            params.put("last_order_id", last_order_id);// 第几页，默认1，1~100
            params.put("page_size", "300");// 页大小，默认20，1~100
            params.put("query_order_type", "1");// 默认推广订单
            JSONObject jsonObject = PddUtils.duoduorouterAPI(PddUtils.router,PddUtils.getrangeorderlist, params);
            System.out.println("拼多多官方返回数据：" + jsonObject.toString());
            if (jsonObject.containsKey("order_list_get_response")) {
                JSONObject dataObject = jsonObject.getJSONObject("order_list_get_response");
                if (dataObject != null) {
                    JSONArray orderList = dataObject.getJSONArray("order_list");
                    if (orderList != null && orderList.size() > 0) {
                        for (int i = 0; i < orderList.size(); i++) {
                            JSONObject o = orderList.getJSONObject(i);
                            TTaokeOrder to = tTaokeOrderService.get(o.getString("order_sn"));
                            if(to!=null){
                                if(true){
                                    to.setTkStatus(o.getString("order_status"));
                                    to.setPubSharePreFee((o.getDouble("promotion_amount")/100)+"");
                                    to.setTotalCommissionFee((o.getDouble("promotion_amount")/100)+"");
                                    tTaokeOrderService.save(to);
                                    if(to.getMember()!=null && StringUtils.isNotBlank(to.getMember().getId())){

                                        if("5".equals(o.getString("order_status")) && "0".equals(to.getIsJs())){
                                            // 用户加铜板
                                            memberService.updatePoint(to.getMember(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");
                                            // 调用三方会员系统
                                            logger.error("拼多多结算订单号"+to.getId());
                                            //JSONObject object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), to.getTotalCommissionFee());
                                            JSONObject object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                            if (!"0000".equals(object.getString("respCode"))) {
                                                System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                logger.error("我惠会员系统请求结算订单失败：" + object.getString("respMsg")+"拼多多结算订单号"+to.getId());
                                            }
                                            to.setIsJs("1");
                                            tTaokeOrderService.save(to);

                                        }else if("4".equals(o.getString("order_status"))||"10".equals(o.getString("order_status"))){
                                            //取消订单
                                            List<WohuiEntity> list = Lists.newArrayList();
                                            list.add(new WohuiEntity("OrderNO", to.getId()));
                                            logger.error("拼多多取消订单号"+to.getId());
                                            JSONObject object = WohuiUtils.send(WohuiUtils.UnionOrderInvalid, list);
                                            if (!"0000".equals(object.getString("respCode"))) {

                                                System.out.println("取消订单号"+to.getId()+"失败：" + object.getString("respMsg"));
                                                logger.error("取消订单号"+to.getId()+"失败：" + object.getString("respMsg")+"拼多多取消订单号"+to.getId());
                                            }
                                        }
                                    }

                                }
                            }else{
                                TTaokeOrder taokeOrder = new TTaokeOrder();
                                taokeOrder.setIsNewRecord(true);
                                taokeOrder.setType("1");
                                taokeOrder.setId(o.getString("order_sn"));
                                // 根据推广位ID查询用户
                                Member member = memberService.findUniqueByProperty("pdd_id",
                                        o.getString("p_id"));
                                if (member != null) {
                                    taokeOrder.setMember(member);
                                }
                                taokeOrder.setAlipayTotalPrice((o.getDouble("order_amount")/100)+"");
                                taokeOrder.setItemTitle(o.getString("goods_name"));
                                taokeOrder.setItemImg(o.getString("goods_thumbnail_url"));
                                taokeOrder.setItemNum(o.getInteger("goods_quantity"));
                                taokeOrder.setTradeParentId(o.getString("order_sn"));
                                taokeOrder.setTkStatus(o.getString("order_status"));
                                taokeOrder.setItemPrice((o.getDouble("goods_price")/100)+"");
                                taokeOrder.setItemId(o.getString("goods_id"));
                                taokeOrder.setPubSharePreFee((o.getDouble("promotion_amount")/100)+"");
                                taokeOrder.setTotalCommissionFee((o.getDouble("promotion_amount")/100)+"");
                                taokeOrder.setTbPaidTime(StringUtils.isNotBlank(o.getString("order_pay_time"))
                                        ? new Date(o.getLong("order_pay_time")*1000) : null);
                                taokeOrder.setTkCreateTime(StringUtils.isNotBlank(o.getString("order_create_time"))
                                        ? new Date(o.getLong("order_create_time")*1000) : null);
                                taokeOrder.setGoodsSign(o.getString("goods_sign"));
                                taokeOrder.setIsJs("0");
                                tTaokeOrderService.save(taokeOrder);
                                if(StringUtils.isNotBlank(taokeOrder.getId())){
                                    if(taokeOrder.getMember()!=null && StringUtils.isNotBlank(taokeOrder.getMember().getId())){
                                        to=tTaokeOrderService.get(taokeOrder.getId());
                                        if(to==null){
                                            continue;
                                        }
                                        if("0".equals(to.getTkStatus()) || "1".equals(to.getTkStatus()) || "8".equals(to.getTkStatus())){
                                            logger.error("拼多多支付订单号"+to.getId());
                                            JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()),DateUtils.getDateTime(),"PDD",  taokeOrder.getItemId(), taokeOrder.getItemTitle());
                                            if (!"0000".equals(object.getString("respCode"))) {
                                                System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                logger.error("我惠会员系统订单支付失败：" + object.getString("respMsg")+"拼多多支付订单号"+to.getId());
                                            }
                                        }else if("5".equals(to.getTkStatus())){
                                            logger.error("拼多多支付订单号"+to.getId());
                                            JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()),DateUtils.getDateTime(), "PDD", taokeOrder.getItemId(), taokeOrder.getItemTitle());
                                            if (!"0000".equals(object.getString("respCode"))) {
                                                System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                logger.error("我惠会员系统订单支付失败：" + object.getString("respMsg")+"拼多多支付订单号"+to.getId());
                                            }

                                            // 用户加铜板
                                            memberService.updatePoint(to.getMember(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                            // 调用三方会员系统
                                            //object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), to.getTotalCommissionFee());
                                            logger.error("拼多多结算订单号"+to.getId());
                                            object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                            if (!"0000".equals(object.getString("respCode"))) {
                                                System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                logger.error("我惠会员系统订单结算失败：" + object.getString("respMsg")+"拼多多结算订单号"+to.getId());
                                            }
                                            to.setIsJs("1");
                                            tTaokeOrderService.save(to);
                                        }else if("4".equals(to.getTkStatus())||"10".equals(to.getTkStatus())){
                                            //取消订单
                                            List<WohuiEntity> list = Lists.newArrayList();
                                            list.add(new WohuiEntity("OrderNO", to.getId()));
                                            logger.error("拼多多取消订单号"+to.getId());
                                            JSONObject object = WohuiUtils.send(WohuiUtils.UnionOrderInvalid, list);
                                            if (!"0000".equals(object.getString("respCode"))) {
                                                System.out.println("取消订单号"+to.getId()+"失败：" + object.getString("respMsg"));
                                                logger.error("取消订单号"+to.getId()+"失败：" + object.getString("respMsg")+"拼多多取消订单号"+to.getId());
                                            }
                                        }else{
                                            logger.info("申请支付失败会员信息", to.getMember(),"拼多多订单信息",to);//
                                        }
                                    }
                                }
                            }
                        }
                    }
                     last_order_id = dataObject.getString("last_order_id");
                }
            } else {
                break;
            }
        }
        res.setResult("0");
        res.setResultNote("查询成功");
        return res;
    }
    /**
     * 京东
     *
     * @param req
     * @return
     */
    @PostMapping("/jd")
    @ResponseBody
    public ResJson jd(@RequestBody ReqJson req) {
        Integer pageNo = 1;
        ResJson res = new ResJson();
        if (StringUtils.isBlank(req.getString("startTime"))) {
            res.setResultNote("开始时间");
            return res;
        }
        String before10 = req.getString("startTime");
        //String before10 = "2019112309";  // 前十分钟时间
        while (true) {
            Map<String, String> jdparams = Maps.newHashMap();
            jdparams.put("time", before10);// 查询时间，建议使用分钟级查询，格式：yyyyMMddHH、yyyyMMddHHmm或yyyyMMddHHmmss，如201811031212 的查询范围从12:12:00--12:12:59
            jdparams.put("type", "1");// 订单时间查询类型(1：下单时间，2：完成时间，3：更新时间)
            jdparams.put("pageNo", pageNo.toString());// 第几页
            jdparams.put("pageSize", "100");// 订单结束时间
            jdparams.put("key_id", MiaoyouquanUtils.lmdykey);// 京东联盟授权key
            JSONObject jdObject = MiaoyouquanUtils.miaoyouquanAPI(MiaoyouquanUtils.getJdUnionOrders, jdparams);
            System.out.println("京东喵有券返回数据：" + jdObject.toString());
            if ("200".equals(jdObject.getString("code"))) {
                JSONObject dataObject = jdObject.getJSONObject("data");
                if (dataObject != null) {
                    JSONArray orderList = dataObject.getJSONArray("list");
                    if (orderList != null && orderList.size() > 0) {
                        for (int i = 0; i < orderList.size(); i++) {
                            JSONObject o = orderList.getJSONObject(i);
                            TTaokeOrder taokeOrder = new TTaokeOrder();
                            taokeOrder.setTradeParentId(o.getString("orderId"));
                            List<TTaokeOrder> oList = tTaokeOrderService.findList(taokeOrder);
                            if (oList != null && oList.size() > 0) {
                                for (int j = 0; j < oList.size(); j++) {
                                    JSONArray dataArray = o.getJSONArray("skuList");
                                    if (dataArray != null && dataArray.size() > 0) {
                                        for (int m = 0; m < dataArray.size(); m++) {
                                            JSONObject sku = dataArray.getJSONObject(m);
                                            if (oList.get(j).getItemId().equals(sku.getString("skuId"))) {
                                                /*oList.get(j).setTkStatus(sku.getString("validCode"));*/
                                                if (!"15".equals(sku.getString("validCode")) && !"16".equals(sku.getString("validCode")) && !"17".equals(sku.getString("validCode")) && !"17".equals(sku.getString("validCode"))) {
                                                    oList.get(j).setTkStatus("20");
                                                    //取消订单
                                                    List<WohuiEntity> list = Lists.newArrayList();
                                                    list.add(new WohuiEntity("OrderNO", oList.get(j).getId()));
                                                    JSONObject object = WohuiUtils.send(WohuiUtils.UnionOrderInvalid, list);
                                                    if (!"0000".equals(object.getString("respCode"))) {
                                                        System.out.println("取消订单号" + oList.get(j).getId() + "失败：" + object.getString("respMsg"));
                                                    }
                                                } else {
                                                    oList.get(j).setTkStatus(sku.getString("validCode"));
                                                }
                                                taokeOrder.setPubSharePreFee(sku.getString("estimateFee"));
                                                oList.get(j).setTotalCommissionFee(sku.getString("actualFee"));
                                                tTaokeOrderService.save(oList.get(j));
                                                if (oList.get(j).getMember() != null && StringUtils.isNotBlank(oList.get(j).getMember().getId())) {
                                                    if ("17".equals(oList.get(j).getTkStatus()) && "0".equals(oList.get(j).getIsJs())) {
                                                        // 用户加铜板
                                                        memberService.updatePoint(oList.get(j).getMember(), (new BigDecimal(oList.get(j).getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                                        // 调用三方会员系统
                                                        //JSONObject object = apiService.UnionOrderSettle(oList.get(j).getId(), oList.get(j).getAlipayTotalPrice(), oList.get(j).getTotalCommissionFee());
                                                        JSONObject object = apiService.UnionOrderSettle(oList.get(j).getId(), oList.get(j).getAlipayTotalPrice(), (new BigDecimal(oList.get(j).getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                                        if (!"0000".equals(object.getString("respCode"))) {
                                                            System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                        }
                                                        oList.get(j).setIsJs("1");
                                                        tTaokeOrderService.save(oList.get(j));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                //商品规格列表
                                JSONArray dataArray = o.getJSONArray("skuList");
                                if (dataArray != null && dataArray.size() > 0) {
                                    for (int j = 0; j < dataArray.size(); j++) {
                                        JSONObject sku = dataArray.getJSONObject(j);
                                        taokeOrder = new TTaokeOrder();
                                        taokeOrder.setIsNewRecord(true);
                                        taokeOrder.setType("2");
                                        taokeOrder.setId(o.getString("orderId") + j);
                                        // 根据推广位ID查询用户
                                        Member member = memberService.findUniqueByProperty("jd_id",
                                                sku.getString("positionId"));
                                        if (member != null) {
                                            taokeOrder.setMember(member);
                                        }
                                        taokeOrder.setItemTitle(sku.getString("skuName"));
                                        //根据商品ID查询商品图片
                                        HashMap<String, String> params = Maps.newHashMap();
                                        params.put("skuid", sku.getString("skuId"));
                                        JSONObject infoObject = MiaoyouquanUtils.miaoyouquanAPI(MiaoyouquanUtils.getItemInfo, params);
                                        if (infoObject != null) {
                                            JSONArray itemInfoList = infoObject.getJSONObject("data").getJSONArray("ItemInfo");
                                            if (itemInfoList != null && itemInfoList.size() > 0) {
                                                taokeOrder.setItemImg(itemInfoList.getJSONObject(0).getString("imgUrl"));
                                            }
                                        }

                                        taokeOrder.setItemNum(sku.getInteger("skuNum"));
                                        taokeOrder.setItemPrice(sku.getString("price"));
                                        taokeOrder.setItemId(sku.getString("skuId"));
                                        taokeOrder.setAlipayTotalPrice((new BigDecimal(sku.getString("price")).multiply(new BigDecimal(sku.getString("skuNum")))).toString());
                                        taokeOrder.setTradeParentId(o.getString("orderId"));
                                        if (!"15".equals(sku.getString("validCode")) && !"16".equals(sku.getString("validCode")) && !"17".equals(sku.getString("validCode")) && !"17".equals(sku.getString("validCode"))) {
                                            taokeOrder.setTkStatus("20");
                                        } else {
                                            taokeOrder.setTkStatus(sku.getString("validCode"));
                                        }
                                        taokeOrder.setPubSharePreFee(sku.getString("estimateFee"));
                                        taokeOrder.setTotalCommissionFee(sku.getString("actualFee"));
                                        taokeOrder.setTbPaidTime(StringUtils.isNotBlank(o.getString("orderTime"))
                                                ? new Date(o.getLong("orderTime")) : null);
                                        taokeOrder.setTkCreateTime(StringUtils.isNotBlank(o.getString("orderTime"))
                                                ? new Date(o.getLong("orderTime")) : null);
                                        taokeOrder.setIsJs("0");
                                        tTaokeOrderService.save(taokeOrder);
                                        if (member != null) {
                                            TTaokeOrder to = tTaokeOrderService.get(taokeOrder.getId());
                                            if ("16".equals(to.getTkStatus())) {
                                                JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(),  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()),DateUtils.getDateTime(),  "JD", taokeOrder.getItemId(),  taokeOrder.getItemTitle());
                                                if (!"0000".equals(object.getString("respCode"))) {
                                                    System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                }
                                            } else if ("17".equals(to.getTkStatus())) {
                                                JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(),  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()),DateUtils.getDateTime(),  "JD", taokeOrder.getItemId(),  taokeOrder.getItemTitle());
                                                if (!"0000".equals(object.getString("respCode"))) {
                                                    System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                }

                                                // 用户加铜板
                                                memberService.updatePoint(to.getMember(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                                // 调用三方会员系统
                                                //object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), to.getTotalCommissionFee());
                                                object = apiService.UnionOrderSettle(oList.get(j).getId(), oList.get(j).getAlipayTotalPrice(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                                if (!"0000".equals(object.getString("respCode"))) {
                                                    System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                }
                                                to.setIsJs("0");
                                                tTaokeOrderService.save(to);
                                            }
                                        } else {
                                            //logger.info("申请支付失败会员信息", member,"京东订单信息",taokeOrder);//
                                        }
                                    }
                                }
                            }
                        }
                    }
                    boolean has_next = dataObject.getBoolean("hasMore");
                    if (has_next) {
                        pageNo = pageNo + 1;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        res.setResult("0");
        res.setResultNote("查询成功");
        return res;
    }


    @PostMapping("/ordersave")
    @ResponseBody
    public void ordersave(@RequestBody ReqJson req) {
        String ids = req.getString("ids");
        String[] array = ids.split(",");
        for (int j = 0; j < array.length; j++) {
            int orderStrLen=array[j].length();
            //判断是否是京东的订单
            if(orderStrLen==12||orderStrLen==13){
                TTaokeOrder  to=new TTaokeOrder();
                if(orderStrLen==12){
                    to.setTradeParentId(array[j]);
                }else{
                    to.setId(array[j]);
                }
                List<TTaokeOrder> oList=tTaokeOrderService.findList(to);
                if(oList!=null && oList.size()>0){
                    for(int i = 0; i < oList.size(); i++){
                        to=oList.get(i);
                        Member member = to.getMember();
                        if (to.getType().equals("2")) {
                            Map<String, String> jdparams = Maps.newHashMap();

                            Date date = to.getTkCreateTime();
                            SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmss");
                            String nowTime = sd.format(date);
                            jdparams.put("time", nowTime);// 查询时间，建议使用分钟级查询，格式：yyyyMMddHH、yyyyMMddHHmm或yyyyMMddHHmmss，如201811031212 的查询范围从12:12:00--12:12:59
                            jdparams.put("type", "1");// 订单时间查询类型(1：下单时间，2：完成时间，3：更新时间)
                            jdparams.put("pageNo", "1");// 第几页
                            jdparams.put("pageSize", "100");// 订单结束时间
                            jdparams.put("key_id", MiaoyouquanUtils.lmdykey);// 京东联盟授权key
                            JSONObject jdObject = MiaoyouquanUtils.miaoyouquanAPI(MiaoyouquanUtils.getJdUnionOrders, jdparams);
                            System.out.println("京东喵有券返回数据：" + jdObject.toString());
                            if ("200".equals(jdObject.getString("code"))) {
                                JSONObject dataObject = jdObject.getJSONObject("data");
                                if (dataObject != null) {
                                    JSONArray orderList = dataObject.getJSONArray("list");
                                    if (orderList != null && orderList.size() > 0) {
                                        for (int t = 0; t < orderList.size(); t++) {
                                            JSONObject o = orderList.getJSONObject(t);
                                            TTaokeOrder taokeOrder = new TTaokeOrder();
                                            taokeOrder.setTradeParentId(o.getString("orderId"));
                                            List<TTaokeOrder> ol = tTaokeOrderService.findList(taokeOrder);
                                            if (ol != null && ol.size() > 0) {
                                                for (int s = 0; s < ol.size(); s++) {
                                                    JSONArray dataArray = o.getJSONArray("skuList");
                                                    if (dataArray != null && dataArray.size() > 0) {
                                                        for (int m = 0; m < dataArray.size(); m++) {
                                                            JSONObject sku = dataArray.getJSONObject(m);
                                                            if (ol.get(s).getItemId().equals(sku.getString("skuId"))) {
                                                                /*ol.get(s).setTkStatus(sku.getString("validCode"));*/
                                                                if (!"15".equals(sku.getString("validCode")) && !"16".equals(sku.getString("validCode")) && !"17".equals(sku.getString("validCode")) && !"17".equals(sku.getString("validCode"))) {
                                                                    ol.get(s).setTkStatus("20");
                                                                    //取消订单
                                                                    List<WohuiEntity> list = Lists.newArrayList();
                                                                    list.add(new WohuiEntity("OrderNO", ol.get(s).getId()));
                                                                    JSONObject object = WohuiUtils.send(WohuiUtils.UnionOrderInvalid, list);
                                                                    if (!"0000".equals(object.getString("respCode"))) {
                                                                        System.out.println("取消订单号" + ol.get(s).getId() + "失败：" + object.getString("respMsg"));
                                                                    }
                                                                } else {
                                                                    ol.get(s).setTkStatus(sku.getString("validCode"));
                                                                }
                                                                ol.get(s).setPubSharePreFee(sku.getString("estimateFee"));
                                                                ol.get(s).setTotalCommissionFee(sku.getString("actualFee"));
                                                                tTaokeOrderService.save(ol.get(s));
                                                                if (ol.get(s).getMember() != null && StringUtils.isNotBlank(ol.get(s).getMember().getId())) {
                                                                    if ("16".equals(ol.get(s).getTkStatus())) {
                                                                        JSONObject object = apiService.UnionOrderPay(String.valueOf(ol.get(s).getItemNum()),ol.get(s).getId(), member.getId(), member.getProvince(), member.getCity(), String.valueOf(member.getArea()),  ol.get(s).getAlipayTotalPrice(), (new BigDecimal(ol.get(s).getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(),  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(ol.get(s).getTbPaidTime()), DateUtils.getDateTime(), "JD",  ol.get(s).getItemId(),  ol.get(s).getItemTitle());
                                                                        if (!"0000".equals(object.getString("respCode"))) {
                                                                            System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                                        }
                                                                    }

                                                                    if ("17".equals(ol.get(s).getTkStatus()) && "0".equals(ol.get(s).getIsJs())) {
                                                                        // 用户加铜板
                                                                        memberService.updatePoint(ol.get(s).getMember(), (new BigDecimal(ol.get(s).getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                                                        // 调用三方会员系统
                                                                        //JSONObject object = apiService.UnionOrderSettle(ol.get(s).getId(), ol.get(s).getAlipayTotalPrice(), ol.get(s).getTotalCommissionFee());
                                                                        JSONObject object = apiService.UnionOrderSettle(ol.get(s).getId(), ol.get(s).getAlipayTotalPrice(), (new BigDecimal(ol.get(s).getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                                                        if (!"0000".equals(object.getString("respCode"))) {
                                                                            System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                                        }
                                                                        ol.get(s).setIsJs("1");
                                                                        tTaokeOrderService.save(ol.get(s));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }else{
                TTaokeOrder to;
                to = tTaokeOrderService.get(array[j]);
                if (to != null) {
                    Member member = to.getMember();
                    if (to.getType().equals("0")) {
                        if (to.getMember() != null && StringUtils.isNotBlank(to.getMember().getId())) {
                            Map<String, String> params = Maps.newHashMap();
                            params.put("sid", "22375");// 对应的淘客账号授权ID
                            Date date1 = to.getTkCreateTime();
                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String nowTime = sd.format(date1);
//                            params.put("query_type", "1");// 订单查询开始时间，2019-04-05 12:18:22
                            try {
                                params.put("start_time", URLEncoder.encode(nowTime, "utf-8"));// 订单查询开始时间，2019-04-05 12:18:22
                                params.put("end_time", URLEncoder.encode(nowTime, "utf-8"));// 订单查询开始时间，2019-04-05 12:18:22
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            params.put("order_scene", "2");// 场景订单场景类型，1:常规订单，2:渠道订单，3:会员运营订单，默认为1
                            params.put("signurl", "1");// 页大小，默认20，1~100

                            params.put("page_no", "1");// 第几页，默认1，1~100
                            params.put("page_size", "100");// 页大小，默认20，1~100
                            JSONObject jsonObject = MiaoyouquanUtils.zetaokeAPI(MiaoyouquanUtils.open_dingdanchaxun2, params);
                            System.out.println("折淘客返回数据：" + jsonObject.toString());
                            JSONObject result = JSONObject.parseObject(HttpClientUtil.doGet(jsonObject.getString("url")));
                            if (result != null && StringUtils.isNotBlank(result.getString("tbk_sc_order_details_get_response"))) {
                                JSONObject dataObject = result.getJSONObject("tbk_sc_order_details_get_response").getJSONObject("data");
                                if (dataObject != null) {
                                    JSONObject results = JSONObject.parseObject(dataObject.getString("results"));
                                    if (results != null) {
                                        JSONArray orderArr = results.getJSONArray("publisher_order_dto");
                                        if (orderArr != null) {
                                            for (int i = 0; i < orderArr.size(); i++) {
                                                JSONObject o = orderArr.getJSONObject(i);
                                                TTaokeOrder taokeOrder = tTaokeOrderService.get(o.getString("trade_id"));
                                                //类型end
                                                String commission_fee="";
                                                if("饿了么".equals(o.getString("order_type"))){
                                                    float   total_commission_fee =   Float.parseFloat(o.getString("total_commission_fee"));
                                                    float   subsidy_fee   =   Float.parseFloat(o.getString("subsidy_fee"));
                                                    float   zmoney   =     subsidy_fee+total_commission_fee;
                                                    commission_fee  =  Float.toString(zmoney);
                                                }else{
                                                    commission_fee=o.getString("total_commission_fee");
                                                }
                                                taokeOrder.setTotalCommissionFee(commission_fee);
                                                taokeOrder.setPubSharePreFee(o.getString("pub_share_pre_fee"));
                                                tTaokeOrderService.save(taokeOrder);

                                                //时间转换开始
                                                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                                Date date = to.getTkCreateTime();
                                                long ts = date.getTime();
                                                Date current = null;
                                                try {
                                                    current = simpleDateFormat.parse("2020-06-01 00:00:00");
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                long currents = current.getTime();
                                                //时间转换结束
                                                String fl;
                                                if (ts >= currents) {
                                                    fl = "0.3";
                                                } else {
                                                    fl = "0.35";
                                                }
                                                //类型
                                                String taobaoSource="";
                                                if("饿了么".equals(o.getString("order_type"))){
                                                    taobaoSource="ELEME";
                                                }else if("阿里".equals(o.getString("order_type"))){
                                                    taobaoSource="ALI";
                                                }else if("淘宝".equals(o.getString("order_type"))){
                                                    taobaoSource="TAOBAO";
                                                }else if("天猫".equals(o.getString("order_type"))){
                                                    taobaoSource="TMALL";
                                                }else if("天猫国际".equals(o.getString("order_type"))){
                                                    taobaoSource="TMALLHK";
                                                }else if("聚划算".equals(o.getString("order_type"))){
                                                    taobaoSource="JHS";
                                                }else if("".equals(taobaoSource)){
                                                    taobaoSource="ALI";
                                                }
                                                //类型end

                                                if(StringUtils.isNotBlank(taokeOrder.getId())){
                                                    if(member!=null){
                                                        if("12".equals(to.getTkStatus())){
                                                            logger.error("淘宝支付订单号"+taokeOrder.getId());
                                                            JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(),  taobaoSource,  taokeOrder.getItemId(), taokeOrder.getItemTitle());
                                                            if (!"0000".equals(object.getString("respCode"))) {
                                                                System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                                logger.error("我惠会员系统请求支付订单失败：" + object.getString("respMsg")+"淘宝支付订单号"+taokeOrder.getId());
                                                            }
                                                        }else if("3".equals(to.getTkStatus())){
                                                            logger.error("淘宝支付订单号"+taokeOrder.getId());
                                                            JSONObject object = apiService.UnionOrderPay(String.valueOf(taokeOrder.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(),  taobaoSource,  taokeOrder.getItemId(),taokeOrder.getItemTitle());
                                                            if (!"0000".equals(object.getString("respCode"))) {
                                                                System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                                                logger.error("我惠会员系统请求支付订单失败：" + object.getString("respMsg")+"淘宝支付订单号"+taokeOrder.getId());
                                                            }
                                                            // 用户加铜板
                                                            memberService.updatePoint(to.getMember(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                                            // 调用三方会员系统
                                                            logger.error("淘宝结算订单号"+taokeOrder.getId());
                                                            object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                                            if (!"0000".equals(object.getString("respCode"))) {
                                                                System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                                                logger.error("我惠会员系统请求结算订单失败：" + object.getString("respMsg")+"淘宝结算订单号"+taokeOrder.getId());
                                                            }
                                                            to.setIsJs("1");
                                                            tTaokeOrderService.save(to);
                                                        }
                                                    }else{
                                                        logger.info("申请支付失败会员信息", member,"淘宝订单信息",taokeOrder);//
                                                    }
                                                }


                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else if (to.getType().equals("1")) {
                        if (to.getMember() != null && StringUtils.isNotBlank(to.getMember().getId())) {
                            Map<String, String> params = Maps.newHashMap();
                            params.put("order_sn",to.getId());// 页大小，默认20，1~100
                            params.put("query_order_type", "1");// 默认推广订单
                            JSONObject jsonObject = PddUtils.duoduorouterAPI(PddUtils.router,PddUtils.getdetailOrder, params);
                            System.out.println("拼多多官方返回数据：" + jsonObject.toString());
                            if (jsonObject.containsKey("order_detail_response")) {
                                JSONObject dataObject = jsonObject.getJSONObject("order_detail_response");
                                to.setTkStatus(dataObject.getString("order_status"));
                                to.setPubSharePreFee((dataObject.getDouble("promotion_amount")/100)+"");
                                to.setTotalCommissionFee((dataObject.getDouble("promotion_amount")/100)+"");
                                tTaokeOrderService.save(to);

                                if(to.getMember()!=null && StringUtils.isNotBlank(to.getMember().getId())){
                                    to=tTaokeOrderService.get(to.getId());
                                    if(to==null){
                                        continue;
                                    }
                                    if("0".equals(to.getTkStatus()) || "1".equals(to.getTkStatus()) || "8".equals(to.getTkStatus())){
                                        logger.error("拼多多支付订单号"+to.getId());
                                        JSONObject object = apiService.UnionOrderPay(String.valueOf(to.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(),  "PDD",  to.getItemId(), to.getItemTitle());
                                        if (!"0000".equals(object.getString("respCode"))) {
                                            System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                            logger.error("我惠会员系统订单支付失败：" + object.getString("respMsg")+"拼多多支付订单号"+to.getId());
                                        }
                                    }else if("5".equals(to.getTkStatus())){
                                        logger.error("拼多多支付订单号"+to.getId());
                                        JSONObject object = apiService.UnionOrderPay(String.valueOf(to.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(),  "PDD",  to.getItemId(), to.getItemTitle());
                                        if (!"0000".equals(object.getString("respCode"))) {
                                            System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                            logger.error("我惠会员系统订单支付失败：" + object.getString("respMsg")+"拼多多支付订单号"+to.getId());
                                        }

                                        // 用户加铜板
                                        memberService.updatePoint(to.getMember(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                        // 调用三方会员系统
                                        //object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), to.getTotalCommissionFee());
                                        logger.error("拼多多结算订单号"+to.getId());
                                        object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getTotalCommissionFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                        if (!"0000".equals(object.getString("respCode"))) {
                                            System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                            logger.error("我惠会员系统订单结算失败：" + object.getString("respMsg")+"拼多多结算订单号"+to.getId());
                                        }
                                        to.setIsJs("1");
                                        tTaokeOrderService.save(to);
                                    }else if("4".equals(to.getTkStatus())||"10".equals(to.getTkStatus())){
                                        //取消订单
                                        List<WohuiEntity> list = Lists.newArrayList();
                                        list.add(new WohuiEntity("OrderNO", to.getId()));
                                        logger.error("拼多多取消订单号"+to.getId());
                                        JSONObject object = WohuiUtils.send(WohuiUtils.UnionOrderInvalid, list);
                                        if (!"0000".equals(object.getString("respCode"))) {
                                            System.out.println("取消订单号"+to.getId()+"失败：" + object.getString("respMsg"));
                                            logger.error("取消订单号"+to.getId()+"失败：" + object.getString("respMsg")+"拼多多取消订单号"+to.getId());
                                        }
                                    }else{
                                        logger.info("申请支付失败会员信息", to.getMember(),"拼多多订单信息",to);//
                                    }
                                }
                            }else{
                                System.out.println("未找到"+to.getId());
                                continue;
                            }
                        }
                    }
                }
            }
        }
    }

    @PostMapping("/orderjiesuan")
    @ResponseBody
    public void orderjiesuan(@RequestBody ReqJson req) {
        String ids = req.getString("ids");
        String[] array = ids.split(",");
        for (int j = 0; j < array.length; j++) {
            int orderStrLen=array[j].length();
            //判断是否是京东的订单
            if(orderStrLen==12||orderStrLen==13){
                TTaokeOrder  to=new TTaokeOrder();
                to.setId(array[j]);
                List<TTaokeOrder> oList=tTaokeOrderService.findList(to);
                if(oList!=null && oList.size()>0){
                    for(int i = 0; i < oList.size(); i++){
                        to=oList.get(i);
                        Member member = to.getMember();
                        if (to.getType().equals("2")) {
                            Map<String, String> jdparams = Maps.newHashMap();

                            Date date = to.getTkCreateTime();
                            SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmss");
                            String nowTime = sd.format(date);
                            jdparams.put("time", nowTime);// 查询时间，建议使用分钟级查询，格式：yyyyMMddHH、yyyyMMddHHmm或yyyyMMddHHmmss，如201811031212 的查询范围从12:12:00--12:12:59
                            jdparams.put("type", "1");// 订单时间查询类型(1：下单时间，2：完成时间，3：更新时间)
                            jdparams.put("pageNo", "1");// 第几页
                            jdparams.put("pageSize", "100");// 订单结束时间
                            jdparams.put("key_id", MiaoyouquanUtils.lmdykey);// 京东联盟授权key
                            JSONObject jdObject = MiaoyouquanUtils.miaoyouquanAPI(MiaoyouquanUtils.getJdUnionOrders, jdparams);
                            System.out.println("京东喵有券返回数据：" + jdObject.toString());
                            if ("200".equals(jdObject.getString("code"))) {
                                JSONObject dataObject = jdObject.getJSONObject("data");
                                if (dataObject != null) {
                                    JSONArray orderList = dataObject.getJSONArray("list");
                                    if (orderList != null && orderList.size() > 0) {
                                        for (int t = 0; t < orderList.size(); t++) {
                                            JSONObject o = orderList.getJSONObject(t);
                                            TTaokeOrder taokeOrder = new TTaokeOrder();
                                            taokeOrder.setTradeParentId(o.getString("orderId"));
                                            List<TTaokeOrder> ol = tTaokeOrderService.findList(taokeOrder);
                                            if (ol != null && ol.size() > 0) {
                                                for (int s = 0; s < ol.size(); s++) {
                                                    JSONArray dataArray = o.getJSONArray("skuList");
                                                    if (dataArray != null && dataArray.size() > 0) {
                                                        for (int m = 0; m < dataArray.size(); m++) {
                                                            JSONObject sku = dataArray.getJSONObject(m);
                                                            if (ol.get(s).getItemId().equals(sku.getString("skuId"))) {
                                                                ol.get(s).setPubSharePreFee(sku.getString("estimateFee"));
                                                                ol.get(s).setTotalCommissionFee(sku.getString("actualFee"));
                                                                tTaokeOrderService.save(ol.get(s));
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }

                                }
                            }
                            JSONObject object = apiService.UnionOrderPay(String.valueOf(to.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(), "JD",  to.getItemId(), to.getItemTitle());
                            if (!"0000".equals(object.getString("respCode"))) {
                                System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                            }

                            // 用户加铜板
                            memberService.updatePoint(to.getMember(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                            // 调用三方会员系统
                            //object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), to.getTotalCommissionFee());
                            object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                            if (!"0000".equals(object.getString("respCode"))) {
                                System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                            }
                            to.setIsJs("1");
                            tTaokeOrderService.save(to);
                        }
                    }
                 }
            }else{
                TTaokeOrder to;
                to = tTaokeOrderService.get(array[j]);
                if (to != null) {
                    Member member = to.getMember();
                    if (to.getType().equals("0")) {
                        if (to.getMember() != null && StringUtils.isNotBlank(to.getMember().getId())) {
                            Map<String, String> params = Maps.newHashMap();
                            params.put("sid", "22375");// 对应的淘客账号授权ID
                            Date date1 = to.getTkCreateTime();
                            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String nowTime = sd.format(date1);
//                            params.put("query_type", "1");// 订单查询开始时间，2019-04-05 12:18:22
                            try {
                                params.put("start_time", URLEncoder.encode(nowTime, "utf-8"));// 订单查询开始时间，2019-04-05 12:18:22
                                params.put("end_time", URLEncoder.encode(nowTime, "utf-8"));// 订单查询开始时间，2019-04-05 12:18:22
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            params.put("order_scene", "2");// 场景订单场景类型，1:常规订单，2:渠道订单，3:会员运营订单，默认为1
                            params.put("signurl", "1");// 页大小，默认20，1~100

                            params.put("page_no", "1");// 第几页，默认1，1~100
                            params.put("page_size", "100");// 页大小，默认20，1~100
                            JSONObject jsonObject = MiaoyouquanUtils.zetaokeAPI(MiaoyouquanUtils.open_dingdanchaxun2, params);
                            System.out.println("折淘客返回数据：" + jsonObject.toString());
                            JSONObject result = JSONObject.parseObject(HttpClientUtil.doGet(jsonObject.getString("url")));
                            if (result != null && StringUtils.isNotBlank(result.getString("tbk_sc_order_details_get_response"))) {
                                JSONObject dataObject = result.getJSONObject("tbk_sc_order_details_get_response").getJSONObject("data");
                                if (dataObject != null) {
                                    JSONObject results = JSONObject.parseObject(dataObject.getString("results"));
                                    if (results != null) {
                                        JSONArray orderArr = results.getJSONArray("publisher_order_dto");
                                        if (orderArr != null) {
                                            for (int i = 0; i < orderArr.size(); i++) {
                                                JSONObject o = orderArr.getJSONObject(i);
                                                TTaokeOrder taokeOrder = tTaokeOrderService.get(o.getString("trade_id"));
                                                //类型end
                                                String commission_fee="";
                                                if("饿了么".equals(o.getString("order_type"))){
                                                    float   total_commission_fee =   Float.parseFloat(o.getString("total_commission_fee"));
                                                    float   subsidy_fee   =   Float.parseFloat(o.getString("subsidy_fee"));
                                                    float   zmoney   =     subsidy_fee+total_commission_fee;
                                                    commission_fee  =  Float.toString(zmoney);
                                                }else{
                                                    commission_fee=o.getString("total_commission_fee");
                                                }
                                                taokeOrder.setTotalCommissionFee(commission_fee);
                                                taokeOrder.setPubSharePreFee(o.getString("pub_share_pre_fee"));
                                                tTaokeOrderService.save(taokeOrder);
                                            }
                                        }
                                    }
                                }
                            }


                            //时间转换开始
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date date = to.getTkCreateTime();
                            long ts = date.getTime();
                            Date current = null;
                            try {
                                current = simpleDateFormat.parse("2020-06-01 00:00:00");
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            long currents = current.getTime();
                            //时间转换结束
                            String fl;
                            if (ts >= currents) {
                                fl = "0.3";
                            } else {
                                fl = "0.35";
                            }
                            //费率
                            if (StringUtils.isNotBlank(to.getId())) {
                                if (member != null) {
                                    JSONObject object = apiService.UnionOrderPay(String.valueOf(to.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(), "ALI",  to.getItemId(), to.getItemTitle());
                                    if (!"0000".equals(object.getString("respCode"))) {
                                        System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                                    }
                                    // 用户加铜板
                                    memberService.updatePoint(to.getMember(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                                    // 调用三方会员系统
                                    //object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), to.getTotalCommissionFee());
                                    object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal(fl)).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                                    if (!"0000".equals(object.getString("respCode"))) {
                                        System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                                    }
                                    to.setIsJs("1");
                                    tTaokeOrderService.save(to);
                                }
                            }
                        }
                    } else if (to.getType().equals("1")) {
                        if (to.getMember() != null && StringUtils.isNotBlank(to.getMember().getId())) {
                            Map<String, String> params = Maps.newHashMap();
                            params.put("order_sn",to.getId());// 页大小，默认20，1~100
                            params.put("query_order_type", "1");// 默认推广订单
                            JSONObject jsonObject = PddUtils.duoduorouterAPI(PddUtils.router,PddUtils.getdetailOrder, params);
                            System.out.println("拼多多官方返回数据：" + jsonObject.toString());
                            if (jsonObject.containsKey("order_detail_response")) {
                                JSONObject dataObject = jsonObject.getJSONObject("order_detail_response");
                                to.setTkStatus(dataObject.getString("order_status"));
                                to.setPubSharePreFee((dataObject.getDouble("promotion_amount")/100)+"");
                                to.setTotalCommissionFee((dataObject.getDouble("promotion_amount")/100)+"");
                                tTaokeOrderService.save(to);
                            }else{
                                System.out.println("未找到"+to.getId());
                                continue;
                            }

                            JSONObject object = apiService.UnionOrderPay(String.valueOf(to.getItemNum()),to.getId(), member.getId(), member.getProvince(), member.getCity(), member.getArea(),  to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(to.getTbPaidTime()), DateUtils.getDateTime(), "PDD",  to.getItemId(),  to.getItemTitle());
                            if (!"0000".equals(object.getString("respCode"))) {
                                System.out.println("我惠会员系统订单支付失败：" + object.getString("respMsg"));
                            }

                            // 用户加铜板
                            memberService.updatePoint(to.getMember(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString(), "1");

                            // 调用三方会员系统
                            //object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), to.getTotalCommissionFee());
                            object = apiService.UnionOrderSettle(to.getId(), to.getAlipayTotalPrice(), (new BigDecimal(to.getPubSharePreFee()).multiply(new BigDecimal("0.35")).setScale(2, BigDecimal.ROUND_DOWN)).toString());
                            if (!"0000".equals(object.getString("respCode"))) {
                                System.out.println("我惠会员系统请求结算订单失败：" + object.getString("respMsg"));
                            }
                            to.setIsJs("1");
                            tTaokeOrderService.save(to);
                        }
                    }
                }
            }
        }
    }

    @PostMapping("/getpddgoodsxiajia")
    @ResponseBody
    public void gettaokeorder(){
        List<WohuiEntity> xjParam = Lists.newArrayList();//下架
        Map<String, String> params = Maps.newHashMap();
        params = Maps.newHashMap();
        params.put("goods_sign", "[" + "goods_sign" + "]");//下架
        String goodsid = "44625115677";
        String Source = "PDD";//来源
        JSONObject info;
        try {
            Thread.sleep(100);//设置暂停的时间 1 秒
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
        JSONObject jsonObject = MiaoyouquanUtils.miaoyouquanAPI(MiaoyouquanUtils.getGoodsDetailInfo, params);
        if ("200".equals(jsonObject.getString("code")) ) {
            if (!jsonObject.getJSONArray("data").isEmpty()) {
                String rate = jsonObject.getJSONArray("data").getJSONObject(0).getString("promotion_rate");
                if (com.jeeplus.common.utils.StringUtils.isBlank(rate)) {
                    //下架接口
                    xjParam.add(new WohuiEntity("Source",Source));//来源
                    xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
                    info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
                    if (!"0000".equals(info.getString("respCode"))) {
                        //下架成功
                        System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
                        logger.error("下架成功goodsid："+goodsid+"来源"+Source);

                    }else{
                        System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
                        logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

                    }
                }
            }
        }else{
            //下架接口
            xjParam.add(new WohuiEntity("Source",Source));//来源
            xjParam.add(new WohuiEntity("GoodsID", goodsid));//goodsid
            info = WohuiUtils.send(WohuiUtils.MSMGoodsOff, xjParam);
            if (!"0000".equals(info.getString("respCode"))) {
                //下架成功
                System.out.println("下架成功goodsid："+goodsid+"来源"+Source);
                logger.error("下架成功goodsid："+goodsid+"来源"+Source);

            }else{
                System.out.println("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));
                logger.error("下架失败goodsid："+goodsid+"来源"+Source+"原因"+info.getString("respMsg"));

            }
        }
    }

    @PostMapping("/getPgwSig")
    @ResponseBody
    public String getPgwSig(@RequestBody ReqJson req) {
        String sign = PgwSignUtil.genRSASign(String.valueOf(req));
        System.out.println(sign);
        return sign;
    }

    @PostMapping("/getNeiCun")
    @ResponseBody
    public String getNeiCun() {
        Runtime run = Runtime.getRuntime();
        long maxMemory = Runtime.getRuntime().maxMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        long freeMemory = Runtime.getRuntime().freeMemory();
        long usableMemony = maxMemory - totalMemory + freeMemory;
        return "可以获得最大内存是：" + maxMemory / 1024 / 1024 + "M" + "已分配到的内存大小是：" + totalMemory / 1024 / 1024 + "M" + "所分配内存的剩余大小是：" + freeMemory / 1024 / 1024 + "M" + "最大可用内存大小是：" + usableMemony / 1024 / 1024 + "M";
    }

    @PostMapping({"/saveGoodsSign"})
    @ResponseBody
    public String saveGoodsSign() {
        Map<String, String> params1 = Maps.newHashMap();
		System.out.println("________________________________________________________________________");
        TTaokeOrder tTaokeOrder=new TTaokeOrder();
        tTaokeOrder.setType("1");
        List<TTaokeOrder> tTaokeOrderList=tTaokeOrderService.findList(tTaokeOrder);
        if(tTaokeOrderList!=null && tTaokeOrderList.size()>0){
            for (TTaokeOrder tTaokeOrders : tTaokeOrderList) {
                if (!"".equals(tTaokeOrders.getItemId())) {
                    params1.put("sort_type","0");
                    params1.put("with_coupon","false");
                    params1.put("page","1");
                    params1.put("keyword",tTaokeOrders.getItemId());
                    params1.put("page_size","10");
                    params1.put("pid","9191823_175904824");
                    params1.put("custom_parameters", "['uid':'E7CC6C17939B4F8E831D7635F0AC892D','sid':'111']");
                    JSONObject jsonObject1 = PddUtils.duoduorouterAPI(PddUtils.router, PddUtils.searchgoods, params1);
                    JSONArray jsonObject2=  jsonObject1.getJSONObject("goods_search_response").getJSONArray("goods_list");
                    if(jsonObject2.size()>0){
                        JSONObject jsonObject3= (JSONObject) jsonObject2.get(0);
                        TTaokeOrder to = tTaokeOrders;
                        if(to!=null){
                            if(to.getItemId().equals(jsonObject3.getString("goods_id"))){
                                to.setGoodsSign(jsonObject3.getString("goods_sign"));
                                tTaokeOrderService.save(to);
                            }
                        }
                    }else{
                        System.out.println(tTaokeOrders.getItemId()+" "+"为空"+"\n");
                        continue;
                    }
                }
            }
        }
        return  "结束";
    }

}
