/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyorder.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.jeeplus.common.utils.IdGen;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.api.OrderNumPrefix;
import com.jeeplus.modules.api.utils.WohuiEntity;
import com.jeeplus.modules.api.utils.WohuiUtils;
import com.jeeplus.modules.proxyorder.entity.ProxyOrder;
import com.jeeplus.modules.proxyorder.mapper.ProxyOrderMapper;
import com.jeeplus.modules.proxyproduct.entity.ProxyProduct;
import com.jeeplus.modules.proxyproduct.service.ProxyProductService;

/**
 * 单品代理订单Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class ProxyOrderService extends CrudService<ProxyOrderMapper, ProxyOrder> {
	@Autowired
	private ProxyProductService proxyProductService;
	
	public ProxyOrder get(String id) {
		return super.get(id);
	}
	
	public List<ProxyOrder> findList(ProxyOrder proxyOrder) {
		return super.findList(proxyOrder);
	}
	
	public Page<ProxyOrder> findPage(Page<ProxyOrder> page, ProxyOrder proxyOrder) {
		return super.findPage(page, proxyOrder);
	}
	
	@Transactional(readOnly = false)
	public void save(ProxyOrder proxyOrder) {
		super.save(proxyOrder);
	}
	
	@Transactional(readOnly = false)
	public void delete(ProxyOrder proxyOrder) {
		super.delete(proxyOrder);
	}

	/**
	 * 支付处理
	 * @param proxyOrder
	 */
	@Transactional(readOnly = false)
	public void pay(ProxyOrder proxyOrder) {
		super.save(proxyOrder);
		
		// 调用三方接口
		List<WohuiEntity> list = Lists.newArrayList();
		list.add(new WohuiEntity("CardGUID", proxyOrder.getMember().getId()));
		list.add(new WohuiEntity("OrderNO", proxyOrder.getId()));
		list.add(new WohuiEntity("Fee", proxyOrder.getPrice()));
		JSONObject object = WohuiUtils.send(WohuiUtils.PCAOpen, list);
		if (!"0000".equals(object.getString("respCode"))) {
			logger.error("开通单品代理失败：" + object.getString("respMsg"));
		} else {
			ProxyProduct proxyProduct = new ProxyProduct();
			proxyProduct.setIsNewRecord(true);
			proxyProduct.setMember(proxyOrder.getMember());
			proxyProduct.setId(OrderNumPrefix.proxyProductOrder + IdGen.getOrderNo());
			proxyProduct.setProxyId(proxyOrder.getId());
			proxyProduct.setProductId(proxyOrder.getProductId());
			proxyProduct.setProductName(proxyOrder.getProductTitle());
			proxyProduct.setProductImage(proxyOrder.getProductImage());
			proxyProduct.setFee(proxyOrder.getPrice());
			proxyProduct.setPayNo(proxyOrder.getOrderNo());
			proxyProduct.setPayType(proxyOrder.getPayType());
			proxyProduct.setPayDate(new Date());
			proxyProduct.setOrderState("1");
			proxyProduct.setState("0");
			proxyProduct.setType(proxyOrder.getCategory());
			proxyProduct.setProvince(proxyOrder.getProvince());
			proxyProduct.setCity(proxyOrder.getCity());
			proxyProduct.setArea(proxyOrder.getArea());
			proxyProductService.save(proxyProduct);
		}
	}

	/**
	 * 生成订单
	 * @param proxyOrder
	 */
	@Transactional(readOnly = false)
	public void insert(ProxyOrder proxyOrder) {
		super.save(proxyOrder);
	}
//	/**
//	 * 生成单品代理订单
//	 * @param proxyOrder
//	 */
//	@Transactional(readOnly = false)
//	public void insertdpdl(ProxyOrder proxyOrder) {
//		super.save(proxyOrder);
//		List<WohuiEntity> entityList = Lists.newArrayList();
//		entityList.add(new WohuiEntity("CardGUID", proxyOrder.getMember().getId()));
//		entityList.add(new WohuiEntity("OrderNO", proxyOrder.getId()));
//		entityList.add(new WohuiEntity("Fee", proxyOrder.getPrice()));
//		entityList.add(new WohuiEntity("Province", proxyOrder.getProvince()));
//		entityList.add(new WohuiEntity("City", proxyOrder.getCity()));
//		entityList.add(new WohuiEntity("District", proxyOrder.getArea(), false));
//		entityList.add(new WohuiEntity("Source", proxyOrder.getCategory()));
//		entityList.add(new WohuiEntity("GoodsID", proxyOrder.getProductId()));
//		entityList.add(new WohuiEntity("GoodsName", proxyOrder.getProductTitle(), false));
//		entityList.add(new WohuiEntity("ImgUrl", proxyOrder.getProductImage(), false));
//		JSONObject object = WohuiUtils.send(WohuiUtils.PCAApply, entityList);
//		if (!"0000".equals(object.getString("respCode"))) {
//			throw new RuntimeException(object.getString("respMsg")+entityList);
//		}
//	}

	/**
	 * 取消超时未付款的订单
	 */
	@Transactional(readOnly = false)
	public void cancel() {
		ProxyOrder proxyOrder = new ProxyOrder();
		proxyOrder.setState("0");
		proxyOrder.setDataScope(" AND a.create_date < DATE_SUB(NOW(),INTERVAL 15 MINUTE) ");
		while (true) {
			Page<ProxyOrder> page = super.findPage(new Page<ProxyOrder>(1, 100), proxyOrder);
			if (!page.getList().isEmpty()) {
				for (ProxyOrder order : page.getList()) {
					order.setState("2");
					super.save(order);
					
					try {
						// 调用三方取消订单

//						List<WohuiEntity> list = Lists.newArrayList();
//						list.add(new WohuiEntity("OrderNO", order.getId()));
//
//						JSONObject object = WohuiUtils.send(WohuiUtils.MSMCancel, list);
//						if (!"0000".equals(object.getString("respCode"))) {
//							logger.error("取消代理订单号"+order.getId()+"失败：" + object.getString("respMsg"));
//						}
					} catch (Exception e) {
						logger.error("取消代理订单号"+order.getId()+"失败：" + e.getMessage());
					}
					
				}
			}
			if (page.getList().size() < 100) {
				break;
			}
		}
	}
	
}