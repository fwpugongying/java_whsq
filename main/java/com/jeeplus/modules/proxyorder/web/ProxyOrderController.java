/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyorder.web;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.proxyorder.entity.ProxyOrder;
import com.jeeplus.modules.proxyorder.service.ProxyOrderService;

/**
 * 单品代理订单Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/proxyorder/proxyOrder")
public class ProxyOrderController extends BaseController {

	@Autowired
	private ProxyOrderService proxyOrderService;
	
	@ModelAttribute
	public ProxyOrder get(@RequestParam(required=false) String id) {
		ProxyOrder entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = proxyOrderService.get(id);
		}
		if (entity == null){
			entity = new ProxyOrder();
		}
		return entity;
	}
	
	/**
	 * 单品代理订单列表页面
	 */
	@RequiresPermissions("proxyorder:proxyOrder:list")
	@RequestMapping(value = {"list", ""})
	public String list(ProxyOrder proxyOrder, Model model) {
		model.addAttribute("proxyOrder", proxyOrder);
		return "modules/proxyorder/proxyOrderList";
	}
	
		/**
	 * 单品代理订单列表数据
	 */
	@ResponseBody
	@RequiresPermissions("proxyorder:proxyOrder:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(ProxyOrder proxyOrder, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ProxyOrder> page = proxyOrderService.findPage(new Page<ProxyOrder>(request, response), proxyOrder); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑单品代理订单表单页面
	 */
	@RequiresPermissions(value={"proxyorder:proxyOrder:view","proxyorder:proxyOrder:add","proxyorder:proxyOrder:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(ProxyOrder proxyOrder, Model model) {
		model.addAttribute("proxyOrder", proxyOrder);
		return "modules/proxyorder/proxyOrderForm";
	}

	/**
	 * 保存单品代理订单
	 */
	@ResponseBody
	@RequiresPermissions(value={"proxyorder:proxyOrder:add","proxyorder:proxyOrder:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(ProxyOrder proxyOrder, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(proxyOrder);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		proxyOrderService.save(proxyOrder);//保存
		j.setSuccess(true);
		j.setMsg("保存单品代理订单成功");
		return j;
	}
	
	/**
	 * 删除单品代理订单
	 */
	@ResponseBody
	@RequiresPermissions("proxyorder:proxyOrder:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(ProxyOrder proxyOrder) {
		AjaxJson j = new AjaxJson();
		proxyOrderService.delete(proxyOrder);
		j.setMsg("删除单品代理订单成功");
		return j;
	}
	
	/**
	 * 批量删除单品代理订单
	 */
	@ResponseBody
	@RequiresPermissions("proxyorder:proxyOrder:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			proxyOrderService.delete(proxyOrderService.get(id));
		}
		j.setMsg("删除单品代理订单成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("proxyorder:proxyOrder:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(ProxyOrder proxyOrder, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "单品代理订单"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<ProxyOrder> page = proxyOrderService.findPage(new Page<ProxyOrder>(request, response, -1), proxyOrder);
    		new ExportExcel("单品代理订单", ProxyOrder.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出单品代理订单记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("proxyorder:proxyOrder:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<ProxyOrder> list = ei.getDataList(ProxyOrder.class);
			for (ProxyOrder proxyOrder : list){
				try{
					proxyOrderService.save(proxyOrder);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条单品代理订单记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条单品代理订单记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入单品代理订单失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入单品代理订单数据模板
	 */
	@ResponseBody
	@RequiresPermissions("proxyorder:proxyOrder:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "单品代理订单数据导入模板.xlsx";
    		List<ProxyOrder> list = Lists.newArrayList(); 
    		new ExportExcel("单品代理订单数据", ProxyOrder.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}