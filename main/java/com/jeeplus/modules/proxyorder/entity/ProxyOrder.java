/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.proxyorder.entity;

import com.jeeplus.modules.member.entity.Member;
import javax.validation.constraints.NotNull;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 单品代理订单Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class ProxyOrder extends DataEntity<ProxyOrder> {
	
	private static final long serialVersionUID = 1L;
	private Member member;		// 用户
	private String type;		// 类型 0订单 1成长型 2赠送型
	private String province;		// 省
	private String city;		// 市
	private String area;		// 区
	private String code;		// 编码
	private String price;		// 价格
	private Date payDate;		// 付款时间
	private String payType;		// 付款方式 1支付宝 2微信
	private String state;		// 状态 0待付款 1已付款 2已取消
	private String orderId;		// 商品订单号
	private String orderNo;		// 交易单号
	private String productId;// 商品ID
	private String productTitle;// 商品名称
	private String productImage;		// 图片
	private String shopCode;// 店铺编码
	private String category;// 代理类型 0云店 1淘客 2京东 3拼多多
	private String trade_type;

	public ProxyOrder() {
		super();
	}

	public ProxyOrder(String id){
		super(id);
	}

	@NotNull(message="用户不能为空")
	@ExcelField(title="用户", fieldType=Member.class, value="member.phone", align=2, sort=1)
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	@ExcelField(title="类型 0订单 1成长型 2赠送型", dictType="proxy_order_type", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="省", align=2, sort=3)
	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}
	
	@ExcelField(title="市", align=2, sort=4)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	@ExcelField(title="区", align=2, sort=5)
	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	@ExcelField(title="编码", align=2, sort=6)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@ExcelField(title="价格", align=2, sort=7)
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ExcelField(title="付款时间", align=2, sort=9)
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}
	
	@ExcelField(title="付款方式 1支付宝 2微信", dictType="pay_type", align=2, sort=10)
	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	@ExcelField(title="状态 0待付款 1已付款 2已取消", dictType="proxy_order_state", align=2, sort=12)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@ExcelField(title="商品订单号", align=2, sort=13)
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getTrade_type() {
        return trade_type;
    }
}