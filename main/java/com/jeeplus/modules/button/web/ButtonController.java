/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.button.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.CacheUtils;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.api.CacheUtil;
import com.jeeplus.modules.button.entity.Button;
import com.jeeplus.modules.button.service.ButtonService;

/**
 * 首页按钮Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/button/button")
public class ButtonController extends BaseController {

	@Autowired
	private ButtonService buttonService;
	
	@ModelAttribute
	public Button get(@RequestParam(required=false) String id) {
		Button entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = buttonService.get(id);
		}
		if (entity == null){
			entity = new Button();
		}
		return entity;
	}
	
	/**
	 * 首页按钮列表页面
	 */
	@RequiresPermissions("button:button:list")
	@RequestMapping(value = {"list", ""})
	public String list(Button button, Model model) {
		model.addAttribute("button", button);
		return "modules/button/buttonList";
	}
	
		/**
	 * 首页按钮列表数据
	 */
	@ResponseBody
	@RequiresPermissions("button:button:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Button button, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Button> page = buttonService.findPage(new Page<Button>(request, response), button); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑首页按钮表单页面
	 */
	@RequiresPermissions(value={"button:button:view","button:button:add","button:button:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Button button, Model model) {
		model.addAttribute("button", button);
		return "modules/button/buttonForm";
	}

	/**
	 * 保存首页按钮
	 */
	@ResponseBody
	@RequiresPermissions(value={"button:button:add","button:button:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Button button, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(button);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		buttonService.save(button);//保存
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_BUTTON_LIST);
		j.setSuccess(true);
		j.setMsg("保存首页按钮成功");
		return j;
	}
	
	/**
	 * 删除首页按钮
	 */
	@ResponseBody
	@RequiresPermissions("button:button:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Button button) {
		AjaxJson j = new AjaxJson();
		buttonService.delete(button);
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_BUTTON_LIST);
		j.setMsg("删除首页按钮成功");
		return j;
	}
	
	/**
	 * 批量删除首页按钮
	 */
	@ResponseBody
	@RequiresPermissions("button:button:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			buttonService.delete(buttonService.get(id));
		}
		CacheUtils.remove(CacheUtil.APP_CACHE, CacheUtil.CACHE_BUTTON_LIST);
		j.setMsg("删除首页按钮成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("button:button:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Button button, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "首页按钮"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Button> page = buttonService.findPage(new Page<Button>(request, response, -1), button);
    		new ExportExcel("首页按钮", Button.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出首页按钮记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("button:button:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Button> list = ei.getDataList(Button.class);
			for (Button button : list){
				try{
					buttonService.save(button);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条首页按钮记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条首页按钮记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入首页按钮失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入首页按钮数据模板
	 */
	@ResponseBody
	@RequiresPermissions("button:button:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "首页按钮数据导入模板.xlsx";
    		List<Button> list = Lists.newArrayList(); 
    		new ExportExcel("首页按钮数据", Button.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}