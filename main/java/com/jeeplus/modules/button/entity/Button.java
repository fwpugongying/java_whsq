/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.button.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 首页按钮Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Button extends DataEntity<Button> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 名称
	private String type;		// 类型
	private String icon;		// 图标
	private Integer sort;		// 排序
	private Integer wxappshow;		// 小程序是否显示
	private Integer appshow;		// app是否显示
	public Button() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}
	public Button(String id){
		super(id);
	}
	public Integer getWxappshow() {
		return wxappshow;
	}

	public void setWxappshow(Integer wxappshow) {
		this.wxappshow = wxappshow;
	}

	public Integer getAppshow() {
		return appshow;
	}
	public void setAppshow(Integer appshow) {
		this.appshow = appshow;
	}
	@ExcelField(title="名称", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="类型", dictType="button_type", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="图标", align=2, sort=3)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=4)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
}