/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.agreement.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 说明协议Entity
 * @author lixinapp
 * @version 2019-10-14
 */
public class Agreement extends DataEntity<Agreement> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 标题
	private String type;		// 类型 
	private String content;		// 内容
	
	public Agreement() {
		super();
		this.setIdType(IDTYPE_AUTO);
	}

	public Agreement(String id){
		super(id);
	}

	@ExcelField(title="标题", align=2, sort=1)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@ExcelField(title="类型 ", dictType="agreement_type", align=2, sort=2)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@ExcelField(title="内容", align=2, sort=3)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}