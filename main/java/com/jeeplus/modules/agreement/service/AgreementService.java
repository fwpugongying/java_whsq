/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.agreement.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.agreement.entity.Agreement;
import com.jeeplus.modules.agreement.mapper.AgreementMapper;

/**
 * 说明协议Service
 * @author lixinapp
 * @version 2019-10-14
 */
@Service
@Transactional(readOnly = true)
public class AgreementService extends CrudService<AgreementMapper, Agreement> {

	public Agreement get(String id) {
		return super.get(id);
	}
	
	public List<Agreement> findList(Agreement agreement) {
		return super.findList(agreement);
	}
	
	public Page<Agreement> findPage(Page<Agreement> page, Agreement agreement) {
		return super.findPage(page, agreement);
	}
	
	@Transactional(readOnly = false)
	public void save(Agreement agreement) {
		super.save(agreement);
	}
	
	@Transactional(readOnly = false)
	public void delete(Agreement agreement) {
		super.delete(agreement);
	}
	
}