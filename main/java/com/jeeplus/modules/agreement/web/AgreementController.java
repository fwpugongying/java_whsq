/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.agreement.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.agreement.entity.Agreement;
import com.jeeplus.modules.agreement.service.AgreementService;

/**
 * 说明协议Controller
 * @author lixinapp
 * @version 2019-10-14
 */
@Controller
@RequestMapping(value = "${adminPath}/agreement/agreement")
public class AgreementController extends BaseController {

	@Autowired
	private AgreementService agreementService;
	
	@ModelAttribute
	public Agreement get(@RequestParam(required=false) String id) {
		Agreement entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = agreementService.get(id);
		}
		if (entity == null){
			entity = new Agreement();
		}
		return entity;
	}
	
	/**
	 * 说明协议列表页面
	 */
	@RequiresPermissions("agreement:agreement:list")
	@RequestMapping(value = {"list", ""})
	public String list(Agreement agreement, Model model) {
		model.addAttribute("agreement", agreement);
		return "modules/agreement/agreementList";
	}
	
		/**
	 * 说明协议列表数据
	 */
	@ResponseBody
	@RequiresPermissions("agreement:agreement:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(Agreement agreement, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Agreement> page = agreementService.findPage(new Page<Agreement>(request, response), agreement); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑说明协议表单页面
	 */
	@RequiresPermissions(value={"agreement:agreement:view","agreement:agreement:add","agreement:agreement:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Agreement agreement, Model model) {
		model.addAttribute("agreement", agreement);
		return "modules/agreement/agreementForm";
	}

	/**
	 * 保存说明协议
	 */
	@ResponseBody
	@RequiresPermissions(value={"agreement:agreement:add","agreement:agreement:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(Agreement agreement, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(agreement);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		agreementService.save(agreement);//保存
		j.setSuccess(true);
		j.setMsg("保存说明协议成功");
		return j;
	}
	
	/**
	 * 删除说明协议
	 */
	@ResponseBody
	@RequiresPermissions("agreement:agreement:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(Agreement agreement) {
		AjaxJson j = new AjaxJson();
		agreementService.delete(agreement);
		j.setMsg("删除说明协议成功");
		return j;
	}
	
	/**
	 * 批量删除说明协议
	 */
	@ResponseBody
	@RequiresPermissions("agreement:agreement:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			agreementService.delete(agreementService.get(id));
		}
		j.setMsg("删除说明协议成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("agreement:agreement:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(Agreement agreement, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "说明协议"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<Agreement> page = agreementService.findPage(new Page<Agreement>(request, response, -1), agreement);
    		new ExportExcel("说明协议", Agreement.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出说明协议记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("agreement:agreement:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<Agreement> list = ei.getDataList(Agreement.class);
			for (Agreement agreement : list){
				try{
					agreementService.save(agreement);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条说明协议记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条说明协议记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入说明协议失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入说明协议数据模板
	 */
	@ResponseBody
	@RequiresPermissions("agreement:agreement:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "说明协议数据导入模板.xlsx";
    		List<Agreement> list = Lists.newArrayList(); 
    		new ExportExcel("说明协议数据", Agreement.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}