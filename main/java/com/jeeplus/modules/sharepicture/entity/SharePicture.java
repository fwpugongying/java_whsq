/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.sharepicture.entity;


import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 分享背景图Entity
 * @author lixinapp
 * @version 2019-10-25
 */
public class SharePicture extends DataEntity<SharePicture> {
	
	private static final long serialVersionUID = 1L;
	private String image;		// 图片
	
	public SharePicture() {
		super();
	}

	public SharePicture(String id){
		super(id);
	}

	@ExcelField(title="图片", align=2, sort=1)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}