/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.sharepicture.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.sharepicture.entity.SharePicture;
import com.jeeplus.modules.sharepicture.service.SharePictureService;

/**
 * 分享背景图Controller
 * @author lixinapp
 * @version 2019-10-25
 */
@Controller
@RequestMapping(value = "${adminPath}/sharepicture/sharePicture")
public class SharePictureController extends BaseController {

	@Autowired
	private SharePictureService sharePictureService;
	
	@ModelAttribute
	public SharePicture get(@RequestParam(required=false) String id) {
		SharePicture entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = sharePictureService.get(id);
		}
		if (entity == null){
			entity = new SharePicture();
		}
		return entity;
	}
	
	/**
	 * 分享背景图列表页面
	 */
	@RequiresPermissions("sharepicture:sharePicture:list")
	@RequestMapping(value = {"list", ""})
	public String list(SharePicture sharePicture, Model model) {
		model.addAttribute("sharePicture", sharePicture);
		return "modules/sharepicture/sharePictureList";
	}
	
		/**
	 * 分享背景图列表数据
	 */
	@ResponseBody
	@RequiresPermissions("sharepicture:sharePicture:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(SharePicture sharePicture, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<SharePicture> page = sharePictureService.findPage(new Page<SharePicture>(request, response), sharePicture); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑分享背景图表单页面
	 */
	@RequiresPermissions(value={"sharepicture:sharePicture:view","sharepicture:sharePicture:add","sharepicture:sharePicture:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(SharePicture sharePicture, Model model) {
		model.addAttribute("sharePicture", sharePicture);
		return "modules/sharepicture/sharePictureForm";
	}

	/**
	 * 保存分享背景图
	 */
	@ResponseBody
	@RequiresPermissions(value={"sharepicture:sharePicture:add","sharepicture:sharePicture:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(SharePicture sharePicture, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(sharePicture);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		sharePictureService.save(sharePicture);//保存
		j.setSuccess(true);
		j.setMsg("保存分享背景图成功");
		return j;
	}
	
	/**
	 * 删除分享背景图
	 */
	@ResponseBody
	@RequiresPermissions("sharepicture:sharePicture:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(SharePicture sharePicture) {
		AjaxJson j = new AjaxJson();
		sharePictureService.delete(sharePicture);
		j.setMsg("删除分享背景图成功");
		return j;
	}
	
	/**
	 * 批量删除分享背景图
	 */
	@ResponseBody
	@RequiresPermissions("sharepicture:sharePicture:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			sharePictureService.delete(sharePictureService.get(id));
		}
		j.setMsg("删除分享背景图成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("sharepicture:sharePicture:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(SharePicture sharePicture, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "分享背景图"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<SharePicture> page = sharePictureService.findPage(new Page<SharePicture>(request, response, -1), sharePicture);
    		new ExportExcel("分享背景图", SharePicture.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出分享背景图记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("sharepicture:sharePicture:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<SharePicture> list = ei.getDataList(SharePicture.class);
			for (SharePicture sharePicture : list){
				try{
					sharePictureService.save(sharePicture);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条分享背景图记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条分享背景图记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入分享背景图失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入分享背景图数据模板
	 */
	@ResponseBody
	@RequiresPermissions("sharepicture:sharePicture:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "分享背景图数据导入模板.xlsx";
    		List<SharePicture> list = Lists.newArrayList(); 
    		new ExportExcel("分享背景图数据", SharePicture.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}