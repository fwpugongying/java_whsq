/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.sharepicture.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.sharepicture.entity.SharePicture;

/**
 * 分享背景图MAPPER接口
 * @author lixinapp
 * @version 2019-10-25
 */
@MyBatisMapper
public interface SharePictureMapper extends BaseMapper<SharePicture> {
	
}