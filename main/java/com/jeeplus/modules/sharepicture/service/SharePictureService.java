/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.sharepicture.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.sharepicture.entity.SharePicture;
import com.jeeplus.modules.sharepicture.mapper.SharePictureMapper;

/**
 * 分享背景图Service
 * @author lixinapp
 * @version 2019-10-25
 */
@Service
@Transactional(readOnly = true)
public class SharePictureService extends CrudService<SharePictureMapper, SharePicture> {

	public SharePicture get(String id) {
		return super.get(id);
	}
	
	public List<SharePicture> findList(SharePicture sharePicture) {
		return super.findList(sharePicture);
	}
	
	public Page<SharePicture> findPage(Page<SharePicture> page, SharePicture sharePicture) {
		return super.findPage(page, sharePicture);
	}
	
	@Transactional(readOnly = false)
	public void save(SharePicture sharePicture) {
		super.save(sharePicture);
	}
	
	@Transactional(readOnly = false)
	public void delete(SharePicture sharePicture) {
		super.delete(sharePicture);
	}
	
}