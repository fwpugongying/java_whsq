/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pddcategory.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.service.CrudService;
import com.jeeplus.modules.pddcategory.entity.PddCategory;
import com.jeeplus.modules.pddcategory.mapper.PddCategoryMapper;

/**
 * 拼多多分类Service
 * @author lixinapp
 * @version 2019-11-09
 */
@Service
@Transactional(readOnly = true)
public class PddCategoryService extends CrudService<PddCategoryMapper, PddCategory> {

	public PddCategory get(String id) {
		return super.get(id);
	}
	
	public List<PddCategory> findList(PddCategory pddCategory) {
		return super.findList(pddCategory);
	}
	
	public Page<PddCategory> findPage(Page<PddCategory> page, PddCategory pddCategory) {
		return super.findPage(page, pddCategory);
	}
	
	@Transactional(readOnly = false)
	public void save(PddCategory pddCategory) {
		super.save(pddCategory);
	}
	
	@Transactional(readOnly = false)
	public void delete(PddCategory pddCategory) {
		super.delete(pddCategory);
	}
	
}