/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pddcategory.mapper;

import com.jeeplus.core.persistence.BaseMapper;
import com.jeeplus.core.persistence.annotation.MyBatisMapper;
import com.jeeplus.modules.pddcategory.entity.PddCategory;

/**
 * 拼多多分类MAPPER接口
 * @author lixinapp
 * @version 2019-11-09
 */
@MyBatisMapper
public interface PddCategoryMapper extends BaseMapper<PddCategory> {
	
}