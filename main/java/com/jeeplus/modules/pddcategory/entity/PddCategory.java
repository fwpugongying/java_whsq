/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pddcategory.entity;

import javax.validation.constraints.NotNull;

import com.jeeplus.core.persistence.DataEntity;
import com.jeeplus.common.utils.excel.annotation.ExcelField;

/**
 * 拼多多分类Entity
 * @author lixinapp
 * @version 2019-11-09
 */
public class PddCategory extends DataEntity<PddCategory> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// 名称
	private String icon;		// 图标
	private Integer sort;		// 排序
	private String state;		// 状态 0正常 1停用
	
	public PddCategory() {
		super();
	}

	public PddCategory(String id){
		super(id);
	}

	@ExcelField(title="名称", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@ExcelField(title="图标", align=2, sort=2)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@NotNull(message="排序不能为空")
	@ExcelField(title="排序", align=2, sort=3)
	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}
	
	@ExcelField(title="状态 0正常 1停用", dictType="pdd_category_state", align=2, sort=4)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
}