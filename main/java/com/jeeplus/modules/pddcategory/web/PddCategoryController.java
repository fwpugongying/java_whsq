/**
 * Copyright &copy; 2015-2020 <a href="http://www.jeeplus.org/">JeePlus</a> All rights reserved.
 */
package com.jeeplus.modules.pddcategory.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;

import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.jeeplus.common.utils.DateUtils;
import com.jeeplus.common.config.Global;
import com.jeeplus.common.json.AjaxJson;
import com.jeeplus.core.persistence.Page;
import com.jeeplus.core.web.BaseController;
import com.jeeplus.common.utils.StringUtils;
import com.jeeplus.common.utils.excel.ExportExcel;
import com.jeeplus.common.utils.excel.ImportExcel;
import com.jeeplus.modules.pddcategory.entity.PddCategory;
import com.jeeplus.modules.pddcategory.service.PddCategoryService;

/**
 * 拼多多分类Controller
 * @author lixinapp
 * @version 2019-11-09
 */
@Controller
@RequestMapping(value = "${adminPath}/pddcategory/pddCategory")
public class PddCategoryController extends BaseController {

	@Autowired
	private PddCategoryService pddCategoryService;
	
	@ModelAttribute
	public PddCategory get(@RequestParam(required=false) String id) {
		PddCategory entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = pddCategoryService.get(id);
		}
		if (entity == null){
			entity = new PddCategory();
		}
		return entity;
	}
	
	/**
	 * 拼多多分类列表页面
	 */
	@RequiresPermissions("pddcategory:pddCategory:list")
	@RequestMapping(value = {"list", ""})
	public String list(PddCategory pddCategory, Model model) {
		model.addAttribute("pddCategory", pddCategory);
		return "modules/pddcategory/pddCategoryList";
	}
	
		/**
	 * 拼多多分类列表数据
	 */
	@ResponseBody
	@RequiresPermissions("pddcategory:pddCategory:list")
	@RequestMapping(value = "data")
	public Map<String, Object> data(PddCategory pddCategory, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<PddCategory> page = pddCategoryService.findPage(new Page<PddCategory>(request, response), pddCategory); 
		return getBootstrapData(page);
	}

	/**
	 * 查看，增加，编辑拼多多分类表单页面
	 */
	@RequiresPermissions(value={"pddcategory:pddCategory:view","pddcategory:pddCategory:add","pddcategory:pddCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(PddCategory pddCategory, Model model) {
		model.addAttribute("pddCategory", pddCategory);
		return "modules/pddcategory/pddCategoryForm";
	}

	/**
	 * 保存拼多多分类
	 */
	@ResponseBody
	@RequiresPermissions(value={"pddcategory:pddCategory:add","pddcategory:pddCategory:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public AjaxJson save(PddCategory pddCategory, Model model) throws Exception{
		AjaxJson j = new AjaxJson();
		/**
		 * 后台hibernate-validation插件校验
		 */
		String errMsg = beanValidator(pddCategory);
		if (StringUtils.isNotBlank(errMsg)){
			j.setSuccess(false);
			j.setMsg(errMsg);
			return j;
		}
		//新增或编辑表单保存
		pddCategoryService.save(pddCategory);//保存
		j.setSuccess(true);
		j.setMsg("保存拼多多分类成功");
		return j;
	}
	
	/**
	 * 删除拼多多分类
	 */
	@ResponseBody
	@RequiresPermissions("pddcategory:pddCategory:del")
	@RequestMapping(value = "delete")
	public AjaxJson delete(PddCategory pddCategory) {
		AjaxJson j = new AjaxJson();
		pddCategoryService.delete(pddCategory);
		j.setMsg("删除拼多多分类成功");
		return j;
	}
	
	/**
	 * 批量删除拼多多分类
	 */
	@ResponseBody
	@RequiresPermissions("pddcategory:pddCategory:del")
	@RequestMapping(value = "deleteAll")
	public AjaxJson deleteAll(String ids) {
		AjaxJson j = new AjaxJson();
		String idArray[] =ids.split(",");
		for(String id : idArray){
			pddCategoryService.delete(pddCategoryService.get(id));
		}
		j.setMsg("删除拼多多分类成功");
		return j;
	}
	
	/**
	 * 导出excel文件
	 */
	@ResponseBody
	@RequiresPermissions("pddcategory:pddCategory:export")
    @RequestMapping(value = "export")
    public AjaxJson exportFile(PddCategory pddCategory, HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "拼多多分类"+DateUtils.getDate("yyyyMMddHHmmss")+".xlsx";
            Page<PddCategory> page = pddCategoryService.findPage(new Page<PddCategory>(request, response, -1), pddCategory);
    		new ExportExcel("拼多多分类", PddCategory.class).setDataList(page.getList()).write(response, fileName).dispose();
    		j.setSuccess(true);
    		j.setMsg("导出成功！");
    		return j;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导出拼多多分类记录失败！失败信息："+e.getMessage());
		}
			return j;
    }

	/**
	 * 导入Excel数据

	 */
	@ResponseBody
	@RequiresPermissions("pddcategory:pddCategory:import")
    @RequestMapping(value = "import")
   	public AjaxJson importFile(@RequestParam("file")MultipartFile file, HttpServletResponse response, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			int successNum = 0;
			int failureNum = 0;
			StringBuilder failureMsg = new StringBuilder();
			ImportExcel ei = new ImportExcel(file, 1, 0);
			List<PddCategory> list = ei.getDataList(PddCategory.class);
			for (PddCategory pddCategory : list){
				try{
					pddCategoryService.save(pddCategory);
					successNum++;
				}catch(ConstraintViolationException ex){
					failureNum++;
				}catch (Exception ex) {
					failureNum++;
				}
			}
			if (failureNum>0){
				failureMsg.insert(0, "，失败 "+failureNum+" 条拼多多分类记录。");
			}
			j.setMsg( "已成功导入 "+successNum+" 条拼多多分类记录"+failureMsg);
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg("导入拼多多分类失败！失败信息："+e.getMessage());
		}
		return j;
    }
	
	/**
	 * 下载导入拼多多分类数据模板
	 */
	@ResponseBody
	@RequiresPermissions("pddcategory:pddCategory:import")
    @RequestMapping(value = "import/template")
     public AjaxJson importFileTemplate(HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		try {
            String fileName = "拼多多分类数据导入模板.xlsx";
    		List<PddCategory> list = Lists.newArrayList(); 
    		new ExportExcel("拼多多分类数据", PddCategory.class, 1).setDataList(list).write(response, fileName).dispose();
    		return null;
		} catch (Exception e) {
			j.setSuccess(false);
			j.setMsg( "导入模板下载失败！失败信息："+e.getMessage());
		}
		return j;
    }

}