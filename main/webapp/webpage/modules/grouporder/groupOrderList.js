<%@ page contentType="text/html;charset=UTF-8" %>
<script>
$(document).ready(function() {
	$('#groupOrderTable').bootstrapTable({
		 
		  //请求方法
               method: 'post',
               //类型json
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               //显示检索按钮
	       showSearch: true,
               //显示刷新按钮
               showRefresh: true,
               //显示切换手机试图按钮
               showToggle: true,
               //显示 内容列下拉框
    	       showColumns: true,
    	       //显示到处按钮
    	       showExport: true,
    	       //显示切换分页按钮
    	       showPaginationSwitch: true,
    	       //显示详情按钮
    	       detailView: true,
    	       	//显示详细内容函数
	           detailFormatter: "detailFormatter",
    	       //最低显示2行
    	       minimumCountColumns: 2,
               //是否显示行间隔色
               striped: true,
               //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）     
               cache: false,    
               //是否显示分页（*）  
               pagination: true,   
                //排序方式 
               sortOrder: "asc",  
               //初始化加载第一页，默认第一页
               pageNumber:1,   
               //每页的记录行数（*）   
               pageSize: 10,  
               //可供选择的每页的行数（*）    
               pageList: [10, 25, 50, 100],
               //这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据  
               url: "${ctx}/grouporder/groupOrder/data",
               //默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
               //queryParamsType:'',   
               ////查询参数,每次调用是会带上这个参数，可自定义                         
               queryParams : function(params) {
               	var searchParam = $("#searchForm").serializeJSON();
               	searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
               	searchParam.pageSize = params.limit === undefined? -1 : params.limit;
               	searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
                   return searchParam;
               },
               //分页方式：client客户端分页，server服务端分页（*）
               sidePagination: "server",
               contextMenuTrigger:"right",//pc端 按右键弹出菜单
               contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
               contextMenu: '#context-menu',
               onContextMenuItem: function(row, $el){
                   if($el.data("item") == "edit"){
                   		edit(row.id);
                   }else if($el.data("item") == "view"){
                       view(row.id);
                   } else if($el.data("item") == "delete"){
                        jp.confirm('确认要删除该拼团订单记录吗？', function(){
                       	jp.loading();
                       	jp.get("${ctx}/grouporder/groupOrder/delete?id="+row.id, function(data){
                   	  		if(data.success){
                   	  			$('#groupOrderTable').bootstrapTable('refresh');
                   	  			jp.success(data.msg);
                   	  		}else{
                   	  			jp.error(data.msg);
                   	  		}
                   	  	})
                   	   
                   	});
                      
                   } 
               },
              
               onClickRow: function(row, $el){
               },
               	onShowSearch: function () {
			$("#search-collapse").slideToggle();
		},
               columns: [{
		        checkbox: true
		       
		    }
            ,{
   		        field: 'id',
   		        title: '订单号',
   		        sortable: true,
   		        sortName: 'id'
   		        ,formatter:function(value, row , index){
   		        	value = jp.unescapeHTML(value);
   		        	return "<a href='javascript:view(\""+row.id+"\")'>"+value+"</a>";
   		         }
   		    }
			,{
		        field: 'member.phone',
		        title: '用户',
		        sortable: true,
		        sortName: 'member.phone'
		       
		    }
			/*,{
		        field: 'groupId',
		        title: '拼团',
		        sortable: true,
		        sortName: 'groupId'
		       
		    }*/
			/*,{
		        field: 'product.title',
		        title: '商品',
		        sortable: true,
		        sortName: 'product.title'
		       
		    }
			,{
		        field: 'skuId',
		        title: '规格',
		        sortable: true,
		        sortName: 'skuId'
		       
		    }*/
			<c:if test="${fns:getUser().isShop() == false}">
			,{
		        field: 'store.title',
		        title: '店铺',
		        sortable: true,
		        sortName: 'store.title'
		       
		    }
			</c:if>
			/*,{
		        field: 'productCode',
		        title: '商品编码',
		        sortable: true,
		        sortName: 'productCode'
		       
		    }*/
			,{
		        field: 'productTitle',
		        title: '商品名称',
		        sortable: true,
		        sortName: 'productTitle'
		       
		    }
			,{
				field: 'productIcon',
				title: '列表图',
				sortable: true,
				sortName: 'productIcon',
				formatter:function(value, row , index){
					if(value != null && value != ''){
						return '<img   onclick="jp.showPic(\''+value+'\')"'+' height="50px" src="'+value+'">';
					}
					return "";
				}

			}
			,{
		        field: 'skuName',
		        title: '规格名称',
		        sortable: true,
		        sortName: 'skuName'
		       
		    }
			,{
		        field: 'qty',
		        title: '数量',
		        sortable: true,
		        sortName: 'qty'
		       
		    }
			,{
		        field: 'price',
		        title: '价格',
		        sortable: true,
		        sortName: 'price'
		       
		    }
			,{
		        field: 'amount',
		        title: '订单总额',
		        sortable: true,
		        sortName: 'amount'
		       
		    }
			,{
		        field: 'freight',
		        title: '运费',
		        sortable: true,
		        sortName: 'freight'
		       
		    }
			<c:if test="${fns:getUser().isShop() == false}">
			,{
		        field: 'point',
		        title: '铜板',
		        sortable: true,
		        sortName: 'point'
		       
		    }
			</c:if>
			,{
		        field: 'discount',
		        title: '优惠金额',
		        sortable: true,
		        sortName: 'discount'
		       
		    }
			,{
				field: 'money',
				title: '结算金额',
				sortable: true,
				sortName: 'money'
					
			}
			/*,{
		        field: 'proxyType',
		        title: '申请代理',
		        sortable: true,
		        sortName: 'proxyType',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('order_proxy_type'))}, value, "-");
		        }
		       
		    }
			,{
		        field: 'proxyPrice',
		        title: '代理费用',
		        sortable: true,
		        sortName: 'proxyPrice'
		       
		    }*/
			,{
		        field: 'remarks',
		        title: '备注',
		        sortable: true,
		        sortName: 'remarks'
		       
		    }
			
			/*,{
		        field: 'receipt',
		        title: '发票抬头',
		        sortable: true,
		        sortName: 'receipt'
		       
		    }*/
			/*,{
		        field: 'area.name',
		        title: '代理区域',
		        sortable: true,
		        sortName: 'area.name'

		    }*/
			,{
		        field: 'username',
		        title: '收货人',
		        sortable: true,
		        sortName: 'username'

		    }
			,{
		        field: 'phone',
		        title: '收货电话',
		        sortable: true,
		        sortName: 'phone'

		    }
			,{
		        field: 'address',
		        title: '收货地址',
		        sortable: true,
		        sortName: 'address'

		    }
			,{
		        field: 'createDate',
		        title: '下单时间',
		        sortable: true,
		        sortName: 'createDate'
		       
		    }
			,{
		        field: 'payDate',
		        title: '付款时间',
		        sortable: true,
		        sortName: 'payDate'
		       
		    }
			,{
				field: 'pt_fwmoney',
				title: '拼团服务费',
				sortable: true,
				sortName: 'pt_fwmoney'
			}
			,{
				field: 'dlbt',
				title: '代理补贴',
				sortable: true,
				sortName: 'dlbt'
			}
			,{
		        field: 'payType',
		        title: '付款方式',
		        sortable: true,
		        sortName: 'payType',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('pay_type'))}, value, "-");
		        }
		       
		    }
			,{
		        field: 'state',
		        title: '订单状态',
		        sortable: true,
		        sortName: 'state',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('product_order_state'))}, value, "-");
		        }
		       
		    }
		,{
			field: 'groupId',
			title: '拼团id',
			sortable: true,
			sortName: 'groupId'
			,formatter:function(value, row , index){
				value = jp.unescapeHTML(value);
				return "<a href='javascript:view1(\""+row.groupId+"\")'>"+value+"</a>";
			}
		}
			/*,{
		        field: 'sendDate',
		        title: '发货时间',
		        sortable: true,
		        sortName: 'sendDate'
		       
		    }*/
			/*,{
		        field: 'expressCode',
		        title: '物流编码',
		        sortable: true,
		        sortName: 'expressCode'
		       
		    }*/
			/*,{
		        field: 'expressName',
		        title: '物流公司',
		        sortable: true,
		        sortName: 'expressName'
		       
		    }
			,{
		        field: 'expressNo',
		        title: '物流单号',
		        sortable: true,
		        sortName: 'expressNo'
		       
		    }
			,{
		        field: 'cancelDate',
		        title: '取消时间',
		        sortable: true,
		        sortName: 'cancelDate'
		       
		    }
			,{
		        field: 'expireDate',
		        title: '过期时间',
		        sortable: true,
		        sortName: 'expireDate'
		       
		    }
			,{
		        field: 'finishDate',
		        title: '收货时间',
		        sortable: true,
		        sortName: 'finishDate'
		       
		    }
			,{
		        field: 'refundDate',
		        title: '退款时间',
		        sortable: true,
		        sortName: 'refundDate'
		       
		    }
			,{
		        field: 'cancelReason',
		        title: '取消原因',
		        sortable: true,
		        sortName: 'cancelReason'
		       
		    }*/
			,{
				field: '',
				title: '操作',
				formatter:function(value, row , index){
					var result = "";
					if (row.state == '1') {
						result += "<button type='button' onclick='send(\""+row.id+"\")' class='btn btn-success btn-xs'>发货</button>";
					}else if(row.state == '2'){
						result += "<button type='button' onclick='deliveryInfo(\""+row.id+"\")' class='btn btn-info btn-xs'>查看物流</button>";
					}
					return result;
				}
			
			}
		     ]
		
		});
		
		  
	  if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端

		 
		  $('#groupOrderTable').bootstrapTable("toggleView");
		}
	  
	  $('#groupOrderTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $('#remove').prop('disabled', ! $('#groupOrderTable').bootstrapTable('getSelections').length);
            $('#view,#edit').prop('disabled', $('#groupOrderTable').bootstrapTable('getSelections').length!=1);
        });
		  
		$("#btnImport").click(function(){
			jp.open({
			    type: 2,
                area: [500, 200],
                auto: true,
			    title:"导入数据",
			    content: "${ctx}/tag/importExcel" ,
			    btn: ['下载模板','确定', '关闭'],
				btn1: function(index, layero){
					  jp.downloadFile('${ctx}/grouporder/groupOrder/import/template');
				  },
			    btn2: function(index, layero){
						var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
						iframeWin.contentWindow.importExcel('${ctx}/grouporder/groupOrder/import', function (data) {
							if(data.success){
								jp.success(data.msg);
								refresh();
							}else{
								jp.error(data.msg);
							}
						   jp.close(index);
						});//调用保存事件
						return false;
				  },
				 
				  btn3: function(index){ 
					  jp.close(index);
	    	       }
			}); 
		});
	  $("#export").click(function(){//导出Excel文件
	        var searchParam = $("#searchForm").serializeJSON();
	        searchParam.pageNo = 1;
	        searchParam.pageSize = -1;
            var sortName = $('#groupOrderTable').bootstrapTable("getOptions", "none").sortName;
            var sortOrder = $('#groupOrderTable').bootstrapTable("getOptions", "none").sortOrder;
            var values = "";
            for(var key in searchParam){
                values = values + key + "=" + searchParam[key] + "&";
            }
            if(sortName != undefined && sortOrder != undefined){
                values = values + "orderBy=" + sortName + " "+sortOrder;
            }

			jp.downloadFile('${ctx}/grouporder/groupOrder/export?'+values);
	  })
		$("#exporttoday").click(function(){//导出Excel文件
			var searchParam = $("#searchForm").serializeJSON();
			searchParam.pageNo = 1;
			searchParam.pageSize = -1;
			var sortName = $('#groupOrderTable').bootstrapTable("getOptions", "none").sortName;
			var sortOrder = $('#groupOrderTable').bootstrapTable("getOptions", "none").sortOrder;
			var values = "";
			for(var key in searchParam){
				values = values + key + "=" + searchParam[key] + "&";
			}
			if(sortName != undefined && sortOrder != undefined){
				values = values + "orderBy=" + sortName + " "+sortOrder;
			}

			jp.downloadFile('${ctx}/grouporder/groupOrder/exporttoday?'+values);
		})
	  $("#search").click("click", function() {// 绑定查询按扭
		  $('#groupOrderTable').bootstrapTable('refresh');
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
		  $("#searchForm  input").val("");
		  $("#searchForm  select").val("");
		   $("#searchForm  .select-item").html("");
		  $('#groupOrderTable').bootstrapTable('refresh');
		});
	 $('#beginCreateDate').datetimepicker({
		 format: "YYYY-MM-DD HH:mm:ss"
	});
	$('#endCreateDate').datetimepicker({
		 format: "YYYY-MM-DD HH:mm:ss"
	});
		
	});
		
  function getIdSelections() {
        return $.map($("#groupOrderTable").bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }
  
  function deleteAll(){

		jp.confirm('确认要删除该拼团订单记录吗？', function(){
			jp.loading();  	
			jp.get("${ctx}/grouporder/groupOrder/deleteAll?ids=" + getIdSelections(), function(data){
         	  		if(data.success){
         	  			$('#groupOrderTable').bootstrapTable('refresh');
         	  			jp.success(data.msg);
         	  		}else{
         	  			jp.error(data.msg);
         	  		}
         	  	})
          	   
		})
  }
  
    //刷新列表
  function refresh() {
      $('#groupOrderTable').bootstrapTable('refresh');
  }
  function add(){
	  jp.openSaveDialog('新增拼团订单', "${ctx}/grouporder/groupOrder/form",'800px', '500px');
  }
  
   function edit(id){//没有权限时，不显示确定按钮
       if(id == undefined){
	      id = getIdSelections();
	}
	jp.openSaveDialog('编辑拼团订单', "${ctx}/grouporder/groupOrder/form?id=" + id, '800px', '500px');
  }
	function view1(id){//没有权限时，不显示确定按钮
		if(id == undefined){
			id = getIdSelections();
		}
		jp.openViewDialog('查看我的拼团', "${ctx}/productgroup/productGroup/form?id=" + id, '1000px', '500px');
	}
  
 function view(id){//没有权限时，不显示确定按钮
      if(id == undefined){
             id = getIdSelections();
      }
        jp.openViewDialog('查看拼团订单', "${ctx}/grouporder/groupOrder/form?id=" + id, '800px', '500px');
 }
 function deliveryInfo(id){//物流动态
	 jp.openViewDialog('物流动态', "${ctx}/grouporder/groupOrder/deliveryInfo?id=" + id, '800px', '500px');
 }
//发货
 function send(id){
	 if(id == undefined){
		 id = getIdSelections();
	 }
	 jp.openSaveDialog('发货', "${ctx}/grouporder/groupOrder/sendForm?id=" + id, '800px', '500px');
 }
  
  
		   
  function detailFormatter(index, row) {
	  var htmltpl =  $("#groupOrderChildrenTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
	  var html = Mustache.render(htmltpl, {
			idx:row.id
		});
	  $.get("${ctx}/grouporder/groupOrder/detail?id="+row.id, function(groupOrder){
    	var groupOrderChild1RowIdx = 0, groupOrderChild1Tpl = $("#groupOrderChild1Tpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
		var data1 =  groupOrder.orderGiftList;
		for (var i=0; i<data1.length; i++){
			data1[i].dict = {};
			addRow('#groupOrderChild-'+row.id+'-1-List', groupOrderChild1RowIdx, groupOrderChild1Tpl, data1[i]);
			groupOrderChild1RowIdx = groupOrderChild1RowIdx + 1;
		}
				
      	  			
      })
     
        return html;
    }
  
	function addRow(list, idx, tpl, row){
		$(list).append(Mustache.render(tpl, {
			idx: idx, delBtn: true, row: row
		}));
	}
			
</script>
<script type="text/template" id="groupOrderChildrenTpl">//<!--
	<div class="tabs-container">
		<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-{{idx}}-1" aria-expanded="true">拼团订单赠品</a></li>
		</ul>
		<div class="tab-content">
				 <div id="tab-{{idx}}-1" class="tab-pane fade in active">
						<table class="ani table">
						<thead>
							<tr>
								<th>商品名称</th>
								<th>商品图片</th>
								<th>数量</th>
								<th>价格</th>
							</tr>
						</thead>
						<tbody id="groupOrderChild-{{idx}}-1-List">
						</tbody>
					</table>
				</div>
		</div>//-->
	</script>
	<script type="text/template" id="groupOrderChild1Tpl">//<!--
				<tr>
					<td>
						{{row.productTitle}}
					</td>
					<td>
						{{row.productIcon}}
					</td>
					<td>
						{{row.qty}}
					</td>
					<td>
						{{row.price}}
					</td>
				</tr>//-->
	</script>
