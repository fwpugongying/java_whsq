<%@ page contentType="text/html;charset=UTF-8" %>
<script>
$(document).ready(function() {
	$('#llbtTable').bootstrapTable({
		 
		  //请求方法
               method: 'post',
               //类型json
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               //显示检索按钮
	           showSearch: true,
               //显示刷新按钮
               showRefresh: true,
               //显示切换手机试图按钮
               showToggle: true,
               //显示 内容列下拉框
    	       showColumns: true,
    	       //显示到处按钮
    	       showExport: true,
    	       //显示切换分页按钮
    	       showPaginationSwitch: true,
    	       //最低显示2行
    	       minimumCountColumns: 2,
               //是否显示行间隔色
               striped: true,
               //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）     
               cache: false,    
               //是否显示分页（*）  
               pagination: true,   
                //排序方式 
               sortOrder: "asc",  
               //初始化加载第一页，默认第一页
               pageNumber:1,   
               //每页的记录行数（*）   
               pageSize: 10,  
               //可供选择的每页的行数（*）    
               pageList: [10, 25, 50, 100],
               //这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据  
               url: "${ctx}/llbt/llbt/data",
               //默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
               //queryParamsType:'',   
               ////查询参数,每次调用是会带上这个参数，可自定义                         
               queryParams : function(params) {
               	var searchParam = $("#searchForm").serializeJSON();
               	searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
               	searchParam.pageSize = params.limit === undefined? -1 : params.limit;
               	searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
                   return searchParam;
               },
               //分页方式：client客户端分页，server服务端分页（*）
               sidePagination: "server",
               contextMenuTrigger:"right",//pc端 按右键弹出菜单
               contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
               contextMenu: '#context-menu',
               onContextMenuItem: function(row, $el){
                   if($el.data("item") == "edit"){
                   		edit(row.id);
                   }else if($el.data("item") == "view"){
                       view(row.id);
                   } else if($el.data("item") == "delete"){
                        jp.confirm('确认要删除该流量补贴订单记录吗？', function(){
                       	jp.loading();
                       	jp.get("${ctx}/llbt/llbt/delete?id="+row.id, function(data){
                   	  		if(data.success){
                   	  			$('#llbtTable').bootstrapTable('refresh');
                   	  			jp.success(data.msg);
                   	  		}else{
                   	  			jp.error(data.msg);
                   	  		}
                   	  	})
                   	   
                   	});
                      
                   } 
               },
              
               onClickRow: function(row, $el){
               },
               	onShowSearch: function () {
			$("#search-collapse").slideToggle();
		},
               columns: [{
		        checkbox: true
		       
		    }
			,{
				field: 'member.phone',
		        title: '用户',
		        sortable: true,
		        sortName: 'member.phone'
		    }
			,{
		        field: 'orderno',
		        title: '订单号',
		        sortable: true,
		        sortName: 'orderno'
		       
		    }
			,{
		        field: 'goodsnum',
		        title: '商品数量',
		        sortable: true,
		        sortName: 'goodsnum'
		       
		    }
			,{
		        field: 'money',
		        title: '支付金额',
		        sortable: true,
		        sortName: 'money'
		       
		    }
			,{
		        field: 'createdate',
		        title: '申请时间',
		        sortable: true,
		        sortName: 'createdate'
		       
		    }
			,{
		        field: 'paydate',
		        title: '支付时间',
		        sortable: true,
		        sortName: 'paydate'
		       
		    }
			,{
		        field: 'finishdate',
		        title: '完成时间',
		        sortable: true,
		        sortName: 'finishdate'
		       
		    }
			,{
		        field: 'canceldate',
		        title: '取消时间',
		        sortable: true,
		        sortName: 'canceldate'
		       
		    }
			,{
		        field: 'state',
		        title: '状态',
		        sortable: true,
		        sortName: 'state',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('member_state'))}, value, "-");
		        }
		       
		    }
			,{
		        field: '',
		        title: '操作',
		        formatter:function(value, row , index){
		        	var result = "";
		        	<c:if test="${fns:hasPermission('llbt:llbt:edit')}">
						if(row.state == '0'){
							result += "<a href='javascript:updateState(\""+row.id+"\",1)' class=\"label label-default\">冻结</a>";
						} else if(row.state == '1'){
							result += "<a href='javascript:updateState(\""+row.id+"\",0)' class=\"label label-primary\">解冻</a>";
						} 

					</c:if>
					return result;
		        }
		       
		    }
		     ]
		
		});
		
		  
	  if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端

		 
		  $('#llbtTable').bootstrapTable("toggleView");
		}
	  
	  $('#llbtTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $('#remove').prop('disabled', ! $('#llbtTable').bootstrapTable('getSelections').length);
            $('#view,#edit').prop('disabled', $('#llbtTable').bootstrapTable('getSelections').length!=1);
        });
		  
		$("#btnImport").click(function(){
			jp.open({
			    type: 2,
                area: [500, 200],
                auto: true,
			    title:"导入数据",
			    content: "${ctx}/tag/importExcel" ,
			    btn: ['下载模板','确定', '关闭'],
				    btn1: function(index, layero){
					  jp.downloadFile('${ctx}/llbt/llbt/import/template');
				  },
			    btn2: function(index, layero){
				        var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
						iframeWin.contentWindow.importExcel('${ctx}/llbt/llbt/import', function (data) {
							if(data.success){
								jp.success(data.msg);
								refresh();
							}else{
								jp.error(data.msg);
							}
					   		jp.close(index);
						});//调用保存事件
						return false;
				  },
				 
				  btn3: function(index){ 
					  jp.close(index);
	    	       }
			}); 
		});
		
		
	 $("#export").click(function(){//导出Excel文件
	        var searchParam = $("#searchForm").serializeJSON();
	        searchParam.pageNo = 1;
	        searchParam.pageSize = -1;
            var sortName = $('#llbtTable').bootstrapTable("getOptions", "none").sortName;
            var sortOrder = $('#llbtTable').bootstrapTable("getOptions", "none").sortOrder;
            var values = "";
            for(var key in searchParam){
                values = values + key + "=" + searchParam[key] + "&";
            }
            if(sortName != undefined && sortOrder != undefined){
                values = values + "orderBy=" + sortName + " "+sortOrder;
            }

			jp.downloadFile('${ctx}/llbt/llbt/export?'+values);
	  })

		    
	  $("#search").click("click", function() {// 绑定查询按扭
		  $('#llbtTable').bootstrapTable('refresh');
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
		  $("#searchForm  input").val("");
		  $("#searchForm  select").val("");
		  $("#searchForm  .select-item").html("");
		  $('#llbtTable').bootstrapTable('refresh');
		});
		
		
	});
		
  function getIdSelections() {
        return $.map($("#llbtTable").bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }
  
  function deleteAll(){

		jp.confirm('确认要删除该流量补贴订单记录吗？', function(){
			jp.loading();  	
			jp.get("${ctx}/llbt/llbt/deleteAll?ids=" + getIdSelections(), function(data){
         	  		if(data.success){
         	  			$('#llbtTable').bootstrapTable('refresh');
         	  			jp.success(data.msg);
         	  		}else{
         	  			jp.error(data.msg);
         	  		}
         	  	})
          	   
		})
  }

    //刷新列表
  function refresh(){
  	$('#llbtTable').bootstrapTable('refresh');
  }
  
   function add(){
	  jp.openSaveDialog('新增流量补贴订单', "${ctx}/llbt/llbt/form",'800px', '500px');
  }
   function edit(id){//没有权限时，不显示确定按钮
       if(id == undefined){
	      id = getIdSelections();
	}
	jp.openSaveDialog('编辑流量补贴订单', "${ctx}/llbt/llbt/form?id=" + id, '800px', '500px');
  }

 function view(id){//没有权限时，不显示确定按钮
      if(id == undefined){
             id = getIdSelections();
      }
        jp.openViewDialog('查看流量补贴订单', "${ctx}/llbt/llbt/form?id=" + id, '800px', '500px');
 }
 function updateState(id,type){
	 var title = "";
	 if(type=='1'){
		 title="确认要冻结该流量补贴吗？";
	 }else{
		 title="确认要解冻该流量补贴吗？";
	 }
	 jp.confirm(title, function(){
		 jp.loading();  	
		 jp.get("${ctx}/llbt/llbt/updateState?id=" + id + "&type=" + type, function(data){
			 if(data.success){
				 jp.success(data.msg);
			 }else{
				 jp.error(data.msg);
			 }
			 $('#llbtTable').bootstrapTable('refresh');
		 })
		 
	 })
 }

</script>