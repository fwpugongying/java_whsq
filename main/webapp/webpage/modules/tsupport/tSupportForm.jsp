<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>扶贫信息管理管理</title>
	<meta name="decorator" content="ani"/>
	<!-- SUMMERNOTE -->
	<%@include file="/webpage/include/summernote.jsp" %>
	<script type="text/javascript">

		$(document).ready(function() {

					//富文本初始化
			$('#content').summernote({
				height: 300,
                lang: 'zh-CN',
                callbacks: {
                    onChange: function(contents, $editable) {
                        $("input[name='content']").val($('#content').summernote('code'));//取富文本的值
                    },
                    onImageUpload: function(files, editor, $editable) {
		            	for(var i = 0; i < files.length; i++) {
		                     sendFile(files[i], this);
		            	}
		            }
                }
            });
		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/tsupport/tSupport/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="tSupport" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>标题：</label></td>
					<td class="width-35" colspan="3">
						<form:input path="title" htmlEscape="false" maxlength="200"   class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>区县：</label></td>
					<td class="width-35">
						<sys:treeselect id="area" name="area.id" value="${tSupport.area.id}" labelName="area.name" labelValue="${tSupport.area.name}"
							title="区域" url="/sys/area/treeData" cssClass="form-control required" allowClear="true" notAllowSelectParent="true"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>图片：</label></td>
					<td class="width-35">
						<sys:fileUpload path="image"  value="${tSupport.image}" type="image" fileNumLimit="1" uploadPath="/tSupport/tSupport"/>
					</td>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">视频：</label></td>
					<td class="width-35">
						<sys:fileUpload path="video"  value="${tSupport.video}" type="video" fileNumLimit="1" uploadPath="/tSupport/tSupport"/>
					</td>
					<td class="width-15 active"><label class="pull-right">视频封面图：</label></td>
					<td class="width-35">
						<sys:fileUpload path="videoImage"  value="${tSupport.videoImage}" type="image" fileNumLimit="1" uploadPath="/tSupport/tSupport"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>内容：</label></td>
					<td class="width-35" colspan="3">
                        <input type="hidden" name="content" value=" ${tSupport.content}"/>
						<div id="content">
                          ${fns:unescapeHtml(tSupport.content)}
                        </div>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>