<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/webpage/include/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fns" uri="/WEB-INF/tlds/fns.tld" %>
<%@ taglib prefix="sys" tagdir="/WEB-INF/tags/sys" %>
<!doctype html>
<html class="m">
<head>
<meta charset="utf-8">
<meta http-equiv="x-dns-prefetch-control" content="on">
<meta name="viewport" content="initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui">
<meta http-equiv="Cache-Control" content="no-siteapp"/>
<meta name="renderer" content="webkit">
<title>${title}</title>
<!-- head通用模块  -->
<script src="${ctxStatic}/common/js/vendor.js"></script><!-- 该插件已经集成jquery 无需重复引入 -->
<!-- 引入自定义文件 -->
</head>
<style type="text/css">
	img {
			display: block;
		}
	table {
		width:100%;
		word-wrap:break-word; 
		word-break:break-all;
	}
 	 table{border-right:1px solid #000;border-bottom:1px solid #000} 
	 table td{border-left:1px solid #000;border-top:1px solid #000}  
</style>
<body style="text-align: left;margin:5px;">
${fns:unescapeHtml(content)}
</body>
<script type="text/javascript">
	//$(document).ready(function(){$("img").addClass('img-responsive');});
 	$(function() {
		 var imglist =document.getElementsByTagName("img");
		 var videolist =document.getElementsByTagName("video");
		 //安卓4.0+等高版本不支持window.screen.width，安卓2.3.3系统支持
		 var _width;
		 doDraw();
		 
		 window.onresize = function(){
		 	//捕捉屏幕窗口变化，始终保证图片根据屏幕宽度合理显示
		 	doDraw();
		 }
	
		 function doDraw(){
		 	_width = window.innerWidth-10;
		 	for( var i = 0, len = imglist.length; i < len; i++){
		 		DrawImage(imglist[i],_width);
		 	}
		 	
		 	for( var i = 0, len = videolist.length; i < len; i++){
		 		DrawVideo(videolist[i],_width);
		 	}
		 	
		 }
	
		 function DrawImage(ImgD,_width){ 
		 	var image=new Image(); 
		 	image.src=ImgD.src; 
		 	image.onload = function(){
		 		ImgD.style.width="";
		 		ImgD.style.height="";
		 		if(image.width>30 && image.height>30){ 
		 			if(image.width>_width){
		 				ImgD.width=_width; 
		 				ImgD.height=(image.height*_width)/image.width; 
		 			}else{ 
		 				ImgD.width=_width; 
		 				ImgD.height=(image.height*_width)/image.width; 
		 			} 
		 		}	
		 	}
		 }
		 function DrawVideo(videoD,_width){ 
		 	var video=document.createElement("VIDEO"); 
		 	video.src=videoD.src; 
		 	video.width=videoD.width;
		 	video.height=videoD.height;
		 	console.log("屏幕宽度：" + _width);
		 	console.log("视频宽度：" + videoD.width);
		 	console.log("视频高度：" + videoD.height);
		 	if(video.width>=_width){
		 		videoD.width=_width; 
		 		if(video.width>video.height){
	 				videoD.height=(video.height*_width)/video.width; 
			 	}else{
			 		videoD.height=videoD.height/(video.width/videoD.width);
			 	}
		 	}else{
		 		videoD.width=_width; 
		 		videoD.height=_width/video.width*video.height;
		 	}
		 	
	 		/* if(video.width>30 && video.height>30){ 
	 			if(video.width>_width){
	 				videoD.width=_width; 
	 				videoD.height=(video.height*_width)/video.width;
	 			} else{ 
	 				videoD.width=_width; 
	 				videoD.height=(video.height*_width)/video.width; 
	 			}
	 		}  */	
		 }
	}) 
</script>

</html>