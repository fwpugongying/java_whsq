<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="initial-scale=1.0, user-scalable=no, width=device-width">
<title>高德地图</title>
<link rel="stylesheet"
	href="https://cache.amap.com/lbs/static/main1119.css" />
<script type="text/javascript"
	src="https://webapi.amap.com/maps?v=1.4.12&key=2d5ab03e1d37bc82679b9889d048b013&plugin=AMap.Geocoder"></script>
<script type="text/javascript"
	src="https://cache.amap.com/lbs/static/addToolbar.js"></script>
</head>
<body>
	<div id="container"></div>
	<script type="text/javascript">
		var lon = '${lon}';
		var lat = '${lat}';
		var infoWindow;
		var addressDetail = new Object();
		var map;
		var marker;
		if(lon != '' && lat != '') {
			map = new AMap.Map("container", {
				resizeEnable : true,
				center : [ lon, lat ],
				zoom : 13
			});
		} else {
			 map = new AMap.Map('container', {
			        resizeEnable: true
			    });
			 var lnglat = new String(map.getCenter());
			 lon = lnglat.split(",")[0];
			 lat = lnglat.split(",")[1];
		}
		 var marker = new AMap.Marker({
			position : new AMap.LngLat(lon, lat)

		}); 
		map.add(marker);

		map.on('click', function(e) {
			map.remove(marker);
			lon = e.lnglat.getLng();
			lat = e.lnglat.getLat();
			marker = new AMap.Marker({
				position : new AMap.LngLat(lon, lat)
			});
			map.add(marker);
			AMap.event.addListener(marker, 'click', function() {
				openInfo();
			});
			openInfo();
		});

		AMap.event.addListener(marker, 'click', function() {
			openInfo();
		});
		openInfo();

		//在指定位置打开信息窗体
		function openInfo() {
			var geocoder = new AMap.Geocoder({
				radius : 1000,
				extensions : "all"
			});
			geocoder.getAddress([ lon, lat ], function(status, result) {
				if (status === 'complete' && result.info === 'OK') {
					//构建信息窗体中显示的内容
					var info = [];
					info.push("<div>");
					info.push("坐标 : " + lon + "," + lat);
					info.push("地址 :" + result.regeocode.formattedAddress
							+ "</div>");
					infoWindow = new AMap.InfoWindow({
						content : info.join("<br/>")
					//使用默认信息窗体框样式，显示信息内容
					});
					infoWindow.open(map, new AMap.LngLat(lon, lat));
					addressDetail = result.regeocode.addressComponent;
				}
			});
		}

		// 获取百度位置信息，经纬度与文本信息  
		function getBaidullData() {
			var ptlng = lon;
			var ptlat = lat;
			// 判断经纬度是否为空 
			if (ptlng === "" || ptlat === "") {
				return null;
			}
			var llData = new Object();
			llData.lng = ptlng;
			llData.lat = ptlat;
			llData.province = addressDetail.province;
			llData.city = addressDetail.city == '' ? addressDetail.province : addressDetail.city ;
			llData.town = addressDetail.district;
			return llData;
		}
	</script>
	<script type="text/javascript"
		src="https://webapi.amap.com/demos/js/liteToolbar.js"></script>
</body>
</html>