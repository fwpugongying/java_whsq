<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>实体店优惠券管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

	        $('#startDate').datetimepicker({
				 format: "YYYY-MM-DD"
		    });
	        $('#endDate').datetimepicker({
				 format: "YYYY-MM-DD"
		    });
		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/shopcoupon/shopCoupon/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="shopCoupon" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>店铺：</label></td>
					<td class="width-35" colspan="3">
						<sys:gridselect url="${ctx}/shop/shop/data" disabled="disabled" id="shop" name="shop.id" value="${shopCoupon.shop.id}" labelName="shop.title" labelValue="${shopCoupon.shop.title}"
							 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>售价：</label></td>
					<td class="width-35">
						<form:input path="price" htmlEscape="false"    class="form-control required isFloatGtZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>抵用金额：</label></td>
					<td class="width-35">
						<form:input path="money" htmlEscape="false"    class="form-control required isFloatGtZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>结算价：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>铜板：</label></td>
					<td class="width-35">
						<form:input path="point" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>有效起始日期：</label></td>
					<td class="width-35">
						<div class='input-group form_datetime' id='startDate'>
							<input type='text'  name="startDate" class="form-control required"  value="<fmt:formatDate value="${shopCoupon.startDate}" pattern="yyyy-MM-dd"/>"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>有效截止日期：</label></td>
					<td class="width-35">
						<div class='input-group form_datetime' id='endDate'>
							<input type='text'  name="endDate" class="form-control required"  value="<fmt:formatDate value="${shopCoupon.endDate}" pattern="yyyy-MM-dd"/>"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>编码：</label></td>
					<td class="width-35">
						<form:input path="code" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>状态：</label></td>
					<td class="width-35">
						<form:select path="state" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('shopcoupon_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">使用规则：</label></td>
					<td class="width-35" colspan="3">
						<form:textarea path="content" htmlEscape="false" rows="4"    class="form-control "/>
					</td>
				</tr>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>审核状态：</label></td>
					<td class="width-35">
						<form:select path="audit" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('shopcoupon_audit')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"></td>
		   			<td class="width-35" ></td>
		  		</tr> --%>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>