<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<meta charset="utf-8">
	<title>ECharts</title>
	<meta name="decorator" content="ani"/>
	<link rel="stylesheet" href="${ctxStatic}/common/css/vendor.css" />
	<script src="${ctxStatic}/common/js/vendor.js"></script>
	<!-- 引入 echarts.js -->
	<%@ include file="/webpage/include/echarts.jsp"%>
</head>
<body class="bg-white">
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="width: 100%;height: 50%;margin-top:20px;"></div>

<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;">
	<table class="table">
	   <thead>
	      <tr>
	         <th>注册地区</th>
	         <th>会员数量</th>
	      </tr>
	   </thead>
	   <tbody id="memberList">
	   </tbody>
	</table>
</div>
<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;">
	<div class="home-stats">
		<a href="javascript:;" class="stat hvr-wobble-horizontal">
			<div class=" stat-icon">
				<i class="fa fa-users fa-4x text-info "></i>
			</div>
			<div class=" stat-label">
				<div id="totalCount" class="label-header"></div>
				<div class="clearfix stat-detail">
					<div class="label-body">
						<i class=" pull-right text-muted"></i>平台会员总数
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="home-stats">
		<a href="javascript:;" class="stat hvr-wobble-horizontal">
			<div class=" stat-icon">
				<i class="fa fa-list-alt fa-4x text-warning "></i>
			</div>
			<div class=" stat-label">
				<div id="todayOrders" class="label-header"></div>
				<div class="clearfix stat-detail">
					<div class="label-body">
						<i class=" pull-right text-muted"></i>新注册会员成交数
					</div>
				</div>
			</div>
		</a>
	</div>
</div>
		
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例4
    var myChart = echarts.init(document.getElementById('main'));
    window.onresize = myChart.resize;
    $(function () {
        jp.get("${ctx}${dataURL}", function (result) {
        	if (result.result == '0') {
	            // 指定图表的配置项和数据
	            // 使用刚指定的配置项和数据显示图表。
	            myChart.setOption(result.option);
	            $("#todayOrders").html(result.todayOrders);
	            $("#totalCount").html(parseInt(result.totalCount)+6500000);
	            
	            var s = "";
	            if(result.memberList != null){
	            	for(var i=0;i<result.memberList.length;i++){
	            		s += "<tr><td>"+result.memberList[i].area+"</td><td>"+(parseInt(result.memberList[i].count)+20000)+"</td></tr>";
	            	}
	            }
	            $("#memberList").html(s);
        	} else {
        		jp.warning(result.msg);
        	}
        });
        
        $("#search").click("click", function() {// 绑定查询按扭
        	var userId = $("#userId").val();
        	jp.get("${ctx}${dataURL}?userId="+userId, function (result) {
        		if (result.result == '0') {
    	            // 指定图表的配置项和数据
    	            // 使用刚指定的配置项和数据显示图表。
    	            myChart.setOption(result.option,true);
            	} else {
            		jp.warning(result.msg);
            	}
            });
  		});
  	 
  	 $("#reset").click("click", function() {// 绑定查询按扭
	  		$("select").val("");
	  		$("input").val("");
  		});
    })


</script>
</body>
</html>