<%@ page contentType="text/html;charset=UTF-8" %>
<script>
$(document).ready(function() {
	$('#groupTable').bootstrapTable({
		 
		  //请求方法
               method: 'post',
               //类型json
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               //显示检索按钮
	           showSearch: true,
               //显示刷新按钮
               showRefresh: true,
               //显示切换手机试图按钮
               showToggle: true,
               //显示 内容列下拉框
    	       showColumns: true,
    	       //显示到处按钮
    	       showExport: true,
    	       //显示切换分页按钮
    	       showPaginationSwitch: true,
    	       //最低显示2行
    	       minimumCountColumns: 2,
               //是否显示行间隔色
               striped: true,
               //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）     
               cache: false,    
               //是否显示分页（*）  
               pagination: true,   
                //排序方式 
               sortOrder: "asc",  
               //初始化加载第一页，默认第一页
               pageNumber:1,   
               //每页的记录行数（*）   
               pageSize: 10,  
               //可供选择的每页的行数（*）    
               pageList: [10, 25, 50, 100],
               //这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据  
               url: "${ctx}/group/group/data",
               //默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
               //queryParamsType:'',   
               ////查询参数,每次调用是会带上这个参数，可自定义                         
               queryParams : function(params) {
               	var searchParam = $("#searchForm").serializeJSON();
               	searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
               	searchParam.pageSize = params.limit === undefined? -1 : params.limit;
               	searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
                   return searchParam;
               },
               //分页方式：client客户端分页，server服务端分页（*）
               sidePagination: "server",
               contextMenuTrigger:"right",//pc端 按右键弹出菜单
               contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
               contextMenu: '#context-menu',
               onContextMenuItem: function(row, $el){
                   if($el.data("item") == "edit"){
                   		edit(row.id);
                   }else if($el.data("item") == "view"){
                       view(row.id);
                   } else if($el.data("item") == "delete"){
                        jp.confirm('确认要删除该团长记录吗？', function(){
                       	jp.loading();
                       	jp.get("${ctx}/group/group/delete?id="+row.id, function(data){
                   	  		if(data.success){
                   	  			$('#groupTable').bootstrapTable('refresh');
                   	  			jp.success(data.msg);
                   	  		}else{
                   	  			jp.error(data.msg);
                   	  		}
                   	  	})
                   	   
                   	});
                      
                   } 
               },
              
               onClickRow: function(row, $el){
               },
               	onShowSearch: function () {
			$("#search-collapse").slideToggle();
		},
               columns: [{
		        checkbox: true
		       
		    }
			,{
				field: 'member.phone',
		        title: '用户',
		        sortable: true,
		        sortName: 'member.phone'
		    }
		    ,{
		        field: 'icon',
		        title: 'LOGO',
		        sortable: true,
		        sortName: 'icon',
		        formatter:function(value, row , index){
		        	if(value != null && value != ''){
		        		return '<img   onclick="jp.showPic(\''+value+'\')"'+' height="50px" src="'+value+'">';
		        	}
		        	return "";
		        }
		       
		    }
			,{
		        field: 'province',
		        title: '省',
		        sortable: true,
		        sortName: 'province'
		       
		    }
		   ,{
			   field: 'city',
			   title: '城市',
			   sortable: true,
			   sortName: 'city'

		   },{
			   field: 'area',
			   title: '县区',
			   sortable: true,
			   sortName: 'area'

		   }
			,{
		        field: 'address',
		        title: '地址',
		        sortable: true,
		        sortName: 'address'
		       
		    }
			,{
		        field: 'username',
		        title: '联系人',
		        sortable: true,
		        sortName: 'username'
		       
		    }
			,{
		        field: 'phone',
		        title: '手机号',
		        sortable: true,
		        sortName: 'phone'
		       
		    }
			,{
		        field: 'createDate',
		        title: '入驻时间',
		        sortable: true,
		        sortName: 'createDate'
		       
		    }
			,{
		        field: 'state',
		        title: '状态',
		        sortable: true,
		        sortName: 'state',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('member_state'))}, value, "-");
		        }
		       
		    }
			,{
		        field: 'auditState',
		        title: '审核状态',
		        sortable: true,
		        sortName: 'auditState',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('shop_audit'))}, value, "-");
		        }
		       
		    }
			,{
		        field: 'auditDate',
		        title: '审核时间',
		        sortable: true,
		        sortName: 'auditDate'
		       
		    }
			,{
		        field: 'remarks',
		        title: '备注',
		        sortable: true,
		        sortName: 'remarks'
		       
		    }
			,{
		        field: 'balance',
		        title: '余额',
		        sortable: true,
		        sortName: 'balance'
		       
		    }
			,{
		        field: '',
		        title: '操作',
		        formatter:function(value, row , index){
		        	var result = "";
		        	<c:if test="${fns:hasPermission('group:group:edit')}">
					if (row.auditState == '1'||row.auditState == '0') {
						result += "<a href='javascript:audit(\""+row.id+"\",2)' class=\"label label-success\">同意入驻</a>";
						result += "<a href='javascript:audit(\""+row.id+"\",3)' class=\"label label-danger\">拒绝入驻</a>";
					} else {

						if(row.state == '0'){
							result += "<a href='javascript:updateState(\""+row.id+"\",1)' class=\"label label-default\">冻结</a>";
						} else if(row.state == '1'){
							result += "<a href='javascript:updateState(\""+row.id+"\",0)' class=\"label label-primary\">解冻</a>";
						} 
					}
					
					</c:if>
					return result;
		        }
		       
		    }
		     ]
		
		});
		
		  
	  if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端

		 
		  $('#groupTable').bootstrapTable("toggleView");
		}
	  
	  $('#groupTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $('#remove').prop('disabled', ! $('#groupTable').bootstrapTable('getSelections').length);
            $('#view,#edit').prop('disabled', $('#groupTable').bootstrapTable('getSelections').length!=1);
        });
		  
		$("#btnImport").click(function(){
			jp.open({
			    type: 2,
                area: [500, 200],
                auto: true,
			    title:"导入数据",
			    content: "${ctx}/tag/importExcel" ,
			    btn: ['下载模板','确定', '关闭'],
				    btn1: function(index, layero){
					  jp.downloadFile('${ctx}/group/group/import/template');
				  },
			    btn2: function(index, layero){
				        var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
						iframeWin.contentWindow.importExcel('${ctx}/group/group/import', function (data) {
							if(data.success){
								jp.success(data.msg);
								refresh();
							}else{
								jp.error(data.msg);
							}
					   		jp.close(index);
						});//调用保存事件
						return false;
				  },
				 
				  btn3: function(index){ 
					  jp.close(index);
	    	       }
			}); 
		});
		
		
	 $("#export").click(function(){//导出Excel文件
	        var searchParam = $("#searchForm").serializeJSON();
	        searchParam.pageNo = 1;
	        searchParam.pageSize = -1;
            var sortName = $('#groupTable').bootstrapTable("getOptions", "none").sortName;
            var sortOrder = $('#groupTable').bootstrapTable("getOptions", "none").sortOrder;
            var values = "";
            for(var key in searchParam){
                values = values + key + "=" + searchParam[key] + "&";
            }
            if(sortName != undefined && sortOrder != undefined){
                values = values + "orderBy=" + sortName + " "+sortOrder;
            }

			jp.downloadFile('${ctx}/group/group/export?'+values);
	  })

		    
	  $("#search").click("click", function() {// 绑定查询按扭
		  $('#groupTable').bootstrapTable('refresh');
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
		  $("#searchForm  input").val("");
		  $("#searchForm  select").val("");
		  $("#searchForm  .select-item").html("");
		  $('#groupTable').bootstrapTable('refresh');
		});
		
		
	});
		
  function getIdSelections() {
        return $.map($("#groupTable").bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }
  
  function deleteAll(){

		jp.confirm('确认要删除该团长记录吗？', function(){
			jp.loading();  	
			jp.get("${ctx}/group/group/deleteAll?ids=" + getIdSelections(), function(data){
         	  		if(data.success){
         	  			$('#groupTable').bootstrapTable('refresh');
         	  			jp.success(data.msg);
         	  		}else{
         	  			jp.error(data.msg);
         	  		}
         	  	})
          	   
		})
  }

    //刷新列表
  function refresh(){
  	$('#groupTable').bootstrapTable('refresh');
  }
  
   function add(){
	  jp.openSaveDialog('新增团长', "${ctx}/group/group/form",'800px', '500px');
  }


  
   function edit(id){//没有权限时，不显示确定按钮
       if(id == undefined){
	      id = getIdSelections();
	}
	jp.openSaveDialog('编辑团长', "${ctx}/group/group/form?id=" + id, '800px', '500px');
  }
  
 function view(id){//没有权限时，不显示确定按钮
      if(id == undefined){
             id = getIdSelections();
      }
        jp.openViewDialog('查看团长', "${ctx}/group/group/form?id=" + id, '800px', '500px');
 }

 function audit(id,type){
	  var title = "";
	  if(type=='2'){
		  title="确认要通过该入驻审核吗？";
	  }else{
		  title="确认要拒绝该入驻审核吗？";
	  }
	  jp.confirm(title, function(){
		  if(type=='2'){
			  jp.loading();  	
			  jp.get("${ctx}/group/group/audit?id=" + id + "&type=" + type, function(data){
				  if(data.success){
					  jp.success(data.msg);
				  }else{
					  jp.error(data.msg);
				  }
				  $('#groupTable').bootstrapTable('refresh');
			  })
		  } else {
			  jp.prompt("备注", function(text) {
				  jp.loading();  	
				  jp.get("${ctx}/group/group/audit?id=" + id + "&type=" + type + "&reason=" + text, function(data){
					  if(data.success){
						  jp.success(data.msg);
					  }else{
						  jp.error(data.msg);
					  }
					  $('#groupTable').bootstrapTable('refresh');
				  })
				});
		  }
	  })
 }
 function updateState(id,type){
	 var title = "";
	 if(type=='1'){
		 title="确认要冻结该团长信息吗？";
	 }else{
		 title="确认要解冻该团长信息吗？";
	 }
	 jp.confirm(title, function(){
		 jp.loading();  	
		 jp.get("${ctx}/group/group/updateState?id=" + id + "&type=" + type, function(data){
			 if(data.success){
				 jp.success(data.msg);
			 }else{
				 jp.error(data.msg);
			 }
			 $('#groupTable').bootstrapTable('refresh');
		 })
		 
	 })
 }

</script>