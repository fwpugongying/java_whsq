<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>团长管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
			var isValidate = jp.validateForm('#inputForm');//校验表单
			if(!isValidate){
				return false;
			}else{
				jp.loading();
				jp.post("${ctx}/group/group/save",$('#inputForm').serialize(),function(data){
					if(data.success){
						jp.getParent().refresh();
						var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
						parent.layer.close(dialogIndex);
						jp.success(data.msg)

					}else{
						jp.error(data.msg);
					}
				})
			}

		}
	</script>
</head>
<body class="bg-white">
<form:form id="inputForm" modelAttribute="group" class="form-horizontal">
	<form:hidden path="id"/>
	<table class="table table-bordered">
		<tbody>
		<tr>
			<td class="width-15 active"><label class="pull-right"><font color="red">*</font>用户：</label></td>
			<td class="width-35">
				<sys:gridselect disabled="disabled" url="${ctx}/member/member/data" id="member" name="member.id" value="${group.member.id}" labelName="member.phone" labelValue="${group.member.phone}"
								title="选择用户" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
			</td>
				<%--					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商户名称：</label></td>--%>
				<%--					<td class="width-35">--%>
				<%--						<form:input path="title" htmlEscape="false"    class="form-control required"/>--%>
				<%--					</td>--%>
		</tr>
		<tr>
			<td class="width-15 active"><label class="pull-right"><font color="red">*</font>城市：</label></td>
			<td class="width-35">
				<form:input path="city" htmlEscape="false"    class="form-control required"/>
			</td>
			<td class="width-15 active"><label class="pull-right"><font color="red">*</font>地址：</label></td>
			<td class="width-35">
				<form:input path="address" htmlEscape="false"    class="form-control required"/>
			</td>
		</tr>
		<tr>
			<td class="width-15 active"><label class="pull-right"><font color="red">*</font>联系人：</label></td>
			<td class="width-35">
				<form:input path="username" htmlEscape="false"    class="form-control required"/>
			</td>
			<td class="width-15 active"><label class="pull-right"><font color="red">*</font>手机号：</label></td>
			<td class="width-35">
				<form:input path="phone" htmlEscape="false"    class="form-control required"/>
			</td>
		</tr>
		<tr>
			<td class="width-15 active"><label class="pull-right"><font color="red">*</font>状态：</label></td>
			<td class="width-35">
				<form:select path="state" class="form-control required">
					<form:options items="${fns:getDictList('member_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</td>
			<td class="width-15 active"><label class="pull-right"><font color="red">*</font>审核状态：</label></td>
			<td class="width-35">
				<form:select path="auditState" class="form-control required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('shop_audit')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</td>
		</tr>
		<tr>
			<td class="width-15 active"><label class="pull-right">通过或者冻结原因：</label></td>
			<td class="width-35" colspan="3">
				<form:textarea path="content" htmlEscape="false" rows="4"    class="form-control "/>
			</td>
		</tr>
		</tbody>
	</table>
</form:form>
</body>
</html>