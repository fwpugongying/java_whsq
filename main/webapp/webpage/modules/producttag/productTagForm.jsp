<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>商品标签管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/producttag/productTag/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="productTag" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商品：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/product/product/data" id="product" name="product.id" value="${productTag.product.id}" labelName="product.title" labelValue="${productTag.product.title}"
							 title="选择商品" cssClass="form-control required" fieldLabels="商品名称" fieldKeys="title" searchLabels="商品名称" searchKeys="title" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>标签：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/tags/tags/data" id="tags" name="tags.id" value="${productTag.tags.id}" labelName="tags.title" labelValue="${productTag.tags.title}"
							 title="选择标签" cssClass="form-control required" fieldLabels="标签" fieldKeys="title" searchLabels="标签" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>