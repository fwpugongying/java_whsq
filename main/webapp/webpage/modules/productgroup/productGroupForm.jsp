<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>商品拼团/拼购管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function () {

		});

		function save() {
			var isValidate = jp.validateForm('#inputForm');//校验表单
			if (!isValidate) {
				return false;
			} else {
				jp.loading();
				jp.post("${ctx}/productgroup/productGroup/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="productGroup" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>发起人：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/member/member/data" id="member" name="member.id" value="${productGroup.member.id}" labelName="member.phone" labelValue="${productGroup.member.phone}"
							 title="选择发起人" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商品：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/product/product/data" id="product" name="product.id" value="${productGroup.product.id}" labelName="product.title" labelValue="${productGroup.product.title}"
							 title="选择商品" cssClass="form-control required" fieldLabels="商品名称" fieldKeys="title" searchLabels="商品名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>