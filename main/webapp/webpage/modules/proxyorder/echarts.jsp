<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<meta charset="utf-8">
	<title>ECharts</title>
	<meta name="decorator" content="ani"/>
	<link rel="stylesheet" href="${ctxStatic}/common/css/vendor.css" />
	<script src="${ctxStatic}/common/js/vendor.js"></script>
	<!-- 引入 echarts.js -->
	<%@ include file="/webpage/include/echarts.jsp"%>
</head>
<body class="bg-white">
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="width: 100%;height: 50%;margin-top:20px;"></div>

<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;">
	<button id="export" class="btn btn-warning">
			<i class="fa fa-file-excel-o"></i> 导出
	</button>
	<table class="table">
	   <thead>
	      <tr>
	         <th>代理地区</th>
	         <th>代理数量</th>
	      </tr>
	   </thead>
	   <tbody id="proxyList">
	   </tbody>
	</table>
</div>
<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;">
	<div class="home-stats">
		<a href="javascript:;" class="stat hvr-wobble-horizontal">
			<div class=" stat-icon">
				<i class="fa fa-list-alt fa-4x text-warning "></i>
			</div>
			<div class=" stat-label">
				<div id="totalProxys" class="label-header"></div>
				<div class="clearfix stat-detail">
					<div class="label-body">
						<i class=" pull-right text-muted"></i>单品代理总数
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="home-stats">
		<a href="javascript:;" class="stat hvr-wobble-horizontal">
			<div class=" stat-icon">
				<i class="fa fa-list-alt fa-4x text-warning "></i>
			</div>
			<div class=" stat-label">
				<div id="todayProxys" class="label-header"></div>
				<div class="clearfix stat-detail">
					<div class="label-body">
						<i class=" pull-right text-muted"></i>新增单品代理数量
					</div>
				</div>
			</div>
		</a>
	</div>
</div>
		
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例4
    var myChart = echarts.init(document.getElementById('main'));
    window.onresize = myChart.resize;
    $(function () {
        jp.get("${ctx}${dataURL}", function (result) {
        	if (result.result == '0') {
	            // 指定图表的配置项和数据
	            // 使用刚指定的配置项和数据显示图表。
	            myChart.setOption(result.option);
	            $("#totalProxys").html(result.totalProxys);
	            $("#todayProxys").html(result.todayProxys);
	            
	            var s = "";
	            if(result.proxyList != null){
	            	for(var i=0;i<result.proxyList.length;i++){
	            		s += "<tr><td>"+result.proxyList[i].area+"</td><td>"+result.proxyList[i].count+"</td></tr>";
	            	}
	            }
	            $("#proxyList").html(s);
        	} else {
        		jp.warning(result.msg);
        	}
        });
        
        $("#search").click("click", function() {// 绑定查询按扭
        	var userId = $("#userId").val();
        	jp.get("${ctx}${dataURL}?userId="+userId, function (result) {
        		if (result.result == '0') {
    	            // 指定图表的配置项和数据
    	            // 使用刚指定的配置项和数据显示图表。
    	            myChart.setOption(result.option,true);
            	} else {
            		jp.warning(result.msg);
            	}
            });
  		});
  	 
  	 $("#reset").click("click", function() {// 绑定查询按扭
	  		$("select").val("");
	  		$("input").val("");
  		});
    })
	 $("#export").click(function(){//导出Excel文件
	        var searchParam = {};
	        searchParam.pageNo = 1;
	        searchParam.pageSize = -1;
	        searchParam.state = '1';
            var values = "";
            for(var key in searchParam){
                values = values + key + "=" + searchParam[key] + "&";
            }

			jp.downloadFile('${ctx}/proxyorder/proxyOrder/export?'+values);
	  })

</script>
</body>
</html>