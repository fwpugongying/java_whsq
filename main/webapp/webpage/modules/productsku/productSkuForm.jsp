<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>商品规格管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {
			var id = '${productSku.id}';
			var state = '${productSku.product.state}';
			
			if (id != '' && state=='0') {
				$("#oldPrice").attr("readonly","readonly");
				if (${fns:getUser().isShop()} == true) {
					$("#amount").attr("readonly","readonly");
				}
				$(".skuName").attr("readonly","readonly");
				$("#image").attr("disabled","disabled");
			}
		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/productsku/productSku/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        //jp.getParent().refresh();
                        //jp.getParent().childRefresh();
                        jp.getParent().$('#productSkuTable').bootstrapTable('refresh')
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="productSku" class="form-horizontal">
		<form:hidden path="id"/>	
		<form:hidden path="product.id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>市场价：</label></td>
					<td class="width-35">
						<form:input path="oldPrice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
						<%-- <input id="oldPrice" name="oldPrice" htmlEscape="false" value="${productSku.oldPrice}" <c:if test="${not empty productSku.id && productSku.product.state=='0'}">readonly="readonly"</c:if>   class="form-control required isFloatGteZero" /> --%>
						
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>结算价：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
						<%-- <input id="amount" name="amount" htmlEscape="false" value="${productSku.amount}" <c:if test="${not empty productSku.id && productSku.product.state=='0'}">readonly="readonly"</c:if>   class="form-control required isFloatGteZero" /> --%>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>库存：</label></td>
					<td class="width-35">
						<form:input path="stock" htmlEscape="false"    class="form-control required isIntGteZero"/>
					</td>
					
					<td class="width-15 active"><label class="pull-right">图片：</label></td>
					<td class="width-35">
						<sys:fileUpload path="image"  value="${productSku.image}" type="image" fileNumLimit="1" uploadPath="/productsku/productSku"/>
					</td>
					<%-- <td class="width-15 active"><label class="pull-right">券后价：</label></td>
					<td class="width-35">
						<input type="text" readonly="readonly" value="${productSku.price}" htmlEscape="false"    class="form-control" /> 
					</td> --%>
				</tr>
				<c:if test="${fns:getUser().isShop() == false}">
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>优惠券：</label></td>
					<td class="width-35">
						<form:input path="discount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>拼团价：</label></td>
					<td class="width-35">
						<form:input path="groupPrice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>补贴价(C2F产品)：</label></td>
					<td class="width-35">
						<form:input path="subsidyprice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>预售价(C2F产品)：</label></td>
					<td class="width-35">
						<form:input path="openprice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>铜板：</label></td>
					<td class="width-35">
						<form:input path="point" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				</c:if>
				<tr id="sku">
					<c:forEach items="${skuNameList}" var="sku" varStatus="status">
						<td class="width-15 active"><label class="pull-right"><font color="red">*</font>${sku.title}：</label></td>
						<td class="width-35">
							<input type="hidden" name="specificationList[${status.index}].id" value="${sku.id}"/>
							<input type="text" name="specificationList[${status.index}].value" value="${sku.value}" htmlEscape="false"    class="form-control required skuName" />
						</td>
					</c:forEach>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>