<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>云店商品管理</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta name="decorator" content="ani"/>
	<%@ include file="/webpage/include/bootstraptable.jsp"%>
	<%@include file="/webpage/include/treeview.jsp" %>
	<%@include file="productList.js" %>
	
	<!-- 引入样式 -->
  <link rel="stylesheet" href="https://unpkg.com/element-ui@2.13.0/lib/theme-chalk/index.css">
  <script src="${ctxStatic}/tinymce/tinymce.min.js"></script>
  
    <!-- import Vue before Element -->
<script src="https://unpkg.com/vue@2.5.2/dist/vue.js"></script>
<!-- import JavaScript -->
<script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
  <style>
    body{
      line-height: 1.5;
    }
    #app{
      height: 100%;
    }
    body,div,p,a,span,i,em,b,strong,ul,ol,dl,li,table,dt,dl,th,tr,td,thead,tbody,tfoot,
    form,select,h1,input,img,iframe,button,input,textarea{
      margin:0;
      padding: 0;
      /*vertical-align: top;*/
      box-sizing: border-box;
      -webkit-box-sizing:border-box;
      -moz-box-sizing:border-box;
      outline: none;
      -webkit-tap-highlight-color:rgba(255,0,0,0);
      /*line-height: 1.5em;*/
    }
    .clearfix{
      zoom:1;
    }
    .clearfix:after{
      content:"";
      height:0;
      display: block;
      visibility: hidden;
      clear:both;
    }
    .fl{
      float: left;
    }
    .fr{
      float: right;
    }
    .flex{
      display: flex;
      align-items: center;
    }
    .title{
      font-size: 20px;
      padding: 0 20px 0;
    }
    .wrap{
      padding:0 20px;
    }
    .content{
      padding: 0 20px;
      overflow-y: auto;
    }
    .second{
      padding: 0px 40px 10px;
    }
    .el-tabs__header{
      width: 200px;
    }
    .el-tabs__header .el-tabs__item{
      height: 50px;
      line-height: 50px;
    }
    .el-select,.el-cascader{
      width: 100%!important;
    }
    .ggitem{
      padding: 0px 0 10px;
    }
    .el-tag + .el-tag {
      margin-left: 10px;
    }
    .button-new-tag {
      margin-left: 10px;
      height: 32px;
      line-height: 30px;
      padding-top: 0;
      padding-bottom: 0;
    }
    .input-new-tag {
      width: 90px;
      margin-left: 10px;
      vertical-align: bottom;
    }
    .tag{
      margin-top: 10px;
    }
    .ggList{
      width: 100%;
      border-collapse: collapse;
      padding:2px;
    }
    .ggList td{

    }
    .ggList,.ggList tr th, .ggList tr td { border:1px solid #EBEEF5; }
    .ggList td,.ggList th{
      /*width: 100px;*/
      padding: 5px;
      font-size: 14px;
    }
  </style>
</head>
<body>
	<div class="wrapper wrapper-content">
	<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">云店商品列表</h3>
	</div>
	<div class="panel-body"  id="app">
	
	<!-- 搜索 -->
	<div id="left" class="col-sm-12">
	<div id="search-collapse" class="collapse">
		<div class="accordion-inner">
			<form:form id="searchForm" modelAttribute="product" class="form form-horizontal well clearfix">
			<c:if test="${fns:getUser().isShop() == false}">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="商品ID：">商品ID：</label>
				<form:input path="id" htmlEscape="false" maxlength="255" class=" form-control"/>
			</div>
			 <div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="店铺：">店铺：</label>
										<sys:gridselect url="${ctx}/store/store/data" id="store" name="store.id" value="${product.store.id}" labelName="store.title" labelValue="${product.store.title}"
							 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="推荐：">推荐：</label>
				<form:select path="isTuijian"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="商品专区类型：">商品专区类型：</label>
				<form:select path="isbd"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('zone_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
			</c:if>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <label class="label-item single-overflow pull-left" title="代理	：">代理：</label>
                    <select id="isDl" name="isDl" class="form-control m-b">
                        <option value="" selected="selected"></option>
                        <option value="0">否</option>
                        <option value="1">是</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <label class="label-item single-overflow pull-left" title="预购：">预购：</label>
                    <select id="isYg" name="isYg" class="form-control m-b">
                        <option value="" selected="selected"></option>
                        <option value="0">否</option>
                        <option value="1">是</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <label class="label-item single-overflow pull-left" title="商品名称：">商品名称：</label>
                    <form:input path="title" htmlEscape="false" maxlength="255" class=" form-control"/>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<label class="label-item single-overflow pull-left" title="品牌：">品牌：</label>
					<sys:gridselect url="${ctx}/brand/brand/data" id="brand" name="brand.id" value="${product.brand.id}"
									labelName="brand.title" labelValue="${product.brand.title}"
									title="选择品牌" cssClass="form-control required" fieldLabels="品牌名称" fieldKeys="title"
									searchLabels="品牌名称" searchKeys="title"></sys:gridselect>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<label class="label-item single-overflow pull-left" title="分类：">分类：</label>
					<sys:treeselect id="category" name="category.id" value="${product.category.id}"
									labelName="category.name" labelValue="${product.category.name}"
									title="分类" url="/productcategory/productCategory/treeData" extId="${product.id}"
									cssClass="form-control required" allowClear="true"/>
				</div>
			 <div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="状态：">状态：</label>
				<form:select path="state"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('product_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="拼团：">拼团：</label>
				<select id="isGroup" name="isGroup" class="form-control m-b">
					<option value="" selected="selected"></option>
					<option value="0">否</option>
					<option value="1">是</option>
				</select>
			</div>
			 <div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="区县：">区县：</label>
				<sys:treeselect id="area" name="area.id" value="${product.area.id}" labelName="area.name" labelValue="${product.area.name}"
					title="区域" url="/sys/area/treeData" cssClass="form-control" allowClear="true" notAllowSelectParent="true"/>
			</div>
			 <div class="col-xs-12 col-sm-6 col-md-4">
				<label class="label-item single-overflow pull-left" title="审核状态 ：">审核状态：</label>
				<form:select path="auditState"  class="form-control m-b">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('product_audit_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		 <div class="col-xs-12 col-sm-6 col-md-4">
			<div style="margin-top:26px">
			  <a  id="search" class="btn btn-primary btn-rounded  btn-bordered btn-sm"><i class="fa fa-search"></i> 查询</a>
			  <a  id="reset" class="btn btn-primary btn-rounded  btn-bordered btn-sm" ><i class="fa fa-refresh"></i> 重置</a>
			 </div>
	    </div>	
	</form:form>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div id="toolbar">
		<c:if test="${fns:getUser().isShop() == true}">
			<shiro:hasPermission name="product:product:add">
				<button id="add" class="btn btn-primary" onclick="add()">
					<i class="glyphicon glyphicon-plus"></i> 新建
				</button>
			</shiro:hasPermission>
		</c:if>
			<shiro:hasPermission name="product:product:edit">
			    <button id="edit" class="btn btn-success" disabled onclick="edit()">
	            	<i class="glyphicon glyphicon-edit"></i> 修改
	        	</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="product:product:del">
				<button id="remove" class="btn btn-danger" disabled onclick="deleteAll()">
	            	<i class="glyphicon glyphicon-remove"></i> 删除
	        	</button>
			</shiro:hasPermission>
			<%-- <shiro:hasPermission name="product:product:import">
				<button id="btnImport" class="btn btn-info"><i class="fa fa-folder-open-o"></i> 导入</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="product:product:export">
	        		<button id="export" class="btn btn-warning">
					<i class="fa fa-file-excel-o"></i> 导出
				</button>
			 </shiro:hasPermission> --%>
	                 <shiro:hasPermission name="product:product:view">
				<button id="view" class="btn btn-default" disabled onclick="view()">
					<i class="fa fa-search-plus"></i> 查看
				</button>
			</shiro:hasPermission>
			<button id="skuManager" class="btn btn-info" disabled @click="getDetail()">
					<i class="fa fa-list-ul"></i> 规格
				</button>
		    </div>
		
	<!-- 表格 -->
	<table id="productTable"   data-toolbar="#toolbar"></table>

    <!-- context menu -->
    <ul id="context-menu" class="dropdown-menu">
    	<shiro:hasPermission name="product:product:view">
        <li data-item="view"><a>查看</a></li>
        </shiro:hasPermission>
    	<shiro:hasPermission name="product:product:edit">
        <li data-item="edit"><a>编辑</a></li>
        </shiro:hasPermission>
        <shiro:hasPermission name="product:product:del">
        <li data-item="delete"><a>删除</a></li>
        </shiro:hasPermission>
        <li data-item="action1"><a>取消</a></li>
    </ul>  
    </div>
    <div  id="right" class="panel panel-default col-sm-6" style="display:none">
		<div class="panel-heading">
			<h3 class="panel-title"><label>所属商品：</label><font id="productName"></font><input type="hidden" id="productId"/></h3>
		</div>
		<div class="panel-body">
		 <div id="productSkuToolbar">
			<button id="subsectionButton"  class="btn btn-outline btn-primary" title="添加规格"><i class="fa fa-plus-circle"></i> 添加规格</button>
			<button id="back"  class="btn btn-outline btn-primary" title="返回"><i class="fa fa-arrow-left"></i> 返回</button>
		</div>
		 <!-- 表格 -->
	    <table id="productSkuTable" data-toolbar="#productSkuToolbar" data-id-field="id"></table>
	   
	   </div>
	</div>
	
	<div>
    <el-dialog
            title="编辑规格"
            :visible.sync="show"
            width="70%"
            :close-on-click-modal="false"
    >
      <div class="wrap" style="height: 500px;overflow: auto">
        <div class="content second">
          <div v-for="(item, i) in ggItem" :key="i" class="ggitem">
            <el-input placeholder="请输入规格名称（如：颜色）" size="small" v-model="item.name" class="input-with-select">
              <el-button slot="append" icon="el-icon-close" @click="del(item, i)"></el-button>
            </el-input>
            <div>
              <el-tag
                      class="tag"
                      v-for="(sitem, si) in item.items"
                      :key="si"
                      closable
                      :disable-transitions="false"
                      @close="handleClose(item.items,si)">
                {{sitem}}
              </el-tag>
              <el-input
                      class="tag"
                      class="input-new-tag"
                      v-if="item.inputVisible"
                      v-model="inputValue"
                      :ref="`saveTagInput${i}`"
                      size="small"
                      @keyup.enter.native="handleInputConfirm"
                      @blur="handleInputConfirm(item)"
                      style="width: 120px"
              >
              </el-input>
              <el-button v-else class="button-new-tag" size="small" @click="showInput(item,i)" class="tag" style="margin-top: 10px">+ 规格项</el-button>
              <!--                <el-divider></el-divider>-->
            </div>
          </div>
          <div>
            <el-button type="primary" size="small" icon="el-icon-plus" @click="addGgitem" v-if="ggItem.length<2 && !isread">添加规格</el-button>
            <el-button type="primary" size="small" icon="el-icon-refresh" v-if="!isread" @click="getggItem" style="margin-bottom: 10px">刷新规格项列表</el-button>
          </div>
          <template>
            <el-table
                    :data="ggList"
                    border
                    style="width: 100%">
              <el-table-column v-for="(item, i) in NewggItem" :key="i" :label="item.name">
                <template slot-scope="scope">
					<c:if test="${fns:getUser().isShop() == false}">
						<el-input size="mini" v-model="scope.row.gg[i]" @change="validate('gg[i]', scope.$index, 'int')"></el-input>
					</c:if>
                </template>
              </el-table-column>
              <el-table-column prop="stock" label="库存" >
                <template slot-scope="scope">
                  <el-input size="mini" v-model="scope.row.stock" @change="validate('stock', scope.$index, 'int')"></el-input>
                </template>
              </el-table-column>
              <el-table-column prop="oldPrice" label="市场价">
                <template slot-scope="scope">
                  <el-input size="mini" v-model="scope.row.oldPrice" v-bind:readonly="isread" @change="validate('oldPrice', scope.$index)"></el-input>
                </template>
              </el-table-column>
              <el-table-column prop="amount" label="结算价">
                <template slot-scope="scope">
                  <el-input size="mini" v-model="scope.row.amount" v-bind:readonly="isread" @change="validate('amount', scope.$index)"></el-input>
                </template>
              </el-table-column>
              <c:if test="${fns:getUser().isShop() == false}">
              <el-table-column prop="discount" label="优惠券">
                <template slot-scope="scope">
                  <el-input size="mini" v-model="scope.row.discount" @change="validate('discount', scope.$index)"></el-input>
                </template>
              </el-table-column>
              <el-table-column prop="groupPrice" label="拼团价" >
                <template slot-scope="scope">
                  <el-input size="mini" v-model="scope.row.groupPrice" @change="validate('groupPrice', scope.$index)"></el-input>
                </template>
              </el-table-column>
			  <el-table-column prop="subsidyprice" label="补贴价(C2F产品)" >
				  <template slot-scope="scope">
					  <el-input size="mini" v-model="scope.row.subsidyprice" @change="validate('subsidyprice', scope.$index)"></el-input>
				  </template>
			  </el-table-column>
			  <el-table-column prop="openprice" label="预售价(C2F产品)" >
				  <template slot-scope="scope">
					  <el-input size="mini" v-model="scope.row.openprice" @change="validate('openprice', scope.$index)"></el-input>
				  </template>
			  </el-table-column>
			  <el-table-column prop="point" label="铜板" >
				  <template slot-scope="scope">
					  <el-input size="mini" v-model="scope.row.point" @change="validate('point', scope.$index)"></el-input>
				  </template>
			  </el-table-column>
              </c:if>
			<el-table-column prop="sort" label="排序" >
				<template slot-scope="scope">
					<el-input size="mini" v-model="scope.row.sort" @change="validate('sort', scope.$index, 'int')"></el-input>
				</template>
			</el-table-column>
              <el-table-column prop="image" label="图片">
                <template slot-scope="scope">
                  <img :src="scope.row.image" alt="" style="width: 30px;height: 30px;vertical-align: bottom" @click="preview(scope.row.image)">
                  
                  <label>
                  	<input type="file" accept="image/*" style="display: none" @change="upFile($event,scope.row,scope.$index)">
                    <div class="el-icon-upload2" style="padding: 2px;background: #409eff;color: white"></div>
                  </label>
                </template>
              </el-table-column>
            </el-table>
          </template>
        </div>
      </div>
      <div slot="footer">
        <el-button type="primary" size="small" @click="submit">确定</el-button>
        <el-button size="small" @click="cancel">取消</el-button>
      </div>
    </el-dialog>
  </div>
	
	</div>
	</div>
	</div>
	
	
  


<script>

  var vm = new Vue({
    el: '#app',
    data: {
      show: false,
      productId: '',
      dialogImageUrl: '',
      dialogVisible: false,
      inputVisible: false, //
      inputValue: '',
      ggList: [],
      ggItem: [],
      NewggItem: [],
      isread:false,
      detail: {
        "skunameList": [{
          "name": "大小",
          "id": "573cbb1db10843bda12635fa32f17257"
        }, {
          "name": "颜色",
          "id": "558c25554e8447d6a940aa253ad51ee2"
        }],
        "skuList": [{
          "image": "",
          "amount": "0.01",
          "oldPrice": "200.00",
          "price": "200.00",
          "subsidyprice": "200.00",
			"openprice": "200.00",
          "point": "0.00",
			"sort": "0",
			"discount": "0.00",
          "id": "ddb8a629d0f04116aa54845a5060fb7f",
          "stock": 0,
          "content": [{"id":"573cbb1db10843bda12635fa32f17257","value":"70"},{"id":"558c25554e8447d6a940aa253ad51ee2","value":"黄色"}]
        }, {
          "image": "",
          "amount": "0.01",
          "oldPrice": "200.00",
          "price": "200.00",
          "subsidyprice": "200.00",
			"openprice": "200.00",
          "point": "0.00",
			"sort": "0",
			"discount": "0.00",
          "id": "0ea59bf1de574dbbaa7e8b2024467364",
          "stock": 0,
          "content": [{"id":"573cbb1db10843bda12635fa32f17257","value":"85"},{"id":"558c25554e8447d6a940aa253ad51ee2","value":"蛋黄"}]
        }]
      }
    },
    mounted: function(){
    },
    methods: {
      getDetail(){
    	 let that = this
	  	 $.ajax({
          url: '${ctx}/product/product/productSku?productId='+getIdSelections(),
          type : "GET",
          contentType: false,// 当有文件要上传时，此项是必须的，否则后台无法识别文件流的起始位置
          processData: false,// 是否序列化data属性，默认true(注意：false时type必须是post)
          success: function (res) {
        	  that.show = true
               that.detail = res.body
              that.NewggItem = that.detail.skunameList
              that.isread = res.body.isread
              for(let i=0;i<that.detail.skuList.length;i++){
                let gg = []
                let arr= JSON.parse(that.detail.skuList[i].content)
                that.$set(that.detail.skuList[i],'content', arr)
                for(let j=0;j<that.detail.skuList[i].content.length;j++){
                  gg.push(that.detail.skuList[i].content[j].value)
                  that.NewggItem.map((item) => {
                    !item.items?that.$set(item,'items',[]):''
                    if(item.id===that.detail.skuList[i].content[j].id){
                      let arr = item.items.find((sitem) => (sitem === that.detail.skuList[i].content[j].value))
                      if (!arr) {
                        item.items.push(that.detail.skuList[i].content[j].value)
                      }
                    }
                  })
                }
                that.$set(that.detail.skuList[i], 'gg', gg)
              }
        	  console.log(that.NewggItem)
        	  that.ggItem = JSON.parse(JSON.stringify(that.NewggItem))
              that.ggList = that.detail.skuList
          },
          error:function(err){
          }
        })
      },
      // 图片上传
      upFile(e, row, index){
    	  console.log(index);
        let that = this
        let file = e.target.files || e.dataTransfer.files
        let data = new FormData()
        if(file[0].type.indexOf("image")==-1){
        	that.$message({
                message: '只能上传图片文件',
                type: 'warning'
              });
        	 $(`#up${index}`).val('')
        	return
        }
        console.log(file[0].type)
        data.append('file', file[0])
        $(`#up${index}`).val('')
        $.ajax({
          url: '${pageContext.request.contextPath}/api/uploadFile',
          type : "POST",
          data: data,
          contentType: false,// 当有文件要上传时，此项是必须的，否则后台无法识别文件流的起始位置
          processData: false,// 是否序列化data属性，默认true(注意：false时type必须是post)
          success: function (res) {
            that.$set(that.ggList[index],'image', res.url)
            // that.ggList[index].image = res.url
            that.$forceUpdate()
            that.$message({
              message: '上传成功',
              type: 'success'
            });
          },
          error:function(err){
            that.$message({
              message: '上传失败',
              type: 'error'
            });
          }
        })
      },
      handleClose(){
      },
      validate(name, index, type){
        if(type=='int'){
          parseInt(this.ggList[index][name])&&parseInt(this.ggList[index][name])>0? this.$set(this.ggList[index], name, parseInt(this.ggList[index][name])): this.$set(this.ggList[index], name, 0)
        }else {
          parseFloat(this.ggList[index][name])&&parseFloat(this.ggList[index][name])>0? this.$set(this.ggList[index], name, (parseFloat(this.ggList[index][name])).toFixed(2)): this.$set(this.ggList[index], name, 0.00)
        }
      },
      // 图片放大
      preview(url){
        this.$alert('<img src="'+url+'"  alt="" style="width:100%">', {
          dangerouslyUseHTMLString: true,
          center: true,
          showCancelButton:false,
          showConfirmButton:false
        });
      },
      // 添加规格
      addGgitem(){
        let obj = {
          name: '',
          id: '',
          items: []
        }
        this.ggItem.push(obj)
      },
      del(item, index){
        this.ggItem.splice(index,1)
        // this.$set(item, 'delFlag', true)
      },
      // 规格项
      handleClose(item,i) {
        item.splice(i, 1);
      },
      showInput(item, i) {
        this.$set(item,'inputVisible',true)
      },
      // 规格项
      handleInputConfirm(item) {
        let inputValue = this.inputValue;
        if (inputValue) {
          item.items.push(inputValue);
        }
        item.inputVisible = false;
        this.inputValue = '';
      },
      // 刷新规格列表
      getggItem(){
        const textArr= JSON.parse(JSON.stringify(this.ggItem))
        const getCombination=(array)=>{
          let resultArry=[];
          let arrobj= getSelections();
          let arr=arrobj[0];
          console.log(arr);
          array.forEach((arrItem,i)=>{
            if(resultArry.length===0){
              arrItem.items.forEach((item, j)=>{
			  let obj = {
				  gg: [],
				  image: '',
				  stock: '0',
				  amount:arr.amount,
				  discount:'0',
				  groupPrice:arr.groupPrice,
				  subsidyprice:arr.subsidyprice,
				  openprice:arr.openprice,
				  point:'0',
				  sort:'0',
				  oldPrice:arr.oldPrice,
				  price:arr.price
			  }
			  obj.gg.push(item)
                resultArry.push(obj)
              })
            }else{
              const emptyArray=[];
              resultArry.forEach((item)=>{
                arrItem.items.forEach((value)=>{
                  let obj = JSON.parse(JSON.stringify(item))
                  let content = obj.gg?obj.gg:[]
                  content.push(value)
                  this.$set(obj,'gg',content);
                  this.$set(obj,'oldPrice',arr.oldPrice);
                  this.$set(obj,'price',arr.price);
                  this.$set(obj,'groupPrice',arr.groupPrice);
                  this.$set(obj,'subsidyprice',arr.subsidyprice);
                  this.$set(obj,'openprice',arr.openprice);
                  this.$set(obj,'sort',arr.sort);
				  this.$set(obj,'amount',arr.amount);
                  emptyArray.push(obj)
                })
              })
              resultArry=emptyArray
            }
          });
          return resultArry;
        }
        let ggList = (getCombination(textArr))
        for(let i = 0; i<this.ggList.length;i++){
          // console.log(i)
          let item = this.ggList[i]
          let index = this.fun(item, ggList)
          if(index){
            let gg = ggList[index].gg
            ggList[index] = JSON.parse(JSON.stringify(item))
            this.$set(ggList[index], 'gg', gg)
          }
        }
        console.log(ggList)
        this.ggList = JSON.parse(JSON.stringify(ggList))
        this.NewggItem = JSON.parse(JSON.stringify(textArr))
      },
      fun(item,ggList) {
        for(let j = 0; j<ggList.length;j++){
          for (let k = 0; k<item.gg.length;k++ ){
            if(item.gg[k]!==ggList[j].gg[k]){
              break
            }else {
              if(k===item.gg.length-1){
                return j+ ''
              }
            }
          }
        }
        return false
      },
      submit(){
    	  let that = this
    	  let arr= getIdSelections()
    	  let data = {
    			productId: arr[0],
    			skuList: this.ggList,
    			skuNameList: this.NewggItem
    	  }
    	  
    	  $.ajax({
              url: '${ctx}/product/product/saveSku',
              type : "POST",
              data: 'data='+JSON.stringify(data), 
              success: function (res) {
            	  if(res.success){
            		  that.$message({
                          message: '操作成功',
                          type: 'success'
                        })
                        that.show=false
            	  }else{
            		  that.$message({
                          message: res.msg,
                          type: 'error'
                        })
            	  }
              },
              error:function(err){
              }
            }) 
      },
      cancel(){
        this.show=false
      }
    }
  })
</script>
</body>
</html>