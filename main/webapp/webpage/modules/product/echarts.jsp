<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<meta charset="utf-8">
	<title>ECharts</title>
	<meta name="decorator" content="ani"/>
	<link rel="stylesheet" href="${ctxStatic}/common/css/vendor.css" />
	<script src="${ctxStatic}/common/js/vendor.js"></script>
	<!-- 引入 echarts.js -->
	<%@ include file="/webpage/include/echarts.jsp"%>
</head>
<body class="bg-white">
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<!-- <div id="main" style="width: 100%;height: 50%;margin-top:20px;"></div> -->

<div class="panel-body" style="width: 100%;height: 5%;margin-bottom:30px;">
	<%-- <div class="col-xs-12 col-sm-6 col-md-4">
		<label class="label-item single-overflow pull-left" title="店铺：">店铺：</label>
					<sys:gridselect url="${ctx}/store/store/data" id="store" name="storeId" value="${product.store.id}" labelName="store.title" labelValue="${product.store.title}"
		 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
	</div> --%>
	<div class="col-xs-12 col-sm-6 col-md-4">
	<label class="label-item single-overflow pull-left" title="时段：">时段：</label>
		 <div class="form-group">
			<div class="col-xs-12">
				   <div class="col-xs-12 col-sm-5">
			        	  <div class='input-group date' id='beginCreateDate' style="left: -10px;" >
			                   <input type='text' id="begin"  name="beginCreateDate" class="form-control"  />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			             </div>	
			        </div>
			        <div class="col-xs-12 col-sm-1">
			        		~
			       	</div>
			        <div class="col-xs-12 col-sm-5">
			          	<div class='input-group date' id='endCreateDate' style="left: -10px;" >
			                   <input type='text' id="end" name="endCreateDate" class="form-control" />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			           	</div>	
			        </div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div style="margin-top:30px">
		  <a  id="search" class="btn btn-primary btn-rounded  btn-bordered btn-sm"><i class="fa fa-search"></i> 查询</a>
		  <a  id="reset" class="btn btn-primary btn-rounded  btn-bordered btn-sm" ><i class="fa fa-refresh"></i> 重置</a>
		 </div>
    </div>
</div>

<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;">
	<center><h3>云店商品</h3></center>
	<table class="table">
	   <thead>
	      <tr>
	         <th>商品名称</th>
	         <th>销量</th>
	      </tr>
	   </thead>
	   <tbody id="productList">
	   </tbody>
	</table>
</div>
<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;">
	<center><h3>联盟商品</h3></center>
	<table class="table">
	   <thead>
	      <tr>
	         <th>商品名称</th>
	         <th>销量</th>
	      </tr>
	   </thead>
	   <tbody id="tkList">
	   </tbody>
	</table>
</div>
		
<script type="text/javascript">
$(function () {
	$('#beginCreateDate').datetimepicker({
		 format: "YYYY-MM-DD"
	});
	$('#endCreateDate').datetimepicker({
		 format: "YYYY-MM-DD"
	});
	
    jp.get("${ctx}${dataURL}", function (result) {
    	var s = "";
    	if (result != null && result.productList != null) {
    		for(var i=0;i<result.productList.length;i++){
    			s+= "<tr><td>"+result.productList[i].title+"</td><td>"+result.productList[i].sales+"</td></tr>";
    		}
    	}
    	$("#productList").html(s);
    	var r = "";
    	if (result != null && result.tkList != null) {
    		for(var i=0;i<result.tkList.length;i++){
    			r+= "<tr><td>"+result.tkList[i].title+"</td><td>"+result.tkList[i].sales+"</td></tr>";
    		}
    	}
    	$("#tkList").html(r);
    });
    
    $("#search").click("click", function() {// 绑定查询按扭
    	var storeId = $("#storeId").val();
    	var beginDate = $("#begin").val();
    	var endDate = $("#end").val();
    	jp.get("${ctx}${dataURL}?storeId="+storeId+"&beginDate="+beginDate+"&endDate="+endDate, function (result) {
    		var s = "";
        	if (result != null && result.productList != null) {
        		for(var i=0;i<result.productList.length;i++){
        			s+= "<tr><td>"+result.productList[i].title+"</td><td>"+result.productList[i].sales+"</td></tr>";
        		}
        	}
        	$("#productList").html(s);
        	var r = "";
        	if (result != null && result.tkList != null) {
        		for(var i=0;i<result.tkList.length;i++){
        			r+= "<tr><td>"+result.tkList[i].title+"</td><td>"+result.tkList[i].sales+"</td></tr>";
        		}
        	}
        	$("#tkList").html(r);
        });
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
  		$("select").val("");
  		$("input").val("");
		});
})

</script>
</body>
</html>