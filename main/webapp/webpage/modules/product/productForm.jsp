<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>云店商品管理</title>
	<meta name="decorator" content="ani"/>
	<!-- SUMMERNOTE -->
	<%@include file="/webpage/include/summernote.jsp" %>
	<script type="text/javascript">

		$(document).ready(function() {
						//富文本初始化
			$('#content').summernote({
				height: 300,
                lang: 'zh-CN',
                callbacks: {
                    onChange: function(contents, $editable) {
                        $("input[name='content']").val($('#content').summernote('code'));//取富文本的值
                    },
                    onImageUpload: function(files, editor, $editable) {
                    	var images = [];
		            	for(var i = 0; i < files.length; i++) {
		                     var image = sendImage(files[i]);
		                     if(image){
		                    	 images.push(image);
		                     }
		            	}
		            	for(var i = 0; i < images.length; i++) {
		            		(function(i){
		            			setTimeout(function(){$('#content').summernote('insertImage',images[i],'img');}, i*500);
		            		})(i);
		            	}
		            }
                }
            });
		});
		
		function sendImage(file) {
			var formData = new FormData();
		    formData.append('file',file);
		    var image = "";
		    $.ajax({
		        url : '/api/uploadFile',
		        type : 'POST',
		        data : formData,
		        async: false,
		        processData : false,
		        contentType : false,
		        success : function(data) {
		        	image = data.url;
		        },
		        error:function(xhr,state,errorThrown){
				}
		    }); 
		    return image;
		}
		
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                $("input[name='content']").val($('#content').summernote('code'));//取富文本的值
                jp.post("${ctx}/product/product/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
			$(list+idx).find(".form_datetime").each(function(){
				 $(this).datetimepicker({
					 format: "YYYY-MM-DD HH:mm:ss"
			    });
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="product" action="${ctx}/product/product/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table class="table table-bordered">
		   <tbody>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>店铺：</label></td>
					<td class="width-35" colspan="3">
						<sys:gridselect url="${ctx}/store/store/data" id="store" name="store.id" value="${product.store.id}" labelName="store.title" labelValue="${product.store.title}"
							 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr> --%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商品名称：</label></td>
					<td class="width-35" colspan="3">
						<form:input path="title" htmlEscape="false"  maxlength="40" placeholder="商品名称不超过40字" class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">副标题：</label></td>
					<td class="width-35">
						<form:input path="subtitle" htmlEscape="false"  maxlength="40" placeholder="副标题不超过40字" class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>品牌：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/brand/brand/data" id="brand" name="brand.id" value="${product.brand.id}" labelName="brand.title" labelValue="${product.brand.title}"
							 title="选择品牌" cssClass="form-control required" fieldLabels="品牌名称" fieldKeys="title" searchLabels="品牌名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>分类：</label></td>
					<td class="width-35">
						<sys:treeselect id="category" name="category.id" value="${product.category.id}" labelName="category.name" labelValue="${product.category.name}"
							title="分类" url="/productcategory/productCategory/treeData" extId="${product.id}" cssClass="form-control required" allowClear="true" notAllowSelectParent="true"/>
					</td>
					<td class="width-15 active"><label class="pull-right">列表图：</label></td>
					<td class="width-35">
						<sys:fileUpload path="icon"  value="${product.icon}" type="image" fileNumLimit="1" uploadPath="/product/product"/>
					</td>
				</tr>
				<tr>
					<c:if test="${isread =='1'}">
						<td class="width-15 active"><label class="pull-right">轮播图：</label></td>
						<td class="width-35" colspan="3">
							<c:forEach items="${fn:split(product.images, '|')}" var="image">
								<img  onclick="jp.showPic('${image}')" height="100px" src="${image}">
							</c:forEach>
						</td>
					</c:if>
					<c:if test="${isread !='1'}">
						<td class="width-15 active"><label class="pull-right">轮播图：</label></td>
						<td class="width-35" colspan="3">
							<sys:fileUpload path="images"  value="${product.images}" type="image" fileNumLimit="5" uploadPath="/product/product"/>
						</td>
					</c:if>
					
					<%-- <td class="width-15 active"><label class="pull-right"><font color="red">*</font>状态：</label></td>
					<td class="width-35">
						<form:select path="state" class="form-control required">
							<form:options items="${fns:getDictList('product_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td> --%>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>市场价：</label></td>
					<td class="width-35">
						<form:input path="oldPrice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>结算价：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<c:if test="${fns:getUser().isShop() == false}">
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>优惠券：</label></td>
					<td class="width-35">
						<form:input path="discount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right">券后价：</label></td>
					<td class="width-35">
						<input type="text" readonly="readonly" value="${product.price}" htmlEscape="false"    class="form-control " />
					</td>
				</tr>
				<%--<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>套餐专区：</label></td>
					<td class="width-35">
						<form:select path="iscombo" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>--%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>专区的商品：</label></td>
					<td class="width-35">
						<form:select path="isbd" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('zone_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>差价(惠选优品专区)：</label></td>
					<td class="width-35">
						<form:input path="diffprice" htmlEscape="false"    class="form-control isFloatGteZero"/>
					</td>

				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>补贴价(C2F产品)：</label></td>
					<td class="width-35">
						<form:input path="subsidyprice" htmlEscape="false"    class="form-control isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>预售价(C2F产品)：</label></td>
					<td class="width-35">
						<form:input path="openprice" htmlEscape="false"    class="form-control isFloatGteZero"/>
					</td>

				</tr>
				</c:if>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>新品：</label></td>
					<td class="width-35">
						<form:select path="isNew" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
                <tr>
                    <td class="width-15 active"><label class="pull-right">不发货区域：</label></td>
                    <td class="width-35">
                            <%-- <form:input path="distant" htmlEscape="false"    class="form-control "/> --%>
                        <sys:gridselect url="${ctx}/sys/area/data" id="nodelarea" name="nodelarea" value="${product.nodelarea}" labelName="distant.name" labelValue="${product.nodelarea}"
                                        title="选择省份" cssClass="form-control " isMultiSelected="true" fieldLabels="省份" fieldKeys="name" searchLabels="省份" searchKeys="name" ></sys:gridselect>
                    </td>
                </tr>
				<c:if test="${fns:getUser().isShop() == false}">
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>爆品专区：</label></td>
					<td class="width-35">
						<form:select path="isHot" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>扶贫：</label></td>
					<td class="width-35">
						<form:select path="isFupin" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>特价商品(每个会员只能购买一次)：</label></td>
					<td class="width-35">
						<form:select path="isteprice" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>特价商品限购数量(数字)：</label></td>
					<td class="width-35">
						<form:input path="tepricenum" htmlEscape="false"    class="form-control  isIntGtZero"/>
					</td>
				</tr>
				<%--<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>专区的商品：</label></td>
					<td class="width-35">
						<form:select path="isbd" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('zone_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>

				</tr>--%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>列表隐藏的商品(用于搜索和列表隐藏特定商品：是隐藏，否显示)：</label></td>
					<td class="width-35">
						<form:select path="isshow" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>

				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">省份：</label></td>
					<td class="width-35">
						<form:input path="province" htmlEscape="false"  maxlength="32"  class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">城市：</label></td>
					<td class="width-35">
						<form:input path="city" htmlEscape="false"  maxlength="32"  class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">区县：</label></td>
					<td class="width-35">
						<form:input path="district" htmlEscape="false"  maxlength="32"  class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>铜板：</label></td>
					<td class="width-35">
						<form:input path="point" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>销量：</label></td>
					<td class="width-35">
						<form:input path="sales" htmlEscape="false"    class="form-control required isIntGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>推荐：</label></td>
					<td class="width-35">
						<form:select path="isTuijian" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>赠品提前支付价：</label></td>
					<td class="width-35">
						<form:input path="giftprice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>拼团：</label></td>
					<td class="width-35">
						<form:select path="isGroup" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>拼团价：</label></td>
					<td class="width-35">
						<form:input path="groupPrice" htmlEscape="false" class="form-control required isFloatGteZero"/>
					</td>
				</tr>
					<tr>
						<td class="width-15 active"><label class="pull-right"><font color="red">*</font>代理补贴：</label>
						</td>
						<td class="width-35">
							<form:input path="dlbt" htmlEscape="false" class="form-control required isIntGteZero"/>
						</td>
						<td class="width-15 active"><label class="pull-right"><font color="red">*</font>拼团服务费：</label>
						</td>
						<td class="width-35">
							<form:input path="ptFwmoney" htmlEscape="false" class="form-control required isIntGteZero"/>
						</td>
					</tr>
					<tr>
						<td class="width-15 active"><label class="pull-right"><font color="red">*</font>是否是预购：</label>
						</td>
						<td class="width-35">
							<form:select path="isYg" class="form-control required">
								<form:option value="" label=""/>
								<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value"
											  htmlEscape="false"/>
							</form:select>
						</td>
						<td class="width-15 active"><label class="pull-right"><font color="red">*</font>是否是代理：</label>
						</td>
						<td class="width-35">
							<form:select path="isDl" class="form-control required">
								<form:options items="${fns:getDictList('yes_no')}" itemLabel="label" itemValue="value"
											  htmlEscape="false"/>
							</form:select>
						</td>
						<script>
							<c:if test="${empty product.isDl}">
							$("#isDl").find("option[value='1']").attr("selected",true);
							</c:if>
							<c:if test="${empty product.isYg}">
							$("#isYg").find("option[value='0']").attr("selected",true);
							</c:if>
						</script>
					</tr>
				</c:if>
				<%-- <tr>
					
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>审核状态：</label></td>
					<td class="width-35">
						<form:select path="auditState" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('product_audit_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr> --%>
				<tr>
					<td class="width-15 active"><label class="pull-right">详情：</label></td>
					<td class="width-35" colspan="3">
                        <input type="hidden" name="content" value=""/>
						<div id="content">
                          ${fns:unescapeHtml(product.content)}
                        </div>
					</td>
				</tr>
		 	</tbody>
		</table>
		</form:form>
</body>
</html>