<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<meta charset="utf-8">
	<title>ECharts</title>
	<meta name="decorator" content="ani"/>
	<link rel="stylesheet" href="${ctxStatic}/common/css/vendor.css" />
	<script src="${ctxStatic}/common/js/vendor.js"></script>
	<!-- 引入 echarts.js -->
	<%@ include file="/webpage/include/echarts.jsp"%>
</head>
<body class="bg-white">
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<!-- <div id="main" style="width: 100%;height: 50%;margin-top:20px;"></div> -->
<div>
<div class="panel-body" style="width: 100%;height: 5%;margin-bottom:30px;">
	<%-- <div class="col-xs-12 col-sm-6 col-md-4">
		<label class="label-item single-overflow pull-left" title="店铺：">店铺：</label>
					<sys:gridselect url="${ctx}/store/store/data" id="store" name="storeId" value="${product.store.id}" labelName="store.title" labelValue="${product.store.title}"
		 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
	</div> --%>
	<div class="col-xs-12 col-sm-6 col-md-4">
	<label class="label-item single-overflow pull-left" title="时段：">时段：</label>
		 <div class="form-group">
			<div class="col-xs-12">
				   <div class="col-xs-12 col-sm-5">
			        	  <div class='input-group date' id='beginCreateDate' style="left: -10px;" >
			                   <input type='text' id="begin"  name="beginCreateDate" class="form-control"  />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			             </div>	
			        </div>
			        <div class="col-xs-12 col-sm-1">
			        		~
			       	</div>
			        <div class="col-xs-12 col-sm-5">
			          	<div class='input-group date' id='endCreateDate' style="left: -10px;" >
			                   <input type='text' id="end" name="endCreateDate" class="form-control" />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			           	</div>	
			        </div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div style="margin-top:30px">
		  <a  id="search" class="btn btn-primary btn-rounded  btn-bordered btn-sm"><i class="fa fa-search"></i> 查询</a>
		  <a  id="reset" class="btn btn-primary btn-rounded  btn-bordered btn-sm" ><i class="fa fa-refresh"></i> 重置</a>
		 </div>
    </div>
</div>
</div>
<div class="conter-wrapper home-container col-xs-12 ">
<div><h3>上架商品数量</h3></div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="todaySj" class="label-header">${data.todaySj}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							今日
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="yesSj" class="label-header">${data.yesSj}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							昨日
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="totalSj" class="label-header">${data.totalSj}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							总数量
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<div class="conter-wrapper home-container col-xs-12">
<div><h3>下架商品数量</h3></div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="todayXj" class="label-header">${data.todayXj}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							今日
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="yesXj" class="label-header">${data.yesXj}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							昨日
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="totalXj" class="label-header">${data.totalXj}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							总数量
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<div class="conter-wrapper home-container col-xs-12">
<div><h3>驳回商品数量</h3></div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="todayBh" class="label-header">${data.todayBh}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							今日
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="yesBh" class="label-header">${data.yesBh}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							昨日
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="totalBh" class="label-header">${data.totalBh}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							总数量
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>


		
<script type="text/javascript">
$(function () {
	$('#beginCreateDate').datetimepicker({
		 format: "YYYY-MM-DD"
	});
	$('#endCreateDate').datetimepicker({
		 format: "YYYY-MM-DD"
	});
	
    $("#search").click("click", function() {// 绑定查询按扭
    	var beginDate = $("#begin").val();
    	var endDate = $("#end").val();
    	jp.get("${ctx}${dataURL}?&beginDate="+beginDate+"&endDate="+endDate, function (result) {
        	if (result != null) {
        		$("#totalSj").html(result.totalSj);
        		$("#totalXj").html(result.totalXj);
        		$("#totalBh").html(result.totalBh);
        	}
        });
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
  		$("select").val("");
  		$("input").val("");
		});
})

</script>
</body>
</html>