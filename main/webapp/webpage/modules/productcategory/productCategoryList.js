<%@ page contentType="text/html;charset=UTF-8" %>
<script>
	    var $productCategoryTreeTable=null;  
		$(document).ready(function() {
			$productCategoryTreeTable=$('#productCategoryTreeTable').treeTable({  
		    	   theme:'vsStyle',	           
					expandLevel : 1,
					column:0,
					checkbox: false,
		            url:'${ctx}/productcategory/productCategory/getChildren?parentId=',  
		            callback:function(item) { 
		            	 var treeTableTpl= $("#productCategoryTreeTableTpl").html();
		            	 item.dict = {};
						item.dict.state = jp.getDictLabel(${fns:toJson(fns:getDictList('product_category_state'))}, item.state, "-");
						item.dict.hot = jp.getDictLabel(${fns:toJson(fns:getDictList('product_category_hot'))}, item.hot, "-");
						item.dict.type = "";
						if(item.type == '1'){
							item.dict.type = '<li><a href="#" onclick="jp.openSaveDialog(\'添加下级商品分类\', \'${ctx}/productcategory/productCategory/form?parent.id='+item.id+'\',\'800px\', \'500px\')"><i class=\"fa fa-plus\"></i> 添加下级商品分类</a></li>';
						}
						
		            	 var result = laytpl(treeTableTpl).render({
								  row: item
							});
		                return result;                   
		            },  
		            beforeClick: function($productCategoryTreeTable, id) { 
		                //异步获取数据 这里模拟替换处理  
		                $productCategoryTreeTable.refreshPoint(id);  
		            },  
		            beforeExpand : function($productCategoryTreeTable, id) {   
		            },  
		            afterExpand : function($productCategoryTreeTable, id) {  
		            },  
		            beforeClose : function($productCategoryTreeTable, id) {    
		            	
		            }  
		        });
		        
		        $productCategoryTreeTable.initParents('${parentIds}', "0");//在保存编辑时定位展开当前节点
		       
		});
		
		function del(con,id){  
			jp.confirm('确认要删除商品分类吗？', function(){
				jp.loading();
	       	  	$.get("${ctx}/productcategory/productCategory/delete?id="+id, function(data){
	       	  		if(data.success){
	       	  			$productCategoryTreeTable.del(id);
	       	  			jp.success(data.msg);
	       	  		}else{
	       	  			jp.error(data.msg);
	       	  		}
	       	  	})
	       	   
       		});
	
		} 
		
		function refreshNode(data) {//刷新节点
            var current_id = data.body.productCategory.id;
			var target = $productCategoryTreeTable.get(current_id);
			var old_parent_id = target.attr("pid") == undefined?'1':target.attr("pid");
			var current_parent_id = data.body.productCategory.parentId;
			var current_parent_ids = data.body.productCategory.parentIds;
			if(old_parent_id == current_parent_id){
				if(current_parent_id == '0'){
					$productCategoryTreeTable.refreshPoint(-1);
				}else{
					$productCategoryTreeTable.refreshPoint(current_parent_id);
				}
			}else{
				$productCategoryTreeTable.del(current_id);//刷新删除旧节点
				$productCategoryTreeTable.initParents(current_parent_ids, "0");
			}
        }
		function refresh(){//刷新
			var index = jp.loading("正在加载，请稍等...");
			$productCategoryTreeTable.refresh();
			jp.close(index);
		}
		
		function addChild(type, id){
			console.log(id);
			if (type != '1') {
				jp.error("只能选择一级分类添加下级");
			} else {
				jp.openSaveDialog('添加下级商品分类', '${ctx}/productcategory/productCategory/form?parent.id='+id,'800px', '500px');
			}
		}
</script>
<script type="text/html" id="productCategoryTreeTableTpl">
			<td>
			<c:choose>
			      <c:when test="${fns:hasPermission('productcategory:productCategory:edit')}">
				    <a  href="#" onclick="jp.openSaveDialog('编辑商品分类', '${ctx}/productcategory/productCategory/form?id={{d.row.id}}','800px', '500px')">
							{{d.row.name === undefined ? "": d.row.name}}
					</a>
			      </c:when>
			      <c:when test="${fns:hasPermission('productcategory:productCategory:view')}">
				    <a  href="#" onclick="jp.openViewDialog('查看商品分类', '${ctx}/productcategory/productCategory/form?id={{d.row.id}}','800px', '500px')">
							{{d.row.name === undefined ? "": d.row.name}}
					</a>
			      </c:when>
			      <c:otherwise>
							{{d.row.name === undefined ? "": d.row.name}}
			      </c:otherwise>
			</c:choose>
			</td>
			<td>
				<img onclick="jp.showPic('{{d.row.icon}}')" height="50px" src="{{d.row.icon===undefined?'':d.row.icon}}">
			</td>
			<td>
							{{d.row.sort === undefined ? "": d.row.sort}}
			</td>
			<td>
						{{d.row.dict.state === undefined ? "": d.row.dict.state}}
			</td>
			<td>
						{{d.row.dict.hot === undefined ? "": d.row.dict.hot}}
			</td>
			<td>
							{{d.row.updateDate === undefined ? "": d.row.updateDate}}
			</td>
			<td>
				<div class="btn-group">
			 		<button type="button" class="btn  btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-cog"></i>
						<span class="fa fa-chevron-down"></span>
					</button>
				  <ul class="dropdown-menu" role="menu">
					<shiro:hasPermission name="productcategory:productCategory:view">
						<li><a href="#" onclick="jp.openViewDialog('查看商品分类', '${ctx}/productcategory/productCategory/form?id={{d.row.id}}','800px', '500px')"><i class="fa fa-search-plus"></i> 查看</a></li>
					</shiro:hasPermission>
					<shiro:hasPermission name="productcategory:productCategory:edit">
						<li><a href="#" onclick="jp.openSaveDialog('修改商品分类', '${ctx}/productcategory/productCategory/form?id={{d.row.id}}','800px', '500px')"><i class="fa fa-edit"></i> 修改</a></li>
		   			</shiro:hasPermission>
		   			<shiro:hasPermission name="productcategory:productCategory:del">
		   				<li><a  onclick="return del(this, '{{d.row.id}}')"><i class="fa fa-trash"></i> 删除</a></li>
					</shiro:hasPermission>
		   			<shiro:hasPermission name="productcategory:productCategory:add">
		   			{{d.row.dict.type}}
					</shiro:hasPermission>
				  </ul>
				</div>
			</td>
	</script>