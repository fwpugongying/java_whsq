<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>商品分类管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {
			if ($('#id').val() == undefined || $('#id').val() == '' || $('#id').val() == '0' || $('#parentId').val() == undefined || $('#parentId').val() == '' || $('#parentId').val() == '0') {
				$('#hot').val("0");
				$('#hot').attr("disabled","disabled");
			} else {
				$('#hot').removeAttr("disabled");
			}
			$('#parentName').focus(function(){
				if($('#parentId').val() == undefined || $('#parentId').val() == '' || $('#parentId').val() == '0'){
					$('#hot').val("0");
					$('#hot').attr("disabled","disabled");
				} else {
					$('#hot').removeAttr("disabled");
				}
			});
		});

		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                $('#hot').removeAttr("disabled");
                jp.post("${ctx}/productcategory/productCategory/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refreshNode(data);
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="productCategory" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>名称：</label></td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right">上级分类：</label></td>
					<td class="width-35">
						<c:if test="${productCategory.type != '1'}">
							<sys:treeselect id="parent" name="parent.id" value="${productCategory.parent.id}" labelName="parent.name" labelValue="${productCategory.parent.name}"
							title="上级分类ID" url="/productcategory/productCategory/treeData1" extId="${productCategory.id}" cssClass="form-control " allowClear="true"/>
						</c:if>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>图标：</label></td>
					<td class="width-35">
						<sys:fileUpload path="icon"  value="${productCategory.icon}" type="image" fileNumLimit="1" uploadPath="/productcategory/productCategory"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>排序：</label></td>
					<td class="width-35">
						<form:input path="sort" htmlEscape="false"    class="form-control required isIntGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>状态：</label></td>
					<td class="width-35">
						<form:select path="state" class="form-control required">
							<form:options items="${fns:getDictList('product_category_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>推荐：</label></td>
					<td class="width-35">
						<form:select path="hot" class="form-control required">
							<form:options items="${fns:getDictList('product_category_hot')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
		 	</tbody>
		</table>
		</form:form>
</body>
</html>