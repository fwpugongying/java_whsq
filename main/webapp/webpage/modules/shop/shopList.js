<%@ page contentType="text/html;charset=UTF-8" %>
<script>
$(document).ready(function() {
	$('#shopTable').bootstrapTable({
		 
		  //请求方法
               method: 'post',
               //类型json
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               //显示检索按钮
	           showSearch: true,
               //显示刷新按钮
               showRefresh: true,
               //显示切换手机试图按钮
               showToggle: true,
               //显示 内容列下拉框
    	       showColumns: true,
    	       //显示到处按钮
    	       showExport: true,
    	       //显示切换分页按钮
    	       showPaginationSwitch: true,
    	       //最低显示2行
    	       minimumCountColumns: 2,
               //是否显示行间隔色
               striped: true,
               //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）     
               cache: false,    
               //是否显示分页（*）  
               pagination: true,   
                //排序方式 
               sortOrder: "asc",  
               //初始化加载第一页，默认第一页
               pageNumber:1,   
               //每页的记录行数（*）   
               pageSize: 10,  
               //可供选择的每页的行数（*）    
               pageList: [10, 25, 50, 100],
               //这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据  
               url: "${ctx}/shop/shop/data",
               //默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
               //queryParamsType:'',   
               ////查询参数,每次调用是会带上这个参数，可自定义                         
               queryParams : function(params) {
               	var searchParam = $("#searchForm").serializeJSON();
               	searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
               	searchParam.pageSize = params.limit === undefined? -1 : params.limit;
               	searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
                   return searchParam;
               },
               //分页方式：client客户端分页，server服务端分页（*）
               sidePagination: "server",
               contextMenuTrigger:"right",//pc端 按右键弹出菜单
               contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
               contextMenu: '#context-menu',
               onContextMenuItem: function(row, $el){
                   if($el.data("item") == "edit"){
                   		edit(row.id);
                   }else if($el.data("item") == "view"){
                       view(row.id);
                   } else if($el.data("item") == "delete"){
                        jp.confirm('确认要删除该实体商家记录吗？', function(){
                       	jp.loading();
                       	jp.get("${ctx}/shop/shop/delete?id="+row.id, function(data){
                   	  		if(data.success){
                   	  			$('#shopTable').bootstrapTable('refresh');
                   	  			jp.success(data.msg);
                   	  		}else{
                   	  			jp.error(data.msg);
                   	  		}
                   	  	})
                   	   
                   	});
                      
                   } 
               },
              
               onClickRow: function(row, $el){
               },
               	onShowSearch: function () {
			$("#search-collapse").slideToggle();
		},
               columns: [{
		        checkbox: true
		       
		    }
			,{
		        
				field: 'title',
		        title: '商户名称',
		        sortable: true,
		        sortName: 'title'
		        ,formatter:function(value, row , index){

			   if(value == null || value ==""){
				   value = "-";
			   }
			   <c:choose>
				   <c:when test="${fns:hasPermission('shop:shop:edit')}">
				      return "<a href='javascript:edit(\""+row.id+"\")'>"+value+"</a>";
			      </c:when>
				  <c:when test="${fns:hasPermission('shop:shop:view')}">
				      return "<a href='javascript:view(\""+row.id+"\")'>"+value+"</a>";
			      </c:when>
				  <c:otherwise>
				      return value;
			      </c:otherwise>
			   </c:choose>

		        }
		       
		    }
			
			,{
				field: 'member.phone',
		        title: '用户',
		        sortable: true,
		        sortName: 'member.phone'
		    }
		    ,{
		        field: 'icon',
		        title: '店铺LOGO',
		        sortable: true,
		        sortName: 'icon',
		        formatter:function(value, row , index){
		        	if(value != null && value != ''){
		        		return '<img   onclick="jp.showPic(\''+value+'\')"'+' height="50px" src="'+value+'">';
		        	}
		        	return "";
		        }
		       
		    }
			,{
		        field: 'shopCode',
		        title: '编码行业',
		        sortable: true,
		        sortName: 'shopCode',
			   formatter:function(value, row , index){
				   return "店铺编码："+row.shopCode+"</br>"+"优惠券编码："+row.couponCode+"</br>"+"行业："+row.category.title;
			   }
		    }
			,{
		        field: 'city',
		        title: '地址及联系人信息',
		        sortable: true,
		        sortName: 'city',
			   formatter:function(value, row , index){
				   return "联系人："+row.username+"</br>"+"手机号："+row.phone+"</br>"+"城市："+row.city+"</br>"+"地址："+row.address+"</br><span style=\"color: #ffbb02;\">签约人："+row.invitePhone+"</span>";
			   }
		    }
		    ,{
		        field: 'licence',
		        title: '营业执照',
		        formatter:function(value, row , index){
		        	if(value != null && value != ''){
		        		return '<img   onclick="jp.showPic(\''+value+'\')"'+' height="50px" src="'+value+'">';
		        	}
		        	return "";
		        }
		       
		    }
		    ,{
		        field: 'idcard',
		        title: '身份证',
		        formatter:function(value, row , index){
		        	var labelArray = [];
		        	if(value != null && value != ''){
		        		var valueArray = value.split("|");
			        	for(var i =0 ; i<valueArray.length; i++){
			        		if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(valueArray[i]))
			        		{
			        			labelArray[i] = "<a href=\""+valueArray[i]+"\" url=\""+valueArray[i]+"\" target=\"_blank\">"+decodeURIComponent(valueArray[i].substring(valueArray[i].lastIndexOf("/")+1))+"</a>"
			        		}else{
			        			labelArray[i] = '<img   onclick="jp.showPic(\''+valueArray[i]+'\')"'+' height="50px" src="'+valueArray[i]+'">';
			        		}
			        	}
		        	}
		        	
		        	return labelArray.join(" ");
		        }
		       
		    }
		    ,{
		        field: 'others',
		        title: '其他证书',
		        formatter:function(value, row , index){
		        	var labelArray = [];
		        	if(value != null && value != ''){
		        		var valueArray = value.split("|");
			        	for(var i =0 ; i<valueArray.length; i++){
			        		if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(valueArray[i]))
			        		{
			        			labelArray[i] = "<a href=\""+valueArray[i]+"\" url=\""+valueArray[i]+"\" target=\"_blank\">"+decodeURIComponent(valueArray[i].substring(valueArray[i].lastIndexOf("/")+1))+"</a>"
			        		}else{
			        			labelArray[i] = '<img   onclick="jp.showPic(\''+valueArray[i]+'\')"'+' height="50px" src="'+valueArray[i]+'">';
			        		}
			        	}
		        	}
		        	
		        	return labelArray.join(" ");
		        }
		       
		    }
			,{
		        field: 'createDate',
		        title: '入驻时间',
		        sortable: true,
		        sortName: 'createDate',
			   formatter:function(value, row , index){
				   return "入驻时间："+row.createDate+"</br>"+"审核时间："+row.auditDate+"</br>";
			   }
		    }
		   ,{
			   field: 'member.txopenid',
			   title: '是否绑定',
			   formatter:function(value, row , index){
				   if(value != null && value != ''){
					   return '已绑定';
				   }
				   return "未绑定";
			   }

		   }
			,{
		        field: 'state',
		        title: '商户状态显示',
		        sortable: true,
		        sortName: 'state',
		        formatter:function(value, row , index){
					return jp.getDictLabel(${fns:toJson(fns:getDictList('member_state'))}, row.state, "-")+"</br>"+"爱心标记状态："+jp.getDictLabel(${fns:toJson(fns:getDictList('product_category_hot'))}, row.lovetype, "-")+"</br><span style=\"color: #ffbb02;\">审核状态："+jp.getDictLabel(${fns:toJson(fns:getDictList('shop_audit'))}, row.auditState, "-")+"</span>"+"</br><span style=\"color: darkred;\">支付方式："+jp.getDictLabel(${fns:toJson(fns:getDictList('pay_type'))}, row.payType, "-")+"</span>"
		        }
		       
		    }

		   ,{
			   field: 'hyrebate',
			   title: '会员返利比例',
			   sortable: true,
			   sortName: 'hyrebate'
		   }
		   ,{
			   field: 'ptrebate',
			   title: '平台返利比例（铜板）',
			   sortable: true,
			   sortName: 'ptrebate'
		   }
			,{
		        field: 'amount',
		        title: '入驻金额',
		        sortable: true,
		        sortName: 'amount'
		       
		    }
		   ,{
			   field: 'paymoney',
			   title: '代理产品差额',
			   sortable: true,
			   sortName: 'paymoney'

		   }
			,{
		        field: 'remarks',
		        title: '备注',
		        sortable: true,
		        sortName: 'remarks'
		       
		    }
			,{
		        field: 'showState',
		        title: '简介状态',
		        sortable: true,
		        sortName: 'showState',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('shop_show'))}, value, "-");
		        }
		       
		    }
			,{
		        field: 'balance',
		        title: '余额',
		        sortable: true,
		        sortName: 'balance'
		       
		    }
			,{
		        field: '',
		        title: '操作',
		        formatter:function(value, row , index){
		        	var result = "";
		        	<c:if test="${fns:hasPermission('shop:shop:edit')}">
					if (row.auditState == '1'||row.auditState == '0') {
						result += "<a href='javascript:audit(\""+row.id+"\",2)' class=\"label label-success\">同意入驻</a>";
						result += "<a href='javascript:audit(\""+row.id+"\",3)' class=\"label label-danger\">拒绝入驻</a>";
					} else {
						if(row.showState == '0'){
							result += "<a href='javascript:auditShow(\""+row.id+"\",1)' class=\"label label-success\">简介通过</a>";
							result += "<a href='javascript:auditShow(\""+row.id+"\",2)' class=\"label label-danger\">简介拒绝</a>";
						}
						if(row.state == '0'){
							result += "<a href='javascript:updateState(\""+row.id+"\",1)' class=\"label label-default\">冻结</a>";
						} else if(row.state == '1'){
							result += "<a href='javascript:updateState(\""+row.id+"\",0)' class=\"label label-primary\">解冻</a>";
						} 
					}
					
					</c:if>
					return result;
		        }
		       
		    }
		     ]
		
		});
		
		  
	  if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端

		 
		  $('#shopTable').bootstrapTable("toggleView");
		}
	  
	  $('#shopTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $('#remove').prop('disabled', ! $('#shopTable').bootstrapTable('getSelections').length);
            $('#view,#edit').prop('disabled', $('#shopTable').bootstrapTable('getSelections').length!=1);
        });
		  
		$("#btnImport").click(function(){
			jp.open({
			    type: 2,
                area: [500, 200],
                auto: true,
			    title:"导入数据",
			    content: "${ctx}/tag/importExcel" ,
			    btn: ['下载模板','确定', '关闭'],
				    btn1: function(index, layero){
					  jp.downloadFile('${ctx}/shop/shop/import/template');
				  },
			    btn2: function(index, layero){
				        var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
						iframeWin.contentWindow.importExcel('${ctx}/shop/shop/import', function (data) {
							if(data.success){
								jp.success(data.msg);
								refresh();
							}else{
								jp.error(data.msg);
							}
					   		jp.close(index);
						});//调用保存事件
						return false;
				  },
				 
				  btn3: function(index){ 
					  jp.close(index);
	    	       }
			}); 
		});
		
		
	 $("#export").click(function(){//导出Excel文件
	        var searchParam = $("#searchForm").serializeJSON();
	        searchParam.pageNo = 1;
	        searchParam.pageSize = -1;
            var sortName = $('#shopTable').bootstrapTable("getOptions", "none").sortName;
            var sortOrder = $('#shopTable').bootstrapTable("getOptions", "none").sortOrder;
            var values = "";
            for(var key in searchParam){
                values = values + key + "=" + searchParam[key] + "&";
            }
            if(sortName != undefined && sortOrder != undefined){
                values = values + "orderBy=" + sortName + " "+sortOrder;
            }

			jp.downloadFile('${ctx}/shop/shop/export?'+values);
	  })

		    
	  $("#search").click("click", function() {// 绑定查询按扭
		  $('#shopTable').bootstrapTable('refresh');
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
		  $("#searchForm  input").val("");
		  $("#searchForm  select").val("");
		  $("#searchForm  .select-item").html("");
		  $('#shopTable').bootstrapTable('refresh');
		});
		
		
	});
		
  function getIdSelections() {
        return $.map($("#shopTable").bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }
  
  function deleteAll(){

		jp.confirm('确认要删除该实体商家记录吗？', function(){
			jp.loading();  	
			jp.get("${ctx}/shop/shop/deleteAll?ids=" + getIdSelections(), function(data){
         	  		if(data.success){
         	  			$('#shopTable').bootstrapTable('refresh');
         	  			jp.success(data.msg);
         	  		}else{
         	  			jp.error(data.msg);
         	  		}
         	  	})
          	   
		})
  }

    //刷新列表
  function refresh(){
  	$('#shopTable').bootstrapTable('refresh');
  }
  
   function add(){
	  jp.openSaveDialog('新增实体商家', "${ctx}/shop/shop/form",'800px', '500px');
  }


  
   function edit(id){//没有权限时，不显示确定按钮
       if(id == undefined){
	      id = getIdSelections();
	}
	jp.openSaveDialog('编辑实体商家', "${ctx}/shop/shop/form?id=" + id, '800px', '500px');
  }
  
 function view(id){//没有权限时，不显示确定按钮
      if(id == undefined){
             id = getIdSelections();
      }
        jp.openViewDialog('查看实体商家', "${ctx}/shop/shop/form?id=" + id, '800px', '500px');
 }

 function audit(id,type){
	  var title = "";
	  if(type=='2'){
		  title="确认要通过该店铺入驻审核吗？";
	  }else{
		  title="确认要拒绝该店铺入驻审核吗？";
	  }
	  jp.confirm(title, function(){
		  if(type=='2'){
			  jp.loading();  	
			  jp.get("${ctx}/shop/shop/audit?id=" + id + "&type=" + type, function(data){
				  if(data.success){
					  jp.success(data.msg);
				  }else{
					  jp.error(data.msg);
				  }
				  $('#shopTable').bootstrapTable('refresh');
			  })
		  } else {
			  jp.prompt("备注", function(text) {
				  jp.loading();  	
				  jp.get("${ctx}/shop/shop/audit?id=" + id + "&type=" + type + "&reason=" + text, function(data){
					  if(data.success){
						  jp.success(data.msg);
					  }else{
						  jp.error(data.msg);
					  }
					  $('#shopTable').bootstrapTable('refresh');
				  })
				});
		  }
	  })
 }
 function auditShow(id,type){
	 var title = "";
	 if(type=='1'){
		 title="确认要通过该店铺简介展示吗？";
	 }else{
		 title="确认要拒绝该店铺简介展示吗？";
	 }
	 jp.confirm(title, function(){
		 jp.loading();  	
		 jp.get("${ctx}/shop/shop/auditShow?id=" + id + "&type=" + type, function(data){
			 if(data.success){
				 jp.success(data.msg);
			 }else{
				 jp.error(data.msg);
			 }
			 $('#shopTable').bootstrapTable('refresh');
		 })
		 
	 })
 }
 function updateState(id,type){
	 var title = "";
	 if(type=='1'){
		 title="确认要冻结该店铺吗？";
	 }else{
		 title="确认要解冻该店铺吗？";
	 }
	 jp.confirm(title, function(){
		 jp.loading();  	
		 jp.get("${ctx}/shop/shop/updateState?id=" + id + "&type=" + type, function(data){
			 if(data.success){
				 jp.success(data.msg);
			 }else{
				 jp.error(data.msg);
			 }
			 $('#shopTable').bootstrapTable('refresh');
		 })
		 
	 })
 }

</script>