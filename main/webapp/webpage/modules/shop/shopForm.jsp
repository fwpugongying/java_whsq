<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>实体商家管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/shop/shop/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="shop" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>用户：</label></td>
					<td class="width-35">
						<sys:gridselect disabled="disabled" url="${ctx}/member/member/data" id="member" name="member.id" value="${shop.member.id}" labelName="member.phone" labelValue="${shop.member.phone}"
							 title="选择用户" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商户名称：</label></td>
					<td class="width-35">
						<form:input path="title" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">店铺编码：</label></td>
					<td class="width-35">
						<form:input path="shopCode" htmlEscape="false" disabled="true"   class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right">优惠券编码：</label></td>
					<td class="width-35">
						<form:input path="couponCode" htmlEscape="false"    class="form-control required"/>
					</td>
					
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">店铺LOGO：</label></td>
					<td class="width-35">
						<sys:fileUpload path="icon"  value="${shop.icon}" type="image" fileNumLimit="1" uploadPath="/shop/shop"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>行业：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/shopcategory/shopCategory/data" id="category" name="category.id" value="${shop.category.id}" labelName="category.title" labelValue="${shop.category.title}"
							 title="选择行业" cssClass="form-control required" fieldLabels="行业名称" fieldKeys="title" searchLabels="行业名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>城市：</label></td>
					<td class="width-35">
						<form:input path="city" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>地址：</label></td>
					<td class="width-35">
						<form:input path="address" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>经度：</label></td>
					<td class="width-35">
						<form:input path="lon" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>纬度：</label></td>
					<td class="width-35">
						<form:input path="lat" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr> --%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>联系人：</label></td>
					<td class="width-35">
						<form:input path="username" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>手机号：</label></td>
					<td class="width-35">
						<form:input path="phone" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>签约人：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/member/member/data" id="invite" name="invite.id" value="${shop.invite.id}" labelName="invite.phone" labelValue="${shop.invite.phone}"
							 title="选择签约人" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right">营业执照：</label></td>
					<td class="width-35">
						<sys:fileUpload path="licence"  value="${shop.licence}" type="file" uploadPath="/shop/shop"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">身份证：</label></td>
					<td class="width-35">
						<sys:fileUpload path="idcard"  value="${shop.idcard}" type="file" uploadPath="/shop/shop"/>
					</td>
					<td class="width-15 active"><label class="pull-right">其他证书：</label></td>
					<td class="width-35">
						<sys:fileUpload path="others"  value="${shop.others}" type="file" uploadPath="/shop/shop"/>
					</td>
				</tr> --%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>状态：</label></td>
					<td class="width-35">
						<form:select path="state" class="form-control required">
							<form:options items="${fns:getDictList('member_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>入驻金额：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<%-- <td class="width-15 active"><label class="pull-right"><font color="red">*</font>审核状态：</label></td>
					<td class="width-35">
						<form:select path="auditState" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('shop_audit')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td> --%>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>平台返利比例：</label></td>
					<td class="width-35">
						<form:input path="ptrebate" htmlEscape="false" max="1"   class="form-control required isFloatGtZero"/>
						<span class="help-inline">例：0.01表示1%必须大于0</span>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>会员返利比例：</label></td>
					<td class="width-35">
						<form:input path="hyrebate" htmlEscape="false" max="1"   class="form-control required isFloatGteZero"/>
						<span class="help-inline">例：0.01表示1%,可以为0</span>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>是否爱心标记：</label></td>
					<td class="width-35">
						<form:select path="lovetype" class="form-control required">
							<form:options items="${fns:getDictList('product_category_hot')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					
					<%-- <td class="width-15 active"><label class="pull-right">支付方式：</label></td>
					<td class="width-35">
						<form:select path="payType" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('pay_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td> --%>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">店铺简介：</label></td>
					<td class="width-35" colspan="3">
						<form:textarea path="content" htmlEscape="false" rows="4"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">店铺图片：</label></td>
					<td class="width-35" colspan="3">
						<c:forEach items="${fn:split(shop.images, '|')}" var="image">
							<img   onclick="jp.showPic('${image}')" height="50px" src="${image}">
						</c:forEach>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>简介状态：</label></td>
					<td class="width-35">
						<form:select path="showState" class="form-control required">
							<form:options items="${fns:getDictList('shop_show')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>