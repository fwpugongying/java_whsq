<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>信用卡订单管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/creditcardorder/creditCardOrder/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="creditCardOrder" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>用户：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/member/member/data" id="member" name="member.id" value="${creditCardOrder.member.id}" labelName="member.phone" labelValue="${creditCardOrder.member.phone}"
							 title="选择用户" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>信用卡编码：</label></td>
					<td class="width-35">
						<form:input path="code" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<%-- <td class="width-15 active"><label class="pull-right">接口落地页：</label></td>
					<td class="width-35">
						<form:input path="url" htmlEscape="false"    class="form-control "/>
					</td> --%>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>铜板：</label></td>
					<td class="width-35">
						<form:input path="point" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right">结算金额：</label></td>
					<td class="width-35">
						<form:input path="money" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">结算状态：</label></td>
					<td class="width-35">
						<form:select path="orderStatus" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('credit_order_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"> 信用卡状态：</label></td>
					<td class="width-35">
						<form:select path="cardStatus" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('credit_order_card_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>