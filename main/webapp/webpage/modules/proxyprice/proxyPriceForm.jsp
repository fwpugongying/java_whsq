<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>单品代理价格管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/proxyprice/proxyPrice/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="proxyPrice" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>总成长值：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>云店成长值：</label></td>
					<td class="width-35">
						<form:input path="ydAmount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<%-- <td class="width-15 active"><label class="pull-right"><font color="red">*</font>年限：</label></td>
					<td class="width-35">
						<form:input path="years" htmlEscape="false"    class="form-control required isIntGtZero"/>
					</td> --%>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>铜板成长值：</label></td>
					<td class="width-35">
						<form:input path="tbAmount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>年费：</label></td>
					<td class="width-35">
						<form:input path="price" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>增选价格：</label></td>
					<td class="width-35">
						<form:input path="pickPrice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"></td>
					<td class="width-35">
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>单品代理价格：</label></td>
					<td class="width-35">
						<form:input path="dpdlPrice" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"></td>
					<td class="width-35">
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>