<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>提现费率管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {
			$("#prop").val(parseFloat('${cashProp.prop}'));
		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/cashprop/cashProp/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="cashProp" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>费率：</label></td>
					<td class="width-35">
						<form:input path="prop" htmlEscape="false" max="1"   class="form-control required isFloatGteZero"/>
						<span class="help-inline">例：0.01表示1%</span>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>类型 ：</label></td>
					<td class="width-35">
						<form:select path="type" disabled="true" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('cash_prop_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>