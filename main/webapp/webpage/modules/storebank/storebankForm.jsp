<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>厂家提现管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
				var amount = $("#amount").val();
				var balance = $("#balance").val();
				if(amount > balance){
				} else {
					jp.loading();
	                jp.post("${ctx}/storebank/storebank/save",$('#inputForm').serialize(),function(data){
	                    if(data.success){
	                        jp.getParent().refresh();
	                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
	                        parent.layer.close(dialogIndex);
	                        jp.success(data.msg)

	                    }else{
	                        jp.error(data.msg);
	                    }
	                })
				}
			}
        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="storebank" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>姓名：</label></td>
					<td class="width-35">
						<form:input path="username" htmlEscape="false" value="${username}"   class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>银行：</label></td>
					<td class="width-35">
<%--						<form:input path="bank" htmlEscape="false"    class="form-control required"/>--%>
						<form:select path="bank" value="${bank}" class="form-control m-b">
							<form:option value="中国银行" label="中国银行"/>
							<form:option value="交通银行" label="交通银行"/>
							<form:option value="中信银行" label="中信银行"/>
							<form:option value="华夏银行" label="华夏银行"/>
							<form:option value="招商银行" label="招商银行"/>
							<form:option value="平安银行" label="平安银行"/>
							<form:option value="兴业银行" label="兴业银行"/>
							<form:option value="浦发银行" label="浦发银行"/>
							<form:option value="北京银行" label="北京银行"/>
							<form:option value="上海银行" label="上海银行"/>
							<form:option value="江苏银行" label="江苏银行"/>
							<form:option value="浙商银行" label="浙商银行"/>
							<form:option value="恒丰银行" label="恒丰银行"/>
							<form:option value="中国工商银行" label="中国工商银行"/>
							<form:option value="中国建设银行" label="中国建设银行"/>
							<form:option value="中国光大银行" label="中国光大银行"/>
							<form:option value="中国农业银行" label="中国农业银行"/>
							<form:option value="中国民生银行" label="中国民生银行"/>
							<form:option value="中国邮政储蓄银行" label="中国邮政储蓄银行"/>
							<form:option value="广发银行股份有限公司" label="广发银行股份有限公司"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>卡号：</label></td>
					<td class="width-35">
						<form:input path="account" htmlEscape="false" value="${account}"   class="form-control required"/>
					</td>
					<form:input path="state" type="hidden" htmlEscape="false" value="${state}"   class="form-control "/>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>