<%@ page contentType="text/html;charset=UTF-8" %>
	<script>
	$(document).ready(function() {
		$('#tTaokeOrderTable').bootstrapTable({

			//请求方法
			method: 'post',
			//类型json
			dataType: "json",
			contentType: "application/x-www-form-urlencoded",
			//显示检索按钮
			showSearch: true,
			//显示刷新按钮
			showRefresh: true,
			//显示切换手机试图按钮
			showToggle: true,
			//显示 内容列下拉框
			showColumns: true,
			//显示到处按钮
			showExport: true,
			//显示切换分页按钮
			showPaginationSwitch: true,
			//最低显示2行
			minimumCountColumns: 2,
			//是否显示行间隔色
			striped: true,
			//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			cache: false,
			//是否显示分页（*）
			pagination: true,
			//排序方式
			sortOrder: "asc",
			//初始化加载第一页，默认第一页
			pageNumber:1,
			//每页的记录行数（*）
			pageSize: 10,
			//可供选择的每页的行数（*）
			pageList: [10, 25, 50, 100],
			//这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据
			url: "${ctx}/taokeorder/tTaokeOrder/data",
			//默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
			//queryParamsType:'',
			////查询参数,每次调用是会带上这个参数，可自定义
			queryParams : function(params) {
				var searchParam = $("#searchForm").serializeJSON();
				searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
				searchParam.pageSize = params.limit === undefined? -1 : params.limit;
				searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
				return searchParam;
			},
			//分页方式：client客户端分页，server服务端分页（*）
			sidePagination: "server",
			contextMenuTrigger:"right",//pc端 按右键弹出菜单
			contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
			contextMenu: '#context-menu',
			onContextMenuItem: function(row, $el){
				if($el.data("item") == "edit"){
					edit(row.id);
				}else if($el.data("item") == "view"){
					view(row.id);
				} else if($el.data("item") == "delete"){
					jp.confirm('确认要删除该淘客订单管理记录吗？', function(){
						jp.loading();
						jp.get("${ctx}/taokeorder/tTaokeOrder/delete?id="+row.id, function(data){
							if(data.success){
								$('#tTaokeOrderTable').bootstrapTable('refresh');
								jp.success(data.msg);
							}else{
								jp.error(data.msg);
							}
						})

					});

				}
			},

			onClickRow: function(row, $el){
			},
			onShowSearch: function () {
				$("#search-collapse").slideToggle();
			},
			columns: [{
				checkbox: true

			}
				,{
					field: 'member.phone',
					title: '用户',
					sortable: true,
					sortName: 'member.phone'

				}
				,{
					field: 'member.phone',
					title: '订单信息',
					sortable: true,
					sortName: 'member.phone'
					,formatter:function(value, row , index){
						return "<span style=\"color: #ffbb02;\">订单编号："+row.id+"</span></br>"+"创建时间："+row.tkCreateTime+"</br>支付时间："+row.tbPaidTime;
					}
				}

				,{
					field: 'itemImg',
					title: '商品图片',
					formatter:function(value, row , index){
						var labelArray = [];
						if(value != null && value != ''){
							var valueArray = value.split("|");
							for(var i =0 ; i<valueArray.length; i++){
								if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(valueArray[i]))
								{
									labelArray[i] = "<a href=\""+valueArray[i]+"\" url=\""+valueArray[i]+"\" target=\"_blank\">"+decodeURIComponent(valueArray[i].substring(valueArray[i].lastIndexOf("/")+1))+"</a>"
								}else{
									labelArray[i] = '<img   onclick="jp.showPic(\''+valueArray[i]+'\')"'+' height="50px" src="'+valueArray[i]+'">';
								}
							}
						}
						return labelArray.join(" ");
					}
				}
				,{
					field: 'itemId',
					title: '商品ID',
					sortable: true,
					sortName: 'itemId'
					,
					formatter:function(value, row , index){
						return 	'商品ID：'+row.itemId+"</br><span style=\"color: #51b11f;\">( "+row.itemTitle+" )</span ></br><span style=\"color: #ff3b6b;\">数量 : "+row.itemNum+"</span>";
					}

				}

				,{
					field: 'type',
					title: '类型',
					sortable: true,
					sortName: 'type',
					formatter:function(value, row , index){
						return jp.getDictLabel(${fns:toJson(fns:getDictList('tk_type'))}, value, "-");
					}

				}
				,{
					field: 'tkStatus',
					title: '状态',
					sortable: true,
					sortName: 'tkStatus',
					formatter:function(value, row , index){
						//return jp.getDictLabel(${fns:toJson(fns:getDictList('tk_status'))}, value, "-");
						if(row.type=='0'){
							if(value=='3'){
								return "订单结算";
							}else if(value=='12'){
								return "订单付款";
							}else if(value=='13'){
								return "订单失效";
							}else if(value=='14'){
								return "订单成功";
							}
						}else if(row.type=='1'){
							if(value=='-1'){
								return "未支付";
							}else if(value=='0'){
								return "已支付";
							}else if(value=='1'){
								return "已成团";
							}else if(value=='2'){
								return "确认收货";
							}else if(value=='3'){
								return "审核成功";
							}else if(value=='4'){
								return "审核失败";
							}else if(value=='5'){
								return "已经结算";
							}else if(value=='8'){
								return "非多多进宝商品";
							}else if(value=='10'){
								return "已处罚";
							}
						}else if(row.type=='2'){
							if(value=='15'){
								return "待付款";
							}else if(value=='16'){
								return "已付款";
							}else if(value=='17'){
								return "已完成";
							}else if(value=='18'){
								return "已结算";
							}else{
								return "已失效";
							}
						}
						return "";
					}

				}
				,{
					field: 'itemPrice',
					title: '商品价格',
					sortable: true,
					sortName: 'itemPrice'
					,formatter:function(value, row , index){
						return "<span style=\"color: #ffbb02;\">付款金额（不包含运费金额）："+row.alipayTotalPrice+"</span></br>商品单价："+row.itemPrice+"</br>"+"预估佣金："+row.pubSharePreFee+"</br>"+"佣金金额=结算金额*佣金比率："+row.totalCommissionFee;
					}

				}
				,{
					field: 'ygtb',
					title: '铜板',
					sortable: false
					,formatter:function(value, row , index){
						return "预估铜板："+row.ygtb+"</br>"+"结算铜板："+row.jstb;
					}
				}

			]

		});


		if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端


			$('#tTaokeOrderTable').bootstrapTable("toggleView");
		}

		$('#tTaokeOrderTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
			'check-all.bs.table uncheck-all.bs.table', function () {
			$('#remove').prop('disabled', ! $('#tTaokeOrderTable').bootstrapTable('getSelections').length);
			$('#view,#edit').prop('disabled', $('#tTaokeOrderTable').bootstrapTable('getSelections').length!=1);
		});

		$("#btnImport").click(function(){
			jp.open({
				type: 2,
				area: [500, 200],
				auto: true,
				title:"导入数据",
				content: "${ctx}/tag/importExcel" ,
				btn: ['下载模板','确定', '关闭'],
				btn1: function(index, layero){
					jp.downloadFile('${ctx}/taokeorder/tTaokeOrder/import/template');
				},
				btn2: function(index, layero){
					var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
					iframeWin.contentWindow.importExcel('${ctx}/taokeorder/tTaokeOrder/import', function (data) {
						if(data.success){
							jp.success(data.msg);
							refresh();
						}else{
							jp.error(data.msg);
						}
						jp.close(index);
					});//调用保存事件
					return false;
				},

				btn3: function(index){
					jp.close(index);
				}
			});
		});


		$("#export").click(function(){//导出Excel文件
			var searchParam = $("#searchForm").serializeJSON();
			searchParam.pageNo = 1;
			searchParam.pageSize = -1;
			var sortName = $('#tTaokeOrderTable').bootstrapTable("getOptions", "none").sortName;
			var sortOrder = $('#tTaokeOrderTable').bootstrapTable("getOptions", "none").sortOrder;
			var values = "";
			for(var key in searchParam){
				values = values + key + "=" + searchParam[key] + "&";
			}
			if(sortName != undefined && sortOrder != undefined){
				values = values + "orderBy=" + sortName + " "+sortOrder;
			}

			jp.downloadFile('${ctx}/taokeorder/tTaokeOrder/export?'+values);
		})


		$("#search").click("click", function() {// 绑定查询按扭
			$('#tTaokeOrderTable').bootstrapTable('refresh');
		});

		$("#reset").click("click", function() {// 绑定查询按扭
			$("#searchForm  input").val("");
			$("#searchForm  select").val("");
			$("#searchForm  .select-item").html("");
			$('#tTaokeOrderTable').bootstrapTable('refresh');
		});

		$('#beginTkCreateTime').datetimepicker({
			format: "YYYY-MM-DD HH:mm:ss"
		});
		$('#endTkCreateTime').datetimepicker({
			format: "YYYY-MM-DD HH:mm:ss"
		});

	});

function getIdSelections() {
	return $.map($("#tTaokeOrderTable").bootstrapTable('getSelections'), function (row) {
		return row.id
	});
}

function deleteAll(){

	jp.confirm('确认要删除该淘客订单管理记录吗？', function(){
		jp.loading();
		jp.get("${ctx}/taokeorder/tTaokeOrder/deleteAll?ids=" + getIdSelections(), function(data){
			if(data.success){
				$('#tTaokeOrderTable').bootstrapTable('refresh');
				jp.success(data.msg);
			}else{
				jp.error(data.msg);
			}
		})

	})
}

//刷新列表
function refresh(){
	$('#tTaokeOrderTable').bootstrapTable('refresh');
}

function add(){
	jp.openSaveDialog('新增淘客订单管理', "${ctx}/taokeorder/tTaokeOrder/form",'800px', '500px');
}



function edit(id){//没有权限时，不显示确定按钮
	if(id == undefined){
		id = getIdSelections();
	}
	jp.openSaveDialog('编辑淘客订单管理', "${ctx}/taokeorder/tTaokeOrder/form?id=" + id, '800px', '500px');
}

function view(id){//没有权限时，不显示确定按钮
	if(id == undefined){
		id = getIdSelections();
	}
	jp.openViewDialog('查看淘客订单管理', "${ctx}/taokeorder/tTaokeOrder/form?id=" + id, '800px', '500px');
}



</script>