<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>轮播图管理</title>
	<meta name="decorator" content="ani"/>
	<!-- SUMMERNOTE -->
	<%@include file="/webpage/include/summernote.jsp" %>
	<script type="text/javascript">

		$(document).ready(function() {
			// 1富文本 2商品
			if($("#category").val() == '1') {
				$("._content").show();
				$("._product").hide();
				$("._store").hide();
				$("._thirdLink").hide();
				$("._productCategory").hide();
			} else if($("#category").val() == '2') {
				$("._content").hide();
				$("._product").show();
				$("._store").hide();
				$("._thirdLink").hide();
				$("._productCategory").hide();
			}else if($("#category").val() == '3'){
				$("._store").show();
				$("._thirdLink").hide();
				$("._content").hide();
				$("._product").hide();
				$("._productCategory").hide();
			}else if($("#category").val() == '4'){
				$("._thirdLink").show();
				$("._content").hide();
				$("._product").hide();
				$("._store").hide();
				$("._productCategory").hide();
			}else if($("#category").val() == '5'){
				$("._thirdLink").hide();
				$("._content").hide();
				$("._product").hide();
				$("._store").hide();
				$("._productCategory").show();
			}else if($("#category").val() == '6'){
				$("._thirdLink").show();
				$("._content").hide();
				$("._product").hide();
				$("._store").hide();
				$("._productCategory").hide();
			}else if($("#category").val() == '7'){
				$("._thirdLink").show();
				$("._content").hide();
				$("._product").hide();
				$("._store").hide();
				$("._productCategory").hide();
			}else if($("#category").val() == '8'){
				$("._thirdLink").show();
				$("._content").hide();
				$("._product").hide();
				$("._store").hide();
				$("._productCategory").hide();
			}
			
			$("#category").change(function(){
				// 1富文本 2商品
				if($("#category").val() == '1') {
					$("._content").show();
					$("._product").hide();
					$("._store").hide();
					$("._thirdLink").hide();
					$("._productCategory").hide();
				} else if($("#category").val() == '2') {
					$("._content").hide();
					$("._product").show();
					$("._store").hide();
					$("._thirdLink").hide();
					$("._productCategory").hide();
				}else if($("#category").val() == '3'){
					$("._store").show();
					$("._thirdLink").hide();
					$("._content").hide();
					$("._product").hide();
					$("._productCategory").hide();
				}else if($("#category").val() == '4'){
					$("._thirdLink").show();
					$("._content").hide();
					$("._product").hide();
					$("._store").hide();
					$("._productCategory").hide();
				}else if($("#category").val() == '5'){
					$("._thirdLink").hide();
					$("._content").hide();
					$("._product").hide();
					$("._store").hide();
					$("._productCategory").show();
				}else if($("#category").val() == '6'){
					$("._thirdLink").show();
					$("._content").hide();
					$("._product").hide();
					$("._store").hide();
					$("._productCategory").hide();
				}else if($("#category").val() == '7'){
					$("._thirdLink").show();
					$("._content").hide();
					$("._product").hide();
					$("._store").hide();
					$("._productCategory").hide();
				}else if($("#category").val() == '8'){
					$("._thirdLink").show();
					$("._content").hide();
					$("._product").hide();
					$("._store").hide();
					$("._productCategory").hide();
				}
			});
			
			$('#content').summernote({
				height: 300,
                lang: 'zh-CN',
                callbacks: {
                    onChange: function(contents, $editable) {
                        $("input[name='content']").val($('#content').summernote('code'));//取富文本的值
                    },
                    onImageUpload: function(files, editor, $editable) {
		            	for(var i = 0; i < files.length; i++) {
		                     sendFile(files[i], this);
		            	}
		            }
                }
            });
		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
				if($("#category").val() == '1'){
					$("#productId").val("");
					$("#storeId").val("");
					$("#thirdLink").val("");
					$("#productCategoryId").val("");
				} else if($("#category").val() == '2') {
					$("input[name='content']").val("");
					$("#storeId").val("");
					$("#thirdLink").val("");
					$("#productCategoryId").val("");
				}else if($("#category").val() == '3') {
					$("input[name='content']").val("");
					$("#productId").val("");
					$("#thirdLink").val("");
					$("#productCategoryId").val("");
				}else if($("#category").val() == '4') {
					$("input[name='content']").val("");
					$("#productId").val("");
					$("#storeId").val("");
					$("#productCategoryId").val("");
				}else if($("#category").val() == '5') {
					$("input[name='content']").val("");
					$("#productId").val("");
					$("#storeId").val("");
					$("#thirdLink").val("");
				}else if($("#category").val() == '6') {
					$("input[name='content']").val("");
					$("#productId").val("");
					$("#storeId").val("");
					$("#productCategoryId").val("");
				}else if($("#category").val() == '7') {
					$("input[name='content']").val("");
					$("#productId").val("");
					$("#storeId").val("");
					$("#productCategoryId").val("");
				}else if($("#category").val() == '8') {
					$("input[name='content']").val("");
					$("#productId").val("");
					$("#storeId").val("");
					$("#productCategoryId").val("");
				}
                jp.loading();
                jp.post("${ctx}/banner/banner/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="banner" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">图片：</label></td>
					<td class="width-35">
						<sys:fileUpload path="image"  value="${banner.image}" type="image" fileNumLimit="1" uploadPath="/banner/banner"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>位置：</label></td>
					<td class="width-35">
						<form:select path="type" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('banner_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>类型：</label></td>
					<td class="width-35">
						<form:select path="category" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('banner_category')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>排序：</label></td>
					<td class="width-35">
						<form:input path="sort" htmlEscape="false"    class="form-control required isIntGteZero"/>
					</td>
		  		</tr>
		  		<tr class="_product" hidden="hidden">
		  			<td class="width-15 active" ><label class="pull-right">商品：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/product/product/data" id="product" name="product.id" value="${banner.product.id}" labelName="product.title" labelValue="${banner.product.title}"
							 title="选择商品" cssClass="form-control required" fieldLabels="商品名称" fieldKeys="title" searchLabels="商品名称" searchKeys="title" ></sys:gridselect>
					</td>
		  		</tr>
		  		<tr class="_content" hidden="hidden">
					<td class="width-15 active"><label class="pull-right">内容：</label></td>
					<td class="width-35" colspan="3">
                        <input type="hidden" name="content" value=" ${banner.content}"/>
						<div id="content">
                          ${fns:unescapeHtml(banner.content)}
                        </div>
					</td>
		  		</tr>
		  		<tr class="_store" hidden="hidden">
		  			<td class="width-15 active" ><label class="pull-right">厂家店铺：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/store/store/data" id="store" name="store.id" value="${banner.store.id}" labelName="store.title" labelValue="${banner.store.title}"
							 title="选择厂家" cssClass="form-control required" fieldLabels="厂家名称" fieldKeys="title" searchLabels="厂家名称" searchKeys="title" ></sys:gridselect>
					</td>
		  		</tr>
		  		<tr class="_thirdLink" hidden="hidden">
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>外链：</label></td>
					<td class="width-35">
						<form:input path="thirdLink" htmlEscape="false"    class="form-control required"/>
					</td>
		  		</tr>
		  		<tr class="_productCategory" hidden="hidden">
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>分类：</label></td>
					<td class="width-35">
						<sys:treeselect id="productCategory" name="productCategory.id" value="${banner.productCategory.id}" labelName="productCategory.name" labelValue="${banner.productCategory.name}"
							title="分类" url="/productcategory/productCategory/treeData" extId="${banner.id}" cssClass="form-control required" allowClear="true" notAllowSelectParent="true"/>
					</td>
		  		</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>