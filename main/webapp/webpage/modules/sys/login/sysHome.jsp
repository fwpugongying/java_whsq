<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
<title>首页</title>
<meta name="decorator" content="ani" />
<style>
#body-container {
	margin-left: 0px !important;
	/**padding: 10px;*/
	margin-top: 0px !important;
	overflow-x: hidden !important;
	transition: all 0.2s ease-in-out !important;
	height: 100% !important;
}
</style>
<script src="vendor/ckeditor/ckeditor.js" type="text/javascript"></script>
<script src="js/vendor.js"></script>
</head>
<body class="">
	<%-- <div class="alert alert-success" role="alert"><h1><i>欢迎使用${fns:getConfig('productName')}后台管理系统</i></h1></div> --%>
	<div class="jumbotron">
	    <div class="container">
	        <h1>欢迎使用${fns:getConfig('productName')}后台管理系统</h1>
	    </div>
	</div> 
</body>
</html>