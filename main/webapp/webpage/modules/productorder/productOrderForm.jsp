<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>商品订单管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {
		});

		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/productorder/productOrder/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
			$(list+idx).find(".form_datetime").each(function(){
				 $(this).datetimepicker({
					 format: "YYYY-MM-DD HH:mm:ss"
			    });
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="productOrder" action="${ctx}/productorder/productOrder/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>用户：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/member/member/data" id="member" name="member.id" value="${productOrder.member.id}" labelName="member.phone" labelValue="${productOrder.member.phone}"
							 title="选择用户" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>店铺：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/store/store/data" id="store" name="store.id" value="${productOrder.store.id}" labelName="store.title" labelValue="${productOrder.store.title}"
							 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商品编码：</label></td>
					<td class="width-35">
						<form:input path="code" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>总订单号：</label></td>
					<td class="width-35">
						<form:input path="totalId" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr> --%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商品总价：</label></td>
					<td class="width-35">
						<form:input path="price" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>订单总额：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>铜板：</label></td>
					<td class="width-35">
						<form:input path="point" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>运费：</label></td>
					<td class="width-35">
						<form:input path="freight" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
				</tr>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>申请代理：</label></td>
					<td class="width-35">
						<form:select path="proxyType" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('order_proxy_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right">代理费用：</label></td>
					<td class="width-35">
						<form:input path="proxyPrice" htmlEscape="false"    class="form-control "/>
					</td>
				</tr> --%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>优惠金额：</label></td>
					<td class="width-35">
						<form:input path="discount" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>结算金额：</label></td>
					<td class="width-35">
						<form:input path="money" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					
				</tr>
				<tr>
					<%-- <td class="width-15 active"><label class="pull-right">代理区域：</label></td>
					<td class="width-35">
						<sys:treeselect id="area" name="area.id" value="${productOrder.area.id}" labelName="area.name" labelValue="${productOrder.area.name}"
							title="区域" url="/sys/area/treeData" cssClass="form-control " allowClear="true" notAllowSelectParent="true"/>
					</td> --%>
					<td class="width-15 active"><label class="pull-right">发票抬头：</label></td>
					<td class="width-35">
						<form:input path="receipt" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>收货人：</label></td>
					<td class="width-35">
						<form:input path="username" htmlEscape="false"    class="form-control required"/>
					</td>
					
				</tr>
<%--				<tr>--%>
<%--					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>下单时间：</label></td>--%>
<%--					<td class="width-35">--%>
<%--						<form:input path="createDate" htmlEscape="false"    class="form-control required"/>--%>
<%--					</td>--%>
<%--					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>付款时间：</label></td>--%>
<%--					<td class="width-35" >--%>
<%--						<form:input path="payDate" htmlEscape="false"    class="form-control required"/>--%>
<%--					</td>--%>
<%--				</tr>--%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>收货电话：</label></td>
					<td class="width-35">
						<form:input path="phone" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>收货地址：</label></td>
					<td class="width-35" >
						<form:input path="address" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">付款方式：</label></td>
					<td class="width-35">
						<form:select path="payType" class="form-control ">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('pay_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>订单状态 ：</label></td>
					<td class="width-35">
						<form:select path="state" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('product_order_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">物流编码：</label></td>
					<td class="width-35">
						<form:input path="expressCode" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">物流公司：</label></td>
					<td class="width-35">
						<form:input path="expressName" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">物流单号：</label></td>
					<td class="width-35">
						<form:input path="expressNo" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		 	</tbody>
		</table>
		<div class="tabs-container">
            <ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true">商品订单项：</a>
                </li>
            </ul>
            <div class="tab-content">
				<div id="tab-1" class="tab-pane fade in  active">
			<a class="btn btn-white btn-sm" onclick="addRow('#orderItemList', orderItemRowIdx, orderItemTpl);orderItemRowIdx = orderItemRowIdx + 1;" title="新增"><i class="fa fa-plus"></i> 新增</a>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th class="hide"></th>
						<th><font color="red">*</font>商品名称</th>
						<th>规格名称</th>
						<th><font color="red">*</font>数量</th>
						<th><font color="red">*</font>价格</th>
						<th><font color="red">*</font>铜板</th>
						<th width="10">&nbsp;</th>
					</tr>
				</thead>
				<tbody id="orderItemList">
				</tbody>
			</table>
			<script type="text/template" id="orderItemTpl">//<!--
				<tr id="orderItemList{{idx}}">
					<td class="hide">
						<input id="orderItemList{{idx}}_id" name="orderItemList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
						<input id="orderItemList{{idx}}_delFlag" name="orderItemList[{{idx}}].delFlag" type="hidden" value="0"/>
					</td>
					
					<td>
						<input id="orderItemList{{idx}}_productTitle" name="orderItemList[{{idx}}].productTitle" type="text" value="{{row.productTitle}}"    class="form-control required"/>
					</td>
					
					
					<td>
						<input id="orderItemList{{idx}}_skuName" name="orderItemList[{{idx}}].skuName" type="text" value="{{row.skuName}}"    class="form-control "/>
					</td>
					
					
					<td>
						<input id="orderItemList{{idx}}_qty" name="orderItemList[{{idx}}].qty" type="text" value="{{row.qty}}"    class="form-control required isIntGtZero"/>
					</td>
					
					
					<td>
						<input id="orderItemList{{idx}}_price" name="orderItemList[{{idx}}].price" type="text" value="{{row.price}}"    class="form-control required isFloatGteZero"/>
					</td>
					
					
					<td>
						<input id="orderItemList{{idx}}_point" name="orderItemList[{{idx}}].point" type="text" value="{{row.point}}"    class="form-control required isFloatGteZero"/>
					</td>
					
					<td class="text-center" width="10">
						{{#delBtn}}<span class="close" onclick="delRow(this, '#orderItemList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
					</td>
				</tr>//-->
			</script>
			<script type="text/javascript">
				var orderItemRowIdx = 0, orderItemTpl = $("#orderItemTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
				$(document).ready(function() {
					var data = ${fns:toJson(productOrder.orderItemList)};
					for (var i=0; i<data.length; i++){
						addRow('#orderItemList', orderItemRowIdx, orderItemTpl, data[i]);
						orderItemRowIdx = orderItemRowIdx + 1;
					}
				});
			</script>
			</div>
		</div>
		</div>
		</form:form>
</body>
</html>