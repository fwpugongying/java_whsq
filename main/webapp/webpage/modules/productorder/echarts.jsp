<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<meta charset="utf-8">
	<title>ECharts</title>
	<meta name="decorator" content="ani"/>
	<link rel="stylesheet" href="${ctxStatic}/common/css/vendor.css" />
	<script src="${ctxStatic}/common/js/vendor.js"></script>
	<!-- 引入 echarts.js -->
	<%@ include file="/webpage/include/echarts.jsp"%>
</head>
<body class="bg-white">
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<!-- <div id="main" style="width: 100%;height: 50%;margin-top:20px;"></div> -->
<div>
<div class="panel-body" style="width: 100%;height: 5%;margin-bottom:30px;">
<%--	 <div class="col-xs-12 col-sm-6 col-md-4">--%>
<%--		<label class="label-item single-overflow pull-left" title="店铺：">店铺：</label>--%>
<%--					<sys:gridselect url="${ctx}/store/store/data" id="store" name="storeId" value="${product.store.id}" labelName="store.title" labelValue="${product.store.title}"--%>
<%--		 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>--%>
<%--	</div>--%>
	<div class="col-xs-12 col-sm-6 col-md-4">
	<label class="label-item single-overflow pull-left" title="时段：">时段：</label>
		 <div class="form-group">
			<div class="col-xs-12">
				   <div class="col-xs-12 col-sm-5">
			        	  <div class='input-group date' id='beginCreateDate' style="left: -10px;" >
			                   <input type='text' id="begin"  name="beginCreateDate" class="form-control"  />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			             </div>
			        </div>
			        <div class="col-xs-12 col-sm-1">
			        		~
			       	</div>
			        <div class="col-xs-12 col-sm-5">
			          	<div class='input-group date' id='endCreateDate' style="left: -10px;" >
			                   <input type='text' id="end" name="endCreateDate" class="form-control" />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			           	</div>
			        </div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div style="margin-top:30px">
		  <a  id="search" class="btn btn-primary btn-rounded  btn-bordered btn-sm"><i class="fa fa-search"></i> 查询</a>
		  <a  id="reset" class="btn btn-primary btn-rounded  btn-bordered btn-sm" ><i class="fa fa-refresh"></i> 重置</a>
		 </div>
    </div>
</div>
</div>
<div><h3>今日云店成交额</h3></div>
<div class="conter-wrapper home-container">
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-jpy fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="todayAli" class="label-header">${data.todayAli}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							支付宝
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-wechat fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="todayWx" class="label-header">${data.todayWx}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							微信
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="todayPt" class="label-header">${data.todayPt}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							平台购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="todaySc" class="label-header">${data.todaySc}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							商城购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-flag fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="todayMax" class="label-header">${data.todayMax}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							今日总成交额
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<br>
<div><h3>今日云店退款金额</h3></div>
<div class="conter-wrapper home-container">
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-jpy fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="todayRefundAli" class="label-header">${data.todayRefundAli}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							支付宝
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-wechat fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="todayRefundWx" class="label-header">${data.todayRefundWx}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							微信
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="todayRefundPt" class="label-header">${data.todayRefundPt}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							平台购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="todayRefundSc" class="label-header">${data.todayRefundSc}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							商城购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-flag fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="todayRefundMax" class="label-header">${data.todayRefundMax}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							今日退款总金额
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>

</div>
<div><h3>昨日云店成交额</h3></div>
<div class="conter-wrapper home-container">
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-jpy fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="yesAli" class="label-header">${data.yesAli}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							支付宝
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-wechat fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="yesWx" class="label-header">${data.yesWx}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							微信
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="yesPt" class="label-header">${data.yesPt}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							平台购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="yesSc" class="label-header">${data.yesSc}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							商城购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-flag fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="yesMax" class="label-header">${data.yesMax}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							昨日总金额
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<div><h3>昨日云店退款金额</h3></div>
<div class="conter-wrapper home-container">
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-jpy fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="yesRefundAli" class="label-header">${data.yesRefundAli}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							支付宝
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-wechat fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="yesRefundWx" class="label-header">${data.yesRefundWx}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							微信
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="yesRefundPt" class="label-header">${data.yesRefundPt}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							平台购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="yesRefundSc" class="label-header">${data.yesRefundSc}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							商城购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-flag fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="yesRefundMax" class="label-header">${data.yesRefundMax}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							昨日退款总金额
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>

<div><h3>累计云店成交额</h3></div>
<div class="conter-wrapper home-container">
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-jpy fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="totalAli" class="label-header">${data.totalAli}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							支付宝
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-wechat fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="totalWx" class="label-header">${data.totalWx}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							微信
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="totalPt" class="label-header">${data.totalPt}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							平台购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="totalSc" class="label-header">${data.totalSc}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							商城购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-flag fa-4x text-success "></i>
				</div>
				<div class=" stat-label">
					<div id="totalMax" class="label-header">${data.totalMax}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							累计总成交额
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>

</div>
<div><h3>累计云店退款金额</h3></div>
<div class="conter-wrapper home-container">
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-jpy fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="refundAli" class="label-header">${data.refundAli}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							支付宝
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-wechat fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="refundWx" class="label-header">${data.refundWx}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							微信
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="refundPt" class="label-header">${data.refundPt}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							平台购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-credit-card fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="refundSc" class="label-header">${data.refundSc}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							商城购物券
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-md-4 col-lg-3" style="width: 20%;">
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-flag fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="refundMax" class="label-header">${data.refundMax}</div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							累计退款总金额
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>

</div>
<div><h3>淘客订单</h3></div>
<div class="conter-wrapper home-container">
	<div class="col-md-4 col-lg-3" style="margin:20px;">
		<center><h3>总数据</h3></center>
		<table class="table">
		   <thead>
		      <tr>
		         <th>平台</th>
		         <th>预估佣金</th>
		         <th>结算佣金</th>
		      </tr>
		   </thead>
		   <tbody id="totalList">
		   		<c:forEach items="${totalList}" var="data">
		   			<tr>
		   				<c:choose>
		   					<c:when test="${data.type=='0'}">
							   <td>淘宝</td>
						    </c:when>
		   					<c:when test="${data.type=='1'}">
							   <td>拼多多</td>
						    </c:when>
		   					<c:when test="${data.type=='2'}">
							   <td>京东</td>
						    </c:when>
							<c:otherwise>
							   <td></td>
						    </c:otherwise>
		   				</c:choose>
		   				<td>${data.yugu}</td>
		   				<td>${data.total}</td>
		   			</tr>
		   		</c:forEach>
		   </tbody>
		</table>
	</div>
	<div class="col-md-4 col-lg-3" style="margin:20px;">
		<center><h3>今日数据</h3></center>
		<table class="table">
		   <thead>
		      <tr>
		         <th>平台</th>
		         <th>预估佣金</th>
		         <th>结算佣金</th>
		      </tr>
		   </thead>
		   <tbody id="todayList">
		   		<c:forEach items="${todayList}" var="data">
		   			<tr>
		   				<c:choose>
		   					<c:when test="${data.type=='0'}">
							   <td>淘宝</td>
						    </c:when>
		   					<c:when test="${data.type=='1'}">
							   <td>拼多多</td>
						    </c:when>
		   					<c:when test="${data.type=='2'}">
							   <td>京东</td>
						    </c:when>
							<c:otherwise>
							   <td></td>
						    </c:otherwise>
		   				</c:choose>
		   				<td>${data.yugu}</td>
		   				<td>${data.total}</td>
		   			</tr>
		   		</c:forEach>
		   </tbody>
		</table>
	</div>
	<div class="col-md-4 col-lg-3" style="margin:20px;">
		<center><h3>昨日数据</h3></center>
		<table class="table">
		   <thead>
		      <tr>
		         <th>平台</th>
		         <th>预估佣金</th>
		         <th>结算佣金</th>
		      </tr>
		   </thead>
		   <tbody id="yesList">
		   		<c:forEach items="${yesList}" var="data">
		   			<tr>
		   				<c:choose>
		   					<c:when test="${data.type=='0'}">
							   <td>淘宝</td>
						    </c:when>
		   					<c:when test="${data.type=='1'}">
							   <td>拼多多</td>
						    </c:when>
		   					<c:when test="${data.type=='2'}">
							   <td>京东</td>
						    </c:when>
							<c:otherwise>
							   <td></td>
						    </c:otherwise>
		   				</c:choose>
		   				<td>${data.yugu}</td>
		   				<td>${data.total}</td>
		   			</tr>
		   		</c:forEach>
		   </tbody>
		</table>
	</div>
</div>
		
<script type="text/javascript">
$(function () {
	$('#beginCreateDate').datetimepicker({
		 format: "YYYY-MM-DD"
	});
	$('#endCreateDate').datetimepicker({
		 format: "YYYY-MM-DD"
	});
	/* var totalList = JSON.stringify('${totalList}');
	console.log(totalList);
	var todayList = '${todayList}';
	var yesList = '${yesList}';
	if(totalList != null && totalList != ''){
		var s = "";
		for(var i=0; i<totalList.length;i++){
			console.log(totalList[i].type);
			var title = "";
			if(totalList[i].type=='0'){
				title = "淘宝";
			}else if(totalList[i].type=='1'){
				title = "拼多多";
			}else if(totalList[i].type=='2'){
				title = "京东";
			}
			s+="<tr><td>"+title+"</td><td>"+totalList[i].yugu+"</td><td>"+totalList[i].total+"</td></tr>";
		}
		$("#totalList").html(s);
	}
	if(todayList != null && todayList != ''){
		var s = "";
		for(var i=0; i<todayList.length;i++){
			var title = "";
			if(todayList[i].type=='0'){
				title = "淘宝";
			}else if(todayList[i].type=='1'){
				title = "拼多多";
			}else if(todayList[i].type=='2'){
				title = "京东";
			}
			s+="<tr><td>"+title+"</td><td>"+todayList[i].yugu+"</td><td>"+todayList[i].total+"</td></tr>";
		}
		$("#todayList").html(s);
	}
	if(yesList != null && yesList != ''){
		var s = "";
		for(var i=0; i<yesList.length;i++){
			var title = "";
			if(yesList[i].type=='0'){
				title = "淘宝";
			}else if(yesList[i].type=='1'){
				title = "拼多多";
			}else if(yesList[i].type=='2'){
				title = "京东";
			}
			s+="<tr><td>"+title+"</td><td>"+yesList[i].yugu+"</td><td>"+yesList[i].total+"</td></tr>";
		}
		$("#yesList").html(s);
	} */
    /* jp.get("${ctx}${dataURL}", function (result) {
    	var s = "";
    	if (result != null && result.productList != null) {
    		for(var i=0;i<result.productList.length;i++){
    			s+= "<tr><td>"+result.productList[i].title+"</td><td>"+result.productList[i].sales+"</td></tr>";
    		}
    	}
    	$("#productList").html(s);
    	var r = "";
    	if (result != null && result.tkList != null) {
    		for(var i=0;i<result.tkList.length;i++){
    			r+= "<tr><td>"+result.tkList[i].title+"</td><td>"+result.tkList[i].sales+"</td></tr>";
    		}
    	}
    	$("#tkList").html(r);
    }); */
    
    $("#search").click("click", function() {// 绑定查询按扭
    	var storeId = $("#storeId").val();
    	var beginDate = $("#begin").val();
    	var endDate = $("#end").val();
    	jp.get("${ctx}${dataURL}?storeId="+storeId+"&beginDate="+beginDate+"&endDate="+endDate, function (result) {
        	if (result != null) {
        		$("#totalAli").html(result.totalAli);
        		$("#totalWx").html(result.totalWx);
        		$("#totalPt").html(result.totalPt);
        		$("#totalSc").html(result.totalSc);
        		$("#refundAli").html(result.refundAli);
				$("#totalMax").html(result.totalMax);
        		$("#refundWx").html(result.refundWx);
        		$("#refundPt").html(result.refundPt);
        		$("#refundSc").html(result.refundSc);
        	}
        });
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
  		$("select").val("");
  		$("input").val("");
		});
})

</script>
</body>
</html>