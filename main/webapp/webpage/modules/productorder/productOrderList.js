<%@ page contentType="text/html;charset=UTF-8" %>
	<script>
	$(document).ready(function() {
		$('#productOrderTable').bootstrapTable({

			//请求方法
			method: 'post',
			//类型json
			dataType: "json",
			contentType: "application/x-www-form-urlencoded",
			//显示检索按钮
			showSearch: true,
			//显示刷新按钮
			showRefresh: true,
			//显示切换手机试图按钮
			showToggle: true,
			//显示 内容列下拉框
			showColumns: true,
			//显示到处按钮
			showExport: true,
			//显示切换分页按钮
			showPaginationSwitch: true,
			//显示详情按钮
			detailView: true,
			//显示详细内容函数
			detailFormatter: "detailFormatter",
			//最低显示2行
			minimumCountColumns: 2,
			//是否显示行间隔色
			striped: true,
			//是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			cache: false,
			//是否显示分页（*）
			pagination: true,
			//排序方式
			sortOrder: "asc",
			//初始化加载第一页，默认第一页
			pageNumber:1,
			//每页的记录行数（*）
			pageSize: 10,
			//可供选择的每页的行数（*）
			pageList: [10, 25, 50, 100, 500, 1000],
			//这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据
			url: "${ctx}/productorder/productOrder/data",
			//默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
			//queryParamsType:'',
			////查询参数,每次调用是会带上这个参数，可自定义
			queryParams : function(params) {
				var searchParam = $("#searchForm").serializeJSON();
				searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
				searchParam.pageSize = params.limit === undefined? -1 : params.limit;
				searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
				return searchParam;
			},
			//分页方式：client客户端分页，server服务端分页（*）
			sidePagination: "server",
			contextMenuTrigger:"right",//pc端 按右键弹出菜单
			contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
			contextMenu: '#context-menu',
			onContextMenuItem: function(row, $el){
				if($el.data("item") == "edit"){
					edit(row.id);
				}else if($el.data("item") == "view"){
					view(row.id);
				} else if($el.data("item") == "delete"){
					jp.confirm('确认要删除该商品订单记录吗？', function(){
						jp.loading();
						jp.get("${ctx}/productorder/productOrder/delete?id="+row.id, function(data){
							if(data.success){
								$('#productOrderTable').bootstrapTable('refresh');
								jp.success(data.msg);
							}else{
								jp.error(data.msg);
							}
						})

					});

				}
			},

			onClickRow: function(row, $el){
			},
			onShowSearch: function () {
				$("#search-collapse").slideToggle();
			},
			columns: [{
				checkbox: true

			}
				,{
					field: 'id',
					title: '订单号',
					sortable: true,
					sortName: 'id'
					,formatter:function(value, row , index){
						value = jp.unescapeHTML(value);
						return "<a href='javascript:view(\""+row.id+"\")'>"+value+"</a></br>"+"用户："+row.member.phone+"</br>"+"下单时间："+row.createDate;
						/*<c:choose>
                           <c:when test="${fns:getUser().isShop()==false}">
                               return "<a href='javascript:view(\""+row.id+"\")'>"+value+"</a>";
                           </c:when>
                           <c:otherwise>
                               return value;
                           </c:otherwise>
                        </c:choose>*/
					}

				}

				<c:if test="${fns:getUser().isShop() == false}">
			,{
			field: 'store.title',
			title: '店铺',
			sortable: true,
			sortName: 'store.title'

		}
		</c:if>
		/*,{
            field: 'code',
            title: '商品编码',
            sortable: true,
            sortName: 'code'

        }
        ,{
            field: 'totalId',
            title: '总订单号',
            sortable: true,
            sortName: 'totalId'

        }*/
		<c:if test="${fns:getUser().isShop() == false}">
			,{
			field: 'payNo',
			title: '付款单号',
			sortable: true,
			sortName: 'payNo'

		}
		</c:if>
			,{
			field: 'productTitle',
			title: '商品',
			sortable: true,
			sortName: 'productTitle'
			,
			formatter:function(value, row , index){
				return 	'<div style="display: flex;align-items: center;">'+'<img  style="margin-right:10px;" onclick="jp.showPic(\''+row.productIcon+'\')"'+' height="80px" src="'+row.productIcon+'"><div>'+row.productTitle+"<p><span style=\"color: #51b11f;\">( "+row.skuName+" )</span ><span style=\"color: #ff3b6b;margin-left:15px;\">数量 : "+row.qty+"</span></p ><span style=\"color: #ffbb02;\">备注："+row.remarks+"</span></div></div></div>";
			}
		}
			,{
			field: 'username',
			title: '收货人',
			sortable: true,
			sortName: 'username',
			formatter:function(value, row , index){
				return row.username+"</br>"+row.phone+"</br>"+row.address;
			}

		}
		<c:if test="${fns:getUser().isShop() == false}">
			,{
			field: 'price',
			title: '商品总价',
			sortable: true,
			sortName: 'price'
		}
			,{
			field: 'point',
			title: '铜板',
			sortable: true,
			sortName: 'point'

		}
		</c:if>
			,{
			field: 'amount',
			title: '订单金额',
			sortable: true,
			sortName: 'amount',
			formatter:function(value, row , index){
				return "订单总额："+row.amount+"</br>"+"运费："+row.freight+"</br>"+"优惠金额："+row.discount+"</br><span style=\"color: #ffbb02;\">结算金额："+row.money+"</span>";
			}
		}
			,{
			field: 'state',
			title: '订单状态',
			sortable: true,
			sortName: 'state',
			formatter:function(value, row , index){
				return jp.getDictLabel(${fns:toJson(fns:getDictList('product_order_state'))}, value, "-")+"</br>"+jp.getDictLabel(${fns:toJson(fns:getDictList('pay_type'))}, row.payType, "-");
			}

		}
		,{
			field: 'ordertype',
			title: '消费券/专属券',
			sortable: true,
			sortName: 'ordertype',
			formatter:function(value, row , index){
				if(row.ordertype == 1){
					if(row.zsqtype == 1){
						return "</br><span style=\"color: #ffbb02;\">专属券："+row.zsqprice+"</br>消费券："+row.xfqprice+"</span>";
					}
					return "</br><span style=\"color: #ffbb02;\">消费券："+row.xfqprice+"</span>";
				}else if(row.zsqtype == 1){
					return "</br><span style=\"color: #ffbb02;\">专属券："+row.zsqprice+"</span>";
				}
				return "";
			}
		}
			/*,{
		        field: 'expressCode',
		        title: '物流编码',
		        sortable: true,
		        sortName: 'expressCode'

		    }
			,{
		        field: 'expressName',
		        title: '物流公司',
		        sortable: true,
		        sortName: 'expressName'

		    }
			,{
		        field: 'expressNo',
		        title: '物流单号',
		        sortable: true,
		        sortName: 'expressNo'

		    }*/
			/*,{
		        field: 'cancelDate',
		        title: '取消时间',
		        sortable: true,
		        sortName: 'cancelDate'

		    }*/
			/*,{
		        field: 'expireDate',
		        title: '过期时间',
		        sortable: true,
		        sortName: 'expireDate'

		    }*/
			/*,{
		        field: 'finishDate',
		        title: '收货时间',
		        sortable: true,
		        sortName: 'finishDate'

		    }
			,{
		        field: 'refundDate',
		        title: '退款时间',
		        sortable: true,
		        sortName: 'refundDate'

		    }*/
			,{
			field: 'giftprice',
			title: '赠品提前支付金额',
			sortable: true,
			sortName: 'giftprice'

			}
			,{
			field: '',
			title: '操作',
			formatter:function(value, row , index){
				var result = "";
				if (row.state == '1') {
					result += "<button type='button' onclick='send(\""+row.id+"\")' class='btn btn-success btn-xs'>发货</button>";
				}else if(row.state == '2'){
					result += "<button type='button' onclick='deliveryInfo(\""+row.id+"\")' class='btn btn-info btn-xs'>查看物流</button></br></br>";
					result += "<button type='button' onclick='savesend(\""+row.id+"\")' class='btn btn-success btn-xs'>修改物流</button>";
				}
				return result;
			}

		}
		// 	,{
		// 	field: 'member.phone',
		// 	title: '用户信息',
		// 	sortable: true,
		// 	sortName: 'member.phone'
		// 	,
		// 	formatter:function(value, row , index){
		// 		return "用户："+row.member.phone+"</br>"+"下单时间："+row.createDate+"</br>"+"付款时间："+row.payDate+jp.getDictLabel(${fns:toJson(fns:getDictList('pay_type'))}, value, "-");
		// 	}
		// }
			, {
			field: 'payDate',
			title: '付款时间',
			sortable: true,
			sortName: 'payDate'
		}
		<c:if test="${fns:getUser().isShop() == false}">
		,{
			field: '',
			title: '退款',
			formatter:function(value, row , index){
				var result = "";
				if (row.state == '1') {
					result += "<button type='button' onclick='refund(\""+row.id+"\")' class='btn btn-success btn-xs'>申请退款</button>";
				}
				return result;
			}

		}
		,{
			field: 'daimai.phone',
			title: '代买人',
			sortable: true,
			sortName: 'daimai.phone'

		}
		</c:if>
			]

	});


		if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端


			$('#productOrderTable').bootstrapTable("toggleView");
		}

		$('#productOrderTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
			'check-all.bs.table uncheck-all.bs.table', function () {
			$('#remove').prop('disabled', ! $('#productOrderTable').bootstrapTable('getSelections').length);
			$('#view,#edit').prop('disabled', $('#productOrderTable').bootstrapTable('getSelections').length!=1);
		});

		$("#btnImport").click(function(){
			jp.open({
				type: 2,
				area: [500, 200],
				auto: true,
				title:"导入数据",
				content: "${ctx}/tag/importExcel" ,
				btn: ['下载模板','确定', '关闭'],
				btn1: function(index, layero){
					jp.downloadFile('${ctx}/productorder/productOrder/import/template');
				},
				btn2: function(index, layero){
					var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
					iframeWin.contentWindow.importExcel('${ctx}/productorder/productOrder/import', function (data) {
						if(data.success){
							jp.success(data.msg);
							refresh();
						}else{
							jp.error(data.msg);
						}
						jp.close(index);
					});//调用保存事件
					return false;
				},

				btn3: function(index){
					jp.close(index);
				}
			});
		});
		$("#export").click(function(){//导出Excel文件
			var searchParam = $("#searchForm").serializeJSON();
			searchParam.pageNo = 1;
			searchParam.pageSize = -1;
			var sortName = $('#productOrderTable').bootstrapTable("getOptions", "none").sortName;
			var sortOrder = $('#productOrderTable').bootstrapTable("getOptions", "none").sortOrder;
			var values = "";
			for(var key in searchParam){
				values = values + key + "=" + searchParam[key] + "&";
			}
			if(sortName != undefined && sortOrder != undefined){
				values = values + "orderBy=" + sortName + " "+sortOrder;
			}

			jp.downloadFile('${ctx}/productorder/productOrder/export?'+values);
		})
		$("#search").click("click", function() {// 绑定查询按扭
			$('#productOrderTable').bootstrapTable('refresh');
		});

		$("#reset").click("click", function() {// 绑定查询按扭
			$("#searchForm  input").val("");
			$("#searchForm  select").val("");
			$("#searchForm  .select-item").html("");
			$('#productOrderTable').bootstrapTable('refresh');
		});

		$('#beginCreateDate').datetimepicker({
			format: "YYYY-MM-DD HH:mm:ss"
		});
		$('#endCreateDate').datetimepicker({
			format: "YYYY-MM-DD HH:mm:ss"
		});

	});

function getIdSelections() {
	return $.map($("#productOrderTable").bootstrapTable('getSelections'), function (row) {
		return row.id
	});
}

function deleteAll(){

	jp.confirm('确认要删除该商品订单记录吗？', function(){
		jp.loading();
		jp.get("${ctx}/productorder/productOrder/deleteAll?ids=" + getIdSelections(), function(data){
			if(data.success){
				$('#productOrderTable').bootstrapTable('refresh');
				jp.success(data.msg);
			}else{
				jp.error(data.msg);
			}
		})

	})
}

//刷新列表
function refresh() {
	$('#productOrderTable').bootstrapTable('refresh');
}
function add(){
	jp.openSaveDialog('新增商品订单', "${ctx}/productorder/productOrder/form",'800px', '500px');
}

function edit(id){//没有权限时，不显示确定按钮
	if(id == undefined){
		id = getIdSelections();
	}
	jp.openSaveDialog('编辑商品订单', "${ctx}/productorder/productOrder/form?id=" + id, '800px', '500px');
}


function view(id){//没有权限时，不显示确定按钮
	if(id == undefined){
		id = getIdSelections();
	}
	jp.openViewDialog('查看商品订单', "${ctx}/productorder/productOrder/form?id=" + id, '800px', '500px');
}
function deliveryInfo(id){//物流动态
	jp.openViewDialog('物流动态', "${ctx}/productorder/productOrder/deliveryInfo?id=" + id, '800px', '500px');
}

//发货
function send(id){
	if(id == undefined){
		id = getIdSelections();
	}
	jp.openSaveDialog('发货', "${ctx}/productorder/productOrder/sendForm?id=" + id, '800px', '500px');
}
//退款
function refund(id){
	if(id == undefined){
		id = getIdSelections();
	}
	jp.confirm('确认要退款该商品订单吗？', function(){
		jp.loading();
		jp.get("${ctx}/productorder/productOrder/refundForm?id=" + id, function(data){
			if(data.success){
				$('#productOrderTable').bootstrapTable('refresh');
				jp.success(data.msg);
			}else{
				jp.error(data.msg);
			}
		})

	})
}
//修改物流
function savesend(id){
	if(id == undefined){
		id = getIdSelections();
	}
	jp.openSaveDialog('修改物流', "${ctx}/productorder/productOrder/saveSendForm?id=" + id, '800px', '500px');
}



function detailFormatter(index, row) {
	var htmltpl =  $("#productOrderChildrenTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
	var html = Mustache.render(htmltpl, {
		idx:row.id
	});
	$.get("${ctx}/productorder/productOrder/detail?id="+row.id, function(productOrder){
		var productOrderChild1RowIdx = 0, productOrderChild1Tpl = $("#productOrderChild1Tpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
		var data1 =  productOrder.orderItemList;
		for (var i=0; i<data1.length; i++){
			data1[i].dict = {};
			addRow('#productOrderChild-'+row.id+'-1-List', productOrderChild1RowIdx, productOrderChild1Tpl, data1[i]);
			productOrderChild1RowIdx = productOrderChild1RowIdx + 1;
		}


	})

	return html;
}

function addRow(list, idx, tpl, row){
	$(list).append(Mustache.render(tpl, {
		idx: idx, delBtn: true, row: row
	}));
}

</script>
<script type="text/template" id="productOrderChildrenTpl">//<!--
	<div class="tabs-container">
	<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#tab-{{idx}}-1" aria-expanded="true">商品订单项</a></li>
</ul>
<div class="tab-content">
	<div id="tab-{{idx}}-1" class="tab-pane fade in active">
	<table class="ani table">
	<thead>
	<tr>
	<th>商品名称</th>
	<th>规格名称</th>
	<th>数量</th>
	<th>价格</th>
	<th>铜板</th>
	<th>添加时间</th>
	</tr>
	</thead>
	<tbody id="productOrderChild-{{idx}}-1-List">
	</tbody>
	</table>
	</div>
	</div>//-->
</script>
<script type="text/template" id="productOrderChild1Tpl">//<!--
	<tr>
	<td>
	{{row.productTitle}}
</td>
<td>
{{row.skuName}}
</td>
<td>
{{row.qty}}
</td>
<td>
{{row.price}}
</td>
<td>
{{row.point}}
</td>
<td>
{{row.createDate}}
</td>
</tr>//-->
</script>
