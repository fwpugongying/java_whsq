<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
<title>物流动态</title>
<meta name="decorator" content="ani" />
<style>
body, input, textarea, select, button {
	font: 12px "Microsoft YaHei", Verdana, arial, sans-serif;
	line-height: 22px
}
/*result*/
#queryContext {
	z-index: 3
}

.query-box {
	float: left;
	margin-bottom: 10px;
}

.result-top {
	width: 918px;
	height: 43px;
	background-color: #ffffff;
	/*border: 1px solid #a2bbda;*/
	border-bottom: none;
}

.result-top span, .result-top a {
	display: inline-block;
	height: 43px;
	line-height: 43px;
	vertical-align: middle;
	font-size: 14px;
}

.result-top a {
	padding-left: 23px;
	width: 37px;
	font-weight: bold;
	color: #3278e6;
}

.result-top .a-rss {
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 3px -312px no-repeat;
}

.result-top .a-share {
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 8px -342px no-repeat;
}

.result-top .col1 {
	width: 90px;
	text-align: center;
	color: #323232;
	padding-left: 14px;
	font-size: 16px;
}

.result-top .col1-down {
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 80px -941px no-repeat;
	width: 90px;
	text-align: center;
	font-size: 16px;
	color: #5a5a5a;
	cursor: pointer;
	padding-left: 14px;
}

.result-top .col2 {
	width: 303px;
	text-align: left;
	font-size: 16px;
	color: #5a5a5a;
	padding-left: 50px;
}
/*result-info*/
.result-info {
	width: 600px;
	float: left;
}

.queryRight {
	float: right;
	margin-right: 10px;
}

.result-info td {
	padding: 7px;
	color: #828282; /*border-bottom: 1px solid #d2d2d2;
  background-color: #f5f5f5*/
}

.result-info .status {
	width: 40px;
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 13px -764px no-repeat;
}

.result-info-down .status {
	width: 40px;
	background: url("${ctxStatic}/common/images/spider_search_v4.png") -30px -764px no-repeat;
}

.result-info .status-first {
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 13px -804px no-repeat;
}

.result-info .status-check {
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 10px -717px no-repeat;
}

.result-info .status-wait {
	background: url("${ctxStatic}/common/images/spider_search_v4.png") -30px -802px no-repeat;
}

.status .col2 {
	position: relative;
	z-index: -1;
}

.status .line1 {
	position: absolute;
	left: 6.4px;
	width: 0.57em;
	height: 5em;
	border-right: 1px solid #d2d2d2;
	top: -5px;
	z-index: -1;
}

.day {
	display: block;
}

.time {
	font-size: 13px;
}

.result-info .last td, .result-info .last td a {
	color: #ff7800;
	border-bottom: none;
}

.result-info td a:hover {
	color: #3278e6;
}

.result-info .row1 {
	width: 105px;
	text-align: center;
	padding-left: 14px;
	padding-right: 0;
}

.result-companyurl {
	border-left: 1px solid #aaaaaa;
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 10px -993px no-repeat;
	color: #828282;
	font-size: 14px;
	padding-left: 28px;
	font-weight: normal;
}

.sent-order:hover {
	color: #3278e6
}

.context {
	font-size: 14px;
	padding-left: 0px !important;
}

.context a {
	color: #828282;
}

.context a:hover {
	color: #3278e6;
}

/*notfind*/
.notfind {
	padding: 10px 20px;
	border: 1px solid #d2d2d2;
	background-color: #ffffff;
}

.notfind-body {
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 0px -100px no-repeat;
}

.notfind-icon {
	padding-bottom: 10px;
	font-size: 18px;
	line-height: 1.8em;
	text-align: center;
	color: #ff7800;
	border-bottom: 1px dashed #d2d2d2;
	background: url("${ctxStatic}/common/images/spider_search_v4.png") 336px -197px no-repeat;
}

.notfind-ul {
	padding-top: 15px;
	/*padding-left: 200px;*/
}

.notfind-ul li {
	float: left;
	padding-left: 50px;
	color: #323232;
	margin-right: 50px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
	});
</script>
</head>
<body>
	<div>
     <div>	          
		      <div class="relative query-box">
			      <div class="result-top" id="resultTop">
				      <span class="col1">时间</span>
				      <span class="col2">地点和跟踪进度</span>
				  </div>
				  <table class="result-info" cellspacing="0">
					  <tbody>
					  <c:forEach items="${result}" var="trace" varStatus="status">
					  	<c:choose>
					  	<c:when test="${status.first}">
						  	<tr class="last">
								<td class="row1"><span class="day">${trace.time}</span></td>
								<td class="status status-check"><div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td>
								<td class="context">${trace.step}</td>
							</tr>
					      </c:when>
						  <c:when test="${status.last}">
						  	<tr>
								<td class="row1"><span class="day">${trace.time}</span></td>
								<td class="status status-first"><div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td>
								<td class="context">${trace.step}</td>
							</tr>
					      </c:when>
						  
						  <c:otherwise>
						  	<tr>
								<td class="row1"><span class="day">${trace.time}</span></td>
								<td class="status"><div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td>
								<td class="context">${trace.step}</td>
							</tr>
					      </c:otherwise>
					   </c:choose>
					  </c:forEach>
					</tbody>
				</table>
			</div>           
        <div id="notFindTip" class="mt10px notfind hidden" style="display: none;">
           <h4 class="notfind-icon">抱歉，暂无查询记录</h4>
           <ul class="notfind-ul">
             <li id="notFindRight"><span id="notFindRight1">请隔段时间再试，或者前往&nbsp;<span id="links"></span>&nbsp;查询试试</span></li>
           </ul>          
        </div>      
     </div>
   </div>
</body>
</html>