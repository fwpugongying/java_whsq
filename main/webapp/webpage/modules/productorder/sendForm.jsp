<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>订单发货</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {
		});

		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/productorder/productOrder/send",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="productOrder"  method="post" class="form-horizontal">
		<%-- <form:hidden path="id"/> --%>
		<table class="table table-bordered">
		   <tbody>
		   		<tr>
		   			<td class="width-15 active"><label class="pull-right">订单号：</label></td>
		   			<td class="width-35" colspan="3">
		   				<input type="text" id="id" name="id" value="${productOrder.id}" readonly="readonly" class="form-control"/>
		   			</td>
		   		</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">物流公司：</label></td>
					<td class="width-35">
						<select id="deliveryId" name="deliveryId" class="form-control required">
							<c:forEach items="${deliveryList}" var="delivery">
								<option value="${delivery.id}">${delivery.corp}</option>
							</c:forEach>							
						</select>
					</td>
					<td class="width-15 active"><label class="pull-right">物流单号：</label></td>
					<td class="width-35">
						<input type="text" id="emsNo" name="emsNo" maxlength="64" class="form-control required"/>
					</td>
				</tr>
		 	</tbody>
		</table>
		</form:form>
</body>
</html>