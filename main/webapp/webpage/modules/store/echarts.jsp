<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<meta charset="utf-8">
	<title>ECharts</title>
	<meta name="decorator" content="ani"/>
	<link rel="stylesheet" href="${ctxStatic}/common/css/vendor.css" />
	<script src="${ctxStatic}/common/js/vendor.js"></script>
	<!-- 引入 echarts.js -->
	<%@ include file="/webpage/include/echarts.jsp"%>
</head>
<body class="bg-white">
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<!-- <div id="main" style="width: 100%;height: 50%;margin-top:20px;"></div> -->

<div class="panel-body" style="width: 100%;height: 5%;margin-bottom:30px;">
	<div class="col-xs-12 col-sm-6 col-md-4">
		<label class="label-item single-overflow pull-left" title="店铺：">店铺：</label>
					<sys:gridselect url="${ctx}/store/store/data" id="store" name="storeId" value="${product.store.id}" labelName="store.title" labelValue="${product.store.title}"
		 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
	<label class="label-item single-overflow pull-left" title="时段：">时段：</label>
		 <div class="form-group">
			<div class="col-xs-12">
				   <div class="col-xs-12 col-sm-5">
			        	  <div class='input-group date' id='beginCreateDate' style="left: -10px;" >
			                   <input type='text' id="begin"  name="beginCreateDate" class="form-control"  />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			             </div>	
			        </div>
			        <div class="col-xs-12 col-sm-1">
			        		~
			       	</div>
			        <div class="col-xs-12 col-sm-5">
			          	<div class='input-group date' id='endCreateDate' style="left: -10px;" >
			                   <input type='text' id="end" name="endCreateDate" class="form-control" />
			                   <span class="input-group-addon">
			                       <span class="glyphicon glyphicon-calendar"></span>
			                   </span>
			           	</div>	
			        </div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div style="margin-top:30px">
		  <a  id="search" class="btn btn-primary btn-rounded  btn-bordered btn-sm"><i class="fa fa-search"></i> 查询</a>
		  <a  id="reset" class="btn btn-primary btn-rounded  btn-bordered btn-sm" ><i class="fa fa-refresh"></i> 重置</a>
		 </div>
    </div>
</div>

<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;display: none;">
	<center><h3>云店供应商</h3></center>
	<table class="table">
	   <thead>
	      <tr>
	         <th>供应商名称</th>
	         <th>销售额</th>
	         <th>已结算</th>
	         <th>未结算</th>
	      </tr>
	   </thead>
	   <tbody id="storeList">
	   </tbody>
	</table>
</div>
<div class="col-xs-12 col-sm-6 col-md-4" style="margin:20px;">
	<div class="col-xs-12" >
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list-alt fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="totalCount" class="label-header"></div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							<i class=" pull-right text-muted"></i>供应商总数量
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-xs-12" >
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list-alt fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="todayCount" class="label-header"></div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							<i class=" pull-right text-muted"></i>今日新增
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-xs-12 " >
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list-alt fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="yesCount" class="label-header"></div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							<i class=" pull-right text-muted"></i>昨日新增
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-xs-12" >
		<div class="home-stats">
			<a href="javascript:;" class="stat hvr-wobble-horizontal">
				<div class=" stat-icon">
					<i class="fa fa-list-alt fa-4x text-warning "></i>
				</div>
				<div class=" stat-label">
					<div id="monthCount" class="label-header"></div>
					<div class="clearfix stat-detail">
						<div class="label-body">
							<i class=" pull-right text-muted"></i>月新增
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>		
<script type="text/javascript">
    // 基于准备好的dom，初始化echarts实例4
    /* var myChart = echarts.init(document.getElementById('main'));
    window.onresize = myChart.resize; */
    $(function () {
    	$('#beginCreateDate').datetimepicker({
			 format: "YYYY-MM-DD"
		});
		$('#endCreateDate').datetimepicker({
			 format: "YYYY-MM-DD"
		});
		
        jp.get("${ctx}${dataURL}", function (result) {
        	var s = "";
        	if (result != null && result.storeList != null) {
        		for(var i=0;i<result.storeList.length;i++){
        			s+= "<tr><td>"+result.storeList[i].title+"</td><td>"+result.storeList[i].amount+"</td><td>"+result.storeList[i].jiesuan+"</td><td>"+result.storeList[i].weijiesuan+"</td></tr>";
        		}
        	}
        	$("#storeList").html(s);
        	$("#totalCount").html(parseInt(result.totalCount)+7000);
        	$("#todayCount").html(parseInt(result.todayCount));
        	$("#yesCount").html(parseInt(result.yesCount));
        	$("#monthCount").html(parseInt(result.monthCount));
        });
        
        $("#search").click("click", function() {// 绑定查询按扭
        	var storeId = $("#storeId").val();
        	var beginDate = $("#begin").val();
        	var endDate = $("#end").val();
        	jp.get("${ctx}${dataURL}?storeId="+storeId+"&beginDate="+beginDate+"&endDate="+endDate, function (result) {
        		var s = "";
        		if (result != null && result.storeList != null) {
            		for(var i=0;i<result.storeList.length;i++){
            			s+= "<tr><td>"+result.storeList[i].title+"</td><td>"+result.storeList[i].amount+"</td><td>"+result.storeList[i].jiesuan+"</td><td>"+result.storeList[i].weijiesuan+"</td></tr>";
            		}
            	}
            	$("#storeList").html(s);
            	$("#totalCount").html(result.totalCount);
            	$("#todayCount").html(result.todayCount);
            	$("#yesCount").html(result.yesCount);
            	$("#monthCount").html(result.monthCount);
            });
  		});
  	 
  	 $("#reset").click("click", function() {// 绑定查询按扭
	  		$("select").val("");
	  		$("input").val("");
  		});
    })
 

</script>
</body>
</html>