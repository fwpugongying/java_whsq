<%@ page contentType="text/html;charset=UTF-8" %>
<script>
$(document).ready(function() {
	$('#storeTable').bootstrapTable({
		 
		  //请求方法
               method: 'post',
               //类型json
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               //显示检索按钮
	           showSearch: true,
               //显示刷新按钮
               showRefresh: true,
               //显示切换手机试图按钮
               showToggle: true,
               //显示 内容列下拉框
    	       showColumns: true,
    	       //显示到处按钮
    	       showExport: true,
    	       //显示切换分页按钮
    	       showPaginationSwitch: true,
    	       //最低显示2行
    	       minimumCountColumns: 2,
               //是否显示行间隔色
               striped: true,
               //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）     
               cache: false,    
               //是否显示分页（*）  
               pagination: true,   
                //排序方式 
               sortOrder: "asc",  
               //初始化加载第一页，默认第一页
               pageNumber:1,   
               //每页的记录行数（*）   
               pageSize: 10,  
               //可供选择的每页的行数（*）    
               pageList: [10, 25, 50, 100],
               //这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据  
               url: "${ctx}/store/store/data",
               //默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
               //queryParamsType:'',   
               ////查询参数,每次调用是会带上这个参数，可自定义                         
               queryParams : function(params) {
               	var searchParam = $("#searchForm").serializeJSON();
               	searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
               	searchParam.pageSize = params.limit === undefined? -1 : params.limit;
               	searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
                   return searchParam;
               },
               //分页方式：client客户端分页，server服务端分页（*）
               sidePagination: "server",
               contextMenuTrigger:"right",//pc端 按右键弹出菜单
               contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
               contextMenu: '#context-menu',
               onContextMenuItem: function(row, $el){
                   if($el.data("item") == "edit"){
                   		edit(row.id);
                   }else if($el.data("item") == "view"){
                       view(row.id);
                   } else if($el.data("item") == "delete"){
                        jp.confirm('确认要删除该厂家店铺记录吗？', function(){
                       	jp.loading();
                       	jp.get("${ctx}/store/store/delete?id="+row.id, function(data){
                   	  		if(data.success){
                   	  			$('#storeTable').bootstrapTable('refresh');
                   	  			jp.success(data.msg);
                   	  		}else{
                   	  			jp.error(data.msg);
                   	  		}
                   	  	})
                   	   
                   	});
                      
                   } 
               },
              
               onClickRow: function(row, $el){
               },
               	onShowSearch: function () {
			$("#search-collapse").slideToggle();
		},
               columns: [{
		        checkbox: true
		       
		    }
            ,{
   		        field: 'title',
   		        title: '商户名称',
   		        sortable: true,
   		        sortName: 'title'
	        	,formatter:function(value, row , index){
				   if(value == null || value ==""){
					   value = "-";
				   }
				   <c:choose>
					   <c:when test="${fns:hasPermission('store:store:edit')}">
					      return "<a href='javascript:edit(\""+row.id+"\")'>"+value+"</a>";
				      </c:when>
					  <c:when test="${fns:hasPermission('store:store:view')}">
					      return "<a href='javascript:view(\""+row.id+"\")'>"+value+"</a>";
				      </c:when>
					  <c:otherwise>
					      return value;
				      </c:otherwise>
				   </c:choose>
	        	}
   		    }
			,{
		        field: 'member.phone',
		        title: '用户',
		        sortable: true,
		        sortName: 'member.phone'
		    }

			,{
		        field: 'category',
		        title: '编码/主营',
		        sortable: true,
		        sortName: 'category',
			   formatter:function(value, row , index){
				   return "店铺编码："+row.storeCode+"</br>"+"主营项目："+row.category;
			   }
		    }
			,{
		        field: 'city',
		        title: '地址及联系人信息',
		        sortable: true,
		        sortName: 'city',
			   formatter:function(value, row , index){
				   return "联系人："+row.username+"</br>"+"手机号："+row.phone+"</br>"+"城市："+row.city+"</br>"+"地址："+row.address+"</br><span style=\"color: #ffbb02;\">签约人："+row.invitePhone+"</span>";
			   }
		    }
		   ,{
			   field: 'icon',
			   title: '店铺LOGO',
			   formatter:function(value, row , index){
				   if(value != null && value != ''){
					   return '<img   onclick="jp.showPic(\''+value+'\')"'+' height="50px" src="'+value+'">';
				   }
				   return "";
			   }
		   }
			,{
		        field: 'licence',
		        title: '营业执照',
		        formatter:function(value, row , index){
		        	if(value != null && value != ''){
		        		return '<img   onclick="jp.showPic(\''+value+'\')"'+' height="50px" src="'+value+'">';
		        	}
		        	return "";
		        }
		       
		    }
			,{
		        field: 'idcard',
		        title: '身份证',
		        formatter:function(value, row , index){
		        	var labelArray = [];
		        	if(value != null && value != ''){
		        		var valueArray = value.split("|");
			        	for(var i =0 ; i<valueArray.length; i++){
			        		if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(valueArray[i]))
			        		{
			        			labelArray[i] = "<a href=\""+valueArray[i]+"\" url=\""+valueArray[i]+"\" target=\"_blank\">"+decodeURIComponent(valueArray[i].substring(valueArray[i].lastIndexOf("/")+1))+"</a>"
			        		}else{
			        			labelArray[i] = '<img   onclick="jp.showPic(\''+valueArray[i]+'\')"'+' height="50px" src="'+valueArray[i]+'">';
			        		}
			        	}
		        	}
		        	
		        	return labelArray.join(" ");
		        }
		    }
			,{
		        field: 'others',
		        title: '其他证书',
		        formatter:function(value, row , index){
		        	var labelArray = [];
		        	if(value != null && value != ''){
		        		var valueArray = value.split("|");
			        	for(var i =0 ; i<valueArray.length; i++){
			        		if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(valueArray[i]))
			        		{
			        			labelArray[i] = "<a href=\""+valueArray[i]+"\" url=\""+valueArray[i]+"\" target=\"_blank\">"+decodeURIComponent(valueArray[i].substring(valueArray[i].lastIndexOf("/")+1))+"</a>"
			        		}else{
			        			labelArray[i] = '<img   onclick="jp.showPic(\''+valueArray[i]+'\')"'+' height="50px" src="'+valueArray[i]+'">';
			        		}
			        	}
		        	}
		        	
		        	return labelArray.join(" ");
		        }
		    }
		   ,{
			   field: 'createDate',
			   title: '入驻时间',
			   sortable: true,
			   sortName: 'createDate',
			   formatter:function(value, row , index){
				   return "入驻时间："+row.createDate+"</br>"+"审核时间："+row.auditDate+"</br>";
			   }
		   }
		   ,{
			   field: 'state',
			   title: '商户状态显示',
			   sortable: true,
			   sortName: 'state',
			   formatter:function(value, row , index){
				   return jp.getDictLabel(${fns:toJson(fns:getDictList('member_state'))}, row.state, "-")+"</br>"+"回填状态："+jp.getDictLabel(${fns:toJson(fns:getDictList('huitian'))}, row.huitian, "-")+"</br><span style=\"color: #ffbb02;\">审核状态："+jp.getDictLabel(${fns:toJson(fns:getDictList('shop_audit'))}, row.auditState, "-")+"</span>"+"</br><span style=\"color: darkred;\">支付方式："+jp.getDictLabel(${fns:toJson(fns:getDictList('pay_type'))}, row.payType, "-")+"</span>"
			   }

		   }
			,{
		        field: 'amount',
		        title: '金额',
		        sortable: true,
		        sortName: 'amount',
			   formatter:function(value, row , index){
				   return "入驻金额："+row.amount+"</br><span style=\"color: #ffbb02;\">"+"余额："+row.balance+"</span></br>";
			   }
		    }
		   ,{
			   field: 'paymoney',
			   title: '代理产品差额',
			   sortable: true,
			   sortName: 'paymoney'

		   }

			/*,{
		        field: 'payType',
		        title: '支付方式',
		        sortable: true,
		        sortName: 'payType',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('pay_type'))}, value, "-");
		        }
		       
		    }*/

			,{
		        field: 'freight',
		        title: '偏远运费',
		        sortable: true,
		        sortName: 'freight'
		    }
		   ,{
			   field: 'distant',
			   title: '偏远省份',
			   sortable: true,
			   sortName: 'distant'
		   }
		   ,{
			   field: 'remarks',
			   title: '备注',
			   sortable: true,
			   sortName: 'remarks'
		   }
		   ,{
			   field: 'nodelarea',
			   title: '不发货区域提示',
			   sortable: true,
			   sortName: 'nodelarea'
		   }
			,{
		        field: '',
		        title: '操作',
		        formatter:function(value, row , index){
		        	var result = "";
		        	<c:if test="${fns:hasPermission('store:store:audit')}">
					if (row.auditState == '0' || row.auditState == '1') {
						   result += "<a href='javascript:audit(\""+row.id+"\",2)' class=\"label label-success\">通过</a>";
						   result += "<a href='javascript:audit(\""+row.id+"\",3)' class=\"label label-danger\">拒绝</a>";
					} else if(row.auditState == '2') {
						if(row.state == '1'){
							result += "<a href='javascript:updateState(\""+row.id+"\",0)' class=\"label label-primary\">解冻</a>";
						} else {
							result += "<a href='javascript:updateState(\""+row.id+"\",1)' class=\"label label-default\">冻结</a>";
						} 
					}
					</c:if>
					return result;
		        }
		       
		    }
		     ]
		
		});
		
		  
	  if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端

		 
		  $('#storeTable').bootstrapTable("toggleView");
		}
	  
	  $('#storeTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $('#remove').prop('disabled', ! $('#storeTable').bootstrapTable('getSelections').length);
            $('#view,#edit').prop('disabled', $('#storeTable').bootstrapTable('getSelections').length!=1);
        });
		  
		$("#btnImport").click(function(){
			jp.open({
			    type: 2,
                area: [500, 200],
                auto: true,
			    title:"导入数据",
			    content: "${ctx}/tag/importExcel" ,
			    btn: ['下载模板','确定', '关闭'],
				    btn1: function(index, layero){
					  jp.downloadFile('${ctx}/store/store/import/template');
				  },
			    btn2: function(index, layero){
				        var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
						iframeWin.contentWindow.importExcel('${ctx}/store/store/import', function (data) {
							if(data.success){
								jp.success(data.msg);
								refresh();
							}else{
								jp.error(data.msg);
							}
					   		jp.close(index);
						});//调用保存事件
						return false;
				  },
				 
				  btn3: function(index){ 
					  jp.close(index);
	    	       }
			}); 
		});
		
		
	 $("#export").click(function(){//导出Excel文件
	        var searchParam = $("#searchForm").serializeJSON();
	        searchParam.pageNo = 1;
	        searchParam.pageSize = -1;
            var sortName = $('#storeTable').bootstrapTable("getOptions", "none").sortName;
            var sortOrder = $('#storeTable').bootstrapTable("getOptions", "none").sortOrder;
            var values = "";
            for(var key in searchParam){
                values = values + key + "=" + searchParam[key] + "&";
            }
            if(sortName != undefined && sortOrder != undefined){
                values = values + "orderBy=" + sortName + " "+sortOrder;
            }

			jp.downloadFile('${ctx}/store/store/export?'+values);
	  })

		    
	  $("#search").click("click", function() {// 绑定查询按扭
		  $('#storeTable').bootstrapTable('refresh');
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
		  $("#searchForm  input").val("");
		  $("#searchForm  select").val("");
		  $("#searchForm  .select-item").html("");
		  $('#storeTable').bootstrapTable('refresh');
		});
		
		
	});
		
  function getIdSelections() {
        return $.map($("#storeTable").bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }
  
  function deleteAll(){

		jp.confirm('确认要删除该厂家店铺记录吗？', function(){
			jp.loading();  	
			jp.get("${ctx}/store/store/deleteAll?ids=" + getIdSelections(), function(data){
         	  		if(data.success){
         	  			$('#storeTable').bootstrapTable('refresh');
         	  			jp.success(data.msg);
         	  		}else{
         	  			jp.error(data.msg);
         	  		}
         	  	})
          	   
		})
  }

  function audit(id,type){
	  var title = "";
	  if(type=='2'){
		  title="确认要通过该店铺审核吗？";
	  }else{
		  title="确认要拒绝该店铺审核吗？";
	  }
	  jp.confirm(title, function(){
		  if(type=='2'){
			  jp.loading();  	
			  jp.get("${ctx}/store/store/audit?id=" + id + "&type=" + type, function(data){
				  if(data.success){
					  jp.success(data.msg);
				  }else{
					  jp.error(data.msg);
				  }
				  $('#storeTable').bootstrapTable('refresh');
			  })
		  } else {
			  jp.prompt("备注", function(text) {
				  jp.loading();  	
				  jp.get("${ctx}/store/store/audit?id=" + id + "&type=" + type + "&reason=" + text, function(data){
					  if(data.success){
						  jp.success(data.msg);
					  }else{
						  jp.error(data.msg);
					  }
					  $('#storeTable').bootstrapTable('refresh');
				  })
				});
		  }
	  })
  }
  function updateState(id,type){
		 var title = "";
		 if(type=='1'){
			 title="确认要冻结该店铺吗？";
		 }else{
			 title="确认要解冻该店铺吗？";
		 }
		 jp.confirm(title, function(){
			 jp.loading();  	
			 jp.get("${ctx}/store/store/updateState?id=" + id + "&type=" + type, function(data){
				 if(data.success){
					 jp.success(data.msg);
				 }else{
					 jp.error(data.msg);
				 }
				 $('#storeTable').bootstrapTable('refresh');
			 })
			 
		 })
	 }
    //刷新列表
  function refresh(){
  	$('#storeTable').bootstrapTable('refresh');
  }
  
   function add(){
	  jp.openSaveDialog('新增厂家店铺', "${ctx}/store/store/form",'800px', '500px');
  }


  
   function edit(id){//没有权限时，不显示确定按钮
       if(id == undefined){
	      id = getIdSelections();
	}
	jp.openSaveDialog('编辑厂家店铺', "${ctx}/store/store/form?id=" + id, '800px', '500px');
  }
  
 function view(id){//没有权限时，不显示确定按钮
      if(id == undefined){
             id = getIdSelections();
      }
        jp.openViewDialog('查看厂家店铺', "${ctx}/store/store/form?id=" + id, '800px', '500px');
 }



</script>