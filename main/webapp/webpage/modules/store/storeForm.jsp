<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>厂家店铺管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

	        $('#auditDate').datetimepicker({
				 format: "YYYY-MM-DD HH:mm:ss"
		    });
		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/store/store/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="store" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>商户名称：</label></td>
					<td class="width-35">
						<form:input path="title" htmlEscape="false"  disabled="true"  class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>用户：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/member/member/data" disabled="disabled" id="member" name="member.id" value="${store.member.id}" labelName="member.phone" labelValue="${store.member.phone}"
							 title="选择用户" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
				</tr>
				<c:if test="${fns:getUser().isShop()==false}">
				<tr>
					<td class="width-15 active"><label class="pull-right">店铺编码：</label></td>
					<td class="width-35">
						<form:input path="storeCode" htmlEscape="false"  disabled="true"  class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">商品编码：</label></td>
					<td class="width-35">
						<form:input path="productCode" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				</c:if>
				<tr>
					<td class="width-15 active"><label class="pull-right">店铺LOGO：</label></td>
					<td class="width-35">
						<sys:fileUpload path="icon"  value="${store.icon}" type="image" fileNumLimit="1" uploadPath="/store/store"/>
					</td>
					<td class="width-15 active"><label class="pull-right">主营项目：</label></td>
					<td class="width-35">
						<form:input path="category" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>城市：</label></td>
					<td class="width-35">
						<form:input path="city" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>地址：</label></td>
					<td class="width-35">
						<form:input path="address" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>联系人：</label></td>
					<td class="width-35">
						<form:input path="username" htmlEscape="false"    class="form-control required"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>手机号：</label></td>
					<td class="width-35">
						<form:input path="phone" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>签约人：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/member/member/data" id="invite" name="invite.id" value="${store.invite.id}" labelName="invite.phone" labelValue="${store.invite.phone}"
							 title="选择签约人" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right">营业执照：</label></td>
					<td class="width-35">
						<form:input path="licence" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">身份证：</label></td>
					<td class="width-35">
						<form:input path="idcard" htmlEscape="false"    class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">其他证书：</label></td>
					<td class="width-35">
						<form:input path="others" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>审核状态 0待支付 1待审核 2审核通过 3审核拒绝 4已退款：</label></td>
					<td class="width-35">
						<form:select path="auditState" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('shop_audit')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr> --%>
				<%-- <tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>入驻金额：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false" disabled="true"   class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right">支付方式：</label></td>
					<td class="width-35">
						<form:select path="payType" class="form-control " disabled="true">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('pay_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">审核时间：</label></td>
					<td class="width-35">
						<div class='input-group form_datetime' id='auditDate'>
							<input type='text'  name="auditDate" class="form-control "  value="<fmt:formatDate value="${store.auditDate}" pattern="yyyy-MM-dd HH:mm:ss"/>"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>回填状态：</label></td>
					<td class="width-35">
						<form:select path="huitian" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('huitian')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>状态：</label></td>
					<td class="width-35">
						<form:select path="state" class="form-control required">
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('member_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
						</form:select>
					</td>
				</tr> --%>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>偏远运费：</label></td>
					<td class="width-35">
						<form:input path="freight" htmlEscape="false"    class="form-control required isFloatGteZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right">偏远省份：</label></td>
					<td class="width-35">
						<%-- <form:input path="distant" htmlEscape="false"    class="form-control "/> --%>
						<sys:gridselect url="${ctx}/sys/area/data" id="distant" name="distant" value="${store.distant}" labelName="area.name" labelValue="${store.distant}"
							 title="选择省份" cssClass="form-control " isMultiSelected="true" fieldLabels="省份" fieldKeys="name" searchLabels="省份" searchKeys="name" ></sys:gridselect>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">不发货区域：</label></td>
					<td class="width-35">
							<%-- <form:input path="distant" htmlEscape="false"    class="form-control "/> --%>
						<sys:gridselect url="${ctx}/sys/area/data" id="nodelarea" name="nodelarea" value="${store.nodelarea}" labelName="areatwo.name" labelValue="${store.nodelarea}"
												title="选择省份" cssClass="form-control " isMultiSelected="true" fieldLabels="省份" fieldKeys="name" searchLabels="省份" searchKeys="name" ></sys:gridselect>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">店铺简介：</label></td>
					<td class="width-35" colspan="3">
						<form:input path="content" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">百度商桥：</label></td>
					<td class="width-35" colspan="3">
						<form:input path="shangqiao" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				<c:if test="${fns:getUser().isShop()==false}">
				<tr>
					<td class="width-15 active"><label class="pull-right">营业执照：</label></td>
					<td class="width-35">
						<sys:fileUpload path="licence"  value="${store.licence}" type="image" fileNumLimit="1" uploadPath="/store/store"/>
					</td>
					<td class="width-15 active"><label class="pull-right">身份证：</label></td>
					<td class="width-35">
						<sys:fileUpload path="idcard"  value="${store.idcard}" type="image" fileNumLimit="2" uploadPath="/store/store"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">其他证书：</label></td>
					<td class="width-35">
						<sys:fileUpload path="others"  value="${store.others}" type="image" fileNumLimit="1" uploadPath="/store/store"/>
					</td>
					<td class="width-15 active"><label class="pull-right">备注：</label></td>
					<td class="width-35">
						<form:input path="remarks" htmlEscape="false"    class="form-control "/>
					</td>
				</tr>
				</c:if>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>