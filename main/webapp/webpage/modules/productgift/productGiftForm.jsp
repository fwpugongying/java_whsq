<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>拼团商品赠品管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/productgift/productGift/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="productGift" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>拼团商品：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/product/product/data?isGroup=1" id="product" name="product.id" value="${productGift.product.id}" labelName="product.title" labelValue="${productGift.product.title}"
							 title="选择拼团商品" cssClass="form-control required" fieldLabels="商品名称" fieldKeys="title" searchLabels="商品名称" searchKeys="title" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>赠品：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/product/product/data" id="gift" name="gift.id" value="${productGift.gift.id}" labelName="gift.title" labelValue="${productGift.gift.title}"
							 title="选择赠品" cssClass="form-control required" fieldLabels="赠品名称" fieldKeys="title" searchLabels="赠品名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>数量：</label></td>
					<td class="width-35">
						<form:input path="qty" htmlEscape="false"    class="form-control required isIntGtZero"/>
					</td>
					<td class="width-15 active"></td>
		   			<td class="width-35" ></td>
		  		</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>