<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>厂家微信提现管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
				var amount = $("#amount").val();
				var balance = $("#balance").val();
				if(amount > balance){
					jp.warning("提现金额不能大于现有余额");
				} else {
					jp.loading();
	                jp.post("${ctx}/storecash/storeCash/save",$('#inputForm').serialize(),function(data){
	                    if(data.success){
	                        jp.getParent().refresh();
	                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
	                        parent.layer.close(dialogIndex);
	                        jp.success(data.msg)

	                    }else{
	                        jp.error(data.msg);
	                    }
	                })
				}
                
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="storeCash" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">账户余额：</label></td>
					<td class="width-35">
						<input type="text" id="balance" value="${balance}" htmlEscape="false" readonly="readonly"  class="form-control " />
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>提现金额：</label></td>
					<td class="width-35">
						<form:input path="amount" htmlEscape="false"    class="form-control required isIntGtZero"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>姓名：</label></td>
					<td class="width-35">
						<form:input path="username" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>提现账号：</label></td>
					<td class="width-35">
						<input type="text" htmlEscape="false" value="${txname}" disabled="true" readonly="true"   class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15">
						<font color="red">*</font>提现类型：</label>
					</td>
					<td class="width-35">
						<form:select path="pay"  class="form-control m-b">
							<form:option value="1" label="微信提现"/>
						</form:select>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>