<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/webpage/include/taglib.jsp"%>
<html>
<head>
	<title>店铺评分管理</title>
	<meta name="decorator" content="ani"/>
	<script type="text/javascript">

		$(document).ready(function() {

		});
		function save() {
            var isValidate = jp.validateForm('#inputForm');//校验表单
            if(!isValidate){
                return false;
			}else{
                jp.loading();
                jp.post("${ctx}/storescore/storeScore/save",$('#inputForm').serialize(),function(data){
                    if(data.success){
                        jp.getParent().refresh();
                        var dialogIndex = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                        parent.layer.close(dialogIndex);
                        jp.success(data.msg)

                    }else{
                        jp.error(data.msg);
                    }
                })
			}

        }
	</script>
</head>
<body class="bg-white">
		<form:form id="inputForm" modelAttribute="storeScore" class="form-horizontal">
		<form:hidden path="id"/>	
		<table class="table table-bordered">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>用户：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/member/member/data" id="member" name="member.id" value="${storeScore.member.id}" labelName="member.phone" labelValue="${storeScore.member.phone}"
							 title="选择用户" cssClass="form-control required" fieldLabels="账号|昵称" fieldKeys="phone|nickname" searchLabels="账号|昵称" searchKeys="phone|nickname" ></sys:gridselect>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>店铺：</label></td>
					<td class="width-35">
						<sys:gridselect url="${ctx}/store/store/data" id="store" name="store.id" value="${storeScore.store.id}" labelName="store.title" labelValue="${storeScore.store.title}"
							 title="选择店铺" cssClass="form-control required" fieldLabels="店铺名称" fieldKeys="title" searchLabels="店铺名称" searchKeys="title" ></sys:gridselect>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>评分：</label></td>
					<td class="width-35">
						<form:input path="score" htmlEscape="false"    class="form-control required isIntGtZero"/>
					</td>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>订单号：</label></td>
					<td class="width-35">
						<form:input path="orderId" htmlEscape="false" readonly="true"   class="form-control required"/>
					</td>
				</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>