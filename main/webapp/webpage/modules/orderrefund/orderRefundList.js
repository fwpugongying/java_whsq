<%@ page contentType="text/html;charset=UTF-8" %>
<script>
$(document).ready(function() {
	$('#orderRefundTable').bootstrapTable({
		 
		  //请求方法
               method: 'post',
               //类型json
               dataType: "json",
               contentType: "application/x-www-form-urlencoded",
               //显示检索按钮
	           showSearch: true,
               //显示刷新按钮
               showRefresh: true,
               //显示切换手机试图按钮
               showToggle: true,
               //显示 内容列下拉框
    	       showColumns: true,
    	       //显示到处按钮
    	       showExport: true,
    	       //显示切换分页按钮
    	       showPaginationSwitch: true,
    	       //最低显示2行
    	       minimumCountColumns: 2,
               //是否显示行间隔色
               striped: true,
               //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）     
               cache: false,    
               //是否显示分页（*）  
               pagination: true,   
                //排序方式 
               sortOrder: "asc",  
               //初始化加载第一页，默认第一页
               pageNumber:1,   
               //每页的记录行数（*）   
               pageSize: 10,  
               //可供选择的每页的行数（*）    
               pageList: [10, 25, 50, 100],
               //这个接口需要处理bootstrap table传递的固定参数,并返回特定格式的json数据  
               url: "${ctx}/orderrefund/orderRefund/data",
               //默认值为 'limit',传给服务端的参数为：limit, offset, search, sort, order Else
               //queryParamsType:'',   
               ////查询参数,每次调用是会带上这个参数，可自定义                         
               queryParams : function(params) {
               	var searchParam = $("#searchForm").serializeJSON();
               	searchParam.pageNo = params.limit === undefined? "1" :params.offset/params.limit+1;
               	searchParam.pageSize = params.limit === undefined? -1 : params.limit;
               	searchParam.orderBy = params.sort === undefined? "" : params.sort+ " "+  params.order;
                   return searchParam;
               },
               //分页方式：client客户端分页，server服务端分页（*）
               sidePagination: "server",
               contextMenuTrigger:"right",//pc端 按右键弹出菜单
               contextMenuTriggerMobile:"press",//手机端 弹出菜单，click：单击， press：长按。
               contextMenu: '#context-menu',
               onContextMenuItem: function(row, $el){
                   if($el.data("item") == "edit"){
                   		edit(row.id);
                   }else if($el.data("item") == "view"){
                       view(row.id);
                   } else if($el.data("item") == "delete"){
                        jp.confirm('确认要删除该商品订单售后记录吗？', function(){
                       	jp.loading();
                       	jp.get("${ctx}/orderrefund/orderRefund/delete?id="+row.id, function(data){
                   	  		if(data.success){
                   	  			$('#orderRefundTable').bootstrapTable('refresh');
                   	  			jp.success(data.msg);
                   	  		}else{
                   	  			jp.error(data.msg);
                   	  		}
                   	  	})
                   	   
                   	});
                      
                   } 
               },
              
               onClickRow: function(row, $el){
               },
               	onShowSearch: function () {
			$("#search-collapse").slideToggle();
		},
               columns: [{
		        checkbox: true
		       
		    }
			,{
		        field: 'orderId',
		        title: '订单号',
		        sortable: true,
		        sortName: 'orderId'
				 ,formatter:function(value, row , index){
				value = jp.unescapeHTML(value);
				return "<a href='javascript:vieworder(\""+row.orderId+"\")'>"+value+"</a>";
				/*<c:choose>
				   <c:when test="${fns:getUser().isShop()==false}">
					   return "<a href='javascript:view(\""+row.id+"\")'>"+value+"</a>";
				   </c:when>
				   <c:otherwise>
					   return value;
				   </c:otherwise>
				</c:choose>*/
		    	}
		    }
			,{
		        field: 'reason',
		        title: '退款原因',
		        sortable: true,
		        sortName: 'reason'
		       
		    }
			,{
		        field: 'content',
		        title: '描述',
		        sortable: true,
		        sortName: 'content'
		       
		    }
			,{
		        field: 'images',
		        title: '图片',
		        formatter:function(value, row , index){
		        	var labelArray = [];
		        	if(value != null && value != ''){
		        		var valueArray = value.split("|");
		        		for(var i =0 ; i<valueArray.length; i++){
		        			if(!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(valueArray[i]))
		        			{
		        				labelArray[i] = "<a href=\""+valueArray[i]+"\" url=\""+valueArray[i]+"\" target=\"_blank\">"+decodeURIComponent(valueArray[i].substring(valueArray[i].lastIndexOf("/")+1))+"</a>"
		        			}else{
		        				labelArray[i] = '<img   onclick="jp.showPic(\''+valueArray[i]+'\')"'+' height="50px" src="'+valueArray[i]+'">';
		        			}
		        		}
		        	}
		        	return labelArray.join(" ");
		        }
		    }
			,{
		        field: 'amount',
		        title: '申请金额',
		        sortable: true,
		        sortName: 'amount'
		       
		    }
			,{
		        field: 'createDate',
		        title: '申请时间',
		        sortable: true,
		        sortName: 'createDate'
		       
		    }
			,{
		        field: 'state',
		        title: '状态',
		        sortable: true,
		        sortName: 'state',
		        formatter:function(value, row , index){
		        	return jp.getDictLabel(${fns:toJson(fns:getDictList('order_refund_state'))}, value, "-");
		        }
		    }
			,{
		        field: 'auditDate',
		        title: '审核时间',
		        sortable: true,
		        sortName: 'auditDate'
		       
		    }
			,{
		        field: 'remarks',
		        title: '备注',
		        sortable: true,
		        sortName: 'remarks'
		       
		    }
			,{
		        field: '',
		        title: '操作',
		        formatter:function(value, row , index){
		        	var result = "";
					if (row.state == '0') {
						   result += "<a href='javascript:modify(\""+row.id+"\",3)' class=\"label label-primary\">修改退款金额</a>";
						   result += "<a href='javascript:audit(\""+row.id+"\",1)' class=\"label label-success\">同意</a>";
						   result += "<a href='javascript:audit(\""+row.id+"\",2)' class=\"label label-danger\">拒绝</a>";
					}
					return result;
		        }
		    }
		     ]
		
		});
		
		  
	  if(navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)){//如果是移动端

		 
		  $('#orderRefundTable').bootstrapTable("toggleView");
		}
	  
	  $('#orderRefundTable').on('check.bs.table uncheck.bs.table load-success.bs.table ' +
                'check-all.bs.table uncheck-all.bs.table', function () {
            $('#remove').prop('disabled', ! $('#orderRefundTable').bootstrapTable('getSelections').length);
            $('#view,#edit').prop('disabled', $('#orderRefundTable').bootstrapTable('getSelections').length!=1);
        });
		  
		$("#btnImport").click(function(){
			jp.open({
			    type: 2,
                area: [500, 200],
                auto: true,
			    title:"导入数据",
			    content: "${ctx}/tag/importExcel" ,
			    btn: ['下载模板','确定', '关闭'],
				    btn1: function(index, layero){
					  jp.downloadFile('${ctx}/orderrefund/orderRefund/import/template');
				  },
			    btn2: function(index, layero){
				        var iframeWin = layero.find('iframe')[0]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
						iframeWin.contentWindow.importExcel('${ctx}/orderrefund/orderRefund/import', function (data) {
							if(data.success){
								jp.success(data.msg);
								refresh();
							}else{
								jp.error(data.msg);
							}
					   		jp.close(index);
						});//调用保存事件
						return false;
				  },
				 
				  btn3: function(index){ 
					  jp.close(index);
	    	       }
			}); 
		});
		
		
	 $("#export").click(function(){//导出Excel文件
	        var searchParam = $("#searchForm").serializeJSON();
	        searchParam.pageNo = 1;
	        searchParam.pageSize = -1;
            var sortName = $('#orderRefundTable').bootstrapTable("getOptions", "none").sortName;
            var sortOrder = $('#orderRefundTable').bootstrapTable("getOptions", "none").sortOrder;
            var values = "";
            for(var key in searchParam){
                values = values + key + "=" + searchParam[key] + "&";
            }
            if(sortName != undefined && sortOrder != undefined){
                values = values + "orderBy=" + sortName + " "+sortOrder;
            }

			jp.downloadFile('${ctx}/orderrefund/orderRefund/export?'+values);
	  })

		    
	  $("#search").click("click", function() {// 绑定查询按扭
		  $('#orderRefundTable').bootstrapTable('refresh');
		});
	 
	 $("#reset").click("click", function() {// 绑定查询按扭
		  $("#searchForm  input").val("");
		  $("#searchForm  select").val("");
		  $("#searchForm  .select-item").html("");
		  $('#orderRefundTable').bootstrapTable('refresh');
		});
		
		
	});
		
  function getIdSelections() {
        return $.map($("#orderRefundTable").bootstrapTable('getSelections'), function (row) {
            return row.id
        });
    }
   function vieworder(id){//没有权限时，不显示确定按钮
		if(id == undefined){
			id = getIdSelections();
		}
		jp.openViewDialog('查看商品订单', "${ctx}/productorder/productOrder/form?id=" + id, '800px', '500px');
	}
  
  function deleteAll(){

		jp.confirm('确认要删除该商品订单售后记录吗？', function(){
			jp.loading();  	
			jp.get("${ctx}/orderrefund/orderRefund/deleteAll?ids=" + getIdSelections(), function(data){
         	  		if(data.success){
         	  			$('#orderRefundTable').bootstrapTable('refresh');
         	  			jp.success(data.msg);
         	  		}else{
         	  			jp.error(data.msg);
         	  		}
         	  	})
          	   
		})
  }

    //刷新列表
  function refresh(){
  	$('#orderRefundTable').bootstrapTable('refresh');
  }
  
   function add(){
	  jp.openSaveDialog('新增商品订单售后', "${ctx}/orderrefund/orderRefund/form",'800px', '500px');
  }


  
   function edit(id){//没有权限时，不显示确定按钮
       if(id == undefined){
	      id = getIdSelections();
	}
	jp.openSaveDialog('编辑商品订单售后', "${ctx}/orderrefund/orderRefund/form?id=" + id, '800px', '500px');
  }
  
 function view(id){//没有权限时，不显示确定按钮
      if(id == undefined){
             id = getIdSelections();
      }
        jp.openViewDialog('查看商品订单售后', "${ctx}/orderrefund/orderRefund/form?id=" + id, '800px', '500px');
 }

 function audit(id,type){
	  var title = "";
	  if(type=='1'){
		  title="确认要同意该订单退款申请吗？";
	  }else{
		  title="确认要拒绝该订单退款申请吗？";
	  }
	  jp.confirm(title, function(){
			  jp.loading();  	
			  jp.get("${ctx}/orderrefund/orderRefund/audit?id=" + id + "&type=" + type, function(data){
				  if(data.success){
					  jp.success(data.msg);
				  }else{
					  jp.error(data.msg);
				  }
				  $('#orderRefundTable').bootstrapTable('refresh');
			  })
		  
	  })
/*	  jp.confirm(title, function(){
		  if(type=='1'){
			  jp.loading();  	
			  jp.get("${ctx}/orderrefund/orderRefund/audit?id=" + id + "&type=" + type, function(data){
				  if(data.success){
					  jp.success(data.msg);
				  }else{
					  jp.error(data.msg);
				  }
				  $('#orderRefundTable').bootstrapTable('refresh');
			  })
		  } else {
			  jp.prompt("备注", function(text) {
				  jp.loading();  	
				  jp.get("${ctx}/orderrefund/orderRefund/audit?id=" + id + "&type=" + type + "&remarks=" + text, function(data){
					  if(data.success){
						  jp.success(data.msg);
					  }else{
						  jp.error(data.msg);
					  }
					  $('#orderRefundTable').bootstrapTable('refresh');
				  })
			  });
		  }
		  
	  })
*/}
function modify(id,type){//修改金额
	if(type=='3') {
		jp.prompt("输入退款金额", function (text) {
			jp.loading();
			jp.get("${ctx}/orderrefund/orderRefund/audit?id=" + id + "&type=" + type + "&price=" + text, function (data) {
				if (data.success) {
					jp.success(data.msg);
				} else {
					jp.error(data.msg);
				}
				$('#orderRefundTable').bootstrapTable('refresh');
			})
		});
	}
}

</script>