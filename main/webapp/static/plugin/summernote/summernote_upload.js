// 上传图片
function sendFile(file, editor) {
	var formData = new FormData();
    formData.append('file',file);
    $.ajax({
        url : '/api/uploadFile',
        type : 'POST',
        data : formData,
        processData : false,
        contentType : false,
        success : function(data) {
        	$(editor).summernote('editor.insertImage',data.url,'img');
        }
    }); 
}